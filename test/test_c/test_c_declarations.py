from .test_c_base import *


class TestParseVariables(TestParser):
    def test_variable(self):
        par = self.mkpar("int x")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.VariableDeclaration(
                "x",
                t.Arithmetic.DWORD,
                None
            )
        )
        self.assertTrue(par._is_eof())

    def test_variable_assign(self):
        par = self.mkpar("int x = 0")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.VariableDeclaration(
                "x",
                t.Arithmetic.DWORD,
                n.Literal(0, t.Arithmetic.DWORD)
            )
        )
        self.assertTrue(par._is_eof())

    def test_variable_multi(self):
        par = self.mkpar("int x = 0.5, y")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.StatementBlock([
                n.VariableDeclaration(
                    "x",
                    t.Arithmetic.DWORD,
                    n.TypeCast(
                        n.Literal(0.5, t.Arithmetic.FLOAT),
                        t.Arithmetic.DWORD
                    )
                ),
                n.VariableDeclaration(
                    "y",
                    t.Arithmetic.DWORD,
                    None
                )
            ])
        )
        self.assertTrue(par._is_eof())

    def test_variable_noname(self):
        par = self.mkpar("int x, 3")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl)

    def test_variable_badname(self):
        par = self.mkpar("int if")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl)


class TestParseFunction(TestParser):
    def test_noargs(self):
        par = self.mkpar("void foo();")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([])
            )
        )
        self.assertTrue(par._is_eof())

    def test_void(self):
        par = self.mkpar("void foo(void);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([])
            )
        )
        self.assertTrue(par._is_eof())

    def test_argtypes(self):
        par = self.mkpar("void foo(int, float);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration("", t.Arithmetic.DWORD),
                    n.VariableDeclaration("", t.Arithmetic.FLOAT)
                ])
            )
        )
        self.assertTrue(par._is_eof())

    def test_arg_names(self):
        par = self.mkpar("void foo(int x, float y);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration("x", t.Arithmetic.DWORD),
                    n.VariableDeclaration("y", t.Arithmetic.FLOAT)
                ])
            )
        )
        self.assertTrue(par._is_eof())

    def test_arg_names_defaults(self):
        par = self.mkpar("void foo(int x = 2, float y = 2);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration(
                        "x",
                        t.Arithmetic.DWORD,
                        n.Literal(2, t.Arithmetic.DWORD),
                    ),
                    n.VariableDeclaration(
                        "y",
                        t.Arithmetic.FLOAT,
                        n.TypeCast(
                            n.Literal(2, t.Arithmetic.DWORD),
                            t.Arithmetic.FLOAT
                        ),
                    )
                ])
            )
        )
        self.assertTrue(par._is_eof())

    def test_arg_nonames_defaults(self):
        par = self.mkpar("void foo(int = 2, float = 2);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration(
                        "",
                        t.Arithmetic.DWORD,
                        n.Literal(2, t.Arithmetic.DWORD),
                    ),
                    n.VariableDeclaration(
                        "",
                        t.Arithmetic.FLOAT,
                        n.TypeCast(
                            n.Literal(2, t.Arithmetic.DWORD),
                            t.Arithmetic.FLOAT
                        ),
                    )
                ])
            )
        )
        self.assertTrue(par._is_eof())

    def test_arg_invalid(self):
        par = self.mkpar("void foo(int 2);")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl)

    def test_arg_nobrack(self):
        par = self.mkpar("void foo(int ")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl)

    def test_body(self):
        par = self.mkpar("int foo(int x){ return x; }")
        par._scan()
        node = par._parse_decl(True)
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Arithmetic.DWORD,
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration(
                        "x",
                        t.Arithmetic.DWORD
                    ),
                ]),
                n.Return(
                    n.VariableReference("x", t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                )
            )
        )
        self.assertTrue(par._is_eof())

    def test_body_scope_not_found(self):
        par = self.mkpar("int foo(int x){ return y; }")
        par._scan()
        self.assertRaises(ScopeError, par._parse_decl, True)

    def test_body_scope_found(self):
        par = self.mkpar("int foo(int x){ return y; }")
        par.symbol_table.declare_raw("y", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_decl(True)
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Arithmetic.DWORD,
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration(
                        "x",
                        t.Arithmetic.DWORD
                    ),
                ]),
                n.Return(
                    n.VariableReference("y", t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                )
            )
        )
        self.assertTrue(par._is_eof())

    def test_body_scope_no_return(self):
        par = self.mkpar("int foo(int x){}")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl, True)

    def test_body_nobrace(self):
        par = self.mkpar("int foo(int x) if (x) return 1; else return 2;")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl, True)

    def test_body_not_allowed(self):
        par = self.mkpar("int foo(int x){ return x; }")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl, False)

    def test_arg_ellipsis(self):
        par = self.mkpar("void foo(int x, ...);")
        par._scan()
        node = par._parse_decl()
        self.assert_node(
            node,
            n.FunctionDeclaration(
                t.Void(),
                "foo",
                n.FunctionParameterDeclaration([
                    n.VariableDeclaration("x", t.Arithmetic.DWORD),
                    n.VarArgs()
                ])
            )
        )
        self.assertTrue(par._is_eof())

    def test_arg_ellipsis_wrong(self):
        par = self.mkpar("void foo(int x, ..., int y);")
        par._scan()
        self.assertRaises(ParseError, par._parse_decl)
