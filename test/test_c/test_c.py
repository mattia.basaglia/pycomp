import unittest

from pycomp.front.base import TokenType
from pycomp.ast_types import Arithmetic

from .test_c_base import *


class TestCLexer(unittest.TestCase):
    def mklex(self, text):
        return c.CLexer(io.StringIO(text))

    def test_eof(self):
        self.assertEqual(self.mklex("").next().type, TokenType.EOF)

    def test_keyword(self):
        lex = self.mklex("fo0_bar")
        token = lex.next()
        self.assertEqual(token.type, TokenType.KEYWORD)
        self.assertEqual(token.str, "fo0_bar")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_skips_space(self):
        lex = self.mklex("  \n\t \vfoo")
        token = lex.next()
        self.assertEqual(token.str, "foo")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_10(self):
        lex = self.mklex("12345")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 12345)
        self.assertEqual(token.valtype, Arithmetic.DWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_010(self):
        lex = self.mklex("010")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 8)
        self.assertEqual(token.valtype, Arithmetic.DWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_0109(self):
        lex = self.mklex("0109")
        self.assertRaises(ParseError, lex.next)

    def test_int_0x10(self):
        lex = self.mklex("0x10")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 0x10)
        self.assertEqual(token.valtype, Arithmetic.DWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_10l(self):
        lex = self.mklex("12345l")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 12345)
        self.assertEqual(token.valtype, Arithmetic.DWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_10Ll(self):
        lex = self.mklex("12345Ll")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 12345)
        self.assertEqual(token.valtype, Arithmetic.QWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_10u(self):
        lex = self.mklex("12345u")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 12345)
        self.assertEqual(token.valtype, Arithmetic.UDWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_int_10ull(self):
        lex = self.mklex("12345ull")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 12345)
        self.assertEqual(token.valtype, Arithmetic.UQWORD)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_zerostart(self):
        lex = self.mklex("0.5")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 0.5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_octstart(self):
        lex = self.mklex("0369.5")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 369.5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_frac(self):
        lex = self.mklex(".5")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, .5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_int_frac(self):
        lex = self.mklex("3.5")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 3.5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_int_frac_f(self):
        lex = self.mklex("3.5f")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 3.5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_int_frac_unk(self):
        lex = self.mklex("3.5p")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 3.5)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertNotEqual(lex.next().type, TokenType.EOF)

    def test_float_int_ex(self):
        lex = self.mklex("3e2")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 300)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_int_ex_sign(self):
        lex = self.mklex("3e-2")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 0.03)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_float_int_ex_bork(self):
        lex = self.mklex("3e-")
        self.assertRaises(ParseError, lex.next)

    def test_float_int_frac_ex(self):
        lex = self.mklex("3.5e+2")
        token = lex.next()
        self.assertEqual(token.type, TokenType.LITERAL)
        self.assertEqual(token.val, 350)
        self.assertEqual(token.valtype, Arithmetic.FLOAT)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_string_dbl(self):
        lex = self.mklex(r'"foo\r\n\b\a\v\t\f\"\'\\"')
        token = lex.next()
        self.assertEqual(token.type, TokenType.STRING)
        self.assertEqual(token.str, "foo\r\n\b\a\v\t\f\"\'\\")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_string_sng(self):
        lex = self.mklex(r"'foo\r\n\b\a\v\t\f\"\'\\'")
        token = lex.next()
        self.assertEqual(token.type, TokenType.STRING)
        self.assertEqual(token.str, "foo\r\n\b\a\v\t\f\"\'\\")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_string_noend(self):
        lex = self.mklex(r'"foo')
        self.assertRaises(ParseError, lex.next)

    def test_string_unk(self):
        lex = self.mklex(r'"\w"')
        self.assertRaises(ParseError, lex.next)

    def test_op_single(self):
        ops = "*()%!,.[]{}~;=?:+-&|/%<>^"
        lex = self.mklex(ops)
        for c in ops:
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_dotstar(self):
        lex = self.mklex(".*")
        token = lex.next()
        self.assertEqual(token.type, TokenType.OPERATOR)
        self.assertEqual(token.str, ".*")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_ellipsis(self):
        lex = self.mklex("...")
        token = lex.next()
        self.assertEqual(token.type, TokenType.OPERATOR)
        self.assertEqual(token.str, "...")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_string_dotdotwrong(self):
        lex = self.mklex("..,")
        self.assertRaises(ParseError, lex.next)

    def test_op_arrow(self):
        lex = self.mklex("->")
        token = lex.next()
        self.assertEqual(token.type, TokenType.OPERATOR)
        self.assertEqual(token.str, "->")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_arrowstar(self):
        lex = self.mklex("->*")
        token = lex.next()
        self.assertEqual(token.type, TokenType.OPERATOR)
        self.assertEqual(token.str, "->*")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_eq(self):
        ops = "=*%!~+-&|/%<>^"
        str = ""
        for op in ops:
            str += op + " " + op + "= "
        lex = self.mklex(str)
        for c in ops:
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c)
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c + "=")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_double(self):
        ops = "&|<>+-"
        str = ""
        for op in ops:
            str += op + " " + op + op + " "
        lex = self.mklex(str)
        for c in ops:
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c)
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c * 2)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_op_double_eq(self):
        ops = "<>"
        str = ""
        for op in ops:
            str += op + op + "= "
        lex = self.mklex(str)
        for c in ops:
            token = next(lex)
            self.assertEqual(token.type, TokenType.OPERATOR)
            self.assertEqual(token.str, c + c + "=")
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_comment_line(self):
        lex = self.mklex("1//foo\n2")
        self.assertEqual(next(lex).val, 1)
        self.assertEqual(next(lex).val, 2)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_comment_multi(self):
        lex = self.mklex("1/*/\n3*/2")
        self.assertEqual(next(lex).val, 1)
        self.assertEqual(next(lex).val, 2)
        self.assertEqual(lex.next().type, TokenType.EOF)

    def test_comment_multi_eof(self):
        lex = self.mklex("1/*/\n3*2")
        self.assertEqual(next(lex).val, 1)
        self.assertRaises(ParseError, lex.next)

    def test_line(self):
        lex = self.mklex("foo bar \nfoobar \nb a r")
        self.assertTupleEqual((lex.line, lex.column), (1, 1))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (1, 4))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (1, 8))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (2, 7))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (3, 2))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (3, 4))
        next(lex)
        self.assertTupleEqual((lex.line, lex.column), (3, 6))

    def test_unknown(self):
        lex = self.mklex("$")
        self.assertRaises(ParseError, lex.next)


class TestParseStmt(TestParser):
    def test_noop_semicolon(self):
        par = self.mkpar(";")
        par._scan()
        self.assert_node(par._parse_stmt(t.Void()), n.Noop())
        self.assertTrue(par._is_eof())

    def test_noop_braces(self):
        par = self.mkpar("{}")
        par._scan()
        self.assert_node(par._parse_stmt(t.Void()), n.Noop())
        self.assertTrue(par._is_eof())

    def test_expression(self):
        par = self.mkpar("3;")
        par._scan()
        self.assert_node(par._parse_stmt(t.Void()), n.Literal(3, t.Arithmetic.DWORD))
        self.assertTrue(par._is_eof())

    def test_expression_nosemicolon(self):
        par = self.mkpar("3 4;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())

    def test_if_ok(self):
        par = self.mkpar("if(1) 2;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.If(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Literal(2, t.Arithmetic.DWORD)
            )
        )
        self.assertTrue(par._is_eof())

    def test_if_else(self):
        par = self.mkpar("if(1) 2; else 3;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.If(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Literal(2, t.Arithmetic.DWORD),
                n.Literal(3, t.Arithmetic.DWORD)
            )
        )
        self.assertTrue(par._is_eof())

    def test_if_chain(self):
        par = self.mkpar("if(1) 2; else if (3) 4; else 5;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.If(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Literal(2, t.Arithmetic.DWORD),
                n.If(
                    n.TypeCast(
                        n.Literal(3, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.Literal(4, t.Arithmetic.DWORD),
                    n.Literal(5, t.Arithmetic.DWORD)
                )
            )
        )
        self.assertTrue(par._is_eof())

    def test_if_nobracket(self):
        par = self.mkpar("if 1;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())

    def test_return_void_void(self):
        par = self.mkpar("return;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.Return(
                n.Noop(),
                t.Void()
            )
        )
        self.assertTrue(par._is_eof())

    def test_return_void_type(self):
        par = self.mkpar("return;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Arithmetic.WORD)

    def test_return_expression_cast(self):
        par = self.mkpar("return 3;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Arithmetic.FLOAT),
            n.Return(
                n.TypeCast(n.Literal(3, t.Arithmetic.DWORD), t.Arithmetic.FLOAT),
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_block_single(self):
        par = self.mkpar("{ 3; }")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.Literal(3, t.Arithmetic.DWORD)
        )
        self.assertTrue(par._is_eof())

    def test_block_multi(self):
        par = self.mkpar("{ 3; 4; }")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.StatementBlock([
                n.Literal(3, t.Arithmetic.DWORD),
                n.Literal(4, t.Arithmetic.DWORD)
            ])
        )
        self.assertTrue(par._is_eof())

    def test_block_nested(self):
        par = self.mkpar("{ {3; 4;} {{5; 6;}}  }")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.StatementBlock([
                n.StatementBlock([
                    n.Literal(3, t.Arithmetic.DWORD),
                    n.Literal(4, t.Arithmetic.DWORD)
                ]),
                n.StatementBlock([
                    n.Literal(5, t.Arithmetic.DWORD),
                    n.Literal(6, t.Arithmetic.DWORD)
                ])
            ])
        )
        self.assertTrue(par._is_eof())

    def test_block_noclose(self):
        par = self.mkpar("{ 3; 4;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())

    def test_stmt_decl(self):
        par = self.mkpar("int x = 0;")
        par._scan()
        node = par._parse_stmt(t.Void())
        self.assert_node(
            node,
            n.VariableDeclaration(
                "x",
                t.Arithmetic.DWORD,
                n.Literal(0, t.Arithmetic.DWORD)
            )
        )
        self.assertTrue(par._is_eof())

    def test_while(self):
        par = self.mkpar("while(1) 2;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.While(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Literal(2, t.Arithmetic.DWORD),
            )
        )
        self.assertTrue(par._is_eof())

    def test_while_nobrack(self):
        par = self.mkpar("while 1;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())

    def test_while_nobod(self):
        par = self.mkpar("while(1);")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.While(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Noop(),
            )
        )
        self.assertTrue(par._is_eof())

    def test_for(self):
        par = self.mkpar("for(1; 2; 3){4;5;}")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.StatementBlock([
                n.Literal(1, t.Arithmetic.DWORD),
                n.While(
                    n.TypeCast(
                        n.Literal(2, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.StatementBlock([
                        n.Literal(4, t.Arithmetic.DWORD),
                        n.Literal(5, t.Arithmetic.DWORD),
                        n.Literal(3, t.Arithmetic.DWORD),
                    ])
                )
            ]),
        )
        self.assertTrue(par._is_eof())

    def test_for_noblock(self):
        par = self.mkpar("for(1; 2; 3)4;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.StatementBlock([
                n.Literal(1, t.Arithmetic.DWORD),
                n.While(
                    n.TypeCast(
                        n.Literal(2, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.StatementBlock([
                        n.Literal(4, t.Arithmetic.DWORD),
                        n.Literal(3, t.Arithmetic.DWORD),
                    ])
                )
            ]),
        )
        self.assertTrue(par._is_eof())

    def test_for_ever(self):
        par = self.mkpar("for(;;)4;")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.While(
                n.Literal(True, t.Arithmetic.BOOL),
                n.Literal(4, t.Arithmetic.DWORD),
            )
        )
        self.assertTrue(par._is_eof())

    def test_for_badsyntax(self):
        par = self.mkpar("for 4;")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())
        par = self.mkpar("for( 4 5")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())
        par = self.mkpar("for( 4; 5")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())
        par = self.mkpar("for( 4; 5; ")
        par._scan()
        self.assertRaises(ParseError, par._parse_stmt, t.Void())

    def test_for_decl(self):
        par = self.mkpar("for(int i; 2; 3);")
        par._scan()
        self.assert_node(
            par._parse_stmt(t.Void()),
            n.StatementBlock([
                n.VariableDeclaration("i", t.Arithmetic.DWORD),
                n.While(
                    n.TypeCast(
                        n.Literal(2, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.Literal(3, t.Arithmetic.DWORD),
                )
            ]),
        )
        self.assertTrue(par._is_eof())


class TestParseProgram(TestParser):
    def test_parse_empty(self):
        par = self.mkpar("")
        node = par.parse()
        self.assert_node(
            node,
            n.Program([])
        )
        self.assertTrue(par._is_eof())

    def test_parse_decl(self):
        par = self.mkpar("int x = 0; int main() { return x; }")
        node = par.parse()
        self.assert_node(
            node,
            n.Program([
                n.VariableDeclaration(
                    "x",
                    t.Arithmetic.DWORD,
                    n.Literal(0, t.Arithmetic.DWORD)
                ),
                n.FunctionDeclaration(
                    t.Arithmetic.DWORD,
                    "main",
                    n.FunctionParameterDeclaration([]),
                    n.Return(
                        n.VariableReference("x", t.Arithmetic.DWORD),
                        t.Arithmetic.DWORD
                    )
                )
            ])
        )
        self.assertTrue(par._is_eof())

    def test_parse_scope_error(self):
        par = self.mkpar("int main() { return x; }")
        self.assertRaises(ParseError, par.parse)

    def test_parse_nodecl(self):
        par = self.mkpar("3+2;")
        self.assertRaises(ParseError, par.parse)
