import io
from unittest import mock

from pycomp.front import c
from pycomp.front.base import ParseError
from pycomp.symbol_table import SymbolType as symt, ScopeError, TypeSizer
from pycomp import ast_nodes as n, ast_types as t
from .. import util


class TestParser(util.TestParser):
    def mkpar(self, text, **ns):
        mockns = mock.MagicMock()
        for k, v in ns.items():
            setattr(mockns, k, v)
        return c.CParser(c.CLexer(io.StringIO(text)), TypeSizer())

