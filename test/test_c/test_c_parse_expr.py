from .test_c_base import *


class TestParseExpr(TestParser):
    def test__parse_ex_prim__string(self):
        par = self.mkpar('"foobar"')
        par._scan()
        node = par._parse_ex_prim()
        self.assert_node(node, n.Literal("foobar", t.Pointer(t.Arithmetic.BYTE)))
        self.assertTrue(par._is_eof())

    def test__parse_ex_prim__num(self):
        par = self.mkpar('123')
        par._scan()
        node = par._parse_ex_prim()
        self.assert_node(node, n.Literal(123, t.Arithmetic.DWORD))
        self.assertTrue(par._is_eof())

    def test__parse_ex_prim__unk(self):
        par = self.mkpar('->')
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_prim)

    def test__parse_ex_prim__var_declared(self):
        par = self.mkpar("foo")
        par._scan()
        par.symbol_table.declare_raw("foo", symt.variable, t.Arithmetic.DWORD)
        node = par._parse_ex_prim()
        self.assert_node(node, n.VariableReference("foo", t.Arithmetic.DWORD))
        self.assertTrue(par._is_eof())

    def test__parse_ex_prim__var_notdeclared(self):
        par = self.mkpar("foo")
        par._scan()
        self.assertRaises(ScopeError, par._parse_ex_prim)

    def test__parse_ex_prim__var_wrong_symbol(self):
        par = self.mkpar("foo")
        par._scan()
        par.symbol_table.declare_raw("foo", symt.type, t.Arithmetic.DWORD)
        self.assertRaises(ParseError, par._parse_ex_prim)

    def test__parse_ex_prim__paren_nested(self):
        par = self.mkpar('((123))')
        par._scan()
        node = par._parse_ex_prim()
        self.assert_node(node, n.Literal(123, t.Arithmetic.DWORD))
        self.assertTrue(par._is_eof())

    def test__parse_ex_prim__paren_assoc(self):
        par = self.mkpar('(3*(1+2))')
        par._scan()
        node = par._parse_ex_prim()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(3, t.Arithmetic.DWORD),
                "*",
                n.BinaryOp(
                    n.Literal(1, t.Arithmetic.DWORD),
                    "+",
                    n.Literal(2, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_prim__paren_noclose(self):
        par = self.mkpar('(123')
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_prim)

    def test_parse_ex_top__noop(self):
        par = self.mkpar("123")
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(node, n.Literal(123, t.Arithmetic.DWORD))
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__membaccess_ok(self):
        par = self.mkpar("x.y")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.MemberAccess(
                n.VariableReference("x", struct),
                "y",
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__membaccess_prim(self):
        par = self.mkpar("(x).y")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.MemberAccess(
                n.VariableReference("x", struct),
                "y",
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__membaccess_nocomp(self):
        par = self.mkpar("(123).y")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test_parse_ex_top__membaccess_nomem(self):
        par = self.mkpar("x.x")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        self.assertRaises(ScopeError, par._parse_ex_top)

    def test_parse_ex_top__membaccess_nomemname(self):
        par = self.mkpar("x. 123")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test_parse_ex_top__membaccess_chain(self):
        par = self.mkpar("x.y.z")
        struct = t.Struct("foo")
        struct.scope.declare_raw("z", symt.variable, t.Arithmetic.FLOAT)
        struct.scope.declare_raw("y", symt.variable, struct)
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.MemberAccess(
                n.MemberAccess(
                    n.VariableReference("x", struct),
                    "y",
                    struct
                ),
                "z",
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__membaccess_arrow(self):
        par = self.mkpar("x->y")
        struct = t.Struct("foo")
        struct.scope.declare_raw("y", symt.variable, t.Arithmetic.FLOAT)
        par.symbol_table.declare_raw("x", symt.variable, t.Pointer(struct))
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.MemberAccess(
                n.UnaryOp("*", n.VariableReference("x", t.Pointer(struct)), struct, True),
                "y",
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__typecast_ok(self):
        par = self.mkpar("int(2.3)")
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.TypeCast(
                n.Literal(2.3, t.Arithmetic.FLOAT),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__typecast_noop(self):
        par = self.mkpar("int(23)")
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.Literal(23, t.Arithmetic.DWORD),
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__typecast_unmatched(self):
        par = self.mkpar("int(23")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)


    def test_parse_ex_top__typecast_nobracket(self):
        par = self.mkpar("int 23")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test_parse_ex_top__typecast_brack(self):
        par = self.mkpar("(unsigned int)2.3")
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.TypeCast(
                n.Literal(2.3, t.Arithmetic.FLOAT),
                t.Arithmetic.UDWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__typecast_brack_nomatch(self):
        par = self.mkpar("(unsigned int 2.3")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_prim)

    def test_is_callable_func(self):
        par = self.mkpar("")
        self.assertTrue(par._is_callable(t.Function(t.Void(), [])))

    def test_is_callable_ptrfunc(self):
        par = self.mkpar("")
        self.assertTrue(par._is_callable(t.Pointer(t.Function(t.Void(), []))))

    def test_is_callable_nofunc(self):
        par = self.mkpar("")
        self.assertFalse(par._is_callable(t.Arithmetic.DWORD))

    def test_is_callable_ptrfunc_return(self):
        par = self.mkpar("")
        self.assertEqual(
            t.Function(t.Void(), []),
            par._is_callable(t.Pointer(t.Function(t.Void(), [])))
        )

    def test_parse_ex_top__postinc_lval(self):
        par = self.mkpar("x++")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.UnaryOp(
                "++2",
                n.VariableReference("x", t.Arithmetic.DWORD),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__postinc_rval(self):
        par = self.mkpar("123++")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test_parse_ex_top__subscr(self):
        par = self.mkpar("x[2]")
        par.symbol_table.declare_raw("x", symt.variable, t.Pointer(t.Arithmetic.FLOAT))
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.UnaryOp(
                "*",
                n.BinaryOp(
                    n.VariableReference("x", t.Pointer(t.Arithmetic.FLOAT)),
                    "+",
                    n.Literal(2, t.Arithmetic.DWORD),
                    t.Pointer(t.Arithmetic.FLOAT)
                ),
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__subscr_unclosed(self):
        par = self.mkpar("x[2;")
        par.symbol_table.declare_raw("x", symt.variable, t.Pointer(t.Arithmetic.FLOAT))
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test__parse_ex_unary__inc_lval(self):
        par = self.mkpar("++x")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "++",
                n.VariableReference("x", t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
                True
            )
        )
        self.assertTrue(par._is_eof())

    def test_parse_ex_top__inc_rval(self):
        par = self.mkpar("++123")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test__parse_ex_unary__minus(self):
        par = self.mkpar("-4")
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "-",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_unary__bang(self):
        par = self.mkpar("!4.5")
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "!",
                n.TypeCast(
                    n.Literal(4.5, t.Arithmetic.FLOAT),
                    t.Arithmetic.BOOL
                ),
                t.Arithmetic.BOOL
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_unary__chain(self):
        par = self.mkpar("+-+-4")
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "+",
                n.UnaryOp(
                    "-",
                    n.UnaryOp(
                        "+",
                        n.UnaryOp(
                            "-",
                            n.Literal(4, t.Arithmetic.DWORD),
                            t.Arithmetic.DWORD
                        ),
                        t.Arithmetic.DWORD
                    ),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_unary__deref(self):
        par = self.mkpar("*x")
        par.symbol_table.declare_raw("x", symt.variable, t.Pointer(t.Arithmetic.DWORD))
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "*",
                n.VariableReference("x", t.Pointer(t.Arithmetic.DWORD)),
                t.Arithmetic.DWORD,
                True
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_unary__deref_noptr(self):
        par = self.mkpar("*x")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_unary)

    def test__parse_ex_unary__noptr(self):
        par = self.mkpar("*x")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_unary)

    def test__parse_ex_unary__ref(self):
        par = self.mkpar("&x")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "&",
                n.VariableReference("x", t.Arithmetic.DWORD),
                t.Pointer(t.Arithmetic.DWORD)
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_unary__ref_rval(self):
        par = self.mkpar("&123")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_unary)

    def test__parse_ex_unary__top(self):
        par = self.mkpar("!x++")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_unary()
        self.assert_node(
            node,
            n.UnaryOp(
                "!",
                n.TypeCast(
                    n.UnaryOp(
                        "++2",
                        n.VariableReference("x", t.Arithmetic.DWORD),
                        t.Arithmetic.DWORD
                    ),
                    t.Arithmetic.BOOL
                ),
                t.Arithmetic.BOOL
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_ptrmem_ok(self):
        par = self.mkpar("x.*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.FLOAT, struct)
        )
        par._scan()
        node = par._parse_ex_ptrmem()
        self.assert_node(
            node,
            n.BinaryOp(
                n.VariableReference("x", struct),
                ".*",
                n.VariableReference("y", t.PointerToMember(t.Arithmetic.FLOAT, struct)),
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_ptrmem_wrong_ptr(self):
        par = self.mkpar("x.*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.FLOAT, t.Struct("bar"))
        )
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_ptrmem)

    def test__parse_ex_ptrmem_no_ptr(self):
        par = self.mkpar("x.*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.Arithmetic.FLOAT
        )
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_ptrmem)

    def test__parse_ex_ptrmem_arrow_ok(self):
        par = self.mkpar("x->*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, t.Pointer(struct))
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.FLOAT, struct)
        )
        par._scan()
        node = par._parse_ex_ptrmem()
        self.assert_node(
            node,
            n.BinaryOp(
                n.UnaryOp(
                    "*",
                    n.VariableReference("x", t.Pointer(struct)),
                    struct,
                    True
                ),
                ".*",
                n.VariableReference("y", t.PointerToMember(t.Arithmetic.FLOAT, struct)),
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_ptrmem_unary_op(self):
        par = self.mkpar("x.*++y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.FLOAT, struct)
        )
        par._scan()
        node = par._parse_ex_ptrmem()
        self.assert_node(
            node,
            n.BinaryOp(
                n.VariableReference("x", struct),
                ".*",
                n.UnaryOp(
                    "++",
                    n.VariableReference("y", t.PointerToMember(t.Arithmetic.FLOAT, struct)),
                    t.PointerToMember(t.Arithmetic.FLOAT, struct),
                    True
                ),
                t.Arithmetic.FLOAT
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_ptrmem_unary(self):
        par = self.mkpar("++y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.FLOAT, struct)
        )
        par._scan()
        node = par._parse_ex_ptrmem()
        self.assert_node(
            node,
            n.UnaryOp(
                "++",
                n.VariableReference("y", t.PointerToMember(t.Arithmetic.FLOAT, struct)),
                t.PointerToMember(t.Arithmetic.FLOAT, struct),
                True
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_mul(self):
        par = self.mkpar("2*3")
        par._scan()
        node = par._parse_ex_mul()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "*",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_mul_chain(self):
        par = self.mkpar("2*3/4")
        par._scan()
        node = par._parse_ex_mul()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "*",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "/",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_mul_ptrmem_arg(self):
        par = self.mkpar("2*x.*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.DWORD, struct)
        )
        par._scan()
        node = par._parse_ex_mul()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "*",
                n.BinaryOp(
                    n.VariableReference("x", struct),
                    ".*",
                    n.VariableReference("y", t.PointerToMember(t.Arithmetic.DWORD, struct)),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_mul_ptrmem(self):
        par = self.mkpar("x.*y")
        struct = t.Struct("foo")
        par.symbol_table.declare_raw("x", symt.variable, struct)
        par.symbol_table.declare_raw(
            "y", symt.variable, t.PointerToMember(t.Arithmetic.DWORD, struct)
        )
        par._scan()
        node = par._parse_ex_mul()
        self.assert_node(
            node,
            n.BinaryOp(
                n.VariableReference("x", struct),
                ".*",
                n.VariableReference("y", t.PointerToMember(t.Arithmetic.DWORD, struct)),
                t.Arithmetic.DWORD
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_add(self):
        par = self.mkpar("2+3")
        par._scan()
        node = par._parse_ex_add()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "+",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_add_chain(self):
        par = self.mkpar("2+3-4")
        par._scan()
        node = par._parse_ex_add()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "+",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "-",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_add_mul_arg(self):
        par = self.mkpar("2+3*4")
        par._scan()
        node = par._parse_ex_add()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "+",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "*",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_add_mul(self):
        par = self.mkpar("2*3")
        par._scan()
        node = par._parse_ex_add()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "*",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_shift(self):
        par = self.mkpar("2<<3")
        par._scan()
        node = par._parse_ex_shift()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<<",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_shift_chain(self):
        par = self.mkpar("2<<3>>4")
        par._scan()
        node = par._parse_ex_shift()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "<<",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                ">>",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_shift_add_arg(self):
        par = self.mkpar("2<<3+4")
        par._scan()
        node = par._parse_ex_shift()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<<",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "+",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_shift_add(self):
        par = self.mkpar("2+3")
        par._scan()
        node = par._parse_ex_shift()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "+",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpo(self):
        par = self.mkpar("2<3")
        par._scan()
        node = par._parse_ex_cmpo()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpo_chain(self):
        par = self.mkpar("2<3>4")
        par._scan()
        node = par._parse_ex_cmpo()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "<",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                ">",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpo_shift_arg(self):
        par = self.mkpar("2<3<<4")
        par._scan()
        node = par._parse_ex_cmpo()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "<<",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpo_shift(self):
        par = self.mkpar("2<<3")
        par._scan()
        node = par._parse_ex_cmpo()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<<",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpe(self):
        par = self.mkpar("2==3")
        par._scan()
        node = par._parse_ex_cmpe()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "==",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpe_chain(self):
        par = self.mkpar("2==3!=4")
        par._scan()
        node = par._parse_ex_cmpe()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "==",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "!=",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpe_cmpo_arg(self):
        par = self.mkpar("2==3<4")
        par._scan()
        node = par._parse_ex_cmpe()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "==",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "<",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_cmpe_cmpo(self):
        par = self.mkpar("2<3")
        par._scan()
        node = par._parse_ex_cmpe()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "<",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_and(self):
        par = self.mkpar("2&3")
        par._scan()
        node = par._parse_ex_b_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_and_chain(self):
        par = self.mkpar("2&3&4")
        par._scan()
        node = par._parse_ex_b_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "&",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "&",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_and_cmpe_arg(self):
        par = self.mkpar("2&3==4")
        par._scan()
        node = par._parse_ex_b_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "==",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_and_cmpe(self):
        par = self.mkpar("2==3")
        par._scan()
        node = par._parse_ex_b_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "==",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_xor(self):
        par = self.mkpar("2^3")
        par._scan()
        node = par._parse_ex_b_xor()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "^",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_xor_chain(self):
        par = self.mkpar("2^3^4")
        par._scan()
        node = par._parse_ex_b_xor()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "^",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "^",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_xor_b_and_arg(self):
        par = self.mkpar("2^3&4")
        par._scan()
        node = par._parse_ex_b_xor()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "^",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "&",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_xor_b_and(self):
        par = self.mkpar("2&3")
        par._scan()
        node = par._parse_ex_b_xor()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_or(self):
        par = self.mkpar("2|3")
        par._scan()
        node = par._parse_ex_b_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "|",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_or_chain(self):
        par = self.mkpar("2|3|4")
        par._scan()
        node = par._parse_ex_b_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "|",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "|",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_or_b_xor_arg(self):
        par = self.mkpar("2|3^4")
        par._scan()
        node = par._parse_ex_b_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "|",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "^",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_b_or_b_xor(self):
        par = self.mkpar("2^3")
        par._scan()
        node = par._parse_ex_b_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "^",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_and(self):
        par = self.mkpar("2&&3")
        par._scan()
        node = par._parse_ex_l_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&&",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_and_chain(self):
        par = self.mkpar("2&&3&&4")
        par._scan()
        node = par._parse_ex_l_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "&&",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "&&",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_and_b_or_arg(self):
        par = self.mkpar("2&&3|4")
        par._scan()
        node = par._parse_ex_l_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&&",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "|",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_and_b_or(self):
        par = self.mkpar("2|3")
        par._scan()
        node = par._parse_ex_l_and()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "|",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())
        ##

    def test__parse_ex_l_or(self):
        par = self.mkpar("2||3")
        par._scan()
        node = par._parse_ex_l_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "||",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_or_chain(self):
        par = self.mkpar("2||3||4")
        par._scan()
        node = par._parse_ex_l_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.BinaryOp(
                    n.Literal(2, t.Arithmetic.DWORD),
                    "||",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                "||",
                n.Literal(4, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_or_l_and_arg(self):
        par = self.mkpar("2||3&&4")
        par._scan()
        node = par._parse_ex_l_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "||",
                n.BinaryOp(
                    n.Literal(3, t.Arithmetic.DWORD),
                    "&&",
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_l_or_l_and(self):
        par = self.mkpar("2&&3")
        par._scan()
        node = par._parse_ex_l_or()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "&&",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_logic(self):
        par = self.mkpar("2||3")
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.BinaryOp(
                n.Literal(2, t.Arithmetic.DWORD),
                "||",
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_assign_lval(self):
        par = self.mkpar("x = 3")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.Assign(
                n.VariableReference("x", t.Arithmetic.DWORD),
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_assign_rval(self):
        par = self.mkpar("2 = 3")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_assign)

    def test__parse_ex_assign_assign_chain(self):
        par = self.mkpar("x = y = 3")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par.symbol_table.declare_raw("y", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.Assign(
                n.VariableReference("x", t.Arithmetic.DWORD),
                n.Assign(
                    n.VariableReference("y", t.Arithmetic.DWORD),
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_op_lval(self):
        par = self.mkpar("x += 3")
        par.symbol_table.declare_raw("x", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.Assign(
                n.VariableReference("x", t.Arithmetic.DWORD),
                n.BinaryOp(
                    n.VariableReference("x", t.Arithmetic.DWORD),
                    "+",
                    n.Literal(3, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_inc_rval(self):
        par = self.mkpar("2 += 3")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_assign)

    def test__parse_ex_assign_ternary_ok(self):
        par = self.mkpar("1 ? 2 : 3")
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.If(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.Literal(2, t.Arithmetic.DWORD),
                n.Literal(3, t.Arithmetic.DWORD),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_assign_ternary_noelse(self):
        par = self.mkpar("1 ? 2 ;")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_assign)

    def test__parse_ex_assign_ternary_etero(self):
        par = self.mkpar("1 ? 2 : 3.6")
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_assign)

    def test__parse_ex_assign_ternary_chain(self):
        par = self.mkpar("1 ? 2 ? 3 : 4 : 5 ? 6 : 7")
        par._scan()
        node = par._parse_ex_assign()
        self.assert_node(
            node,
            n.If(
                n.TypeCast(
                    n.Literal(1, t.Arithmetic.DWORD),
                    t.Arithmetic.BOOL
                ),
                n.If(
                    n.TypeCast(
                        n.Literal(2, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.Literal(3, t.Arithmetic.DWORD),
                    n.Literal(4, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                n.If(
                    n.TypeCast(
                        n.Literal(5, t.Arithmetic.DWORD),
                        t.Arithmetic.BOOL
                    ),
                    n.Literal(6, t.Arithmetic.DWORD),
                    n.Literal(7, t.Arithmetic.DWORD),
                    t.Arithmetic.DWORD,
                ),
                t.Arithmetic.DWORD,
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_top_funcall_noargs(self):
        par = self.mkpar("foo()")
        type = t.Function(t.Void(), [])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.FunctionCall(
                n.VariableReference("foo", type),
                n.FunctionArgumentsPassed(),
                t.Void()
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_top_funcall_args(self):
        par = self.mkpar("foo(3, 4)")
        type = t.Function(t.Void(), [t.Arithmetic.DWORD, t.Arithmetic.FLOAT])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.FunctionCall(
                n.VariableReference("foo", type),
                n.FunctionArgumentsPassed([
                    n.Literal(3, t.Arithmetic.DWORD),
                    n.TypeCast(
                        n.Literal(4, t.Arithmetic.DWORD),
                        t.Arithmetic.FLOAT
                    )
                ]),
                t.Void()
            )
        )
        self.assertTrue(par._is_eof())

    def test__parse_ex_top_funcall_args_too_many(self):
        par = self.mkpar("foo(3, 4, 5)")
        type = t.Function(t.Void(), [t.Arithmetic.DWORD, t.Arithmetic.FLOAT])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test__parse_ex_top_funcall_args_too_few(self):
        par = self.mkpar("foo(3, 4)")
        type = t.Function(t.Void(), [t.Arithmetic.DWORD, t.Arithmetic.FLOAT, t.Arithmetic.DWORD])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test__parse_ex_top_funcall_nobrack(self):
        par = self.mkpar("foo(3, 4;")
        type = t.Function(t.Void(), [t.Arithmetic.DWORD, t.Arithmetic.FLOAT])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)

    def test__parse_ex_top_funcall_nofunc(self):
        par = self.mkpar("foo()")
        par.symbol_table.declare_raw("foo", symt.variable, t.Arithmetic.DWORD)
        par._scan()
        self.assertRaises(ParseError, par._parse_ex_top)


    def test__parse_ex_top_funcall_varargs(self):
        par = self.mkpar("foo(3, 4, 5, 6)")
        type = t.Function(t.Void(), [t.Arithmetic.DWORD, t.VarArgs()])
        par.symbol_table.declare_raw("foo", symt.variable, type)
        par._scan()
        node = par._parse_ex_top()
        self.assert_node(
            node,
            n.FunctionCall(
                n.VariableReference("foo", type),
                n.FunctionArgumentsPassed([
                    n.Literal(3, t.Arithmetic.DWORD),
                    n.Literal(4, t.Arithmetic.DWORD),
                    n.Literal(5, t.Arithmetic.DWORD),
                    n.Literal(6, t.Arithmetic.DWORD)
                ]),
                t.Void()
            )
        )
        self.assertTrue(par._is_eof())
