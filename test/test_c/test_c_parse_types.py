from .test_c_base import *


class TestParseType(TestParser):
    def test_parse_int(self):
        par = self.mkpar("int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

    def test_parse_char(self):
        par = self.mkpar("char")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.BYTE
        )
        self.assertTrue(par._is_eof())

    def test_parse_float(self):
        par = self.mkpar("float")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.FLOAT
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("double")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.FLOAT
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("long double")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.FLOAT
        )
        self.assertTrue(par._is_eof())

    def test_parse_signed(self):
        par = self.mkpar("signed")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("signed int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("signed long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("signed short")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.WORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("signed char")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.BYTE
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("signed float")
        par._scan()
        par._parse_type()
        self.assertFalse(par._is_eof())

        par = self.mkpar("signed unsigned")
        par._scan()
        par._parse_type()
        self.assertFalse(par._is_eof())

    def test_parse_ununsigned(self):
        par = self.mkpar("unsigned")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UDWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UDWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UDWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned short")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned char")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UBYTE
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned float")
        par._scan()
        par._parse_type()
        self.assertFalse(par._is_eof())

        par = self.mkpar("unsigned signed")
        par._scan()
        par._parse_type()
        self.assertFalse(par._is_eof())

    def test_parse_short(self):
        par = self.mkpar("short")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.WORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("short int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.WORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned short")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned short int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UWORD
        )
        self.assertTrue(par._is_eof())

    def test_parse_long(self):
        par = self.mkpar("long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("long int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.DWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UDWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned long int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UDWORD
        )
        self.assertTrue(par._is_eof())

    def test_parse_long_long(self):
        par = self.mkpar("long long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.QWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("long long int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.QWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned long long")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UQWORD
        )
        self.assertTrue(par._is_eof())

        par = self.mkpar("unsigned long long int")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Arithmetic.UQWORD
        )
        self.assertTrue(par._is_eof())

    def test_parse_void(self):
        par = self.mkpar("void")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Void()
        )
        self.assertTrue(par._is_eof())

    def test_parse_compound_ok(self):
        par = self.mkpar("struct foo")
        par._scan()
        par.symbol_table.declare_raw("foo", symt.type, t.Struct("foo"))
        self.assertEqual(
            par._parse_type(),
            t.Struct("foo")
        )
        self.assertTrue(par._is_eof())

    def test_parse_compound_nodecl(self):
        par = self.mkpar("struct foo")
        par._scan()
        self.assertRaises(ScopeError, par._parse_type)

    def test_parse_typedef_ok(self):
        par = self.mkpar("foo")
        par.symbol_table.declare_raw("foo", symt.type, t.Struct("foo"))
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Struct("foo")
        )
        self.assertTrue(par._is_eof())

    def test_parse_typedef_nodecl(self):
        par = self.mkpar("foo")
        par._scan()
        self.assertRaises(ScopeError, par._parse_type)

    def test_parse_pointer(self):
        par = self.mkpar("void**")
        par._scan()
        self.assertEqual(
            par._parse_type(),
            t.Pointer(t.Pointer(t.Void()))
        )

    def test_parse_notype(self):
        par = self.mkpar("123")
        par._scan()
        self.assertRaises(ParseError, par._parse_type)
