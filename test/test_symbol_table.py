import unittest
from unittest import mock


from pycomp import symbol_table as st
from pycomp import ast_nodes
from pycomp import ast_types


class TestIScope(unittest.TestCase):
    def test_declare_type(self):
        scope = st.IScope()
        scope.declare_raw = mock.MagicMock()
        scope.declare(ast_nodes.TypeDeclaration("foo", ast_types.Void()))
        scope.declare_raw.assert_called_once_with(
            "foo", st.SymbolType.type, ast_types.Void()
        )

    def test_declare_var(self):
        scope = st.IScope()
        scope.declare_raw = mock.MagicMock()
        scope.declare(ast_nodes.VariableDeclaration("foo", ast_types.Arithmetic.WORD))
        scope.declare_raw.assert_called_once_with(
            "foo", st.SymbolType.variable, ast_types.Arithmetic.WORD
        )

    def test_declare_func(self):
        scope = st.IScope()
        scope.declare_raw = mock.MagicMock()
        scope.declare(ast_nodes.FunctionDeclaration(
            ast_types.Void(),
            "foo",
            ast_nodes.FunctionParameterDeclaration([
                ast_nodes.VariableDeclaration("bar", ast_types.Arithmetic.WORD),
                ast_nodes.VariableDeclaration("baz", ast_types.Arithmetic.FLOAT),
            ])
        ))
        scope.declare_raw.assert_called_once_with(
            "foo",
            st.SymbolType.function,
            ast_types.Function(ast_types.Void(), [
                ast_types.Arithmetic.WORD,
                ast_types.Arithmetic.FLOAT
            ])
        )

    def test_declare_unk(self):
        scope = st.IScope()
        scope.declare_raw = mock.MagicMock()
        self.assertRaises(Exception, scope.declare, ast_nodes.Noop())
        scope.declare_raw.assert_not_called()

    def test_get_expect_ok(self):
        scope = st.IScope()
        scope.get = mock.MagicMock(return_value=st.Symbol(
            st.SymbolType.variable, ast_types.Arithmetic.WORD
        ))
        self.assertEqual(
            scope.get_expect("foo", st.SymbolType.variable).type,
            ast_types.Arithmetic.WORD
        )
        scope.get.assert_called_once_with("foo")

    def test_get_expect_wrong(self):
        scope = st.IScope()
        scope.get = mock.MagicMock(return_value=st.Symbol(
            st.SymbolType.variable, ast_types.Arithmetic.WORD
        ))
        self.assertRaises(st.ScopeError, scope.get_expect, "foo", st.SymbolType.function)
        scope.get.assert_called_once_with("foo")

    def test_interface(self):
        scope = st.IScope()
        self.assertRaises(NotImplementedError, scope.declare_raw,
                          "foo", st.SymbolType.variable, ast_types.Arithmetic.WORD)
        self.assertRaises(NotImplementedError, scope.get, "foo")
        self.assertRaises(NotImplementedError, scope.contains, "foo")

    def test_contains_expect_ok(self):
        scope = st.IScope()
        scope.get = mock.MagicMock(return_value=st.Symbol(
            st.SymbolType.variable, ast_types.Arithmetic.WORD
        ))
        self.assertTrue(scope.contains_expect("foo", st.SymbolType.variable))

    def test_contains_expect_wrong_type(self):
        scope = st.IScope()
        scope.get = mock.MagicMock(return_value=st.Symbol(
            st.SymbolType.variable, ast_types.Arithmetic.WORD
        ))
        self.assertFalse(scope.contains_expect("foo", st.SymbolType.function))

    def test_contains_expect_not_found(self):
        scope = st.IScope()
        scope.get = mock.MagicMock(side_effect=st.ScopeError())
        self.assertFalse(scope.contains_expect("foo", st.SymbolType.function))


class TestScope(unittest.TestCase):
    def test_init(self):
        scope = st.Scope()
        self.assertEqual(scope.symbols, {})
        self.assertFalse(scope.contains("foo"))
        self.assertRaises(st.ScopeError, scope.get, "foo")

    def test_declare_ok(self):
        scope = st.Scope()
        scope.declare_raw("foo", st.SymbolType.variable, ast_types.Arithmetic.WORD)
        self.assertTrue(scope.contains("foo"))
        sym = scope.get("foo")
        self.assertEqual(sym.what, st.SymbolType.variable)
        self.assertEqual(sym.type, ast_types.Arithmetic.WORD)

    def test_declare_multi(self):
        scope = st.Scope()
        scope.declare_raw("foo", st.SymbolType.variable, ast_types.Arithmetic.WORD)
        scope.declare_raw("bar", st.SymbolType.type, ast_types.Arithmetic.FLOAT)

        self.assertTrue(scope.contains("foo"))
        sym = scope.get("foo")
        self.assertEqual(sym.what, st.SymbolType.variable)
        self.assertEqual(sym.type, ast_types.Arithmetic.WORD)

        self.assertTrue(scope.contains("bar"))
        sym = scope.get("bar")
        self.assertEqual(sym.what, st.SymbolType.type)
        self.assertEqual(sym.type, ast_types.Arithmetic.FLOAT)

    def test_redeclare(self):
        scope = st.Scope()
        scope.declare_raw("foo", st.SymbolType.variable, ast_types.Arithmetic.WORD)
        self.assertRaises(st.ScopeError, scope.declare_raw,
                          "foo", st.SymbolType.type, ast_types.Arithmetic.FLOAT)

        self.assertTrue(scope.contains("foo"))
        sym = scope.get("foo")
        self.assertEqual(sym.what, st.SymbolType.variable)
        self.assertEqual(sym.type, ast_types.Arithmetic.WORD)


class TestSymbolTable(unittest.TestCase):
    def test_init(self):
        scope = st.SymbolTable()
        self.assertEqual(len(scope.scopes), 1)
        self.assertIs(scope.scope, scope.scopes[0])

    def test_push(self):
        scope = st.SymbolTable()
        glob = scope.scope
        scope.push()
        self.assertEqual(len(scope.scopes), 2)
        self.assertIs(scope.scopes[1], glob)
        self.assertIsInstance(scope.scope, st.Scope)
        self.assertIsNot(scope.scope, glob)

        expl = st.Scope()
        pushed = scope.push(expl)
        self.assertIs(scope.scope, expl)
        self.assertIs(pushed, expl)
        self.assertEqual(len(scope.scopes), 3)

    def test_pop(self):
        scope = st.SymbolTable()
        s1 = scope.push()
        s2 = scope.push()
        s3 = scope.push()
        self.assertEqual(len(scope.scopes), 4)
        self.assertIs(scope.pop(), s3)
        self.assertEqual(len(scope.scopes), 3)
        self.assertIs(scope.pop(), s2)
        self.assertEqual(len(scope.scopes), 2)
        self.assertIs(scope.pop(), s1)
        self.assertEqual(len(scope.scopes), 1)
        self.assertRaises(Exception, scope.pop)

    def test_declare_raw(self):
        scope = st.SymbolTable()
        scope.declare_raw("foo", st.SymbolType.variable, ast_types.Arithmetic.WORD)
        self.assertEqual(
            scope.scope.get_expect("foo", st.SymbolType.variable).type,
            ast_types.Arithmetic.WORD
        )
        scope.push()
        scope.declare_raw("foo", st.SymbolType.variable, ast_types.Arithmetic.UWORD)
        self.assertEqual(
            scope.scope.get_expect("foo", st.SymbolType.variable).type,
            ast_types.Arithmetic.UWORD
        )
        self.assertEqual(
            scope.scopes[1].get_expect("foo", st.SymbolType.variable).type,
            ast_types.Arithmetic.WORD
        )

    def test_get_current(self):
        scope = st.SymbolTable()
        scope.declare_raw("foo", st.SymbolType.type, ast_types.Arithmetic.WORD)
        scope.push()
        scope.declare_raw("bar", st.SymbolType.variable, ast_types.Arithmetic.FLOAT)
        sym = scope.get("bar")
        self.assertEqual(sym.what, st.SymbolType.variable)
        self.assertEqual(sym.type, ast_types.Arithmetic.FLOAT)


    def test_get_old_scope(self):
        scope = st.SymbolTable()
        scope.declare_raw("foo", st.SymbolType.type, ast_types.Arithmetic.WORD)
        scope.push()
        scope.declare_raw("bar", st.SymbolType.variable, ast_types.Arithmetic.FLOAT)
        sym = scope.get("foo")
        self.assertEqual(sym.what, st.SymbolType.type)
        self.assertEqual(sym.type, ast_types.Arithmetic.WORD)

    def test_get_not_found(self):
        scope = st.SymbolTable()
        scope.declare_raw("foo", st.SymbolType.type, ast_types.Arithmetic.WORD)
        scope.push()
        scope.declare_raw("bar", st.SymbolType.variable, ast_types.Arithmetic.FLOAT)
        self.assertRaises(st.ScopeError, scope.get, "baz")

    def test_contains(self):
        scope = st.SymbolTable()
        scope.declare_raw("foo", st.SymbolType.type, ast_types.Arithmetic.WORD)
        scope.push()
        scope.declare_raw("bar", st.SymbolType.variable, ast_types.Arithmetic.FLOAT)
        self.assertTrue(scope.contains("foo"))
        self.assertTrue(scope.contains("bar"))
        self.assertFalse(scope.contains("baz"))
