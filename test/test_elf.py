import io
import unittest

from pycomp.back.elf import elf_file, c_api, linker
from pycomp.util.print_r import print_hex
from pycomp.util.debugtools import strlen_stripansi, ansi_align, AnsiAnnotator
from pycomp import fixint


class HexTestCase(unittest.TestCase):
    def assert_bytes_equal(self, a, b):
        if a == b:
            return

        strm = io.StringIO()
        print_hex(a, 16, "", strm)
        lines_a = strm.getvalue().splitlines()
        strm = io.StringIO()
        print_hex(b, 16, "", strm)
        lines_b = strm.getvalue().splitlines()

        diffn = len(lines_a) - len(lines_b)
        diff = [""] * abs(diffn)
        if diffn < 0:
            lines_a += diff
        else:
            lines_b += diff

        msg = "Byte strings do not match\n"
        pada = max(map(strlen_stripansi, lines_a))
        padb = max(map(strlen_stripansi, lines_b))
        for la, lb in zip(lines_a, lines_b):
            if la == lb:
                msg += " %s || %s\n" % (ansi_align(la, pada), ansi_align(lb, padb))
            else:
                anna = AnsiAnnotator(la)
                annb = AnsiAnnotator(lb)

                for i, (ca, cb) in enumerate(zip(la, lb)):
                    if ca != cb:
                        anna.annotate(i, "31")
                        annb.annotate(i, "31")

                msg += ">%s !! %s<\n" % (
                    ansi_align(str(anna), pada),
                    ansi_align(str(annb), padb)
                )

        self.fail(msg)


class TestElfFile(HexTestCase):
    def test_write_empty(self):
        ef = elf_file.ElfFile()
        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident
            b"\0\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry
            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\0\0\0\0\0\0\0\0" # e_shoff
            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize
            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\0\0" # e_shnum
            b"\0\0" # e_shstrndx
        )


class TestOutElfFileRel(HexTestCase):
    def test_empty(self):
        ef = elf_file.OutElfFileRel()
        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\7\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\x2f\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\1\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text\0.data\0.rodata\0.symtab\0.strtab\0"
            # Data (.strtab)
            b"\0"
        )

    def test_progbits_data(self):
        ef = elf_file.OutElfFileRel()
        ef.find_section(".text").data.append(b"\0.text is here!\0\0")
        ef.find_section(".data").data.append(b".data is here!\0\0")
        ef.find_section(".rodata").data.append(b".rodata is here!")
        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\7\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\x2f\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\x11\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x50\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x60\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x60\x02\0\0\0\0\0\0" # sh_offset

            b"\1\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0" # missing 1 byte to align 16
            # Data (.text)
            b"\0"
            b".text is here!\0\0"
            # Data (.data)
            b".data is here!\0\0"
            # Data (.rodata)
            b".rodata is here!"
            # Data (.strtab)
            b"\0" # missing 15 bytes to align 16

        )

    def test_symbols_data(self):
        ef = elf_file.OutElfFileRel()
        ef.find_section(".text").data.append(b"\0.text is here!\0\0")
        ef.find_section(".data").data.append(b".data is here!\0\0")
        ef.find_section(".rodata").data.append(b".rodata is here!")

        ef.add_symbol(
            b"foobar",
            ef.find_section(".text"),
            0x1234,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.FUNC,
                c_api.SymbolTableBinding.GLOBAL,
            )
        )
        ef.add_symbol(
            b"",
            ef.find_section(".data"),
            0xf00b,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.OBJECT,
                c_api.SymbolTableBinding.LOCAL,
            )
        )

        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\7\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\x2f\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\x11\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x50\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x60\x02\0\0\0\0\0\0" # sh_offset

            b"\x30\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\1\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x90\x02\0\0\0\0\0\0" # sh_offset

            b"\x08\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0" # missing 1 byte to align 16
            # Data (.text)
            b"\0"
            b".text is here!\0\0"
            # Data (.data)
            b".data is here!\0\0"
            # Data (.rodata)
            b".rodata is here!"

            # Data (.symtab)
            # Note: local objects first
            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\3\0" # st_shndx
            b"\x0b\xf0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\1\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x34\x12\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            # Data (.strtab)
            b"\0foobar\0" # missing 8 bytes to align 16
        )

    def test_rela_data(self):
        ef = elf_file.OutElfFileRel()
        ef.find_section(".text").data.append(b"\0\0\0\0\0\0.text is here!\0\0")
        ef.find_section(".data").data.append(b".data is here!\0\0")
        ef.find_section(".rodata").data.append(b".rodata is here!")

        ef.add_symbol(
            b"foobar1",
            ef.find_section(".text"),
            0x1234,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.FUNC,
                c_api.SymbolTableBinding.GLOBAL,
            )
        )
        s = ef.add_symbol(
            b"",
            ef.find_section(".data"),
            0xf00b,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.OBJECT,
                c_api.SymbolTableBinding.LOCAL,
            )
        )
        ef.add_symbol(
            b"",
            ef.find_section(".text"),
            0xff77,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.OBJECT,
                c_api.SymbolTableBinding.LOCAL,
            )
        )
        pf = ef.add_symbol(
            b"printf",
            c_api.SpecialSections.UNDEF,
            0,
            c_api.SymbolTableInfo(
                c_api.SymbolTableType.FUNC,
                c_api.SymbolTableBinding.GLOBAL,
            )
        )
        s.temp_relocations.append(elf_file.Symbol.TempRel(ef.find_section(".text"), 3, 2))
        pf.temp_relocations.append(elf_file.Symbol.TempRel(ef.find_section(".text"), 4, 5))

        ef.create_rela_section(ef.find_section(".text"))

        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\x08\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\x3a\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x7a\x02\0\0\0\0\0\0" # sh_offset

            b"\x16\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x90\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xa0\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xb0\x02\0\0\0\0\0\0" # sh_offset

            b"\x60\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x10\x03\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rela.text)
            b"\x2f\0\0\0" # sh_name
            b"\4\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x20\x03\0\0\0\0\0\0" # sh_offset

            b"\x30\0\0\0\0\0\0\0" # sh_size
            b"\5\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0."
            b"rela.text\0" # missing 6 to 16
            # Data (.text)
            b"\0\0\0\0\0\0"
            b".text is here!\0\0"
            # Data (.data)
            b".data is here!\0\0"
            # Data (.rodata)
            b".rodata is here!"

            # Data (.symtab)
            # Note: local objects first
            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\3\0" # st_shndx
            b"\x0b\xf0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x77\xff\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            b"\1\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x34\x12\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\x09\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\0\0" # st_shndx
            b"\0\0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            # Data (.strtab)
            b"\0foobar1\0printf\0"

            # Data (.rela.text)
            b"\x05\0\0\0\0\0\0\0" # r_offset
            b"\x0b\0\0\0" # r_info.type
            b"\0\0\0\0" # r_info.sym
            b"\x00\0\0\0\0\0\0\0" # r_addend

            b"\x09\0\0\0\0\0\0\0"
            b"\4\0\0\0" # r_info.type
            b"\3\0\0\0" # r_info.sym
            + bytes(fixint.Int64(-4)) + b""
        )


class TestElfFileFromFile(HexTestCase):
    def test_empty(self):
        ef = ()
        stream = io.BytesIO(
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident
            b"\0\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry
            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\0\0\0\0\0\0\0\0" # e_shoff
            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize
            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\0\0" # e_shnum
            b"\0\0" # e_shstrndx
        )
        ef = elf_file.ElfFile.from_file(stream)

        self.assertEqual(len(ef.sections), 0)
        self.assertEqual(len(ef.programs), 0)
        self.assertEqual(len(ef.data), 0)

        self.assert_bytes_equal(ef.header.e_ident, b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0")
        self.assertEqual(ef.header.e_type, c_api.Type.NONE)
        self.assertEqual(ef.header.e_machine, c_api.Machine.X86_64)
        self.assertEqual(ef.header.e_version, c_api.Version.CURRENT)
        self.assertEqual(ef.header.e_entry, 0)
        self.assertEqual(ef.header.e_phoff, 0)
        self.assertEqual(ef.header.e_shoff, 0)
        self.assertEqual(ef.header.e_flags, 0)
        self.assertEqual(ef.header.e_ehsize, c_api.Elf64_Ehdr.sizeof)
        self.assertEqual(ef.header.e_phentsize, c_api.Elf64_Phdr.sizeof)
        self.assertEqual(ef.header.e_phnum, 0)
        self.assertEqual(ef.header.e_shentsize, c_api.Elf64_Shdr.sizeof)
        self.assertEqual(ef.header.e_shnum, 0)
        self.assertEqual(ef.header.e_shstrndx, 0)

    def test_rel_empty(self):
        stream = io.BytesIO(
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\7\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x00\x02\0\0\0\0\0\0" # sh_offset

            b"\x2f\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x2f\x02\0\0\0\0\0\0" # sh_offset

            b"\1\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text\0.data\0.rodata\0.symtab\0.strtab\0"
            # Data (.strtab)
            b"\0"
        )
        ef = elf_file.ElfFile.from_file(stream)

        self.assert_bytes_equal(ef.header.e_ident, b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0")
        self.assertEqual(ef.header.e_type, c_api.Type.REL)
        self.assertEqual(ef.header.e_machine, c_api.Machine.X86_64)
        self.assertEqual(ef.header.e_version, c_api.Version.CURRENT)
        self.assertEqual(ef.header.e_entry, 0)
        self.assertEqual(ef.header.e_phoff, 0)
        self.assertEqual(ef.header.e_shoff, 64)
        self.assertEqual(ef.header.e_flags, 0)
        self.assertEqual(ef.header.e_ehsize, c_api.Elf64_Ehdr.sizeof)
        self.assertEqual(ef.header.e_phentsize, c_api.Elf64_Phdr.sizeof)
        self.assertEqual(ef.header.e_phnum, 0)
        self.assertEqual(ef.header.e_shentsize, c_api.Elf64_Shdr.sizeof)
        self.assertEqual(ef.header.e_shnum, 7)
        self.assertEqual(ef.header.e_shstrndx, 1)

        self.assertEqual(len(ef.sections), 7)
        self.assertEqual(len(ef.programs), 0)
        self.assertEqual(len(ef.data), 7)

    def test_rel_data(self):
        stream = io.BytesIO(
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x40\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize

            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\x08\0" # e_shnum
            b"\1\0" # e_shstrndx

            # Elf64_Shdr (NULL)
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\x02\0\0\0\0\0\0" # sh_offset

            b"\x3a\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x7a\x02\0\0\0\0\0\0" # sh_offset

            b"\x16\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x90\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xa0\x02\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xb0\x02\0\0\0\0\0\0" # sh_offset

            b"\x60\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x10\x03\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rela.text)
            b"\x2f\0\0\0" # sh_name
            b"\4\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x20\x03\0\0\0\0\0\0" # sh_offset

            b"\x30\0\0\0\0\0\0\0" # sh_size
            b"\5\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Data (.shstrtab)
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0."
            b"rela.text\0" # missing 6 to 16
            # Data (.text)
            b"\0\0\0\0\0\0"
            b".text is here!\0\0"
            # Data (.data)
            b".data is here!\0\0"
            # Data (.rodata)
            b".rodata is here!"

            # Data (.symtab)
            # Note: local objects first
            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\3\0" # st_shndx
            b"\x0b\xf0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x77\xff\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            b"\1\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x34\x12\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\x09\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\0\0" # st_shndx
            b"\0\0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            # Data (.strtab)
            b"\0foobar1\0printf\0"

            # Data (.rela.text)
            b"\x05\0\0\0\0\0\0\0" # r_offset
            b"\x0b\0\0\0" # r_info.type
            b"\0\0\0\0" # r_info.sym
            b"\x00\0\0\0\0\0\0\0" # r_addend

            b"\x09\0\0\0\0\0\0\0"
            b"\4\0\0\0" # r_info.type
            b"\3\0\0\0" # r_info.sym
            + bytes(fixint.Int64(-4)) + b""
        )
        ef = elf_file.ElfFile.from_file(stream)

        self.assert_bytes_equal(ef.header.e_ident, b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0")
        self.assertEqual(ef.header.e_type, c_api.Type.REL)
        self.assertEqual(ef.header.e_machine, c_api.Machine.X86_64)
        self.assertEqual(ef.header.e_version, c_api.Version.CURRENT)
        self.assertEqual(ef.header.e_entry, 0)
        self.assertEqual(ef.header.e_phoff, 0)
        self.assertEqual(ef.header.e_shoff, 64)
        self.assertEqual(ef.header.e_flags, 0)
        self.assertEqual(ef.header.e_ehsize, c_api.Elf64_Ehdr.sizeof)
        self.assertEqual(ef.header.e_phentsize, c_api.Elf64_Phdr.sizeof)
        self.assertEqual(ef.header.e_phnum, 0)
        self.assertEqual(ef.header.e_shentsize, c_api.Elf64_Shdr.sizeof)
        self.assertEqual(ef.header.e_shnum, 8)
        self.assertEqual(ef.header.e_shstrndx, 1)

        self.assertEqual(len(ef.sections), 8)
        self.assertEqual(len(ef.programs), 0)
        self.assertEqual(len(ef.data), 8)

        self.assertEqual(ef.sections[0].header.sh_name, 0)
        self.assertEqual(
            ef.sections[0].header.sh_type, c_api.SectionHeaderType.NULL
        )
        self.assertEqual(ef.sections[0].header.sh_flags, 0)
        self.assertEqual(ef.sections[0].header.sh_addr, 0)
        self.assertEqual(ef.sections[0].header.sh_offset, 0x0240)
        self.assertEqual(ef.sections[0].header.sh_size, 0)
        self.assertEqual(ef.sections[0].header.sh_link, 0)
        self.assertEqual(ef.sections[0].header.sh_info, 0)
        self.assertEqual(ef.sections[0].header.sh_addralign, 0)
        self.assertEqual(ef.sections[0].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".shstrtab"), ef.sections[1])
        self.assert_bytes_equal(
            ef.find_section(".shstrtab").data.data,
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0."
            b"rela.text\0"
        )
        self.assertEqual(ef.sections[1].header.sh_name, 1)
        self.assertEqual(
            ef.sections[1].header.sh_type, c_api.SectionHeaderType.STRTAB
        )
        self.assertEqual(ef.sections[1].header.sh_flags, 0)
        self.assertEqual(ef.sections[1].header.sh_addr, 0)
        self.assertEqual(ef.sections[1].header.sh_offset, 0x0240)
        self.assertEqual(ef.sections[1].header.sh_size, 0x3a)
        self.assertEqual(ef.sections[1].header.sh_link, 0)
        self.assertEqual(ef.sections[1].header.sh_info, 0)
        self.assertEqual(ef.sections[1].header.sh_addralign, 0)
        self.assertEqual(ef.sections[1].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".text"), ef.sections[2])
        self.assert_bytes_equal(
            ef.find_section(".text").data.data,
            b"\0\0\0\0\0\0.text is here!\0\0"
        )
        self.assertEqual(ef.sections[2].header.sh_name, 0xb)
        self.assertEqual(
            ef.sections[2].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[2].header.sh_flags, 6)
        self.assertEqual(ef.sections[2].header.sh_addr, 0)
        self.assertEqual(ef.sections[2].header.sh_offset, 0x027a)
        self.assertEqual(ef.sections[2].header.sh_size, 0x16)
        self.assertEqual(ef.sections[2].header.sh_link, 0)
        self.assertEqual(ef.sections[2].header.sh_info, 0)
        self.assertEqual(ef.sections[2].header.sh_addralign, 0)
        self.assertEqual(ef.sections[2].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".data"), ef.sections[3])
        self.assert_bytes_equal(
            ef.find_section(".data").data.data,
            b".data is here!\0\0"
        )
        self.assertEqual(ef.sections[3].header.sh_name, 0x11)
        self.assertEqual(
            ef.sections[3].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[3].header.sh_flags, 3)
        self.assertEqual(ef.sections[3].header.sh_addr, 0)
        self.assertEqual(ef.sections[3].header.sh_offset, 0x0290)
        self.assertEqual(ef.sections[3].header.sh_size, 0x10)
        self.assertEqual(ef.sections[3].header.sh_link, 0)
        self.assertEqual(ef.sections[3].header.sh_info, 0)
        self.assertEqual(ef.sections[3].header.sh_addralign, 0)
        self.assertEqual(ef.sections[3].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".rodata"), ef.sections[4])
        self.assert_bytes_equal(
            ef.find_section(".data").data.data,
            b".data is here!\0\0"
        )
        self.assertEqual(ef.sections[4].header.sh_name, 0x17)
        self.assertEqual(
            ef.sections[4].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[4].header.sh_flags, 2)
        self.assertEqual(ef.sections[4].header.sh_addr, 0)
        self.assertEqual(ef.sections[4].header.sh_offset, 0x02a0)
        self.assertEqual(ef.sections[4].header.sh_size, 0x10)
        self.assertEqual(ef.sections[4].header.sh_link, 0)
        self.assertEqual(ef.sections[4].header.sh_info, 0)
        self.assertEqual(ef.sections[4].header.sh_addralign, 0)
        self.assertEqual(ef.sections[4].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".symtab"), ef.sections[5])
        self.assertEqual(len(ef.find_section(".symtab").data.data), 4)
        self.assertEqual(ef.sections[5].header.sh_name, 0x1f)
        self.assertEqual(ef.sections[5].header.sh_type, c_api.SectionHeaderType.SYMTAB)
        self.assertEqual(ef.sections[5].header.sh_flags, 0)
        self.assertEqual(ef.sections[5].header.sh_addr, 0)
        self.assertEqual(ef.sections[5].header.sh_offset, 0x02b0)
        self.assertEqual(ef.sections[5].header.sh_size, 0x60)
        self.assertEqual(ef.sections[5].header.sh_link, 6)
        self.assertEqual(ef.sections[5].header.sh_info, 2)
        self.assertEqual(ef.sections[5].header.sh_addralign, 8)
        self.assertEqual(ef.sections[5].header.sh_entsize, 0x18)

        self.assertIs(ef.find_section(".strtab"), ef.sections[6])
        self.assert_bytes_equal(
            ef.find_section(".strtab").data.data,
            b"\0foobar1\0printf\0"
        )
        self.assertEqual(ef.sections[6].header.sh_name, 0x27)
        self.assertEqual(ef.sections[6].header.sh_type, c_api.SectionHeaderType.STRTAB)
        self.assertEqual(ef.sections[6].header.sh_flags, 0)
        self.assertEqual(ef.sections[6].header.sh_addr, 0)
        self.assertEqual(ef.sections[6].header.sh_offset, 0x0310)
        self.assertEqual(ef.sections[6].header.sh_size, 0x10)
        self.assertEqual(ef.sections[6].header.sh_link, 0)
        self.assertEqual(ef.sections[6].header.sh_info, 0)
        self.assertEqual(ef.sections[6].header.sh_addralign, 0)
        self.assertEqual(ef.sections[6].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".rela.text"), ef.sections[7])
        self.assertEqual(len(ef.find_section(".rela.text").data.data), 2)
        self.assertEqual(ef.sections[7].header.sh_name, 0x2f)
        self.assertEqual(ef.sections[7].header.sh_type, c_api.SectionHeaderType.RELA)
        self.assertEqual(ef.sections[7].header.sh_flags, 2)
        self.assertEqual(ef.sections[7].header.sh_addr, 0)
        self.assertEqual(ef.sections[7].header.sh_offset, 0x0320)
        self.assertEqual(ef.sections[7].header.sh_size, 0x30)
        self.assertEqual(ef.sections[7].header.sh_link, 5)
        self.assertEqual(ef.sections[7].header.sh_info, 2)
        self.assertEqual(ef.sections[7].header.sh_addralign, 0)
        self.assertEqual(ef.sections[7].header.sh_entsize, 0x18)

        self.assertEqual(ef.symtab.data.data[0].st_name, 0)
        self.assertEqual(ef.symtab.data.data[0].st_info, 1)
        self.assertEqual(ef.symtab.data.data[0].st_other, 0)
        self.assertEqual(ef.symtab.data.data[0].st_shndx, 3)
        self.assertEqual(ef.symtab.data.data[0].st_value, 0xf00b)
        self.assertEqual(ef.symtab.data.data[0].st_size, 0)

        self.assertEqual(ef.symtab.data.data[1].st_name, 0)
        self.assertEqual(ef.symtab.data.data[1].st_info, 1)
        self.assertEqual(ef.symtab.data.data[1].st_other, 0)
        self.assertEqual(ef.symtab.data.data[1].st_shndx, 2)
        self.assertEqual(ef.symtab.data.data[1].st_value, 0xff77)
        self.assertEqual(ef.symtab.data.data[1].st_size, 0)

        self.assertEqual(ef.symtab.data.data[2].st_name, 1)
        self.assertEqual(ef.symtab.data.data[2].st_info, 0x12)
        self.assertEqual(ef.symtab.data.data[2].st_other, 0)
        self.assertEqual(ef.symtab.data.data[2].st_shndx, 2)
        self.assertEqual(ef.symtab.data.data[2].st_value, 0x1234)
        self.assertEqual(ef.symtab.data.data[2].st_size, 0)

        self.assertEqual(ef.symtab.data.data[3].st_name, 0x9)
        self.assertEqual(ef.symtab.data.data[3].st_info, 0x12)
        self.assertEqual(ef.symtab.data.data[3].st_other, 0)
        self.assertEqual(ef.symtab.data.data[3].st_shndx, 0)
        self.assertEqual(ef.symtab.data.data[3].st_value, 0)
        self.assertEqual(ef.symtab.data.data[3].st_size, 0)


        relatext = ef.find_section(".rela.text")
        self.assertEqual(relatext.data.data[0].r_offset, 0x5)
        self.assertEqual(relatext.data.data[0].r_info.type, 0xb)
        self.assertEqual(relatext.data.data[0].r_info.sym, 0x0)
        self.assertEqual(relatext.data.data[0].r_addend, 0x0)

        self.assertEqual(relatext.data.data[1].r_offset, 0x9)
        self.assertEqual(relatext.data.data[1].r_info.type, 0x4)
        self.assertEqual(relatext.data.data[1].r_info.sym, 0x3)
        self.assertEqual(relatext.data.data[1].r_addend, -4)

    def test_rel_data_headafterdata(self):
        stream = io.BytesIO(
            #00
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident
            #10
            b"\1\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry
            #20
            b"\0\0\0\0\0\0\0\0" # e_phoff
            b"\x50\x01\0\0\0\0\0\0" # e_shoff
            #30
            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize
            b"\0\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\x08\0" # e_shnum
            b"\1\0" # e_shstrndx
            #40
            # Data (.shstrtab)
            b"\0.shstrtab\0.text"
            #50
            b"\0.data\0.rodata\0."
            #60
            b"symtab\0.strtab\0."
            #70
            b"rela.text\0" # missing 6 to 16
            # Data (.text)
            b"\0\0\0\0\0\0"
            #80
            b".text is here!\0\0"
            # Data (.data)
            #90
            b".data is here!\0\0"
            # Data (.rodata)
            #a0
            b".rodata is here!"

            # Data (.symtab)
            # Note: local objects first
            #b0
            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\3\0" # st_shndx
            b"\x0b\xf0\0\0\0\0\0\0" # st_value
            #c0
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\0\0\0\0" # st_name
            b"\x01" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            #d0
            b"\x77\xff\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size
            #e0
            b"\1\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\2\0" # st_shndx
            b"\x34\x12\0\0\0\0\0\0" # st_value
            #f0
            b"\0\0\0\0\0\0\0\0" # st_size
            # missing 8 bytes to align 16

            b"\x09\0\0\0" # st_name
            b"\x12" # st_info
            b"\0" # st_other
            b"\0\0" # st_shndx
            #100
            b"\0\0\0\0\0\0\0\0" # st_value
            b"\0\0\0\0\0\0\0\0" # st_size

            # Data (.strtab)
            #110
            b"\0foobar1\0printf\0"

            # Data (.rela.text)
            #120
            b"\x05\0\0\0\0\0\0\0" # r_offset
            b"\x0b\0\0\0" # r_info.type
            b"\0\0\0\0" # r_info.sym
            #130
            b"\x00\0\0\0\0\0\0\0" # r_addend

            b"\x09\0\0\0\0\0\0\0"
            #140
            b"\4\0\0\0" # r_info.type
            b"\3\0\0\0" # r_info.sym
            + bytes(fixint.Int64(-4)) + b""

            # Elf64_Shdr (NULL)
            #150
            b"\0\0\0\0" # sh_name
            b"\0\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\0\0\0\0\0\0\0" # sh_offset

            b"\0\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.shstrtab)
            b"\1\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x40\0\0\0\0\0\0\0" # sh_offset

            b"\x3a\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.text)
            b"\x0b\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\6\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x7a\0\0\0\0\0\0\0" # sh_offset

            b"\x16\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.data)
            b"\x11\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\3\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x90\0\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rodata)
            b"\x17\0\0\0" # sh_name
            b"\1\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xa0\x00\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.symtab)
            b"\x1f\0\0\0" # sh_name
            b"\2\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\xb0\x00\0\0\0\0\0\0" # sh_offset

            b"\x60\0\0\0\0\0\0\0" # sh_size
            b"\6\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\x08\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.strtab)
            b"\x27\0\0\0" # sh_name
            b"\3\0\0\0" # sh_type
            b"\0\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x10\x01\0\0\0\0\0\0" # sh_offset

            b"\x10\0\0\0\0\0\0\0" # sh_size
            b"\0\0\0\0" # sh_link
            b"\0\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\0\0\0\0\0\0\0\0" # sh_entsize

            # Elf64_Shdr (.rela.text)
            b"\x2f\0\0\0" # sh_name
            b"\4\0\0\0" # sh_type
            b"\2\0\0\0\0\0\0\0" # sh_flags

            b"\0\0\0\0\0\0\0\0" # sh_addr
            b"\x20\x01\0\0\0\0\0\0" # sh_offset

            b"\x30\0\0\0\0\0\0\0" # sh_size
            b"\5\0\0\0" # sh_link
            b"\2\0\0\0" # sh_info

            b"\0\0\0\0\0\0\0\0" # sh_addralign
            b"\x18\0\0\0\0\0\0\0" # sh_entsize
        )
        ef = elf_file.ElfFile.from_file(stream)

        self.assert_bytes_equal(ef.header.e_ident, b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0")
        self.assertEqual(ef.header.e_type, c_api.Type.REL)
        self.assertEqual(ef.header.e_machine, c_api.Machine.X86_64)
        self.assertEqual(ef.header.e_version, c_api.Version.CURRENT)
        self.assertEqual(ef.header.e_entry, 0)
        self.assertEqual(ef.header.e_phoff, 0)
        self.assertEqual(ef.header.e_shoff, 0x150)
        self.assertEqual(ef.header.e_flags, 0)
        self.assertEqual(ef.header.e_ehsize, c_api.Elf64_Ehdr.sizeof)
        self.assertEqual(ef.header.e_phentsize, c_api.Elf64_Phdr.sizeof)
        self.assertEqual(ef.header.e_phnum, 0)
        self.assertEqual(ef.header.e_shentsize, c_api.Elf64_Shdr.sizeof)
        self.assertEqual(ef.header.e_shnum, 8)
        self.assertEqual(ef.header.e_shstrndx, 1)

        self.assertEqual(len(ef.sections), 8)
        self.assertEqual(len(ef.programs), 0)
        self.assertEqual(len(ef.data), 8)

        self.assertEqual(ef.sections[0].header.sh_name, 0)
        self.assertEqual(
            ef.sections[0].header.sh_type, c_api.SectionHeaderType.NULL
        )
        self.assertEqual(ef.sections[0].header.sh_flags, 0)
        self.assertEqual(ef.sections[0].header.sh_addr, 0)
        self.assertEqual(ef.sections[0].header.sh_offset, 0x40)
        self.assertEqual(ef.sections[0].header.sh_size, 0)
        self.assertEqual(ef.sections[0].header.sh_link, 0)
        self.assertEqual(ef.sections[0].header.sh_info, 0)
        self.assertEqual(ef.sections[0].header.sh_addralign, 0)
        self.assertEqual(ef.sections[0].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".shstrtab"), ef.sections[1])
        self.assert_bytes_equal(
            ef.find_section(".shstrtab").data.data,
            b"\0.shstrtab\0.text"
            b"\0.data\0.rodata\0."
            b"symtab\0.strtab\0."
            b"rela.text\0"
        )
        self.assertEqual(ef.sections[1].header.sh_name, 1)
        self.assertEqual(
            ef.sections[1].header.sh_type, c_api.SectionHeaderType.STRTAB
        )
        self.assertEqual(ef.sections[1].header.sh_flags, 0)
        self.assertEqual(ef.sections[1].header.sh_addr, 0)
        self.assertEqual(ef.sections[1].header.sh_offset, 0x40)
        self.assertEqual(ef.sections[1].header.sh_size, 0x3a)
        self.assertEqual(ef.sections[1].header.sh_link, 0)
        self.assertEqual(ef.sections[1].header.sh_info, 0)
        self.assertEqual(ef.sections[1].header.sh_addralign, 0)
        self.assertEqual(ef.sections[1].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".text"), ef.sections[2])
        self.assert_bytes_equal(
            ef.find_section(".text").data.data,
            b"\0\0\0\0\0\0.text is here!\0\0"
        )
        self.assertEqual(ef.sections[2].header.sh_name, 0xb)
        self.assertEqual(
            ef.sections[2].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[2].header.sh_flags, 6)
        self.assertEqual(ef.sections[2].header.sh_addr, 0)
        self.assertEqual(ef.sections[2].header.sh_offset, 0x7a)
        self.assertEqual(ef.sections[2].header.sh_size, 0x16)
        self.assertEqual(ef.sections[2].header.sh_link, 0)
        self.assertEqual(ef.sections[2].header.sh_info, 0)
        self.assertEqual(ef.sections[2].header.sh_addralign, 0)
        self.assertEqual(ef.sections[2].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".data"), ef.sections[3])
        self.assert_bytes_equal(
            ef.find_section(".data").data.data,
            b".data is here!\0\0"
        )
        self.assertEqual(ef.sections[3].header.sh_name, 0x11)
        self.assertEqual(
            ef.sections[3].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[3].header.sh_flags, 3)
        self.assertEqual(ef.sections[3].header.sh_addr, 0)
        self.assertEqual(ef.sections[3].header.sh_offset, 0x90)
        self.assertEqual(ef.sections[3].header.sh_size, 0x10)
        self.assertEqual(ef.sections[3].header.sh_link, 0)
        self.assertEqual(ef.sections[3].header.sh_info, 0)
        self.assertEqual(ef.sections[3].header.sh_addralign, 0)
        self.assertEqual(ef.sections[3].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".rodata"), ef.sections[4])
        self.assert_bytes_equal(
            ef.find_section(".data").data.data,
            b".data is here!\0\0"
        )
        self.assertEqual(ef.sections[4].header.sh_name, 0x17)
        self.assertEqual(
            ef.sections[4].header.sh_type, c_api.SectionHeaderType.PROGBITS
        )
        self.assertEqual(ef.sections[4].header.sh_flags, 2)
        self.assertEqual(ef.sections[4].header.sh_addr, 0)
        self.assertEqual(ef.sections[4].header.sh_offset, 0xa0)
        self.assertEqual(ef.sections[4].header.sh_size, 0x10)
        self.assertEqual(ef.sections[4].header.sh_link, 0)
        self.assertEqual(ef.sections[4].header.sh_info, 0)
        self.assertEqual(ef.sections[4].header.sh_addralign, 0)
        self.assertEqual(ef.sections[4].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".symtab"), ef.sections[5])
        self.assertEqual(len(ef.find_section(".symtab").data.data), 4)
        self.assertEqual(ef.sections[5].header.sh_name, 0x1f)
        self.assertEqual(ef.sections[5].header.sh_type, c_api.SectionHeaderType.SYMTAB)
        self.assertEqual(ef.sections[5].header.sh_flags, 0)
        self.assertEqual(ef.sections[5].header.sh_addr, 0)
        self.assertEqual(ef.sections[5].header.sh_offset, 0xb0)
        self.assertEqual(ef.sections[5].header.sh_size, 0x60)
        self.assertEqual(ef.sections[5].header.sh_link, 6)
        self.assertEqual(ef.sections[5].header.sh_info, 2)
        self.assertEqual(ef.sections[5].header.sh_addralign, 8)
        self.assertEqual(ef.sections[5].header.sh_entsize, 0x18)

        self.assertIs(ef.find_section(".strtab"), ef.sections[6])
        self.assert_bytes_equal(
            ef.find_section(".strtab").data.data,
            b"\0foobar1\0printf\0"
        )
        self.assertEqual(ef.sections[6].header.sh_name, 0x27)
        self.assertEqual(ef.sections[6].header.sh_type, c_api.SectionHeaderType.STRTAB)
        self.assertEqual(ef.sections[6].header.sh_flags, 0)
        self.assertEqual(ef.sections[6].header.sh_addr, 0)
        self.assertEqual(ef.sections[6].header.sh_offset, 0x0110)
        self.assertEqual(ef.sections[6].header.sh_size, 0x10)
        self.assertEqual(ef.sections[6].header.sh_link, 0)
        self.assertEqual(ef.sections[6].header.sh_info, 0)
        self.assertEqual(ef.sections[6].header.sh_addralign, 0)
        self.assertEqual(ef.sections[6].header.sh_entsize, 0)

        self.assertIs(ef.find_section(".rela.text"), ef.sections[7])
        self.assertEqual(len(ef.find_section(".rela.text").data.data), 2)
        self.assertEqual(ef.sections[7].header.sh_name, 0x2f)
        self.assertEqual(ef.sections[7].header.sh_type, c_api.SectionHeaderType.RELA)
        self.assertEqual(ef.sections[7].header.sh_flags, 2)
        self.assertEqual(ef.sections[7].header.sh_addr, 0)
        self.assertEqual(ef.sections[7].header.sh_offset, 0x0120)
        self.assertEqual(ef.sections[7].header.sh_size, 0x30)
        self.assertEqual(ef.sections[7].header.sh_link, 5)
        self.assertEqual(ef.sections[7].header.sh_info, 2)
        self.assertEqual(ef.sections[7].header.sh_addralign, 0)
        self.assertEqual(ef.sections[7].header.sh_entsize, 0x18)

        self.assertEqual(ef.symtab.data.data[0].st_name, 0)
        self.assertEqual(ef.symtab.data.data[0].st_info, 1)
        self.assertEqual(ef.symtab.data.data[0].st_other, 0)
        self.assertEqual(ef.symtab.data.data[0].st_shndx, 3)
        self.assertEqual(ef.symtab.data.data[0].st_value, 0xf00b)
        self.assertEqual(ef.symtab.data.data[0].st_size, 0)

        self.assertEqual(ef.symtab.data.data[1].st_name, 0)
        self.assertEqual(ef.symtab.data.data[1].st_info, 1)
        self.assertEqual(ef.symtab.data.data[1].st_other, 0)
        self.assertEqual(ef.symtab.data.data[1].st_shndx, 2)
        self.assertEqual(ef.symtab.data.data[1].st_value, 0xff77)
        self.assertEqual(ef.symtab.data.data[1].st_size, 0)

        self.assertEqual(ef.symtab.data.data[2].st_name, 1)
        self.assertEqual(ef.symtab.data.data[2].st_info, 0x12)
        self.assertEqual(ef.symtab.data.data[2].st_other, 0)
        self.assertEqual(ef.symtab.data.data[2].st_shndx, 2)
        self.assertEqual(ef.symtab.data.data[2].st_value, 0x1234)
        self.assertEqual(ef.symtab.data.data[2].st_size, 0)

        self.assertEqual(ef.symtab.data.data[3].st_name, 0x9)
        self.assertEqual(ef.symtab.data.data[3].st_info, 0x12)
        self.assertEqual(ef.symtab.data.data[3].st_other, 0)
        self.assertEqual(ef.symtab.data.data[3].st_shndx, 0)
        self.assertEqual(ef.symtab.data.data[3].st_value, 0)
        self.assertEqual(ef.symtab.data.data[3].st_size, 0)


        relatext = ef.find_section(".rela.text")
        self.assertEqual(relatext.data.data[0].r_offset, 0x5)
        self.assertEqual(relatext.data.data[0].r_info.type, 0xb)
        self.assertEqual(relatext.data.data[0].r_info.sym, 0x0)
        self.assertEqual(relatext.data.data[0].r_addend, 0x0)

        self.assertEqual(relatext.data.data[1].r_offset, 0x9)
        self.assertEqual(relatext.data.data[1].r_info.type, 0x4)
        self.assertEqual(relatext.data.data[1].r_info.sym, 0x3)
        self.assertEqual(relatext.data.data[1].r_addend, -4)


class TestOutElfFileExec(HexTestCase):
    def test_empty(self):
        ef = elf_file.OutElfFileExec()
        ef.set_interpreter(linker.Linker().interpreter)
        stream = io.BytesIO()
        ef.write(stream)

        self.assert_bytes_equal(
            stream.getvalue(),
            # Elf64_Ehdr
            b"\x7fELF\2\1\1\3\0\0\0\0\0\0\0\0" # e_ident

            b"\2\0" # e_type
            b"\x3e\0" # e_machine
            b"\1\0\0\0" # e_version
            b"\0\0\0\0\0\0\0\0" # e_entry

            b"\x40\0\0\0\0\0\0\0" # e_phoff
            b"\0\0\0\0\0\0\0\0" # e_shoff

            b"\0\0\0\0" # e_flags
            b"\x40\0" # e_ehsize
            b"\x38\0" # e_phentsize
            b"\2\0" # e_phnum
            b"\x40\0" # e_shentsize
            b"\0\0" # e_shnum
            b"\0\0" # e_shstrndx

            # Elf64_Phdr (PHDR)
            b"\6\0\0\0" # p_type
            b"\4\0\0\0" # p_flags
            b"\x40\0\0\0\0\0\0\0" # p_offset

            b"\x40\x00\x40\0\0\0\0\0" # p_vaddr
            b"\x40\x00\x40\0\0\0\0\0" # p_paddr

            b"\x70\0\0\0\0\0\0\0" # p_filesz
            b"\x70\0\0\0\0\0\0\0" # p_memsz

            b"\0\0\0\0\0\0\0\0" # p_align
            # missing 8 bytes to align 16

            # Elf64_Phdr (INTERP)
            b"\3\0\0\0" # p_type
            b"\4\0\0\0" # p_flags
            b"\xb0\0\0\0\0\0\0\0" # p_offset

            b"\xb0\x00\x40\0\0\0\0\0" # p_vaddr
            b"\xb0\x00\x40\0\0\0\0\0" # p_paddr

            b"\x1c\0\0\0\0\0\0\0" # p_filesz
            b"\x1c\0\0\0\0\0\0\0" # p_memsz

            b"\0\0\0\0\0\0\0\0" # p_align

            # Data
            b"/lib64/ld-linux-"

            b"x86-64.so.2\0"
            # missing 4 bytes to align 16
        )
