import unittest

from pycomp.ast_nodes import AstNode

class TestParser(unittest.TestCase):
    maxDiff = None

    def assert_node(self, a, b):
        self.assertIsInstance(a, AstNode)
        self.assertIsInstance(b, AstNode)
        stra = self.node2str(a).splitlines()
        strb = self.node2str(b).splitlines()
        maxa = max(map(len, stra))
        maxb = max(map(len, strb))

        ok = True
        msg = ""
        for i in range(max(len(stra), len(strb))):
            la = stra[i] if i < len(stra) else ""
            lb = strb[i] if i < len(strb) else ""

            if la != lb:
                ok = False
                prefix = ">\x1b[31m"
                infix = "\x1b[m#\x1b[31m"
                suffix = "\x1b[m<\n"
            else:
                prefix = " "
                infix = " "
                suffix = "\n"

            msg += prefix + la.ljust(maxa) + infix + lb.ljust(maxb) + suffix

        if not ok:
            self.fail(
                "Nodes not equal:\n%s" % msg
            )

    def tostr(self, val, ind=""):
        if isinstance(val, AstNode):
            return self.node2str(val, ind)
        elif isinstance(val, list):
            ind2 = ind + "    "
            return "[\n%s%s\n]" % (
                "\n".join(
                    "%s%s" % (ind2, self.tostr(item, ind2))
                    for item in val
                ),
                ind
            )
        return str(val)

    def node2str(self, node, ind=""):
        result = "%s [\n" % (node.__class__.__name__)
        attrs = []
        ind2 = ind + "    "
        for nam, val in sorted(vars(node).items()):
            if isinstance(val, AstNode):
                attrs.append((nam, val))
            else:
                result += "%s%s: %s\n" % (ind2, nam, self.tostr(val, ind2))
        for nam, val in attrs:
            result += "%s%s: %s" % (ind2, nam, self.node2str(val, ind2))
        result += "%s]\n" % ind
        return result
