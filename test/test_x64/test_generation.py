import io
import os
import tempfile
import unittest
import subprocess
import contextlib

from pycomp.back.asm_flavours.gas import Gas
from pycomp.back.asm_flavours.nasm import Nasm
from pycomp.back.elf.elf_file import ElfFile
from pycomp.back.asm import Operation, Label, IntLiteral, DerefRegister, Address
from pycomp.back.arch.x86_64 import x86_64


class AssembleTestCase(unittest.TestCase):
    arch = x86_64
    _default = object()
    default_flavour = Gas

    int64 = IntLiteral(8, 0x1122334455667788)
    int32 = IntLiteral(4, 0x11223344)
    int16 = IntLiteral(2, 0x1122)
    int8 = IntLiteral(1, 0x11)

    def register(self, name):
        return self.arch.register_class.from_name(name)

    def _flavour_gas(self, stdin, stdout):
        pipe = subprocess.Popen(
            ["gcc", "-no-pie", "-c", "-x", "assembler", "/dev/stdin", "-o", stdout.name],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        formatter = Gas.formatter(stdin)
        return pipe, formatter

    def _flavour_nasm(self, stdin, stdout):
        pipe = subprocess.Popen(
            ["nasm", "-f", "elf64", stdin.name, "-o", stdout.name],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        formatter = Nasm.formatter(stdin)
        return pipe, formatter

    @contextlib.contextmanager
    def asm_flavour(self, flavour):
        fl = self.default_flavour
        try:
            self.default_flavour = flavour
            yield
        finally:
            self.default_flavour = fl

    def external_assembler(self, operations, flavour=_default):
        stdout = tempfile.NamedTemporaryFile("wb", suffix=".o", delete=False)
        stdin = tempfile.NamedTemporaryFile("w", suffix=".s", delete=False)

        if flavour is self._default:
            flavour = self.default_flavour

        if flavour == Gas:
            pipe, formatter = self._flavour_gas(stdin, stdout)
        else:
            pipe, formatter = self._flavour_nasm(stdin, stdout)

        labels = set()
        for instr in operations:
            formatter.print_op(instr)
            if isinstance(instr.op1, Label):
                labels.add(instr.op1.name)
            elif isinstance(instr.op1, Address) and isinstance(instr.op1.child, Label):
                labels.add(instr.op1.child.name)

            if isinstance(instr.op2, Label):
                labels.add(instr.op2.name)
            elif isinstance(instr.op2, Address) and isinstance(instr.op2.child, Label):
                labels.add(instr.op2.child.name)

        for label in labels:
            formatter.define_extern(label)

        stdin.close()
        with open(stdin.name, "r") as stdin:
            pipe.communicate(stdin.read().encode("utf-8"))
        if pipe.returncode != 0:
            raise Exception(
                "Could not call the external assembler on %s" %
                "".join(map(str, operations))
            )

        stdout.close()
        with open(stdout.name, "rb") as stdout:
            elf = ElfFile.from_file(stdout)

        os.unlink(stdout.name)
        os.unlink(stdin.name)

        return elf.find_section(".text").data

    def fmt_chunk(self, chunk):
        msg = ""
        for b in chunk:
            msg += ("%02x" % b).upper().center(8)+" "
        msg += "\n"

        for b in chunk:
            b2 = bin(b)[2:]
            msg += b2.rjust(8, '0')+" "
        msg += "\n"
        return msg

    def assert_assembles(self, *args, suf="", flavour=_default, **kwargs):
        op = Operation(*args, **kwargs)

        old_name = op.mnem
        if flavour is self._default or flavour is Gas:
            op.mnem += suf
        exas = self.external_assembler([op], flavour).data
        op.mnem = old_name

        intas = bytes(self.arch.opcode_table.instruction(op))

        if intas != exas:
            wrong = set()
            for i, (h, e) in enumerate(zip(intas, exas)):
                if h != e:
                    wrong.add(i)

            msg = "Wrong generation: %s -- %s\n" % (op, self.arch.opcode_table.descriptor(op))
            if wrong:
                msg += "Bytes %s mismatch" % " ".join(map(str, wrong))
            else:
                msg += "Length mismatch"
            msg += "\nExpected / Assembled:\n" + self.fmt_chunk(exas) + self.fmt_chunk(intas)
            self.fail(msg)


class TestMov(AssembleTestCase):
    def test_r64_r64(self):
        self.assert_assembles("mov", self.register("rax"), self.register("rbx"))
        self.assert_assembles("mov", self.register("rsi"), self.register("r10"))
        self.assert_assembles("mov", self.register("r9"), self.register("r10"))
        self.assert_assembles("mov", self.register("r9"), self.register("rbx"))

    def test_r32_r32(self):
        self.assert_assembles("mov", self.register("eax"), self.register("ebx"))
        self.assert_assembles("mov", self.register("esi"), self.register("r10d"))
        self.assert_assembles("mov", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("mov", self.register("r9d"), self.register("ebx"))

    def test_r16_r16(self):
        self.assert_assembles("mov", self.register("ax"), self.register("bx"))
        self.assert_assembles("mov", self.register("si"), self.register("r10w"))
        self.assert_assembles("mov", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("mov", self.register("r9w"), self.register("bx"))

    def test_r8_r8(self):
        self.assert_assembles("mov", self.register("al"), self.register("bl"))
        self.assert_assembles("mov", self.register("sil"), self.register("r10b"))
        self.assert_assembles("mov", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("mov", self.register("r9b"), self.register("bl"))
        self.assert_assembles("mov", self.register("ah"), self.register("bh"))

    def test_r64_imm64(self):
        self.assert_assembles("mov", self.register("rax"), self.int64)
        self.assert_assembles("mov", self.register("rsi"), self.int64)
        self.assert_assembles("mov", self.register("r9"), self.int64)

    def test_r64_imm32(self):
        self.assert_assembles("mov", self.register("rax"), Label(4, "foo"))
        self.assert_assembles("mov", self.register("rax"), self.int32)
        self.assert_assembles("mov", self.register("rsi"), self.int32)
        self.assert_assembles("mov", self.register("r9"), self.int32)

    def test_r32_imm32(self):
        self.assert_assembles("mov", self.register("eax"), self.int32)
        self.assert_assembles("mov", self.register("esi"), self.int32)
        self.assert_assembles("mov", self.register("r9d"), self.int32)

    def test_r16_imm16(self):
        self.assert_assembles("mov", self.register("ax"), self.int16)
        self.assert_assembles("mov", self.register("si"), self.int16)
        self.assert_assembles("mov", self.register("r9w"), self.int16)

    def test_m64_imm32_displacements(self):
        self.assert_assembles(
            "mov", DerefRegister(8, self.register("rax"), 0), self.int32,
            suf="q"
        )
        self.assert_assembles(
            "mov", DerefRegister(8, self.register("rax"), 0x11), self.int32,
            suf="q"
        )
        self.assert_assembles(
            "mov", DerefRegister(8, self.register("rax"), 0x1122), self.int32,
            suf="q"
        )
        self.assert_assembles(
            "mov", DerefRegister(8, self.register("rax"), 0x11223344), self.int32,
            suf="q"
        )

    def test_m64_imm32(self):
        self.assert_assembles("mov", DerefRegister(8, self.register("rax"), 2), self.int32, suf="q")
        self.assert_assembles("mov", DerefRegister(8, self.register("rsi"), 2), self.int32, suf="q")
        self.assert_assembles("mov", DerefRegister(8, self.register("r9"), 2), self.int32, suf="q")

    def test_m32_imm32(self):
        self.assert_assembles("mov", DerefRegister(4, self.register("eax"), 2), self.int32, suf="l")
        self.assert_assembles("mov", DerefRegister(4, self.register("esi"), 2), self.int32, suf="l")
        self.assert_assembles("mov", DerefRegister(4, self.register("r9d"), 2), self.int32, suf="l")

    def test_m16_imm16(self):
        self.assert_assembles("mov", DerefRegister(2, self.register("rax"), 2), self.int16, suf="w")
        self.assert_assembles("mov", DerefRegister(2, self.register("rsi"), 2), self.int16, suf="w")
        self.assert_assembles("mov", DerefRegister(2, self.register("r9"), 2), self.int16, suf="w")

    def test_r64_m64(self):
        self.assert_assembles(
            "mov", self.register("rax"), DerefRegister(8, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "mov", self.register("rsi"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9"), DerefRegister(8, self.register("rbx"), 2)
        )

    def test_r32_m32(self):
        self.assert_assembles(
            "mov", self.register("eax"), DerefRegister(4, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "mov", self.register("esi"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9d"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9d"), DerefRegister(4, self.register("ebx"), 2)
        )

    def test_r16_m16(self):
        self.assert_assembles(
            "mov", self.register("ax"), DerefRegister(2, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "mov", self.register("si"), DerefRegister(2, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9w"), DerefRegister(2, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9w"), DerefRegister(2, self.register("ebx"), 2)
        )

    def test_r8_m8(self):
        self.assert_assembles(
            "mov", self.register("al"), DerefRegister(1, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "mov", self.register("sil"), DerefRegister(1, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9b"), DerefRegister(1, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "mov", self.register("r9b"), DerefRegister(1, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "mov", self.register("ah"), DerefRegister(1, self.register("ebx"), 2)
        )


class TestJumps(AssembleTestCase):
    def test_ret(self):
        self.assert_assembles("ret")

    def test_call_imm32(self):
        # gcc/gas for some reason creates relocations for literals in jumps
        with self.asm_flavour(Nasm):
            self.assert_assembles("call", Address(Label(4, "foo")))
            self.assert_assembles("call", Address(IntLiteral(4, 0x11223344)))

    def test_jmp_imm32(self):
        with self.asm_flavour(Nasm):
            self.assert_assembles("jmp", Address(Label(4, "foo")))
            self.assert_assembles("jmp", Address(IntLiteral(4, 0x11223344)))


class TestStack(AssembleTestCase):
    def test_push_r64(self):
        self.assert_assembles("push", self.register("rax"))
        self.assert_assembles("push", self.register("rsi"))
        self.assert_assembles("push", self.register("r9"))

    def test_push_r16(self):
        self.assert_assembles("push", self.register("ax"))
        self.assert_assembles("push", self.register("si"))
        self.assert_assembles("push", self.register("r9w"))

    def test_push_imm(self):
        self.assert_assembles("push", self.int32, suf="q")
        self.assert_assembles("push", self.int16, suf="w")

    def test_push_r64(self):
        self.assert_assembles("pop", self.register("rax"))
        self.assert_assembles("pop", self.register("rsi"))
        self.assert_assembles("pop", self.register("r9"))

    def test_push_r16(self):
        self.assert_assembles("pop", self.register("ax"))
        self.assert_assembles("pop", self.register("si"))
        self.assert_assembles("pop", self.register("r9w"))


class TestAdd(AssembleTestCase):
    def test_r8_r8(self):
        self.assert_assembles("add", self.register("al"), self.register("bl"))
        self.assert_assembles("add", self.register("sil"), self.register("r10b"))
        self.assert_assembles("add", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("add", self.register("r9b"), self.register("bl"))
        self.assert_assembles("add", self.register("ah"), self.register("bh"))

    def test_m8_r8(self):
        self.assert_assembles(
            "add", DerefRegister(1, self.register("rax"), 0), self.register("bl"),
            suf="b"
        )
        self.assert_assembles(
            "add", DerefRegister(1, self.register("rsi"), 0x1122), self.register("r10b"),
            suf="b"
        )
        self.assert_assembles(
            "add", DerefRegister(1, self.register("r9"), 0x1122), self.register("r10b"),
            suf="b"
        )
        self.assert_assembles(
            "add", DerefRegister(1, self.register("r9"), 0x11223344), self.register("bl"),
            suf="b"
        )
        self.assert_assembles(
            "add", DerefRegister(1, self.register("rax"), 0x11223344), self.register("bh"),
            suf="b"
        )

    def test_r8_imm8(self):
        self.assert_assembles("add", self.register("al"), self.int8)
        self.assert_assembles("add", self.register("bl"), self.int8)
        self.assert_assembles("add", self.register("sil"), self.int8)
        self.assert_assembles("add", self.register("r9b"), self.int8)
        self.assert_assembles("add", self.register("ah"), self.int8)

    def test_r64_imm32(self):
        self.assert_assembles("add", self.register("rax"), self.int32)
        self.assert_assembles("add", self.register("rbx"), self.int32)
        self.assert_assembles("add", self.register("rsi"), self.int32)
        self.assert_assembles("add", self.register("r9"), self.int32)

    def test_r64_imm8(self):
        self.assert_assembles("add", self.register("rax"), self.int8)
        self.assert_assembles("add", self.register("rbx"), self.int8)
        self.assert_assembles("add", self.register("rsi"), self.int8)
        self.assert_assembles("add", self.register("r9"), self.int8)

    def test_r16_r16(self):
        self.assert_assembles("add", self.register("ax"), self.register("bx"))
        self.assert_assembles("add", self.register("si"), self.register("r10w"))
        self.assert_assembles("add", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("add", self.register("r9w"), self.register("bx"))

    def test_r32_r32(self):
        self.assert_assembles("add", self.register("eax"), self.register("ebx"))
        self.assert_assembles("add", self.register("esi"), self.register("r10d"))
        self.assert_assembles("add", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("add", self.register("r9d"), self.register("ebx"))

    def test_r64_r64(self):
        self.assert_assembles("add", self.register("rax"), self.register("rbx"))
        self.assert_assembles("add", self.register("rsi"), self.register("r10"))
        self.assert_assembles("add", self.register("r9"), self.register("r10"))
        self.assert_assembles("add", self.register("r9"), self.register("rbx"))


class TestImul(AssembleTestCase):
    def test_r16_r16(self):
        self.assert_assembles("imul", self.register("ax"), self.register("bx"))
        self.assert_assembles("imul", self.register("si"), self.register("r10w"))
        self.assert_assembles("imul", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("imul", self.register("r9w"), self.register("bx"))

    def test_r32_r32(self):
        self.assert_assembles("imul", self.register("eax"), self.register("ebx"))
        self.assert_assembles("imul", self.register("esi"), self.register("r10d"))
        self.assert_assembles("imul", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("imul", self.register("r9d"), self.register("ebx"))

    def test_r64_r64(self):
        self.assert_assembles("imul", self.register("rax"), self.register("rbx"))
        self.assert_assembles("imul", self.register("rsi"), self.register("r10"))
        self.assert_assembles("imul", self.register("r9"), self.register("r10"))
        self.assert_assembles("imul", self.register("r9"), self.register("rbx"))

    def test_r16_m16(self):
        self.assert_assembles(
            "imul", self.register("ax"), DerefRegister(2, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "imul", self.register("si"), DerefRegister(2, self.register("r10"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9w"), DerefRegister(2, self.register("r10"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9w"), DerefRegister(2, self.register("rbx"), 2)
        )

    def test_r32_m32(self):
        self.assert_assembles(
            "imul", self.register("eax"), DerefRegister(4, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "imul", self.register("esi"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9d"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9d"), DerefRegister(4, self.register("ebx"), 2)
        )

    def test_r64_m64(self):
        self.assert_assembles(
            "imul", self.register("rax"), DerefRegister(8, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "imul", self.register("rsi"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "imul", self.register("r9"), DerefRegister(8, self.register("rbx"), 2)
        )


class TestXor(AssembleTestCase):
    def test_r8_r8(self):
        self.assert_assembles("xor", self.register("al"), self.register("bl"))
        self.assert_assembles("xor", self.register("sil"), self.register("r10b"))
        self.assert_assembles("xor", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("xor", self.register("r9b"), self.register("bl"))
        self.assert_assembles("xor", self.register("ah"), self.register("bl"))

    def test_r16_r16(self):
        self.assert_assembles("xor", self.register("ax"), self.register("bx"))
        self.assert_assembles("xor", self.register("si"), self.register("r10w"))
        self.assert_assembles("xor", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("xor", self.register("r9w"), self.register("bx"))

    def test_r32_r32(self):
        self.assert_assembles("xor", self.register("eax"), self.register("ebx"))
        self.assert_assembles("xor", self.register("esi"), self.register("r10d"))
        self.assert_assembles("xor", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("xor", self.register("r9d"), self.register("ebx"))

    def test_r64_r64(self):
        self.assert_assembles("xor", self.register("rax"), self.register("rbx"))
        self.assert_assembles("xor", self.register("rsi"), self.register("r10"))
        self.assert_assembles("xor", self.register("r9"), self.register("r10"))
        self.assert_assembles("xor", self.register("r9"), self.register("rbx"))

    def test_r8_m8(self):
        self.assert_assembles(
            "xor", self.register("al"), DerefRegister(1, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "xor", self.register("sil"), DerefRegister(1, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9b"), DerefRegister(1, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9b"), DerefRegister(1, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "xor", self.register("ah"), DerefRegister(1, self.register("rbx"), 2)
        )

    def test_r16_m16(self):
        self.assert_assembles(
            "xor", self.register("ax"), DerefRegister(2, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "xor", self.register("si"), DerefRegister(2, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9w"), DerefRegister(2, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9w"), DerefRegister(2, self.register("rbx"), 2)
        )

    def test_r32_m32(self):
        self.assert_assembles(
            "xor", self.register("eax"), DerefRegister(4, self.register("ebx"), 2)
        )
        self.assert_assembles(
            "xor", self.register("esi"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9d"), DerefRegister(4, self.register("r10d"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9d"), DerefRegister(4, self.register("ebx"), 2)
        )

    def test_r64_m64(self):
        self.assert_assembles(
            "xor", self.register("rax"), DerefRegister(8, self.register("rbx"), 2)
        )
        self.assert_assembles(
            "xor", self.register("rsi"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9"), DerefRegister(8, self.register("r10"), 2)
        )
        self.assert_assembles(
            "xor", self.register("r9"), DerefRegister(8, self.register("rbx"), 2)
        )


class TestSetcc(AssembleTestCase):
    def test_seta(self):
        self.assert_assembles("seta", self.register("al"))
        self.assert_assembles("seta", self.register("sil"))
        self.assert_assembles("seta", self.register("r9b"))
        self.assert_assembles("seta", self.register("ah"))

    def test_setae(self):
        self.assert_assembles("setae", self.register("al"))
        self.assert_assembles("setae", self.register("sil"))
        self.assert_assembles("setae", self.register("r9b"))
        self.assert_assembles("setae", self.register("ah"))

    def test_setb(self):
        self.assert_assembles("setb", self.register("al"))
        self.assert_assembles("setb", self.register("sil"))
        self.assert_assembles("setb", self.register("r9b"))
        self.assert_assembles("setb", self.register("ah"))

    def test_setbe(self):
        self.assert_assembles("setbe", self.register("al"))
        self.assert_assembles("setbe", self.register("sil"))
        self.assert_assembles("setbe", self.register("r9b"))
        self.assert_assembles("setbe", self.register("ah"))

    def test_setc(self):
        self.assert_assembles("setc", self.register("al"))
        self.assert_assembles("setc", self.register("sil"))
        self.assert_assembles("setc", self.register("r9b"))
        self.assert_assembles("setc", self.register("ah"))

    def test_sete(self):
        self.assert_assembles("sete", self.register("al"))
        self.assert_assembles("sete", self.register("sil"))
        self.assert_assembles("sete", self.register("r9b"))
        self.assert_assembles("sete", self.register("ah"))

    def test_setg(self):
        self.assert_assembles("setg", self.register("al"))
        self.assert_assembles("setg", self.register("sil"))
        self.assert_assembles("setg", self.register("r9b"))
        self.assert_assembles("setg", self.register("ah"))

    def test_setge(self):
        self.assert_assembles("setge", self.register("al"))
        self.assert_assembles("setge", self.register("sil"))
        self.assert_assembles("setge", self.register("r9b"))
        self.assert_assembles("setge", self.register("ah"))

    def test_setl(self):
        self.assert_assembles("setl", self.register("al"))
        self.assert_assembles("setl", self.register("sil"))
        self.assert_assembles("setl", self.register("r9b"))
        self.assert_assembles("setl", self.register("ah"))

    def test_setle(self):
        self.assert_assembles("setle", self.register("al"))
        self.assert_assembles("setle", self.register("sil"))
        self.assert_assembles("setle", self.register("r9b"))
        self.assert_assembles("setle", self.register("ah"))

    def test_setna(self):
        self.assert_assembles("setna", self.register("al"))
        self.assert_assembles("setna", self.register("sil"))
        self.assert_assembles("setna", self.register("r9b"))
        self.assert_assembles("setna", self.register("ah"))

    def test_setnae(self):
        self.assert_assembles("setnae", self.register("al"))
        self.assert_assembles("setnae", self.register("sil"))
        self.assert_assembles("setnae", self.register("r9b"))
        self.assert_assembles("setnae", self.register("ah"))

    def test_setnb(self):
        self.assert_assembles("setnb", self.register("al"))
        self.assert_assembles("setnb", self.register("sil"))
        self.assert_assembles("setnb", self.register("r9b"))
        self.assert_assembles("setnb", self.register("ah"))

    def test_setnbe(self):
        self.assert_assembles("setnbe", self.register("al"))
        self.assert_assembles("setnbe", self.register("sil"))
        self.assert_assembles("setnbe", self.register("r9b"))
        self.assert_assembles("setnbe", self.register("ah"))

    def test_setnc(self):
        self.assert_assembles("setnc", self.register("al"))
        self.assert_assembles("setnc", self.register("sil"))
        self.assert_assembles("setnc", self.register("r9b"))
        self.assert_assembles("setnc", self.register("ah"))

    def test_setne(self):
        self.assert_assembles("setne", self.register("al"))
        self.assert_assembles("setne", self.register("sil"))
        self.assert_assembles("setne", self.register("r9b"))
        self.assert_assembles("setne", self.register("ah"))

    def test_setng(self):
        self.assert_assembles("setng", self.register("al"))
        self.assert_assembles("setng", self.register("sil"))
        self.assert_assembles("setng", self.register("r9b"))
        self.assert_assembles("setng", self.register("ah"))

    def test_setnge(self):
        self.assert_assembles("setnge", self.register("al"))
        self.assert_assembles("setnge", self.register("sil"))
        self.assert_assembles("setnge", self.register("r9b"))
        self.assert_assembles("setnge", self.register("ah"))

    def test_setnl(self):
        self.assert_assembles("setnl", self.register("al"))
        self.assert_assembles("setnl", self.register("sil"))
        self.assert_assembles("setnl", self.register("r9b"))
        self.assert_assembles("setnl", self.register("ah"))

    def test_setnle(self):
        self.assert_assembles("setnle", self.register("al"))
        self.assert_assembles("setnle", self.register("sil"))
        self.assert_assembles("setnle", self.register("r9b"))
        self.assert_assembles("setnle", self.register("ah"))

    def test_setno(self):
        self.assert_assembles("setno", self.register("al"))
        self.assert_assembles("setno", self.register("sil"))
        self.assert_assembles("setno", self.register("r9b"))
        self.assert_assembles("setno", self.register("ah"))

    def test_setnp(self):
        self.assert_assembles("setnp", self.register("al"))
        self.assert_assembles("setnp", self.register("sil"))
        self.assert_assembles("setnp", self.register("r9b"))
        self.assert_assembles("setnp", self.register("ah"))

    def test_setns(self):
        self.assert_assembles("setns", self.register("al"))
        self.assert_assembles("setns", self.register("sil"))
        self.assert_assembles("setns", self.register("r9b"))
        self.assert_assembles("setns", self.register("ah"))

    def test_setnz(self):
        self.assert_assembles("setnz", self.register("al"))
        self.assert_assembles("setnz", self.register("sil"))
        self.assert_assembles("setnz", self.register("r9b"))
        self.assert_assembles("setnz", self.register("ah"))

    def test_seto(self):
        self.assert_assembles("seto", self.register("al"))
        self.assert_assembles("seto", self.register("sil"))
        self.assert_assembles("seto", self.register("r9b"))
        self.assert_assembles("seto", self.register("ah"))

    def test_setp(self):
        self.assert_assembles("setp", self.register("al"))
        self.assert_assembles("setp", self.register("sil"))
        self.assert_assembles("setp", self.register("r9b"))
        self.assert_assembles("setp", self.register("ah"))

    def test_setpe(self):
        self.assert_assembles("setpe", self.register("al"))
        self.assert_assembles("setpe", self.register("sil"))
        self.assert_assembles("setpe", self.register("r9b"))
        self.assert_assembles("setpe", self.register("ah"))

    def test_setpo(self):
        self.assert_assembles("setpo", self.register("al"))
        self.assert_assembles("setpo", self.register("sil"))
        self.assert_assembles("setpo", self.register("r9b"))
        self.assert_assembles("setpo", self.register("ah"))

    def test_sets(self):
        self.assert_assembles("sets", self.register("al"))
        self.assert_assembles("sets", self.register("sil"))
        self.assert_assembles("sets", self.register("r9b"))
        self.assert_assembles("sets", self.register("ah"))

    def test_setz(self):
        self.assert_assembles("setz", self.register("al"))
        self.assert_assembles("setz", self.register("sil"))
        self.assert_assembles("setz", self.register("r9b"))
        self.assert_assembles("setz", self.register("ah"))


class TestTest(AssembleTestCase):
    def test_test_r8_r8(self):
        self.assert_assembles("test", self.register("al"), self.register("bl"))
        self.assert_assembles("test", self.register("sil"), self.register("r10b"))
        self.assert_assembles("test", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("test", self.register("r9b"), self.register("bl"))
        self.assert_assembles("test", self.register("ah"), self.register("bl"))

    def test_test_r16_r16(self):
        self.assert_assembles("test", self.register("ax"), self.register("bx"))
        self.assert_assembles("test", self.register("si"), self.register("r10w"))
        self.assert_assembles("test", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("test", self.register("r9w"), self.register("bx"))

    def test_test_r32_r32(self):
        self.assert_assembles("test", self.register("eax"), self.register("ebx"))
        self.assert_assembles("test", self.register("esi"), self.register("r10d"))
        self.assert_assembles("test", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("test", self.register("r9d"), self.register("ebx"))

    def test_test_r64_r64(self):
        self.assert_assembles("test", self.register("rax"), self.register("rbx"))
        self.assert_assembles("test", self.register("rsi"), self.register("r10"))
        self.assert_assembles("test", self.register("r9"), self.register("r10"))
        self.assert_assembles("test", self.register("r9"), self.register("rbx"))

    def test_test_r8_imm8(self):
        self.assert_assembles("test", self.register("al"),  self.int8)
        self.assert_assembles("test", self.register("sil"), self.int8)
        self.assert_assembles("test", self.register("r9b"), self.int8)
        self.assert_assembles("test", self.register("r9b"), self.int8)
        self.assert_assembles("test", self.register("ah"),  self.int8)

    def test_test_r16_imm16(self):
        self.assert_assembles("test", self.register("ax"),  self.int16)
        self.assert_assembles("test", self.register("si"),  self.int16)
        self.assert_assembles("test", self.register("r9w"), self.int16)
        self.assert_assembles("test", self.register("r9w"), self.int16)

    def test_test_r32_imm32(self):
        self.assert_assembles("test", self.register("eax"), self.int32)
        self.assert_assembles("test", self.register("esi"), self.int32)
        self.assert_assembles("test", self.register("r9d"), self.int32)
        self.assert_assembles("test", self.register("r9d"), self.int32)

    def test_test_r64_imm32(self):
        self.assert_assembles("test", self.register("rax"), self.int32)
        self.assert_assembles("test", self.register("rsi"), self.int32)
        self.assert_assembles("test", self.register("r9"),  self.int32)
        self.assert_assembles("test", self.register("r9"),  self.int32)


class TestCmp(AssembleTestCase):
    def test_cmp_r8_r8(self):
        self.assert_assembles("cmp", self.register("al"), self.register("bl"))
        self.assert_assembles("cmp", self.register("sil"), self.register("r10b"))
        self.assert_assembles("cmp", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("cmp", self.register("r9b"), self.register("bl"))
        self.assert_assembles("cmp", self.register("ah"), self.register("bl"))

    def test_cmp_r16_r16(self):
        self.assert_assembles("cmp", self.register("ax"), self.register("bx"))
        self.assert_assembles("cmp", self.register("si"), self.register("r10w"))
        self.assert_assembles("cmp", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("cmp", self.register("r9w"), self.register("bx"))

    def test_cmp_r32_r32(self):
        self.assert_assembles("cmp", self.register("eax"), self.register("ebx"))
        self.assert_assembles("cmp", self.register("esi"), self.register("r10d"))
        self.assert_assembles("cmp", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("cmp", self.register("r9d"), self.register("ebx"))

    def test_cmp_r64_r64(self):
        self.assert_assembles("cmp", self.register("rax"), self.register("rbx"))
        self.assert_assembles("cmp", self.register("rsi"), self.register("r10"))
        self.assert_assembles("cmp", self.register("r9"), self.register("r10"))
        self.assert_assembles("cmp", self.register("r9"), self.register("rbx"))

    def test_cmp_r8_imm8(self):
        self.assert_assembles("cmp", self.register("al"),  self.int8)
        self.assert_assembles("cmp", self.register("sil"), self.int8)
        self.assert_assembles("cmp", self.register("r9b"), self.int8)
        self.assert_assembles("cmp", self.register("r9b"), self.int8)
        self.assert_assembles("cmp", self.register("ah"),  self.int8)

    def test_cmp_r16_imm16(self):
        self.assert_assembles("cmp", self.register("ax"),  self.int16)
        self.assert_assembles("cmp", self.register("si"),  self.int16)
        self.assert_assembles("cmp", self.register("r9w"), self.int16)
        self.assert_assembles("cmp", self.register("r9w"), self.int16)

    def test_cmp_r32_imm32(self):
        self.assert_assembles("cmp", self.register("eax"), self.int32)
        self.assert_assembles("cmp", self.register("esi"), self.int32)
        self.assert_assembles("cmp", self.register("r9d"), self.int32)
        self.assert_assembles("cmp", self.register("r9d"), self.int32)

    def test_cmp_r64_imm32(self):
        self.assert_assembles("cmp", self.register("rax"), self.int32)
        self.assert_assembles("cmp", self.register("rsi"), self.int32)
        self.assert_assembles("cmp", self.register("r9"),  self.int32)
        self.assert_assembles("cmp", self.register("r9"),  self.int32)


class TestJcc(AssembleTestCase):
    def test_ja(self):
        self.assert_assembles("ja", Address(self.int32), flavour=Nasm)

    def test_jae(self):
        self.assert_assembles("jae", Address(self.int32), flavour=Nasm)

    def test_jb(self):
        self.assert_assembles("jb", Address(self.int32), flavour=Nasm)

    def test_jbe(self):
        self.assert_assembles("jbe", Address(self.int32), flavour=Nasm)

    def test_jc(self):
        self.assert_assembles("jc", Address(self.int32), flavour=Nasm)

    def test_je(self):
        self.assert_assembles("je", Address(self.int32), flavour=Nasm)

    def test_jg(self):
        self.assert_assembles("jg", Address(self.int32), flavour=Nasm)

    def test_jge(self):
        self.assert_assembles("jge", Address(self.int32), flavour=Nasm)

    def test_jl(self):
        self.assert_assembles("jl", Address(self.int32), flavour=Nasm)

    def test_jle(self):
        self.assert_assembles("jle", Address(self.int32), flavour=Nasm)

    def test_jna(self):
        self.assert_assembles("jna", Address(self.int32), flavour=Nasm)

    def test_jnae(self):
        self.assert_assembles("jnae", Address(self.int32), flavour=Nasm)

    def test_jnb(self):
        self.assert_assembles("jnb", Address(self.int32), flavour=Nasm)

    def test_jnbe(self):
        self.assert_assembles("jnbe", Address(self.int32), flavour=Nasm)

    def test_jnc(self):
        self.assert_assembles("jnc", Address(self.int32), flavour=Nasm)

    def test_jne(self):
        self.assert_assembles("jne", Address(self.int32), flavour=Nasm)

    def test_jng(self):
        self.assert_assembles("jng", Address(self.int32), flavour=Nasm)

    def test_jnge(self):
        self.assert_assembles("jnge", Address(self.int32), flavour=Nasm)

    def test_jnl(self):
        self.assert_assembles("jnl", Address(self.int32), flavour=Nasm)

    def test_jnle(self):
        self.assert_assembles("jnle", Address(self.int32), flavour=Nasm)

    def test_jno(self):
        self.assert_assembles("jno", Address(self.int32), flavour=Nasm)

    def test_jnp(self):
        self.assert_assembles("jnp", Address(self.int32), flavour=Nasm)

    def test_jns(self):
        self.assert_assembles("jns", Address(self.int32), flavour=Nasm)

    def test_jnz(self):
        self.assert_assembles("jnz", Address(self.int32), flavour=Nasm)

    def test_jo(self):
        self.assert_assembles("jo", Address(self.int32), flavour=Nasm)

    def test_jp(self):
        self.assert_assembles("jp", Address(self.int32), flavour=Nasm)

    def test_jpe(self):
        self.assert_assembles("jpe", Address(self.int32), flavour=Nasm)

    def test_jpo(self):
        self.assert_assembles("jpo", Address(self.int32), flavour=Nasm)

    def test_js(self):
        self.assert_assembles("js", Address(self.int32), flavour=Nasm)

    def test_jz(self):
        self.assert_assembles("jz", Address(self.int32), flavour=Nasm)



class TestSub(AssembleTestCase):
    def test_r8_r8(self):
        self.assert_assembles("sub", self.register("al"), self.register("bl"))
        self.assert_assembles("sub", self.register("sil"), self.register("r10b"))
        self.assert_assembles("sub", self.register("r9b"), self.register("r10b"))
        self.assert_assembles("sub", self.register("r9b"), self.register("bl"))
        self.assert_assembles("sub", self.register("ah"), self.register("bh"))

    def test_m8_r8(self):
        self.assert_assembles(
            "sub", DerefRegister(1, self.register("rax"), 0), self.register("bl"),
            suf="b"
        )
        self.assert_assembles(
            "sub", DerefRegister(1, self.register("rsi"), 0x1122), self.register("r10b"),
            suf="b"
        )
        self.assert_assembles(
            "sub", DerefRegister(1, self.register("r9"), 0x1122), self.register("r10b"),
            suf="b"
        )
        self.assert_assembles(
            "sub", DerefRegister(1, self.register("r9"), 0x11223344), self.register("bl"),
            suf="b"
        )
        self.assert_assembles(
            "sub", DerefRegister(1, self.register("rax"), 0x11223344), self.register("bh"),
            suf="b"
        )

    def test_r8_imm8(self):
        self.assert_assembles("sub", self.register("al"), self.int8)
        self.assert_assembles("sub", self.register("bl"), self.int8)
        self.assert_assembles("sub", self.register("sil"), self.int8)
        self.assert_assembles("sub", self.register("r9b"), self.int8)
        self.assert_assembles("sub", self.register("ah"), self.int8)

    def test_r64_imm32(self):
        self.assert_assembles("sub", self.register("rax"), self.int32)
        self.assert_assembles("sub", self.register("rbx"), self.int32)
        self.assert_assembles("sub", self.register("rsi"), self.int32)
        self.assert_assembles("sub", self.register("r9"), self.int32)

    def test_r64_imm8(self):
        self.assert_assembles("sub", self.register("rax"), self.int8)
        self.assert_assembles("sub", self.register("rbx"), self.int8)
        self.assert_assembles("sub", self.register("rsi"), self.int8)
        self.assert_assembles("sub", self.register("r9"), self.int8)

    def test_r16_r16(self):
        self.assert_assembles("sub", self.register("ax"), self.register("bx"))
        self.assert_assembles("sub", self.register("si"), self.register("r10w"))
        self.assert_assembles("sub", self.register("r9w"), self.register("r10w"))
        self.assert_assembles("sub", self.register("r9w"), self.register("bx"))

    def test_r32_r32(self):
        self.assert_assembles("sub", self.register("eax"), self.register("ebx"))
        self.assert_assembles("sub", self.register("esi"), self.register("r10d"))
        self.assert_assembles("sub", self.register("r9d"), self.register("r10d"))
        self.assert_assembles("sub", self.register("r9d"), self.register("ebx"))

    def test_r64_r64(self):
        self.assert_assembles("sub", self.register("rax"), self.register("rbx"))
        self.assert_assembles("sub", self.register("rsi"), self.register("r10"))
        self.assert_assembles("sub", self.register("r9"), self.register("r10"))
        self.assert_assembles("sub", self.register("r9"), self.register("rbx"))

