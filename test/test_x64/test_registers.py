import unittest

from pycomp.back.arch.x86_64.registers import *


class TestRegisterEncoding(unittest.TestCase):
    def test_get_register(self):
        gr = register_encoding.get_register
        self.assertTupleEqual(gr("rax"), ("rax", 0, 8))
        self.assertTupleEqual(gr("rcx"), ("rcx", 1, 8))
        self.assertTupleEqual(gr("rdx"), ("rdx", 2, 8))
        self.assertTupleEqual(gr("rbx"), ("rbx", 3, 8))

        self.assertTupleEqual(gr("eax"), ("rax", 0, 4))
        self.assertTupleEqual(gr("ax"), ("rax", 0, 2))
        self.assertTupleEqual(gr("ah"), ("rax", 4, 1))
        self.assertTupleEqual(gr("al"), ("rax", 0, 1))

        self.assertTupleEqual(gr("rsp"), ("rsp", 4, 8))
        self.assertTupleEqual(gr("rbp"), ("rbp", 5, 8))
        self.assertTupleEqual(gr("rsi"), ("rsi", 6, 8))
        self.assertTupleEqual(gr("rdi"), ("rdi", 7, 8))

        self.assertTupleEqual(gr("esp"), ("rsp", 4, 4))
        self.assertTupleEqual(gr("sp"), ("rsp", 4, 2))
        self.assertTupleEqual(gr("spl"), ("rsp", 4, 1))

        self.assertTupleEqual(gr("r8"), ("r8", 8, 8))
        self.assertTupleEqual(gr("r9"), ("r9", 9, 8))
        self.assertTupleEqual(gr("r10"), ("r10", 10, 8))
        self.assertTupleEqual(gr("r11"), ("r11", 11, 8))
        self.assertTupleEqual(gr("r12"), ("r12", 12, 8))
        self.assertTupleEqual(gr("r13"), ("r13", 13, 8))
        self.assertTupleEqual(gr("r14"), ("r14", 14, 8))
        self.assertTupleEqual(gr("r15"), ("r15", 15, 8))

        self.assertTupleEqual(gr("r8d"), ("r8", 8, 4))
        self.assertTupleEqual(gr("r8w"), ("r8", 8, 2))
        self.assertTupleEqual(gr("r8b"), ("r8", 8, 1))


class TestRegister(unittest.TestCase):
    def test_sized_rax(self):
        sz = x86_64Register.sized
        self.assertEqual(sz(8, "rax").name, "rax")
        self.assertEqual(sz(4, "rax").name, "eax")
        self.assertEqual(sz(2, "rax").name, "ax")
        self.assertEqual(sz(1, "rax").name, "al")

        self.assertEqual(sz(8, "eax").name, "rax")
        self.assertEqual(sz(4, "eax").name, "eax")
        self.assertEqual(sz(2, "eax").name, "ax")
        self.assertEqual(sz(1, "eax").name, "al")

        self.assertEqual(sz(8, "ax").name, "rax")
        self.assertEqual(sz(4, "ax").name, "eax")
        self.assertEqual(sz(2, "ax").name, "ax")
        self.assertEqual(sz(1, "ax").name, "al")

        self.assertEqual(sz(8, "al").name, "rax")
        self.assertEqual(sz(4, "al").name, "eax")
        self.assertEqual(sz(2, "al").name, "ax")
        self.assertEqual(sz(1, "al").name, "al")

    def test_sized_rsp(self):
        sz = x86_64Register.sized
        self.assertEqual(sz(8, "rsp").name, "rsp")
        self.assertEqual(sz(4, "rsp").name, "esp")
        self.assertEqual(sz(2, "rsp").name, "sp")
        self.assertEqual(sz(1, "rsp").name, "spl")

        self.assertEqual(sz(8, "esp").name, "rsp")
        self.assertEqual(sz(4, "esp").name, "esp")
        self.assertEqual(sz(2, "esp").name, "sp")
        self.assertEqual(sz(1, "esp").name, "spl")

        self.assertEqual(sz(8, "sp").name, "rsp")
        self.assertEqual(sz(4, "sp").name, "esp")
        self.assertEqual(sz(2, "sp").name, "sp")
        self.assertEqual(sz(1, "sp").name, "spl")

        self.assertEqual(sz(8, "spl").name, "rsp")
        self.assertEqual(sz(4, "spl").name, "esp")
        self.assertEqual(sz(2, "spl").name, "sp")
        self.assertEqual(sz(1, "spl").name, "spl")

    def test_sized_r8(self):
        sz = x86_64Register.sized
        self.assertEqual(sz(8, "r8").name, "r8")
        self.assertEqual(sz(4, "r8").name, "r8d")
        self.assertEqual(sz(2, "r8").name, "r8w")
        self.assertEqual(sz(1, "r8").name, "r8b")

        self.assertEqual(sz(8, "r8d").name, "r8")
        self.assertEqual(sz(4, "r8d").name, "r8d")
        self.assertEqual(sz(2, "r8d").name, "r8w")
        self.assertEqual(sz(1, "r8d").name, "r8b")

        self.assertEqual(sz(8, "r8w").name, "r8")
        self.assertEqual(sz(4, "r8w").name, "r8d")
        self.assertEqual(sz(2, "r8w").name, "r8w")
        self.assertEqual(sz(1, "r8w").name, "r8b")

        self.assertEqual(sz(8, "r8b").name, "r8")
        self.assertEqual(sz(4, "r8b").name, "r8d")
        self.assertEqual(sz(2, "r8b").name, "r8w")
        self.assertEqual(sz(1, "r8b").name, "r8b")

    def test_from_name(self):
        n = x86_64Register.from_name
        self.assertEqual(n("rax").size, 8)
        self.assertEqual(n("rcx").size, 8)
        self.assertEqual(n("rdx").size, 8)
        self.assertEqual(n("rbx").size, 8)

        self.assertEqual(n("eax").size, 4)
        self.assertEqual(n("ax").size, 2)
        self.assertEqual(n("ah").size, 1)
        self.assertEqual(n("al").size, 1)

        self.assertEqual(n("rsp").size, 8)
        self.assertEqual(n("rbp").size, 8)
        self.assertEqual(n("rsi").size, 8)
        self.assertEqual(n("rdi").size, 8)

        self.assertEqual(n("esp").size, 4)
        self.assertEqual(n("sp").size, 2)
        self.assertEqual(n("spl").size, 1)

        self.assertEqual(n("r8").size, 8)
        self.assertEqual(n("r9").size, 8)
        self.assertEqual(n("r10").size, 8)
        self.assertEqual(n("r11").size, 8)
        self.assertEqual(n("r12").size, 8)
        self.assertEqual(n("r13").size, 8)
        self.assertEqual(n("r14").size, 8)
        self.assertEqual(n("r15").size, 8)

        self.assertEqual(n("r8d").size, 4)
        self.assertEqual(n("r8w").size, 2)
        self.assertEqual(n("r8b").size, 1)





