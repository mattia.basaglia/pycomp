import math
import unittest

from pycomp import fixint


class TestUint8(unittest.TestCase):
    intcls = fixint.UInt8
    neg2 = 0xfe # (~2+1)&0xff

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 1)
        self.assertEqual(self.intcls.signed, False)
        self.assertEqual(self.intcls.nbits, 8)
        self.assertEqual(self.intcls.min_value, 0)
        self.assertEqual(self.intcls.overflow, 0x100)
        self.assertEqual(self.intcls.bit_mask, 0xff)
        self.assertEqual(self.intcls.max_value, 0xff)
        self.assertEqual(self.intcls.pack_format, "B")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, self.neg2)
        self.assertEqual(self.intcls(258)._value, 2)

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(self.intcls.from_bytes(b'\x34')._value, 0x34)

    def test_to_bytes(self):
        self.assertEqual(self.intcls(0x34).to_bytes(), b'\x34')
        self.assertEqual(bytes(self.intcls(0x34)), b'\x34')

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<UInt8 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) + 2)._value, 1)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0) - 2)._value, self.neg2)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x80) * 2)._value, 0)

    def test_floordiv(self):
        a = self.intcls(3)
        self.assert_result(a, a//2, 3, 1)
        self.assert_result(a, 10//a, 3, 3)
        self.assert_result(a, a//a, 3, 1)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a **= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(17) ** 2)._value, 33)

    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)

        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0b11111100)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)

        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)

        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)

        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)

        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, self.neg2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(a), 2, 2)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfd)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestUint16(unittest.TestCase):
    intcls = fixint.UInt16
    neg2 = 0xfffe # (~2+1)&0xffff

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 2)
        self.assertEqual(self.intcls.signed, False)
        self.assertEqual(self.intcls.nbits, 16)
        self.assertEqual(self.intcls.min_value, 0)
        self.assertEqual(self.intcls.overflow, 0x10000)
        self.assertEqual(self.intcls.bit_mask, 0xffff)
        self.assertEqual(self.intcls.max_value, 0xffff)
        self.assertEqual(self.intcls.pack_format, "H")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, self.neg2)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(b'\x34\x12', fixint.Endianness.Little)._value,
            0x1234
        )
        self.assertEqual(
            self.intcls.from_bytes(b'\x34\x12', fixint.Endianness.Big)._value,
            0x3412
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x1234).to_bytes(fixint.Endianness.Little),
            b'\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x1234).to_bytes(fixint.Endianness.Big),
            b'\x12\x34'
        )
        self.assertEqual(bytes(self.intcls(0x1234)), b'\x34\x12')

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<UInt16 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) + 2)._value, 0x101)
        self.assertEqual((self.intcls(0xffff) + 2)._value, 1)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0) - 2)._value, self.neg2)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x80) * 2)._value, 0x100)
        self.assertEqual((self.intcls(0x8000) * 2)._value, 0)

    def test_floordiv(self):
        a = self.intcls(3)
        self.assert_result(a, a//2, 3, 1)
        self.assert_result(a, 10//a, 3, 3)
        self.assert_result(a, a//a, 3, 1)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a **= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 289)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 513)

    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)

        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0x3fc)
        self.assertEqual((self.intcls(0xffff) << 2)._value, 0xfffc)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)

        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)

        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)

        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)

        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, self.neg2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(a), 2, 2)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfffd)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestUint32(unittest.TestCase):
    intcls = fixint.UInt32
    neg2 = 0xfffffffe # (~2+1)&0xffffffff

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 4)
        self.assertEqual(self.intcls.signed, False)
        self.assertEqual(self.intcls.nbits, 32)
        self.assertEqual(self.intcls.min_value, 0)
        self.assertEqual(self.intcls.overflow, 0x100000000)
        self.assertEqual(self.intcls.bit_mask, 0xffffffff)
        self.assertEqual(self.intcls.max_value, 0xffffffff)
        self.assertEqual(self.intcls.pack_format, "L")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, self.neg2)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x78\x56\x34\x12',
                fixint.Endianness.Little
            )._value,
            0x12345678
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x78\x56\x34\x12',
                fixint.Endianness.Big
            )._value,
            0x78563412
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x12345678).to_bytes(fixint.Endianness.Little),
            b'\x78\x56\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x12345678).to_bytes(fixint.Endianness.Big),
            b'\x12\x34\x56\x78'
        )
        self.assertEqual(bytes(self.intcls(0x12345678)), b'\x78\x56\x34\x12')

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<UInt32 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) + 2)._value, 0x101)
        self.assertEqual((self.intcls(0xffff) + 2)._value, 0x10001)
        self.assertEqual((self.intcls(0xffffffff) + 2)._value, 1)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0) - 2)._value, self.neg2)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x80) * 2)._value, 0x100)
        self.assertEqual((self.intcls(0x8000) * 2)._value, 0x10000)
        self.assertEqual((self.intcls(0x80000000) * 2)._value, 0)

    def test_floordiv(self):
        a = self.intcls(3)
        self.assert_result(a, a//2, 3, 1)
        self.assert_result(a, 10//a, 3, 3)
        self.assert_result(a, a//a, 3, 1)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a **= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x10201)
        self.assertEqual((self.intcls(0x10000001) ** 2)._value, 0x20000001)

    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)

        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0x3fc)
        self.assertEqual((self.intcls(0xffff) << 2)._value, 0x3fffc)
        self.assertEqual((self.intcls(0xffffffff) << 2)._value, 0xfffffffc)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)

        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)

        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)

        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)

        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, self.neg2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(a), 2, 2)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfffffffd)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestUint64(unittest.TestCase):
    intcls = fixint.UInt64
    neg2 = 0xfffffffffffffffe # (~2+1)&0xffffffffffffffff

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 8)
        self.assertEqual(self.intcls.signed, False)
        self.assertEqual(self.intcls.nbits, 64)
        self.assertEqual(self.intcls.min_value, 0)
        self.assertEqual(self.intcls.overflow, 0x10000000000000000)
        self.assertEqual(self.intcls.bit_mask, 0xffffffffffffffff)
        self.assertEqual(self.intcls.max_value, 0xffffffffffffffff)
        self.assertEqual(self.intcls.pack_format, "Q")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, self.neg2)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12\x90\x78\x56\x34\x12',
                fixint.Endianness.Little
            )._value,
            0x1234567890123456
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12\x90\x78\x56\x34\x12',
                fixint.Endianness.Big
            )._value,
            0x5634129078563412
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x1234567890123456).to_bytes(fixint.Endianness.Little),
            b'\x56\x34\x12\x90\x78\x56\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x1234567890123456).to_bytes(fixint.Endianness.Big),
            b'\x12\x34\x56\x78\x90\x12\x34\x56'
        )
        self.assertEqual(
            bytes(self.intcls(0x1234567890123456)),
            b'\x56\x34\x12\x90\x78\x56\x34\x12'
        )

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<UInt64 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) + 2)._value, 0x101)
        self.assertEqual((self.intcls(0xffff) + 2)._value, 0x10001)
        self.assertEqual((self.intcls(0xffffffff) + 2)._value, 0x100000001)
        self.assertEqual((self.intcls(0xffffffffffffffff) + 2)._value, 1)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0) - 2)._value, self.neg2)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x80) * 2)._value, 0x100)
        self.assertEqual((self.intcls(0x8000) * 2)._value, 0x10000)
        self.assertEqual((self.intcls(0x80000000) * 2)._value, 0x100000000)
        self.assertEqual((self.intcls(0x8000000000000000) * 2)._value, 0)

    def test_floordiv(self):
        a = self.intcls(3)
        self.assert_result(a, a//2, 3, 1)
        self.assert_result(a, 10//a, 3, 3)
        self.assert_result(a, a//a, 3, 1)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a **= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x10201)
        self.assertEqual((self.intcls(0x10000001) ** 2)._value, 0x100000020000001)
        self.assertEqual((self.intcls(0x1000000000000001) ** 2)._value, 0x2000000000000001)

    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)

        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0x3fc)
        self.assertEqual((self.intcls(0xffff) << 2)._value, 0x3fffc)
        self.assertEqual((self.intcls(0xffffffff) << 2)._value, 0x3fffffffc)
        self.assertEqual((self.intcls(0xffffffffffffffff) << 2)._value, 0xfffffffffffffffc)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)

        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)

        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)

        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)

        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, self.neg2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(a), 2, 2)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfffffffffffffffd)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestInt8(unittest.TestCase):
    intcls = fixint.Int8

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 1)
        self.assertEqual(self.intcls.signed, True)
        self.assertEqual(self.intcls.nbits, 8)
        self.assertEqual(self.intcls.overflow, 0x100)
        self.assertEqual(self.intcls.bit_mask, 0xff)
        self.assertEqual(self.intcls.min_value, -128)
        self.assertEqual(self.intcls.max_value, 127)
        self.assertEqual(self.intcls.pack_format, "b")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, -2)
        self.assertEqual(self.intcls(128)._value, -128)
        self.assertEqual(self.intcls(127)._value, 127)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)
        self.assertEqual(self.intcls("-34")._value, -34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(self.intcls.from_bytes(b'\x34')._value, 0x34)
        self.assertEqual(self.intcls.from_bytes(b'\xfe')._value, -2)

    def test_to_bytes(self):
        self.assertEqual(self.intcls(0x34).to_bytes(), b'\x34')
        self.assertEqual(bytes(self.intcls(0x34)), b'\x34')
        self.assertEqual(self.intcls(-2).to_bytes(), b'\xfe')

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")
        self.assertEqual(str(self.intcls(-34)), "-34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<Int8 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))
        self.assertEqual(hash(self.intcls(-2)), hash(self.intcls(-2)))
        self.assertNotEqual(hash(self.intcls(2)), hash(self.intcls(-2)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))
        self.assertTrue(self.intcls(-2))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        self.assert_result(a, a+-10, 3, -7)
        self.assert_result(a, -10+a, 3, -7)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x7f) + 2)._value, -127)

    def test_add_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a+10, -3, 7)
        self.assert_result(a, 10+a, -3, 7)
        self.assert_result(a, a+a, -3, -6)

        self.assert_result(a, a+-10, -3, -13)
        self.assert_result(a, -10+a, -3, -13)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-128) + -2)._value, 126)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)
        self.assert_result(a, a-10, 3, -7)

        self.assert_result(a, a--1, 3, 4)
        self.assert_result(a, -10-a, 3, -13)
        self.assert_result(a, a--10, 3, 13)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)

    def test_sub_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a-1, -3, -4)
        self.assert_result(a, 10-a, -3, 13)
        self.assert_result(a, a-a, -3, 0)
        self.assert_result(a, a-10, -3, -13)

        self.assert_result(a, a--1, -3, -2)
        self.assert_result(a, -10-a, -3, -7)
        self.assert_result(a, a--10, -3, 7)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        self.assert_result(a, a*-10, 3, -30)
        self.assert_result(a, -10*a, 3, -30)

        a = self.intcls(-3)
        self.assert_result(a, a*10, -3, -30)
        self.assert_result(a, 10*a, -3, -30)
        self.assert_result(a, a*a, -3, 9)

        self.assert_result(a, a*-10, -3, 30)
        self.assert_result(a, -10*a, -3, 30)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-128) * 2)._value, 0)
        self.assertEqual((self.intcls(127) * 2)._value, -2)
        self.assertEqual((self.intcls(-127) * 2)._value, 2)

    def test_floordiv(self):
        a = self.intcls(5)
        self.assert_result(a, a//2, 5, 2)
        self.assert_result(a, 10//a, 5, 2)
        self.assert_result(a, a//a, 5, 1)

        self.assert_result(a, a//-2, 5, -3)
        self.assert_result(a, -10//a, 5, -2)

        a = self.intcls(-5)
        self.assert_result(a, a//2, -5, -3)
        self.assert_result(a, 10//a, -5, -2)
        self.assert_result(a, a//a, -5, 1)

        self.assert_result(a, a//-2, -5, 2)
        self.assert_result(a, -10//a, -5, 2)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        self.assert_result(a, a % -2, 3, -1)
        self.assert_result(a, -11 % a, 3, 1)

        a = self.intcls(-3)
        self.assert_result(a, a % 2, -3, 1)
        self.assert_result(a, 11 % a, -3, -1)
        self.assert_result(a, a % a, -3, 0)

        self.assert_result(a, a % -2, -3, -1)
        self.assert_result(a, -11 % a, -3, -2)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)
        self.assert_result(a, -2**a, 3, -8)

        a = self.intcls(-3)
        self.assert_result(a, a**2, -3, 9)

        def bar(a):
            a ** -1

        def foo(a):
            a **= 10

        self.assertRaises(TypeError, foo, a)
        self.assertRaises(TypeError, bar, a)
        self.assertEqual((self.intcls(15) ** 2)._value, -31)
        self.assertEqual((self.intcls(16) ** 2)._value, 0)
        self.assertEqual((self.intcls(17) ** 2)._value, 33)

    """
    TODO
    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)
        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0b11111100)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)
        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)
        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)
        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)
        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfd)
    """

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, -2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(-a), 2, 2)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

        a = self.intcls(-34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), -34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), -34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(-34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)
        self.assertEqual(x[self.intcls(-1)], 4)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

        a = self.intcls(-2)
        self.assert_result(a, round(a), -2, -2)
        self.assert_result(a, math.trunc(a), -2, -2)
        self.assert_result(a, math.floor(a), -2, -2)
        self.assert_result(a, math.ceil(a), -2, -2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestInt16(unittest.TestCase):
    intcls = fixint.Int16

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 2)
        self.assertEqual(self.intcls.signed, True)
        self.assertEqual(self.intcls.nbits, 16)
        self.assertEqual(self.intcls.overflow,  0x10000)
        self.assertEqual(self.intcls.bit_mask,   0xffff)
        self.assertEqual(self.intcls.min_value, -0x8000)
        self.assertEqual(self.intcls.max_value,  0x7fff)
        self.assertEqual(self.intcls.pack_format, "h")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, -2)
        self.assertEqual(self.intcls(0x80)._value, 0x80)
        self.assertEqual(self.intcls(0x8000)._value, -0x8000)
        self.assertEqual(self.intcls(0x7fff)._value, 0x7fff)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )
        self.assertEqual(
            self.intcls(self.intcls.min_value - 2)._value,
            self.intcls.max_value - 1
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)
        self.assertEqual(self.intcls("-34")._value, -34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x34\x12', fixint.Endianness.Little
            )._value,
            0x1234
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x34\x12', fixint.Endianness.Big
            )._value,
            0x3412
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xfe\xff', fixint.Endianness.Little
            )._value,
            -2
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xff\xfe', fixint.Endianness.Big
            )._value,
            -2
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x1234).to_bytes(fixint.Endianness.Little),
            b'\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x1234).to_bytes(fixint.Endianness.Big),
            b'\x12\x34'
        )
        self.assertEqual(
            bytes(self.intcls(0x1234)),
            b'\x34\x12'
        )

        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Little),
            b'\xfe\xff'
        )
        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Big),
            b'\xff\xfe'
        )
        self.assertEqual(
            bytes(self.intcls(-2)),
            b'\xfe\xff'
        )


    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")
        self.assertEqual(str(self.intcls(-34)), "-34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<Int16 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))
        self.assertEqual(hash(self.intcls(-2)), hash(self.intcls(-2)))
        self.assertNotEqual(hash(self.intcls(2)), hash(self.intcls(-2)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))
        self.assertTrue(self.intcls(-2))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        self.assert_result(a, a+-10, 3, -7)
        self.assert_result(a, -10+a, 3, -7)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x7f) + 2)._value, 0x81)
        self.assertEqual((self.intcls(0x7fff) + 2)._value, -0x7fff)

    def test_add_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a+10, -3, 7)
        self.assert_result(a, 10+a, -3, 7)
        self.assert_result(a, a+a, -3, -6)

        self.assert_result(a, a+-10, -3, -13)
        self.assert_result(a, -10+a, -3, -13)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-0x80) + -2)._value, -0x82)
        self.assertEqual((self.intcls(-0x8000) + -2)._value, 0x7ffe)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)
        self.assert_result(a, a-10, 3, -7)

        self.assert_result(a, a--1, 3, 4)
        self.assert_result(a, -10-a, 3, -13)
        self.assert_result(a, a--10, 3, 13)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)

    def test_sub_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a-1, -3, -4)
        self.assert_result(a, 10-a, -3, 13)
        self.assert_result(a, a-a, -3, 0)
        self.assert_result(a, a-10, -3, -13)

        self.assert_result(a, a--1, -3, -2)
        self.assert_result(a, -10-a, -3, -7)
        self.assert_result(a, a--10, -3, 7)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        self.assert_result(a, a*-10, 3, -30)
        self.assert_result(a, -10*a, 3, -30)

        a = self.intcls(-3)
        self.assert_result(a, a*10, -3, -30)
        self.assert_result(a, 10*a, -3, -30)
        self.assert_result(a, a*a, -3, 9)

        self.assert_result(a, a*-10, -3, 30)
        self.assert_result(a, -10*a, -3, 30)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-0x80) * 2)._value, -0x100)
        self.assertEqual((self.intcls(-0x8000) * 2)._value, 0)
        self.assertEqual((self.intcls(0x7f) * 2)._value, 0xfe)
        self.assertEqual((self.intcls(0x7fff) * 2)._value, -2)
        self.assertEqual((self.intcls(-0x7f) * 2)._value, -0xfe)
        self.assertEqual((self.intcls(-0x7fff) * 2)._value, 2)

    def test_floordiv(self):
        a = self.intcls(5)
        self.assert_result(a, a//2, 5, 2)
        self.assert_result(a, 10//a, 5, 2)
        self.assert_result(a, a//a, 5, 1)

        self.assert_result(a, a//-2, 5, -3)
        self.assert_result(a, -10//a, 5, -2)

        a = self.intcls(-5)
        self.assert_result(a, a//2, -5, -3)
        self.assert_result(a, 10//a, -5, -2)
        self.assert_result(a, a//a, -5, 1)

        self.assert_result(a, a//-2, -5, 2)
        self.assert_result(a, -10//a, -5, 2)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        self.assert_result(a, a % -2, 3, -1)
        self.assert_result(a, -11 % a, 3, 1)

        a = self.intcls(-3)
        self.assert_result(a, a % 2, -3, 1)
        self.assert_result(a, 11 % a, -3, -1)
        self.assert_result(a, a % a, -3, 0)

        self.assert_result(a, a % -2, -3, -1)
        self.assert_result(a, -11 % a, -3, -2)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)
        self.assert_result(a, -2**a, 3, -8)

        a = self.intcls(-3)
        self.assert_result(a, a**2, -3, 9)

        def bar(a):
            a ** -1

        def foo(a):
            a **= 10

        self.assertRaises(TypeError, foo, a)
        self.assertRaises(TypeError, bar, a)
        self.assertEqual((self.intcls(0x0f) ** 2)._value, 0xe1)
        self.assertEqual((self.intcls(0x10) ** 2)._value, 0x100)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)

        self.assertEqual((self.intcls(0x0ff) ** 2)._value, -0x1ff)
        self.assertEqual((self.intcls(0x100) ** 2)._value, 0)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x201)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, -2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(-a), 2, 2)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

        a = self.intcls(-34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), -34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), -34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(-34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)
        self.assertEqual(x[self.intcls(-1)], 4)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

        a = self.intcls(-2)
        self.assert_result(a, round(a), -2, -2)
        self.assert_result(a, math.trunc(a), -2, -2)
        self.assert_result(a, math.floor(a), -2, -2)
        self.assert_result(a, math.ceil(a), -2, -2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestInt32(unittest.TestCase):
    intcls = fixint.Int32

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 4)
        self.assertEqual(self.intcls.signed, True)
        self.assertEqual(self.intcls.nbits, 32)
        self.assertEqual(self.intcls.overflow,  0x100000000)
        self.assertEqual(self.intcls.bit_mask,   0xffffffff)
        self.assertEqual(self.intcls.min_value, -0x80000000)
        self.assertEqual(self.intcls.max_value,  0x7fffffff)
        self.assertEqual(self.intcls.pack_format, "l")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, -2)
        self.assertEqual(self.intcls(0x80)._value, 0x80)
        self.assertEqual(self.intcls(0x8000)._value, 0x8000)
        self.assertEqual(self.intcls(0x80000000)._value, -0x80000000)
        self.assertEqual(self.intcls(0x7fffffff)._value, 0x7fffffff)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )
        self.assertEqual(
            self.intcls(self.intcls.min_value - 2)._value,
            self.intcls.max_value - 1
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)
        self.assertEqual(self.intcls("-34")._value, -34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x78\x56\x34\x12', fixint.Endianness.Little
            )._value,
            0x12345678
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x78\x56\x34\x12', fixint.Endianness.Big
            )._value,
            0x78563412
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xfe\xff\xff\xff', fixint.Endianness.Little
            )._value,
            -2
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xff\xff\xff\xfe', fixint.Endianness.Big
            )._value,
            -2
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x12345678).to_bytes(fixint.Endianness.Little),
            b'\x78\x56\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x12345678).to_bytes(fixint.Endianness.Big),
            b'\x12\x34\x56\x78'
        )
        self.assertEqual(
            bytes(self.intcls(0x12345678)),
            b'\x78\x56\x34\x12'
        )

        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Little),
            b'\xfe\xff\xff\xff'
        )
        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Big),
            b'\xff\xff\xff\xfe'
        )
        self.assertEqual(
            bytes(self.intcls(-2)),
            b'\xfe\xff\xff\xff'
        )

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")
        self.assertEqual(str(self.intcls(-34)), "-34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<Int32 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))
        self.assertEqual(hash(self.intcls(-2)), hash(self.intcls(-2)))
        self.assertNotEqual(hash(self.intcls(2)), hash(self.intcls(-2)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))
        self.assertTrue(self.intcls(-2))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        self.assert_result(a, a+-10, 3, -7)
        self.assert_result(a, -10+a, 3, -7)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x7f) + 2)._value, 0x81)
        self.assertEqual((self.intcls(0x7fff) + 2)._value, 0x8001)
        self.assertEqual((self.intcls(0x7fffffff) + 2)._value, -0x7fffffff)

    def test_add_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a+10, -3, 7)
        self.assert_result(a, 10+a, -3, 7)
        self.assert_result(a, a+a, -3, -6)

        self.assert_result(a, a+-10, -3, -13)
        self.assert_result(a, -10+a, -3, -13)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-0x80) + -2)._value, -0x82)
        self.assertEqual((self.intcls(-0x8000) + -2)._value, -0x8002)
        self.assertEqual((self.intcls(-0x80000000) + -2)._value, 0x7ffffffe)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)
        self.assert_result(a, a-10, 3, -7)

        self.assert_result(a, a--1, 3, 4)
        self.assert_result(a, -10-a, 3, -13)
        self.assert_result(a, a--10, 3, 13)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)

    def test_sub_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a-1, -3, -4)
        self.assert_result(a, 10-a, -3, 13)
        self.assert_result(a, a-a, -3, 0)
        self.assert_result(a, a-10, -3, -13)

        self.assert_result(a, a--1, -3, -2)
        self.assert_result(a, -10-a, -3, -7)
        self.assert_result(a, a--10, -3, 7)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        self.assert_result(a, a*-10, 3, -30)
        self.assert_result(a, -10*a, 3, -30)

        a = self.intcls(-3)
        self.assert_result(a, a*10, -3, -30)
        self.assert_result(a, 10*a, -3, -30)
        self.assert_result(a, a*a, -3, 9)

        self.assert_result(a, a*-10, -3, 30)
        self.assert_result(a, -10*a, -3, 30)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)

        self.assertEqual((self.intcls(-0x80) * 2)._value, -0x100)
        self.assertEqual((self.intcls(-0x8000) * 2)._value, -0x10000)
        self.assertEqual((self.intcls(-0x80000000) * 2)._value, 0)

        self.assertEqual((self.intcls(0x7f) * 2)._value, 0xfe)
        self.assertEqual((self.intcls(0x7fff) * 2)._value, 0xfffe)
        self.assertEqual((self.intcls(0x7fffffff) * 2)._value, -2)

        self.assertEqual((self.intcls(-0x7f) * 2)._value, -0xfe)
        self.assertEqual((self.intcls(-0x7fff) * 2)._value, -0xfffe)
        self.assertEqual((self.intcls(-0x7fffffff) * 2)._value, 2)

    def test_floordiv(self):
        a = self.intcls(5)
        self.assert_result(a, a//2, 5, 2)
        self.assert_result(a, 10//a, 5, 2)
        self.assert_result(a, a//a, 5, 1)

        self.assert_result(a, a//-2, 5, -3)
        self.assert_result(a, -10//a, 5, -2)

        a = self.intcls(-5)
        self.assert_result(a, a//2, -5, -3)
        self.assert_result(a, 10//a, -5, -2)
        self.assert_result(a, a//a, -5, 1)

        self.assert_result(a, a//-2, -5, 2)
        self.assert_result(a, -10//a, -5, 2)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        self.assert_result(a, a % -2, 3, -1)
        self.assert_result(a, -11 % a, 3, 1)

        a = self.intcls(-3)
        self.assert_result(a, a % 2, -3, 1)
        self.assert_result(a, 11 % a, -3, -1)
        self.assert_result(a, a % a, -3, 0)

        self.assert_result(a, a % -2, -3, -1)
        self.assert_result(a, -11 % a, -3, -2)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)
        self.assert_result(a, -2**a, 3, -8)

        a = self.intcls(-3)
        self.assert_result(a, a**2, -3, 9)

        def bar(a):
            a ** -1

        def foo(a):
            a **= 10

        self.assertRaises(TypeError, foo, a)
        self.assertRaises(TypeError, bar, a)
        self.assertEqual((self.intcls(0x0f) ** 2)._value, 0xe1)
        self.assertEqual((self.intcls(0x10) ** 2)._value, 0x100)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)

        self.assertEqual((self.intcls(0x0ff) ** 2)._value, 0xfe01)
        self.assertEqual((self.intcls(0x100) ** 2)._value, 0x10000)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x10201)

        self.assertEqual((self.intcls(0x0ffffff) ** 2)._value, -0x1ffffff)
        self.assertEqual((self.intcls(0x1000000) ** 2)._value, 0)
        self.assertEqual((self.intcls(0x1000001) ** 2)._value, 0x2000001)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, -2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(-a), 2, 2)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

        a = self.intcls(-34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), -34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), -34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(-34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)
        self.assertEqual(x[self.intcls(-1)], 4)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

        a = self.intcls(-2)
        self.assert_result(a, round(a), -2, -2)
        self.assert_result(a, math.trunc(a), -2, -2)
        self.assert_result(a, math.floor(a), -2, -2)
        self.assert_result(a, math.ceil(a), -2, -2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestInt64(unittest.TestCase):
    intcls = fixint.Int64

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 8)
        self.assertEqual(self.intcls.signed, True)
        self.assertEqual(self.intcls.nbits, 64)
        self.assertEqual(self.intcls.overflow,  0x10000000000000000)
        self.assertEqual(self.intcls.bit_mask,   0xffffffffffffffff)
        self.assertEqual(self.intcls.min_value, -0x8000000000000000)
        self.assertEqual(self.intcls.max_value,  0x7fffffffffffffff)
        self.assertEqual(self.intcls.pack_format, "q")
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, -2)
        self.assertEqual(self.intcls(0x80)._value, 0x80)
        self.assertEqual(self.intcls(0x8000)._value, 0x8000)
        self.assertEqual(self.intcls(0x80000000)._value, 0x80000000)
        self.assertEqual(self.intcls(0x8000000000000000)._value, -0x8000000000000000)
        self.assertEqual(self.intcls(0x7fffffffffffffff)._value, 0x7fffffffffffffff)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )
        self.assertEqual(
            self.intcls(self.intcls.min_value - 2)._value,
            self.intcls.max_value - 1
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)
        self.assertEqual(self.intcls("-34")._value, -34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12\x90\x78\x56\x34\x12', fixint.Endianness.Little
            )._value,
            0x1234567890123456
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12\x90\x78\x56\x34\x12', fixint.Endianness.Big
            )._value,
            0x5634129078563412
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xfe\xff\xff\xff\xff\xff\xff\xff', fixint.Endianness.Little
            )._value,
            -2
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\xff\xff\xff\xff\xff\xff\xff\xfe', fixint.Endianness.Big
            )._value,
            -2
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x1234567890123456).to_bytes(fixint.Endianness.Little),
            b'\x56\x34\x12\x90\x78\x56\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x1234567890123456).to_bytes(fixint.Endianness.Big),
            b'\x12\x34\x56\x78\x90\x12\x34\x56'
        )
        self.assertEqual(
            bytes(self.intcls(0x1234567890123456)),
            b'\x56\x34\x12\x90\x78\x56\x34\x12'
        )

        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Little),
            b'\xfe\xff\xff\xff\xff\xff\xff\xff'
        )
        self.assertEqual(
            self.intcls(-2).to_bytes(fixint.Endianness.Big),
            b'\xff\xff\xff\xff\xff\xff\xff\xfe'
        )
        self.assertEqual(
            bytes(self.intcls(-2)),
            b'\xfe\xff\xff\xff\xff\xff\xff\xff'
        )

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")
        self.assertEqual(str(self.intcls(-34)), "-34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<Int64 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))
        self.assertEqual(hash(self.intcls(-2)), hash(self.intcls(-2)))
        self.assertNotEqual(hash(self.intcls(2)), hash(self.intcls(-2)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))
        self.assertTrue(self.intcls(-2))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        self.assert_result(a, a+-10, 3, -7)
        self.assert_result(a, -10+a, 3, -7)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x7f) + 2)._value, 0x81)
        self.assertEqual((self.intcls(0x7fff) + 2)._value, 0x8001)
        self.assertEqual((self.intcls(0x7fffffff) + 2)._value, 0x80000001)
        self.assertEqual((self.intcls(0x7fffffffffffffff) + 2)._value, -0x7fffffffffffffff)

    def test_add_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a+10, -3, 7)
        self.assert_result(a, 10+a, -3, 7)
        self.assert_result(a, a+a, -3, -6)

        self.assert_result(a, a+-10, -3, -13)
        self.assert_result(a, -10+a, -3, -13)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(-0x80) + -2)._value, -0x82)
        self.assertEqual((self.intcls(-0x8000) + -2)._value, -0x8002)
        self.assertEqual((self.intcls(-0x80000000) + -2)._value, -0x80000002)
        self.assertEqual((self.intcls(-0x8000000000000000) + -2)._value, 0x7ffffffffffffffe)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)
        self.assert_result(a, a-10, 3, -7)

        self.assert_result(a, a--1, 3, 4)
        self.assert_result(a, -10-a, 3, -13)
        self.assert_result(a, a--10, 3, 13)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)

    def test_sub_n(self):
        a = self.intcls(-3)
        self.assert_result(a, a-1, -3, -4)
        self.assert_result(a, 10-a, -3, 13)
        self.assert_result(a, a-a, -3, 0)
        self.assert_result(a, a-10, -3, -13)

        self.assert_result(a, a--1, -3, -2)
        self.assert_result(a, -10-a, -3, -7)
        self.assert_result(a, a--10, -3, 7)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        self.assert_result(a, a*-10, 3, -30)
        self.assert_result(a, -10*a, 3, -30)

        a = self.intcls(-3)
        self.assert_result(a, a*10, -3, -30)
        self.assert_result(a, 10*a, -3, -30)
        self.assert_result(a, a*a, -3, 9)

        self.assert_result(a, a*-10, -3, 30)
        self.assert_result(a, -10*a, -3, 30)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)

        self.assertEqual((self.intcls(-0x80) * 2)._value, -0x100)
        self.assertEqual((self.intcls(-0x8000) * 2)._value, -0x10000)
        self.assertEqual((self.intcls(-0x80000000) * 2)._value, -0x100000000)
        self.assertEqual((self.intcls(-0x8000000000000000) * 2)._value, 0)

        self.assertEqual((self.intcls(0x7f) * 2)._value, 0xfe)
        self.assertEqual((self.intcls(0x7fff) * 2)._value, 0xfffe)
        self.assertEqual((self.intcls(0x7fffffff) * 2)._value, 0xfffffffe)
        self.assertEqual((self.intcls(0x7fffffffffffffff) * 2)._value, -2)

        self.assertEqual((self.intcls(-0x7f) * 2)._value, -0xfe)
        self.assertEqual((self.intcls(-0x7fff) * 2)._value, -0xfffe)
        self.assertEqual((self.intcls(-0x7fffffff) * 2)._value, -0xfffffffe)
        self.assertEqual((self.intcls(-0x7fffffffffffffff) * 2)._value, 2)

    def test_floordiv(self):
        a = self.intcls(5)
        self.assert_result(a, a//2, 5, 2)
        self.assert_result(a, 10//a, 5, 2)
        self.assert_result(a, a//a, 5, 1)

        self.assert_result(a, a//-2, 5, -3)
        self.assert_result(a, -10//a, 5, -2)

        a = self.intcls(-5)
        self.assert_result(a, a//2, -5, -3)
        self.assert_result(a, 10//a, -5, -2)
        self.assert_result(a, a//a, -5, 1)

        self.assert_result(a, a//-2, -5, 2)
        self.assert_result(a, -10//a, -5, 2)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        self.assert_result(a, a % -2, 3, -1)
        self.assert_result(a, -11 % a, 3, 1)

        a = self.intcls(-3)
        self.assert_result(a, a % 2, -3, 1)
        self.assert_result(a, 11 % a, -3, -1)
        self.assert_result(a, a % a, -3, 0)

        self.assert_result(a, a % -2, -3, -1)
        self.assert_result(a, -11 % a, -3, -2)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)
        self.assert_result(a, -2**a, 3, -8)

        a = self.intcls(-3)
        self.assert_result(a, a**2, -3, 9)

        def bar(a):
            a ** -1

        def foo(a):
            a **= 10

        self.assertRaises(TypeError, foo, a)
        self.assertRaises(TypeError, bar, a)
        self.assertEqual((self.intcls(0x0f) ** 2)._value, 0xe1)
        self.assertEqual((self.intcls(0x10) ** 2)._value, 0x100)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)

        self.assertEqual((self.intcls(0x0ff) ** 2)._value, 0xfe01)
        self.assertEqual((self.intcls(0x100) ** 2)._value, 0x10000)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x10201)

        self.assertEqual((self.intcls(0x0ffffff) ** 2)._value, 0xfffffe000001)
        self.assertEqual((self.intcls(0x1000000) ** 2)._value, 0x1000000000000)
        self.assertEqual((self.intcls(0x1000001) ** 2)._value, 0x1000002000001)

        self.assertEqual((self.intcls(0x0ffffffffffffff) ** 2)._value, -0x1ffffffffffffff)
        self.assertEqual((self.intcls(0x100000000000000) ** 2)._value, 0)
        self.assertEqual((self.intcls(0x100000000000001) ** 2)._value, 0x200000000000001)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, -2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(-a), 2, 2)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

        a = self.intcls(-34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), -34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), -34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(-34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)
        self.assertEqual(x[self.intcls(-1)], 4)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

        a = self.intcls(-2)
        self.assert_result(a, round(a), -2, -2)
        self.assert_result(a, math.trunc(a), -2, -2)
        self.assert_result(a, math.floor(a), -2, -2)
        self.assert_result(a, math.ceil(a), -2, -2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))


class TestSignCast(unittest.TestCase):
    def test_same_class(self):
        self.assertIs(fixint.UInt8,  fixint.int_factory(1, False, False))
        self.assertIs(fixint.UInt16, fixint.int_factory(2, False, False))
        self.assertIs(fixint.UInt32, fixint.int_factory(4, False, False))
        self.assertIs(fixint.UInt64, fixint.int_factory(8, False, False))
        self.assertIs(fixint.Int8,  fixint.int_factory(1, True, False))
        self.assertIs(fixint.Int16, fixint.int_factory(2, True, False))
        self.assertIs(fixint.Int32, fixint.int_factory(4, True, False))
        self.assertIs(fixint.Int64, fixint.int_factory(8, True, False))

    def assert_cast(self, obj, value, signed):
        self.assertIsInstance(obj, fixint.int_factory(type(obj).nbytes, signed, False))
        self.assertEqual(obj._value, value)

    def test_u8(self):
        self.assert_cast(fixint.UInt8(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.UInt8(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.UInt8(0x80).make_signed(), -0x80, True)
        self.assert_cast(fixint.UInt8(0x81).make_signed(), -0x7f, True)

    def test_s8(self):
        self.assert_cast(fixint.Int8(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.Int8(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.Int8(-0x80).make_unsigned(), 0x80, False)
        self.assert_cast(fixint.Int8(-0x7f).make_unsigned(), 0x81, False)

    def test_u16(self):
        self.assert_cast(fixint.UInt16(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.UInt16(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.UInt16(0x80).make_signed(), 0x80, True)
        self.assert_cast(fixint.UInt16(0x8000).make_signed(), -0x8000, True)
        self.assert_cast(fixint.UInt16(0x81).make_signed(), 0x81, True)
        self.assert_cast(fixint.UInt16(0x8001).make_signed(), -0x7fff, True)

    def test_s16(self):
        self.assert_cast(fixint.Int16(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.Int16(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.Int16(-0x8000).make_unsigned(), 0x8000, False)
        self.assert_cast(fixint.Int16(-0x7fff).make_unsigned(), 0x8001, False)

    def test_u32(self):
        self.assert_cast(fixint.UInt32(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.UInt32(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.UInt32(0x80).make_signed(), 0x80, True)
        self.assert_cast(fixint.UInt32(0x8000).make_signed(), 0x8000, True)
        self.assert_cast(fixint.UInt32(0x80000000).make_signed(), -0x80000000, True)
        self.assert_cast(fixint.UInt32(0x8001).make_signed(), 0x8001, True)
        self.assert_cast(fixint.UInt32(0x80000001).make_signed(), -0x7fffffff, True)

    def test_s32(self):
        self.assert_cast(fixint.Int32(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.Int32(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.Int32(-0x80000000).make_unsigned(), 0x80000000, False)
        self.assert_cast(fixint.Int32(-0x7fffffff).make_unsigned(), 0x80000001, False)

    def test_u64(self):
        self.assert_cast(fixint.UInt64(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.UInt64(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.UInt64(0x80).make_signed(), 0x80, True)
        self.assert_cast(fixint.UInt64(0x8000).make_signed(), 0x8000, True)
        self.assert_cast(fixint.UInt64(0x80000000).make_signed(), 0x80000000, True)
        self.assert_cast(fixint.UInt64(0x8000000000000000).make_signed(),
                         -0x8000000000000000, True)
        self.assert_cast(fixint.UInt64(0x8001).make_signed(), 0x8001, True)
        self.assert_cast(fixint.UInt64(0x80000001).make_signed(), 0x80000001, True)
        self.assert_cast(fixint.UInt64(0x8000000000000001).make_signed(),
                         -0x7fffffffffffffff, True)

    def test_s64(self):
        self.assert_cast(fixint.Int64(0x34).make_signed(), 0x34, True)
        self.assert_cast(fixint.Int64(0x34).make_unsigned(), 0x34, False)
        self.assert_cast(fixint.Int64(-0x8000000000000000).make_unsigned(),
                         0x8000000000000000, False)
        self.assert_cast(fixint.Int64(-0x7fffffffffffffff).make_unsigned(),
                         0x8000000000000001, False)


class TestUint24(unittest.TestCase):
    intcls = fixint.int_factory(3, False, False)
    neg2 = 0xfffffe # (~2+1)&0xffffff

    def test_class_attrs(self):
        self.assertEqual(self.intcls.nbytes, 3)
        self.assertEqual(self.intcls.signed, False)
        self.assertEqual(self.intcls.nbits, 24)
        self.assertEqual(self.intcls.min_value, 0)
        self.assertEqual(self.intcls.overflow, 0x1000000)
        self.assertEqual(self.intcls.bit_mask, 0xffffff)
        self.assertEqual(self.intcls.max_value, 0xffffff)
        self.assertEqual(self.intcls.pack_format, None)
        self.assertEqual(self.intcls.mutable, False)

    def test_init_int(self):
        self.assertEqual(self.intcls(34)._value, 34)
        self.assertEqual(self.intcls(-2)._value, self.neg2)
        self.assertEqual(
            self.intcls(self.intcls.max_value + 3)._value,
            self.intcls.min_value + 2
        )

    def test_init_str(self):
        self.assertEqual(self.intcls("34")._value, 34)
        self.assertEqual(self.intcls("34", 16)._value, 0x34)

    def test_init_oth(self):
        self.assertEqual(self.intcls(self.intcls(34))._value, 34)

    def test_copy(self):
        ob = self.intcls(34)
        c = ob.copy()
        self.assertIsNot(c, ob)
        self.assertEqual(c._value, 34)
        c._value = 55
        self.assertEqual(c._value, 55)
        self.assertEqual(ob._value, 34)

    def test_from_bytes(self):
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12',
                fixint.Endianness.Little
            )._value,
            0x123456
        )
        self.assertEqual(
            self.intcls.from_bytes(
                b'\x56\x34\x12',
                fixint.Endianness.Big
            )._value,
            0x563412
        )

    def test_to_bytes(self):
        self.assertEqual(
            self.intcls(0x123456).to_bytes(fixint.Endianness.Little),
            b'\x56\x34\x12'
        )
        self.assertEqual(
            self.intcls(0x123456).to_bytes(fixint.Endianness.Big),
            b'\x12\x34\x56'
        )
        self.assertEqual(bytes(self.intcls(0x123456)), b'\x56\x34\x12')

    def test_str(self):
        self.assertEqual(str(self.intcls(34)), "34")

    def test_repr(self):
        self.assertEqual(repr(self.intcls(34)), "<UInt24 34>")

    def test_format(self):
        self.assertEqual("{x:03d}".format(x=self.intcls(34)), "034")

    def test_hash(self):
        self.assertEqual(hash(self.intcls(34)), hash(self.intcls(34)))
        self.assertNotEqual(hash(self.intcls(34)), hash(self.intcls(35)))

    def test_bool(self):
        self.assertTrue(self.intcls(34))
        self.assertFalse(self.intcls(0))

    def assert_result(self, obj, result, obj_value, result_value):
        self.assertEqual(obj._value, obj_value)
        self.assertIsInstance(result, self.intcls)
        self.assertEqual(result._value, result_value)

    def test_add(self):
        a = self.intcls(3)
        self.assert_result(a, a+10, 3, 13)
        self.assert_result(a, 10+a, 3, 13)
        self.assert_result(a, a+a, 3, 6)

        def foo(a):
            a += 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) + 2)._value, 0x101)
        self.assertEqual((self.intcls(0xffff) + 2)._value, 0x10001)
        self.assertEqual((self.intcls(0xffffff) + 2)._value, 1)

    def test_sub(self):
        a = self.intcls(3)
        self.assert_result(a, a-1, 3, 2)
        self.assert_result(a, 10-a, 3, 7)
        self.assert_result(a, a-a, 3, 0)

        def foo(a):
            a -= 1
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0) - 2)._value, self.neg2)

    def test_mul(self):
        a = self.intcls(3)
        self.assert_result(a, a*10, 3, 30)
        self.assert_result(a, 10*a, 3, 30)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a *= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x80) * 2)._value, 0x100)
        self.assertEqual((self.intcls(0x8000) * 2)._value, 0x10000)
        self.assertEqual((self.intcls(0x800000) * 2)._value, 0)

    def test_floordiv(self):
        a = self.intcls(3)
        self.assert_result(a, a//2, 3, 1)
        self.assert_result(a, 10//a, 3, 3)
        self.assert_result(a, a//a, 3, 1)

        def foo(a):
            a //= 1
        self.assertRaises(TypeError, foo, a)

    def test_mod(self):
        a = self.intcls(3)
        self.assert_result(a, a % 2, 3, 1)
        self.assert_result(a, 11 % a, 3, 2)
        self.assert_result(a, a % a, 3, 0)

        def foo(a):
            a %= 2
        self.assertRaises(TypeError, foo, a)

    def test_divmod(self):
        a = self.intcls(3)
        self.assert_result(a, divmod(a, 2)[0], 3, 1)
        self.assert_result(a, divmod(10, a)[0], 3, 3)
        self.assert_result(a, divmod(a, a)[0], 3, 1)
        self.assert_result(a, divmod(a, 2)[1], 3, 1)
        self.assert_result(a, divmod(11, a)[1], 3, 2)
        self.assert_result(a, divmod(a, a)[1], 3, 0)

    def test_pow(self):
        a = self.intcls(3)
        self.assert_result(a, a**2, 3, 9)
        self.assert_result(a, 2**a, 3, 8)
        self.assert_result(a, a*a, 3, 9)

        def foo(a):
            a **= 10
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0x11) ** 2)._value, 0x121)
        self.assertEqual((self.intcls(0x101) ** 2)._value, 0x10201)
        self.assertEqual((self.intcls(0x100001) ** 2)._value, 0x200001)

    def test_lshift(self):
        a = self.intcls(0b101)
        self.assert_result(a, a << 3, 5, 0b00101000)
        self.assert_result(a, 1 << a, 5, 0b00100000)
        self.assert_result(a, a << a, 5, 0b10100000)

        def foo(a):
            a <<= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0xff) << 2)._value, 0x3fc)
        self.assertEqual((self.intcls(0xffff) << 2)._value, 0x3fffc)
        self.assertEqual((self.intcls(0xffffff) << 2)._value, 0xfffffc)

    def test_rshift(self):
        a = self.intcls(0b100)
        self.assert_result(a, a >> 1, 4, 2)
        self.assert_result(a, 32 >> a, 4, 2)
        self.assert_result(a, a >> self.intcls(1), 4, 2)

        def foo(a):
            a >>= 3
        self.assertRaises(TypeError, foo, a)
        self.assertEqual((self.intcls(0b1011) >> 2)._value, 0b10)

    def test_and(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a & b, 0b1101, 0b0001)
        self.assert_result(a, b & a, 0b1101, 0b0001)
        self.assert_result(a, c & a, 0b1101, 0b0001)

        def foo(a):
            a &= 10
        self.assertRaises(TypeError, foo, a)

    def test_or(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a | b, 0b1101, 0b1111)
        self.assert_result(a, b | a, 0b1101, 0b1111)
        self.assert_result(a, c | a, 0b1101, 0b1111)

        def foo(a):
            a |= 10
        self.assertRaises(TypeError, foo, a)

    def test_xor(self):
        a = self.intcls(0b1101)
        b = 0b0011
        c = self.intcls(b)
        self.assert_result(a, a ^ b, 0b1101, 0b1110)
        self.assert_result(a, b ^ a, 0b1101, 0b1110)
        self.assert_result(a, c ^ a, 0b1101, 0b1110)

        def foo(a):
            a ^= 10
        self.assertRaises(TypeError, foo, a)

    def test_negpos(self):
        a = self.intcls(2)
        self.assert_result(a, -a, 2, self.neg2)
        self.assert_result(a, +a, 2, 2)
        self.assert_result(a, abs(a), 2, 2)

    def test_not(self):
        a = self.intcls(0x02)
        self.assert_result(a, ~a, 2, 0xfffffd)

    def test_cast(self):
        a = self.intcls(34)
        self.assertIsInstance(int(a), int)
        self.assertEqual(int(a), 34)
        self.assertIsInstance(float(a), float)
        self.assertEqual(float(a), 34)
        self.assertIsInstance(complex(a), complex)
        self.assertEqual(complex(a), complex(34))

    def test_index(self):
        x = [0, 1, 2, 3, 4]
        self.assertEqual(x[self.intcls(1)], 1)
        self.assertEqual(x[self.intcls(2)], 2)

    def test_round(self):
        a = self.intcls(2)
        self.assert_result(a, round(a), 2, 2)
        self.assert_result(a, math.trunc(a), 2, 2)
        self.assert_result(a, math.floor(a), 2, 2)
        self.assert_result(a, math.ceil(a), 2, 2)

    def test_lt(self):
        a = self.intcls(3)
        self.assertFalse(a < 2)
        self.assertFalse(a < 3)
        self.assertTrue(a < 4)

        self.assertTrue(2 < a)
        self.assertFalse(3 < a)
        self.assertFalse(4 < a)

        self.assertFalse(a < self.intcls(2))
        self.assertFalse(a < self.intcls(3))
        self.assertTrue(a < self.intcls(4))

    def test_le(self):
        a = self.intcls(3)
        self.assertFalse(a <= 2)
        self.assertTrue(a <= 3)
        self.assertTrue(a <= 4)

        self.assertTrue(2 <= a)
        self.assertTrue(3 <= a)
        self.assertFalse(4 <= a)

        self.assertFalse(a <= self.intcls(2))
        self.assertTrue(a <= self.intcls(3))
        self.assertTrue(a <= self.intcls(4))

    def test_eq(self):
        a = self.intcls(3)
        self.assertFalse(a == 2)
        self.assertTrue(a == 3)
        self.assertFalse(a == 4)

        self.assertFalse(2 == a)
        self.assertTrue(3 == a)
        self.assertFalse(4 == a)

        self.assertFalse(a == self.intcls(2))
        self.assertTrue(a == self.intcls(3))
        self.assertFalse(a == self.intcls(4))

    def test_ne(self):
        a = self.intcls(3)
        self.assertTrue(a != 2)
        self.assertFalse(a != 3)
        self.assertTrue(a != 4)

        self.assertTrue(2 != a)
        self.assertFalse(3 != a)
        self.assertTrue(4 != a)

        self.assertTrue(a != self.intcls(2))
        self.assertFalse(a != self.intcls(3))
        self.assertTrue(a != self.intcls(4))

    def test_ge(self):
        a = self.intcls(3)
        self.assertTrue(a >= 2)
        self.assertTrue(a >= 3)
        self.assertFalse(a >= 4)

        self.assertFalse(2 >= a)
        self.assertTrue(3 >= a)
        self.assertTrue(4 >= a)

        self.assertTrue(a >= self.intcls(2))
        self.assertTrue(a >= self.intcls(3))
        self.assertFalse(a >= self.intcls(4))

    def test_gt(self):
        a = self.intcls(3)
        self.assertTrue(a > 2)
        self.assertFalse(a > 3)
        self.assertFalse(a > 4)

        self.assertFalse(2 > a)
        self.assertFalse(3 > a)
        self.assertTrue(4 > a)

        self.assertTrue(a > self.intcls(2))
        self.assertFalse(a > self.intcls(3))
        self.assertFalse(a > self.intcls(4))
