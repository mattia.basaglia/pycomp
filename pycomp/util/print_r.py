import sys
import enum
import collections

class RecursivePrinter:
    def __init__(self, extra_scalars=None, exclusions=None, max_level=None):
        self.extra_scalars = tuple(extra_scalars) or tuple()
        self.exclusions = dict(exclusions) or {}
        self.indent_str = "  "
        self.max_level = max_level

    def exclude_fields(self, cls, obj):
        exclusions = set()
        for base, exlusion in self.exclusions.items():
            if issubclass(cls, base):
                if callable(exlusion):
                    exclusions |= exlusion(obj)
                else:
                    exclusions |= exlusion
        return exclusions

    def show_field(self, excluded, name):
        return name not in excluded and not name.startswith("__")

    def is_scalar(self, obj):
        if isinstance(obj, (int, float, complex, str, bytes, enum.Enum) + self.extra_scalars):
            return True
        try:
            vars(obj)
            return False
        except TypeError:
            return True

    def _indent(self, level):
        return self.indent_str * level

    def _label_compund(self, output, name, obj, indent_level):
        output.write("%s%s%s%s\n" % (self._indent(indent_level), name, " " if name != "" else "", type(obj).__name__))

    def _safestr(self, obj):
        if type(obj).__str__ is not object.__str__ or type(obj).__repr__ is not object.__repr__:
            try:
                s = str(obj)
                if len(s) < 128:
                    return s
            except:
                pass
        return "<" + type(obj).__name__ + ">"

    def _is_sequence(self, obj):
        return isinstance(obj, (tuple, list, set, frozenset, collections.deque))

    def _is_assoc(self, obj):
        return isinstance(obj, (dict,))

    def _print_r(self, output, name, obj, indent_level, visited):
        if self.max_level is not None and indent_level > self.max_level:
            return
        if id(obj) in visited:
            output.write("%s%s %s\n" % (self._indent(indent_level), name, "<recursion> %s" % self._safestr(obj)))
            return

        if self._is_sequence(obj):
            visited.add(id(obj))
            self._label_compund(output, name, obj, indent_level)
            for k, v in enumerate(obj):
                self._print_r(output, k, v, indent_level+1, visited)
        elif self._is_assoc(obj):
            visited.add(id(obj))
            self._label_compund(output, name, obj, indent_level)
            for k, v in obj.items():
                self._print_r(output, k, v, indent_level+1, visited)
        elif self.is_scalar(obj):
            if isinstance(obj, bytes) and not byte_string_printable(obj):
                self._label_compund(output, name, obj, indent_level)
                print_hex(obj, indent=self._indent(indent_level+1), output=output)
            else:
                output.write("%s%s %s\n" % (self._indent(indent_level), name, obj))
        else:
            visited.add(id(obj))
            self._label_compund(output, name, obj, indent_level)

            cls = type(obj)
            excluded = self.exclude_fields(cls, obj)

            for k, v in vars(obj).items():
                if self.show_field(excluded, k):
                    self._print_r(output, k, v, indent_level+1, visited)

            for k in dir(cls):
                if self.show_field(excluded, k) and isinstance(getattr(cls, k), property):
                    self._print_r(output, k, getattr(obj, k), indent_level+1, visited)

    def __call__(self, obj, skip=None, output=sys.stdout):
        visited = set()
        if skip:
            for v in skip:
                if isinstance(v, int):
                    visited.add(v)
                else:
                    visited.add(id(v))
        return self._print_r(output, "", obj, 0, visited)


def print_hex(bytestring, line_length=16, indent="", output=sys.stdout):
    def fmt_line(line):
        output.write(indent)
        for b in line:
            output.write("%02x " % b)
        output.write(" " * (3 * (line_length-len(line))))
        output.write("| ")
        for b in line:
            ch = chr(b) if byte_printable(b) else ("\x1b[2;53;4m%02x\x1b[m" % b)
            output.write("%s" % ch)
        output.write("\n")

    max_length = len(bytestring)
    j = 0
    i = min(max_length, line_length)
    while i < max_length:
        fmt_line(bytestring[j:i])
        j = i
        i += line_length

    fmt_line(bytestring[j:])


def byte_printable(b):
    return b >= 32 and b < 127


def byte_string_printable(bs):
    return all(map(byte_printable, bs))

