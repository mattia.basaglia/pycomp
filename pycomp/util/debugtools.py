import re
import sys
import types


from .print_r import RecursivePrinter, print_hex
from .. import fixint
from ..back.elf import c_api, elf_file

#__all__ = ["elf_print_r", "print_r", "print_hex"]

strlen_stripansi_re = re.compile("\x1b[^m]*m")
def strlen_stripansi(s):
    return len(strlen_stripansi_re.sub("", s))


def elf_print_r(obj, elffile=None):
    """
    Recursive print for objects in an ELF file structure
    """
    if elffile is None:
        if isinstance(obj, elf_file.ElfFile):
            elffile = obj
        else:
            raise Exception("You must specify an ElfFile to avoid overly verbose output")
    exclusions = set([elffile] + elffile.sections) - set([obj] if not isinstance(obj, (list)) else [])
    print_r(obj, exclusions)


def ansi_align(str, len):
    return str + (" " * (len - strlen_stripansi(str)))


print_r_obj = RecursivePrinter(
    [fixint.FixintBase, c_api.FlagEnumBase],
    {
        c_api.Struct: lambda obj: { "_" + k for k in obj.fields.keys()},
        types.ModuleType: lambda obj: set(dir(obj)),
        type: lambda obj: set(x for x in dir(obj) if x.startswith("_")),
    }
)


def print_r(obj, skip=None, output=sys.stdout):
    """
    Recursively print an object, anything in skip will not be recursed on
    """
    return print_r_obj(obj, skip, output)


class AnsiAnnotator:
    def __init__(self, str):
        self.str = ""
        self.annotations = {}
        self._parse_str(str)

    def _parse_str(self, str):
        i = len(self.str)
        status = 0
        ann = ""
        for c in str:
            if status == 0:
                if c == "\x1b":
                    status = 1
                else:
                    if ann:
                        self.annotations[i] = ann
                    self.str += c
                    i += 1
            elif status == 1:
                if c == "[":
                    status = 2
                    ann = ""
            elif status == 2:
                if c == "m":
                    status = 0
                    if ann == "0":
                        ann = ""
                else:
                    ann += c

    def annotate(self, pos, code):
        if code == "0":
            if pos in self.annotations:
                del self.annotations[pos]
            return

        if pos in self.annotations:
            self.annotations[pos] += ";" + code
        else:
            self.annotations[pos] = code

    def __str__(self):
        s = ""
        for i, c in enumerate(self.str):
            if i in self.annotations:
                s += "\x1b[%sm%s\x1b[m" % (self.annotations[i], c)
            else:
                s += c
        return s
