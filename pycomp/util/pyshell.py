#!/usr/bin/env python
import sys
import ast
import traceback
import atexit
import types


class PyShell:
    """
    Creates an interactive python shell, which works like the default `python`
    invocation but allows for some predefined globals and similar
    """
    default_welcome = "Python " + sys.version + " on " + sys.platform
    main_prompt = ">>> "
    continue_prompt = "... "

    def __init__(self, globals, welcome=default_welcome, filename="stdin"):
        self.globals = globals
        self.welcome = welcome
        self.filename = filename
        self.lineno = 0

    def _read_line(self, prompt):
        """
        Reads a single line from stdin
        """
        self.lineno += 1
        return input(prompt).rstrip()

    def _read_match_brackets(self, prompt, matchables):
        line = self._read_line(prompt)
        script = line
        while not self._matched_brackets(line, matchables):
            line = self._read_line(self.continue_prompt)
            script += line
        return script

    def _matched_brackets(self, line, matchables):
        for c in line:
            m = matchables.get(c, None)
            if isinstance(m, int):
                matchables[c] += 1
            elif isinstance(m, str):
                matchables[m] -= 1

        for v in matchables.values():
            if isinstance(v, int) and v > 0:
                return False
        return True

    def read_stmt(self):
        """
        Reads a full statement from stdin
        """
        matchables = {
            "(": 0,
            "[": 0,
            "{": 0,
            ")": "(",
            "]": "[",
            "}": "{",
        }

        line = self._read_match_brackets(self.main_prompt, matchables)
        stmt = line

        if line and line[-1] == ":":
            while line:
                line = self._read_match_brackets(self.continue_prompt, matchables)
                stmt += "\n" + line
        return stmt

    def stmt_generator(self):
        """
        Yields statement strings until EOF
        """
        while True:
            try:
                yield self.read_stmt()
            except KeyboardInterrupt:
                print("KeyboardInterrupt")
            except EOFError:
                print("")
                return

    def main(self):
        """
        Runs the shell
        """
        if self.welcome:
            print(self.welcome)
        for stmt in self.stmt_generator():
            try:
                val = self.exec_stmt(stmt, self.globals, self.globals)
            except Exception as e:
                extype, value, tb = sys.exc_info()
                tb_start = None
                tb_last = tb
                while tb_last.tb_next:
                    tb_last = tb_last.tb_next
                    if not tb_start and tb_last.tb_frame.f_code.co_filename != __file__:
                        tb_start = tb_last
                if tb_last.tb_frame.f_code.co_filename == __file__:
                    # Exception comes from this script
                    raise
                traceback.print_exception(extype, value, tb_start)
                continue

            if val is not None:
                print(val)

    def exec_stmt(self, script, globals=None, locals=None):
        """
        Executes a script string and returns the value of the last expression
        """
        node = ast.parse(script, "stdin", "exec")
        stmts = list(ast.iter_child_nodes(node))
        if not stmts:
            return None

        if isinstance(stmts[-1], ast.Expr):
            if len(stmts) > 1:
                self.exec_ast(ast.Module(body=stmts[:-1]), "exec", globals, locals)
            return self.exec_ast(ast.Expression(body=stmts[-1].value),  "eval", globals, locals)
        else:
            return self.exec_ast(node, "exec", globals, locals)

    def exec_ast(self, ast_node, mode, globals=None, locals=None):
        """
        Executes the ast node
        """
        code = compile(ast_node, "stdin", mode)
        code = types.CodeType(
            code.co_argcount,
            code.co_kwonlyargcount,
            code.co_nlocals,
            code.co_stacksize,
            code.co_flags,
            code.co_code,
            code.co_consts,
            code.co_names,
            code.co_varnames,
            code.co_filename,
            code.co_name,
            self.lineno,
            code.co_lnotab,
            code.co_freevars,
            code.co_cellvars
        )
        fn = eval if mode == "eval" else exec
        return fn(code, globals, locals)

    def init_readline(self, histfile=None):
        """
        Sets up readline for the shell, including object completion
        """
        import rlcompleter
        import readline

        readline.parse_and_bind("tab: complete")

        if histfile:
            atexit.register(readline.write_history_file, histfile)
            try:
                readline.read_history_file(histfile)
                readline.set_history_length(2000)
            except IOError:
                pass
