import sys
import enum
import math
import struct


class Endianness(enum.Enum):
    Big = '>'
    Little = '<'


Endianness.Machine = Endianness.Big if sys.byteorder == "big" else Endianness.Little


def struct_char(signed, nbytes):
    if nbytes == 1:
        c = 'b'
    elif nbytes == 2:
        c = 'h'
    elif nbytes == 4:
        c = 'l'
    elif nbytes == 8:
        c = 'q'
    else:
        return None

    if not signed:
        return c.upper()

    return c


class FixintBase:
    __slots__ = []


def _int_factory_impl(_nbytes, _signed, _mutable):
    class FixInt(FixintBase):
        nbytes = _nbytes
        signed = _signed
        nbits = nbytes*8
        pack_format = struct_char(signed, nbytes)
        mutable = _mutable
        overflow = 1 << nbits
        bit_mask = overflow - 1
        if _signed:
            min_value = -overflow // 2
            max_value = overflow // 2 - 1
            negbit = overflow >> 1
        else:
            min_value = 0
            max_value = bit_mask

        __slots__ = ["_value"]

        def __init__(self, value=0, base=10):
            if isinstance(value, str):
                value = int(value, base)
            self._value = self._fixval(self._extract_int(value))

        if _signed:
            def _fixval(self, value):
                j = value & self.bit_mask
                if j & self.negbit:
                    return -(~j + 1 & self.bit_mask)
                return j
        else:
            def _fixval(self, value):
                return value & self.bit_mask

        def _mutate(self):
            if not self.mutable:
                raise TypeError("Immutable integer")

        def _extract_int(self, oth):
            if isinstance(oth, FixInt):
                return oth._value
            elif isinstance(oth, FixintBase):
                if oth._value < self.min_value:
                    raise TypeError("Integer underflow")
                elif oth._value > self.max_value:
                    raise TypeError("Integer overflow")
                return oth._value
            elif isinstance(oth, int):
                return oth
            raise TypeError(
                "Cannot perform arithmetic expression between %s and %s" % (
                    self.__class__.__name__,
                    type(oth).__name__
                )
            )

        def copy(self):
            return FixInt(self._value)

        def make_signed(self):
            if self.signed:
                return self.copy()
            return int_factory(self.nbytes, True, self.mutable)(self._value)

        def make_unsigned(self):
            if not self.signed:
                return self.copy()
            return int_factory(self.nbytes, False, self.mutable)(self._value)

        if pack_format:
            @classmethod
            def from_bytes(cls, bytes, endianness=Endianness.Machine):
                return cls(struct.unpack(endianness.value + cls.pack_format, bytes)[0])

            def to_bytes(self, endianness=Endianness.Machine):
                return struct.pack(endianness.value + self.pack_format, self._value)
        else:
            @classmethod
            def from_bytes(cls, bytes, endianness=Endianness.Machine):
                if endianness == Endianness.Little:
                    it = reversed(bytes)
                else:
                    it = bytes
                val = 0
                for byte in it:
                    val <<= 8
                    val |= byte
                return cls(val)

            def to_bytes(self, endianness=Endianness.Machine):
                vals = []
                val = self._value
                for i in range(self.nbytes):
                    vals.append(val & 0xff)
                    val >>= 8

                if endianness == Endianness.Big:
                    vals = reversed(vals)

                return bytes(vals)

        def __str__(self):
            return str(self._value)

        def __repr__(self):
            return "<%s %s>" % (
                type(self).__name__,
                self._value
            )

        def __bytes__(self):
            return self.to_bytes()

        def __format__(self, format_spec):
            return format(self._value, format_spec)

        def __hash__(self):
            return hash(self._value)

        def __bool__(self):
            return bool(self._value)

        def __add__(self, oth):
            return FixInt(self._value + self._extract_int(oth))

        def __sub__(self, oth):
            return FixInt(self._value - self._extract_int(oth))

        def __mul__(self, oth):
            return FixInt(self._value * self._extract_int(oth))

        def __floordiv__(self, oth):
            return FixInt(self._value // self._extract_int(oth))

        def __mod__(self, oth):
            return FixInt(self._value % self._extract_int(oth))

        def __divmod__(self, oth):
            div, mod = divmod(self._value, self._extract_int(oth))
            return FixInt(div), FixInt(mod)

        def _pow(self, a, b):
            if b < 0:
                raise TypeError("Cannot raise integer to negative power")
            return a ** b

        def __pow__(self, oth):
            return FixInt(self._pow(self._value, self._extract_int(oth)))

        if not _signed: # TODO signed bit fiddling?
            def __lshift__(self, oth):
                return FixInt(self._value << self._extract_int(oth))

            def __rshift__(self, oth):
                return FixInt(self._value >> self._extract_int(oth))

            def __and__(self, oth):
                return FixInt(self._value & self._extract_int(oth))

            def __xor__(self, oth):
                return FixInt(self._value ^ self._extract_int(oth))

            def __or__(self, oth):
                return FixInt(self._value | self._extract_int(oth))

            def __rlshift__(self, oth):
                return FixInt(self._extract_int(oth) << self._value)

            def __rrshift__(self, oth):
                return FixInt(self._extract_int(oth) >> self._value)

            def __rand__(self, oth):
                return FixInt(self._extract_int(oth) & self._value)

            def __rxor__(self, oth):
                return FixInt(self._extract_int(oth) ^ self._value)

            def __ror__(self, oth):
                return FixInt(self._extract_int(oth) | self._value)

            def __ilshift__(self, oth):
                self._mutate()
                self._value = self._fixval(self._value << self._extract_int(oth))
                return self

            def __irshift__(self, oth):
                self._mutate()
                self._value = self._fixval(self._value >> self._extract_int(oth))
                return self

            def __iand__(self, oth):
                self._mutate()
                self._value = self._fixval(self._value & self._extract_int(oth))
                return self

            def __ixor__(self, oth):
                self._mutate()
                self._value = self._fixval(self._value ^ self._extract_int(oth))
                return self

            def __ior__(self, oth):
                self._mutate()
                self._value = self._fixval(self._value | self._extract_int(oth))
                return self

            def __invert__(self):
                return FixInt(~self._value)

        def __radd__(self, oth):
            return FixInt(self._extract_int(oth) + self._value)

        def __rsub__(self, oth):
            return FixInt(self._extract_int(oth) - self._value)

        def __rmul__(self, oth):
            return FixInt(self._extract_int(oth) * self._value)

        def __rfloordiv__(self, oth):
            return FixInt(self._extract_int(oth) // self._value)

        def __rmod__(self, oth):
            return FixInt(self._extract_int(oth) % self._value)

        def __rdivmod__(self, oth):
            div, mod = divmod(self._extract_int(oth), self._value)
            return FixInt(div), FixInt(mod)

        def __rpow__(self, oth):
            return FixInt(self._pow(self._extract_int(oth), self._value))

        def __iadd__(self, oth):
            self._mutate()
            self._value = self._fixval(self._value + self._extract_int(oth))
            return self

        def __isub__(self, oth):
            self._mutate()
            self._value = self._fixval(self._value - self._extract_int(oth))
            return self

        def __imul__(self, oth):
            self._mutate()
            self._value = self._fixval(self._value * self._extract_int(oth))
            return self

        def __ifloordiv__(self, oth):
            self._mutate()
            self._value = self._fixval(self._value // self._extract_int(oth))
            return self

        def __imod__(self, oth):
            self._mutate()
            self._value = self._fixval(self._value % self._extract_int(oth))
            return self

        def __ipow__(self, oth):
            self._mutate()
            self._value = self._fixval(self._pow(self._value, self._extract_int(oth)))
            return self

        def __neg__(self):
            return FixInt(-self._value)

        def __pos__(self):
            return FixInt(self._value)

        def __abs__(self):
            return FixInt(abs(self._value))

        def __complex__(self):
            return complex(self._value)

        def __int__(self):
            return int(self._value)

        def __float__(self):
            return float(self._value)

        def __index__(self):
            return self._value

        def __round__(self, ndigits=None):
            return FixInt(self._value)

        def __trunc__(self):
            return FixInt(self._value)

        def __floor__(self):
            return FixInt(self._value)

        def __ceil__(self):
            return FixInt(self._value)

        def __lt__(self, oth):
            return self._value < self._extract_int(oth)

        def __le__(self, oth):
            return self._value <= self._extract_int(oth)

        def __eq__(self, oth):
            return self._value == self._extract_int(oth)

        def __ne__(self, oth):
            return self._value != self._extract_int(oth)

        def __gt__(self, oth):
            return self._value > self._extract_int(oth)

        def __ge__(self, oth):
            return self._value >= self._extract_int(oth)

    FixInt.__name__ = FixInt.__qualname__ = "%sInt%s%s" % (
        "" if _signed else "U",
        _nbytes * 8,
        "M" if _mutable else "",
    )
    return FixInt


def int_factory(nbytes, signed, mutable):
    key = (nbytes, signed, mutable)
    if key in int_factory.cache:
        return int_factory.cache[key]
    cls = _int_factory_impl(nbytes, signed, mutable)
    int_factory.cache[key] = cls
    return cls


int_factory.cache = {}


UInt8  = int_factory(1, False, False)
UInt16 = int_factory(2, False, False)
UInt32 = int_factory(4, False, False)
UInt64 = int_factory(8, False, False)

Int8  = int_factory(1, True, False)
Int16 = int_factory(2, True, False)
Int32 = int_factory(4, True, False)
Int64 = int_factory(8, True, False)

UInt8M  = int_factory(1, False, True)
UInt16M = int_factory(2, False, True)
UInt32M = int_factory(4, False, True)
UInt64M = int_factory(8, False, True)

Int8M  = int_factory(1, True, True)
Int16M = int_factory(2, True, True)
Int32M = int_factory(4, True, True)
Int64M = int_factory(8, True, True)


def best_fit(n: int, signed, mutable=False):
    """
    Returns the best integer type to represent the given int
    """
    if n < 0 and not signed:
        raise TypeError("Cannot store negative value as unsigned integer")
    return int_factory(min_bytes(n), signed, mutable)(n)


def min_bytes(n: int):
    """
    Minimum number of buytes (rounded to the closest power of 2) needed to
    represent the number
    """
    if n == 0:
        return 1
    if n < 0:
        n = -n-1
    unaligned = math.floor(math.log2(n)/8 + 1)
    return 2**math.ceil(math.log2(math.ceil(unaligned)))
