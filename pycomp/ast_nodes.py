from .ast_types import Type
from . import ast_types
import typing


class DebugInfo:
    def __init__(self, filename, line, column=None, is_stmt=False):
        self.filename = filename
        self.line = line
        self.column = column
        self.is_stmt = is_stmt

    def wrap(self, node: "AstNode") -> "AstNode":
        node.debug_info = self
        return node

class NoDebugInfo:
    def wrap(self, node: "AstNode") -> "AstNode":
        return node


class AstNode:
    def __init__(self, datatype: Type):
        self.datatype = datatype
        self.debug_info = None

    @property
    def is_lvalue(self):
        return False

    @property
    def returns(self):
        """
        Whether the node has a return statement that will always be called
        """
        return False

    def findall(self, query):
        if query(self):
            return [self]
        return []


class BinaryNode(AstNode):
    def __init__(self, left: AstNode, right: AstNode, datatype: Type):
        super().__init__(datatype)
        self.left = left
        self.right = right

    def findall(self, query):
        return super().findall(query) + self.left.findall(query) + self.right.findall(query)


class UnaryNode(AstNode):
    def __init__(self, child: AstNode, datatype: Type):
        super().__init__(datatype)
        self.child = child

    def findall(self, query):
        return super().findall(query) + self.child.findall(query)


class TypeCast(UnaryNode):
    pass


class UnaryOp(UnaryNode):
    def __init__(self, op: str, child: AstNode, datatype: Type, lvalue=False):
        super().__init__(child, datatype)
        self.op = op
        self.lvalue = lvalue

    @property
    def is_lvalue(self):
        return self.lvalue


class BinaryOp(BinaryNode):
    def __init__(self, left: AstNode, op: str, right: AstNode, datatype: Type):
        super().__init__(left,  right, datatype)
        self.op = op


class Assign(BinaryOp):
    def __init__(self, left: AstNode, right: AstNode, datatype: Type):
        super().__init__(left, "=", right, datatype)

    @property
    def is_lvalue(self):
        return True


class VariableDeclaration(AstNode):
    def __init__(self, name: str, datatype: Type, value: typing.Optional[AstNode] = None):
        super().__init__(datatype)
        self.name = name
        self.value = value

    def findall(self, query):
        return (
            super().findall(query) +
            (self.value.findall(query) if self.value else [])
        )


class If(AstNode):
    def __init__(
        self,
        condition: AstNode,
        if_true: AstNode,
        if_false: typing.Optional[AstNode] = None,
        datatype: Type = ast_types.Void()
    ):
        super().__init__(datatype)
        self.condition = condition
        self.if_true = if_true
        self.if_false = if_false

    @property
    def returns(self):
        if not self.if_true.returns:
            return False

        if self.if_false:
            return self.if_false.returns

        return True

    def findall(self, query):
        return (
            super().findall(query) +
            self.if_true.findall(query) +
            (self.if_false.findall(query) if self.if_false else [])
        )


class While(AstNode):
    def __init__(
        self,
        condition: AstNode,
        block: AstNode,
        datatype: Type = ast_types.Void()
    ):
        super().__init__(datatype)
        self.condition = condition
        self.block = block

    @property
    def returns(self):
        # TODO this only holds if the condition is True at the start
        return self.block.returns

    def findall(self, query):
        return (
            super().findall(query) +
            self.block.findall(query)
        )


class Noop(AstNode):
    def __init__(self, datatype: Type=ast_types.Void()):
        super().__init__(datatype)


class StatementBlock(AstNode):
    def __init__(self, statements: typing.List[AstNode]=None, datatype: Type=ast_types.Void()):
        super().__init__(datatype)
        self.statements = statements or []

    def add(self, statement: AstNode):
        self.statements.append(statement)

    @property
    def returns(self):
        for s in self.statements:
            if s.returns:
                return True
        return False

    def findall(self, query):
        return super().findall(query) + sum(
            (c.findall(query) for c in self.statements),
            []
        )


class Program(StatementBlock):
    pass


class FunctionArgumentsPassed(AstNode):
    def __init__(self, args: typing.List[AstNode]=None, datatype: Type=ast_types.Void()):
        super().__init__(datatype)
        self.args = args or []

    def add(self, arg: AstNode):
        self.args.append(arg)

    def types(self) -> typing.List[Type]:
        return [
            arg.datatype
            for arg in self.args
        ]

    def findall(self, query):
        return super().findall(query) + sum(
            (c.findall(query) for c in self.args),
            []
        )


class FunctionCall(AstNode):
    def __init__(self, function: AstNode, arg_list: FunctionArgumentsPassed, datatype: Type):
        super().__init__(datatype)
        self.function = function
        self.arg_list = arg_list

    def findall(self, query):
        return super().findall(query) + self.function.findall(query) + self.arg_list.findall(query)


class VarArgs(AstNode):
    def __init__(self, datatype: Type=ast_types.VarArgs()):
        super().__init__(datatype)


class FunctionParameterDeclaration(AstNode):
    def __init__(self, params: typing.List[AstNode], datatype: Type=ast_types.Void()):
        super().__init__(datatype)
        self.params = params

    def add(self, param: AstNode):
        self.params.append(param)

    def types(self) -> typing.List[Type]:
        return [
            param.datatype
            for param in self.params
        ]

    def findall(self, query):
        return super().findall(query) + sum(
            (c.findall(query) for c in self.params),
            []
        )


class FunctionDeclaration(AstNode):
    def __init__(
        self,
        retval: Type,
        name: str,
        params: FunctionParameterDeclaration,
        body: typing.Optional[AstNode] = None
    ):
        super().__init__(ast_types.Function(retval, params.types()))
        self.name = name
        self.params = params
        self.body = body

    def findall(self, query):
        return super().findall(query) + (
            self.body.findall(query) if self.body else []
        )


class Literal(AstNode):
    def __init__(self, value, datatype: Type):
        super().__init__(datatype)
        self.value = value


class Return(UnaryNode):
    def __init__(self, value: AstNode, datatype: typing.Optional[Type]):
        super().__init__(value, datatype or value.datatype)

    @property
    def value(self):
        return self.child

    @property
    def returns(self):
        return True


class VariableReference(AstNode):
    def __init__(self, name: str, datatype: Type):
        super().__init__(datatype)
        self.name = name

    @property
    def is_lvalue(self):
        return True


class TypeDeclaration(AstNode):
    def __init__(self, name: str, datatype: Type):
        super().__init__(datatype)
        self.name = name


class MemberAccess(AstNode):
    def __init__(self, owner: AstNode, name: str, datatype: Type):
        super().__init__(datatype)
        self.owner = owner
        self.name = name

    @property
    def is_lvalue(self):
        return True

    def findall(self, query):
        return super().findall(query) + self.owner.findall(query)


class Break(AstNode):
    def __init__(self, datatype: Type=ast_types.Void()):
        super().__init__(datatype)


class Continue(AstNode):
    def __init__(self, datatype: Type=ast_types.Void()):
        super().__init__(datatype)
