import enum
import typing


class Type:
    def __repr__(self):
        return "<%s.%s %s>" % (self.__class__.__module__, self.__class__.__name__, self)

    def __str__(self):
        return self.__class__.__name__.lower()

    def __eq__(self, other: "Type") -> bool:
        return False


class Arithmetic(Type, enum.Enum):
    BOOL   = -1

    BYTE   = 0
    UBYTE  = 1
    WORD   = 2
    UWORD  = 3
    DWORD  = 4
    UDWORD = 5
    QWORD  = 6
    UQWORD = 7

    FLOAT  = 8

    @property
    def is_int(self):
        return self.value < self.FLOAT.value and self.value > self.BOOL.value

    @property
    def is_unsigned(self):
        return self.is_int and self.value % 2 == 1

    @property
    def is_signed(self):
        return self.is_int and self.value % 2 == 0

    def signed(self) -> "Arithmetic":
        if not self.is_int:
            raise TypeError("Cannot make unsigned float")
        if not self.is_unsigned:
            return self
        return Arithmetic(self.value - 1)

    def unsigned(self) -> "Arithmetic":
        if not self.is_int:
            raise TypeError("Cannot make unsigned float")
        if self.is_unsigned:
            return self
        return Arithmetic(self.value + 1)

    def signedness(self, signed: bool) -> "Arithmetic":
        if signed:
            return self.signed()
        return self.unsigned()

    def __str__(self):
        return self.name.lower()

    def __eq__(self, other: Type) -> bool:
        return isinstance(other, Arithmetic) and other.value == self.value

    def __hash__(self):
        return hash(self.value)


class Pointer(Type):
    def __init__(self, pointee: Type):
        self.pointee = pointee

    def __str__(self):
        return "%s*" % self.pointee

    def __eq__(self, other: Type) -> bool:
        return isinstance(other, Pointer) and other.pointee == self.pointee


class Void(Type):
    def __str__(self):
        return "void"

    def __eq__(self, other: Type) -> bool:
        return isinstance(other, Void)


class CompoundType(Type):
    def __init__(self, typename: str, name: str, scope: typing.Optional["Scope"] = None):
        self.typename = typename
        self.name = name
        if not scope:
            from .symbol_table import Scope
            scope = Scope()
        self.scope = scope

    def __str__(self):
        return "%s %s" % (self.typename, self.name)

    def __eq__(self, other: Type) -> bool:
        return isinstance(other, Struct) and other.name == self.name


class Struct(CompoundType):
    def __init__(self, name: str, scope: typing.Optional["Scope"] = None):
        super().__init__("struct", name, scope)


class PointerToMember(Pointer):
    def __init__(self, pointee: Type, owner: Type):
        super().__init__(pointee)
        self.owner = owner

    def __str__(self):
        return "%s::(*%s)" % (self.owner, self.pointee)

    def __eq__(self, other: Type) -> bool:
        return (
            isinstance(other, PointerToMember) and
            other.pointee == self.pointee and
            other.owner == self.owner
        )


class Function(Type):
    def __init__(self, retval: Type, args: typing.List[Type]):
        self.retval = retval
        self.args = args

    def __str__(self):
        return "%s (%s)" % (self.retval, ",".join(map(str, self.args)))

    def __eq__(self, other: Type) -> bool:
        return (
            isinstance(other, Function) and
            other.retval == self.retval and
            other.args == self.args
        )


class VarArgs(Type):
    def __str__(self):
        return "..."

    def __eq__(self, other: Type) -> bool:
        return isinstance(other, VarArgs)
