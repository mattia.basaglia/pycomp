import os
import inspect
import importlib.util


class Registry:
    def __init__(self, base_class, package, class_paths):
        self.base_class = base_class
        self.package = package
        self.class_paths = class_paths
        self._loaded = None

    def get_class(self, name):
        self._ensure_loaded()
        return self._loaded[name]

    def get_instance(self, name, *a, **kw):
        return self.get_class(name)(*a, **kw)

    def reload(self):
        self._loaded = {}
        for path in self.class_paths:
            for filename in os.listdir(path):
                if filename == "__init__.py" or filename[-3:] != ".py":
                    continue
                spec = importlib.util.spec_from_file_location(
                    self.package + "." + filename[:-3],
                    os.path.join(path, filename)
                )
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)
                for name, value in inspect.getmembers(module):
                    if inspect.isclass(value) and issubclass(value, self.base_class):
                        name = getattr(value, "name", None)
                        if name:
                            self._loaded[name] = value

    def _ensure_loaded(self):
        if self._loaded is None:
            self.reload()

    @property
    def class_dict(self):
        self._ensure_loaded()
        return self._loaded

    @property
    def keys(self):
        self._ensure_loaded()
        return self._loaded.keys()

    @property
    def values(self):
        self._ensure_loaded()
        return self._loaded.values()

    @property
    def instances(self, *a, **kw):
        return [
            cls(*a, **kw)
            for cls in self.values
        ]
