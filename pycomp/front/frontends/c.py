import typing
import argparse
from ..frontend import Frontend
from ..c import CLexer, CParser
from ... import symbol_table, ast_types
from ...autohandler import handler


class C(Frontend):
    name = "c"
    file_extensions = ["c", "C", "h", "H"]

    def __init__(self, ns: argparse.Namespace):
        super().__init__(ns)
        self.debug = ns.debug

    def parser(self, stream: typing.TextIO, typesizer: symbol_table.TypeSizer) -> CParser:
        return CParser(CLexer(stream), typesizer, self.debug)

    @classmethod
    def add_arguments(cls, parser):
        parser.add_argument(
            "-g",
            help="Output debug information",
            action="store_true",
            default=False,
            dest="debug"
        )

    def typesizer(self, sizer: symbol_table.TypeSizer) -> symbol_table.TypeSizer:
        return CTypeSizer(sizer)


class CTypeSizer(symbol_table.TypeSizerDecorator):
    @handler
    def sizeof_arith(self, type: ast_types.Void):
        return 1
