import typing
import argparse
from ..frontend import Frontend
from ...back import asm
from ...back.asm_flavours.gas import Gas
from ... import symbol_table, ast_types
from ...autohandler import handler
from ...back.arch.x86_64 import x86_64


class AsmX86_64Frontend(Frontend):
    name = "x86_64"
    file_extensions = ["s", "S", "asm"]

    def __init__(self, ns: argparse.Namespace):
        super().__init__(ns)
        self.flavour = ns.asm_flavour

    def parser(self, stream: typing.TextIO, typesizer: symbol_table.TypeSizer):
        return self.flavour.parser(stream, typesizer, x86_64)

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            "--asm",
            help="Assembly flavour",
            choices=asm.asm_flavour_registry.values,
            default=Gas,
            type=asm.asm_flavour_registry.get_class,
            dest="asm_flavour"
        )

    def typesizer(self, sizer: symbol_table.TypeSizer) -> symbol_table.TypeSizer:
        return symbol_table.FixedPointerSizer(
            symbol_table.ArithmeticSizer(
                sizer
            ),
            8
        )
