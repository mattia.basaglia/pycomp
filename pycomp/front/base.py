import io
import enum
import typing

from .. import ast_nodes
from .. import symbol_table


class TokenType(enum.Enum):
    EOF = 0
    KEYWORD = 1
    LITERAL = 2
    STRING = 3
    OPERATOR = 4
    COMMENT = 5


class Token:
    def __init__(self, type: TokenType, str, **kwargs):
        self.type = type
        self.str = str
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __str__(self):
        if self.type == TokenType.EOF:
            return "end of file"
        return "%s %s" % (self.type.name, self.str)

    def __repr__(self):
        return "<Token %s>" % self


class ParseError(Exception):
    def __init__(self, message, filename, line, column):
        super().__init__(message)
        self.filename = filename
        self.line = line
        self.column = column

    def __str__(self):
        return "%s:%s:%s: %s" % (
            self.filename, self.line, self.column, super().__str__()
        )


class StreamReader:
    def __init__(self, stream: typing.TextIO):
        self.stream = stream

    def get(self, n):
        return self.stream.read(n)

    def unget(self, c: str):
        # SEEK_CUR doesn't work...
        self.stream.seek(self.stream.tell()-len(c))

    def __getattr__(self, name):
        return getattr(self.stream, name)

    @staticmethod
    def wrap(stream):
        try:
            stream.seek(stream.tell())
            return StreamReader(stream)
        except io.UnsupportedOperation:
            return BufferedStreamReader(stream)


class BufferedStreamReader(StreamReader):
    def __init__(self, stream: typing.TextIO):
        super().__init__(stream)
        self._buffer = ""

    def unget(self, c: str):
        self._buffer += c

    def get(self, n):
        if self._buffer:
            chunk = self._buffer[:n]
            self._buffer = self._buffer[n:]
            if len(chunk) < n:
                chunk += self.stream.read(n + len(chunk))
            return chunk
        return self.stream.read(n)


class Lexer:
    def __init__(self, stream: typing.TextIO):
        self.stream = StreamReader.wrap(stream)
        self.line = 1
        self.column = 1

    def __iter__(self):
        while True:
            tok = self.next()
            yield tok
            if tok.type == TokenType.EOF:
                return

    def __next__(self) -> Token:
        return self.next()

    def next(self) -> Token:
        raise NotImplementedError()

    def _unget(self, c: str):
        if c:
            self.stream.unget(c)
            self.column -= len(c)
            if self.column <= 0:
                self.column = -1
                self.line -= 1

    def _get(self) -> str:
        c = self.stream.get(1)
        if c == '\n':
            self.line += 1
            self.column = 1
        else:
            self.column += len(c)
        return c

    def raise_error(self, message: str):
        raise ParseError(message, self.filename(), self.line, self.column)

    def filename(self):
        return getattr(self.stream, "name", "<input>")


class Parser:
    def __init__(self, lexer: Lexer, typesizer: symbol_table.TypeSizer):
        self.debug = False
        self.lexer = lexer
        self.typesizer = typesizer
        self.la = Token(TokenType.EOF, "")
        self.symbol_table = symbol_table.SymbolTable()

    def raise_error(self, message: str):
        self.lexer.raise_error(message)

    def raise_expected(self, what: str):
        self.raise_error("Expected %s but got %s" % (what, self.la))

    def parse(self) -> ast_nodes.Program:
        try:
            self._scan()
            return self._parse()
        except symbol_table.ScopeError as e:
            self.raise_error(str(e))

    def _scan(self) -> Token:
        self.la = self.lexer.next()
        return self.la

    def _parse(self) -> ast_nodes.Program:
        raise NotImplementedError()

    def _debug_info(self, is_stmt=False) -> ast_nodes.AstNode:
        if not self.debug:
            return ast_nodes.NoDebugInfo()
        return ast_nodes.DebugInfo(
            self.lexer.filename(),
            self.lexer.line,
            self.lexer.column - len(self.la.str),
            is_stmt
        )
