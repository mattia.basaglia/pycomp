import typing

from .base import Lexer, Token, TokenType, ParseError, Parser
from .. import ast_nodes as n
from .. import ast_types
from ..symbol_table import SymbolType, TypeSizer


class CLexer(Lexer):
    def __init__(self, stream: typing.TextIO, preserve_comments: bool=False):
        super().__init__(stream)
        self.preserve_comments = preserve_comments

    def next(self) -> Token:
        c = self._skipws()
        if c == '':
            return Token(TokenType.EOF, "")

        if c.isalpha():
            return Token(TokenType.KEYWORD, str=self._keyword(c))

        if c.isdigit():
            return self._number(c)

        if c == '"' or c == "'":
            return self._string(c)

        if c == '.':
            d = self._get()
            if d == '*':
                return Token(TokenType.OPERATOR, str=".*")
            elif d == '.':
                if not self._get() == ".":
                    self.raise_error("Invalid token")
                return Token(TokenType.OPERATOR, str="...")
            self._unget(d)
            if d.lower() == 'e' or d.isdecimal():
                return self._number(c)
            return Token(TokenType.OPERATOR, str=".")

        if c == '#':
            c = self._skipws()
            return Token(TokenType.KEYWORD, str=self._keyword(c), preprocessor=True)

        op = self._operator(c)
        if op == "//":
            comlit = ""
            while c and c != "\n":
                c = self._get()
                if self.preserve_comments and c != "\n":
                    comlit += c
            if self.preserve_comments:
                return Token(TokenType.COMMENT, str=comlit)
            return self.next()
        elif op == "/*":
            comlit = ""
            while True:
                c = self._get()
                if c == '*':
                    while c == '*':
                        c = self._get()
                        if self.preserve_comments:
                            comlit += c
                    if c == "/":
                        if self.preserve_comments:
                            comlit = comlit[:-1]
                            return Token(TokenType.COMMENT, str=comlit)
                        return self.next()
                elif not c:
                    self.raise_error("Unterminated comment")
                elif self.preserve_comments:
                    comlit += c
        else:
            return Token(TokenType.OPERATOR, str=op)

    def _skipws(self) -> str:
        c = ' '
        while c.isspace():
            c = self._get()
        return c

    def _keyword(self, c: str) -> str:
        result = ''
        while c.isalnum() or c == '_':
            result += c
            c = self._get()
        self._unget(c)
        return result

    def _operator(self, c: str) -> str:
        # ( ) , [ ] { }
        if c in "(),[]{};?:":
            return c

        # + - & | ++ -- && || += -= &= |=
        if c in "+-&|":
            n = self._get()
            if n == c or n == '=': # ++ or +=
                c += n
            elif n == '>' and c == '-': # ->
                s = self._get()
                if s != "*":
                    self._unget(s)
                else:
                    n += s
                c += n
            else: # +
                self._unget(n)
            return c

        # * / % = ! *= /= %= == != // /*
        if c in "*/%=!~^":
            n = self._get()
            if n == '=': # *=
                c += n
            elif c == '/' and n in "/*": # // /*
                c += n
            else: # *
                self._unget(n)
            return c

        # < > << >> <= >= <<= >>=
        if c in "<>":
            n = self._get()
            if n == c: # <<
                c += n
                n = self._get()
                if n == '=': # <<=
                    c += n
                else: # just <<
                    self._unget(n)
            elif n == '=': # <=
                c += n
            else: # <
                self._unget(n)
            return c

        self.raise_error("Unknown token %s" % c)

    def _number(self, c):
        type = ast_types.Arithmetic.DWORD
        if c == '0':
            return self._oct()

        lit = ''
        while c.isdecimal():
            lit += c
            c = self._get()

        if c.lower() == 'e' or c == '.':
            return self._float(lit, c)

        return Token(TokenType.LITERAL, str=lit, val=int(lit), valtype=self._int_suffix(c))

    def _oct(self) -> Token:
        c = self._get()
        if c.lower() == 'x':
            return self._hex()

        lit = '0'
        while c in set("01234567"):
            lit += c
            c = self._get()

        if c in set("89.eE"):
            return self._octfloat(lit, c)

        return Token(TokenType.LITERAL, str=lit, val=int(lit, 8), valtype=self._int_suffix(c))

    def _octfloat(self, lit: str, c: str) -> Token:
        while c.isdecimal():
            lit += c
            c = self._get()

        if c not in set(".eE"):
            self.raise_error("Invalid octal integer")
        return self._float(lit, c)

    def _hex(self) -> Token:
        lit = ''
        c = self._get()
        while c.isdecimal() or c.lower() in set("abcdef"):
            lit += c
            c = self._get()
        return Token(TokenType.LITERAL, str="0x" + lit, val=int(lit, 16), valtype=self._int_suffix(c))

    def _int_suffix(self, c: str) -> ast_types.Arithmetic:
        c = c.lower()
        type = ast_types.Arithmetic.DWORD

        if c == 'u':
            type = type.unsigned()
            c = self._get().lower()

        if c == 'l':
            type = ast_types.Arithmetic.DWORD.signedness(type.is_signed)
            c = self._get().lower()

        if c == 'l':
            type = ast_types.Arithmetic.QWORD.signedness(type.is_signed)
            c = self._get().lower()

        self._unget(c)

        return type

    def _float(self, lit: str, c: str) -> Token:
        if c == '.':
            lit += c
            c = self._get()
            while c.isdigit():
                lit += c
                c = self._get()

        if c.lower() == 'e':
            lit += c
            c = self._get()
            if c in "+-":
                lit += c
                c = self._get()
            if not c.isdecimal():
                self.raise_error("Invalid float literal %s" % lit)

            while c.isdecimal():
                lit += c
                c = self._get()

        return Token(TokenType.LITERAL, str=lit, val=float(lit), valtype=self._float_suffix(c))

    def _float_suffix(self, c: str) -> ast_types.Arithmetic:
        c = c.lower()
        type = ast_types.Arithmetic.FLOAT

        if c not in "fl":
            self._unget(c)

        return type

    def _string(self, terminator: str) -> Token:
        lit = ''
        while True:
            c = self._get()
            if c == terminator:
                break
            if not c:
                self.raise_error("Unterminated string literal")
            if c == '\\':
                lit += self._char_esc()
            else:
                lit += c
        return Token(TokenType.STRING, str=lit)

    def _char_esc(self):
        c = self._get()
        if c in "\\\'\"?":
            return c
        elif c == 'a':
            return '\a'
        elif c == 'b':
            return '\b'
        elif c == 'f':
            return '\f'
        elif c == 'n':
            return '\n'
        elif c == 'r':
            return '\r'
        elif c == 't':
            return '\t'
        elif c == 'v':
            return '\v'
        # TODO \x \0 \u \U
        else:
            self.raise_error("Unknown escape sequence: \%s" % c)


class CParser(Parser):
    types = {
        "char": ast_types.Arithmetic.BYTE,
        "int": ast_types.Arithmetic.DWORD,
        "short": ast_types.Arithmetic.WORD,
        "long": ast_types.Arithmetic.QWORD,
        "float": ast_types.Arithmetic.FLOAT,
        "double": ast_types.Arithmetic.FLOAT,
        "unsigned": ast_types.Arithmetic.UWORD,
        "signed": ast_types.Arithmetic.DWORD,
    }
    type_keywords_comp = {"struct", "union", "enum", "class"}
    type_keywords = set(types.keys()) | type_keywords_comp | {"void", "auto"}
    reserved_keywords = type_keywords | {
        # C
        "if", "else", "while", "do", "for", "sizeof",
        "auto", "volatile", "const", "register", "typedef", "goto", "asm",
        # C++
        "extern", "export", "decltype", "class", "template", "using",
        "typename", "namespace", "constexpr", "true", "false", "nullptr",
        "static_cast", "const_cast", "reinterpret_cast", "throw", "catch",
        "static_assert", "try", "new", "delete", "operator",
        "bool",
    }

    def __init__(self, lexer: Lexer, typesizer: TypeSizer, debug=False):
        super().__init__(lexer, typesizer)
        self.breakable = False
        self.debug = debug

    def _parse(self) -> n.Program:
        """
        PROGRAM := DECL | DECL PROGRAM | (empty)
        """
        root = self._debug_info().wrap(n.Program())
        while not self._is_eof():
            while self._is_op(";"):
                self._scan()
            di = self._debug_info()
            root.add(di.wrap(self._parse_decl(True)))
        return root

    def _is_type(self) -> bool:
        return self.la.type == TokenType.KEYWORD and (
            self.la.str in self.type_keywords or
            self.symbol_table.contains_expect(self.la.str, SymbolType.type)
        )

    def _is_op(self, op) -> bool:
        return self.la.type == TokenType.OPERATOR and self.la.str == op

    def _is_op_in(self, ops) -> bool:
        return self.la.type == TokenType.OPERATOR and self.la.str in ops

    def _is_kw(self, kw) -> bool:
        return self.la.type == TokenType.KEYWORD and self.la.str == kw

    def _is_eof(self) -> bool:
        return self.la.type == TokenType.EOF

    def _parse_decl(self, allow_definitions: bool=False) -> n.AstNode:
        """
        DECL := TYPE name ( DECL_FUNC | TYPE DECL_VARS ;
        """
        type = self._parse_type()
        if self.la.type != TokenType.KEYWORD or self.la.str in self.reserved_keywords:
            self.raise_expected("name")
        name = self.la.str
        self._scan()
        if self._is_op("("):
            self._scan()
            return self._parse_decl_func(type, name, allow_definitions)
        return self._parse_decl_vars(type, name)

    def _parse_decl_vars(self, type: ast_types.Type, name: str) -> n.AstNode:
        """
        VARS   := VAR | VAR ,  VARS
        VAR    := name OPTVAL
        OPTVAL := = EXPR | (empty)
        """
        decl = n.StatementBlock()

        while True:
            if self._is_op("="):
                self._scan()
                val = self._cast(self._parse_expr(), type)
                decl.add(self.symbol_table.declare(n.VariableDeclaration(name, type, val)))
            else:
                decl.add(self.symbol_table.declare(n.VariableDeclaration(name, type)))

            if self._is_op(","):
                self._scan()

                if self.la.type != TokenType.KEYWORD or self.la.str in self.reserved_keywords:
                    self.raise_expected("variable name")

                name = self.la.str
                self._scan()
            else:
                break

        return decl if len(decl.statements) != 1 else decl.statements[0]

    def _parse_decl_func(self, type: ast_types.Type, name: str, allow_body: bool) -> n.FunctionDeclaration:
        """
        DECL_FUNC     := ARG_LIST ) BODY
        DECL_ARG_LIST := ARG | ARG DECL_ARG_LIST | (empty)
        BODY          := ; | { BLOCK
        """
        self.symbol_table.push()
        params = n.FunctionParameterDeclaration([])
        while not self._is_eof():
            if self._is_op(")"):
                self._scan()
                break

            if self._is_op("..."):
                self._scan()
                params.add(n.VarArgs())
                if not self._is_op(")"):
                    self.raise_error("Ellipsis must be the last argument to a function")
                self._scan()
                break

            arg = self._parse_decl_arg()
            if arg.name:
                self.symbol_table.declare(arg)
            params.add(arg)

            if self._is_op(","):
                self._scan()

        if len(params.params) == 1 and params.params[0].datatype == ast_types.Void():
            params.params = []
            self.symbol_table.pop()
            self.symbol_table.push()

        if self._is_op(";"):
            self._scan()
            body = None
        elif not allow_body:
            self.raise_expected(";")
        else:
            body = self._parse_func_body(type)

        self.symbol_table.pop()

        return self.symbol_table.declare(n.FunctionDeclaration(type, name, params, body))

    def _parse_func_body(self, type: ast_types.Type) -> n.AstNode:
        if not self._is_op("{"):
            self.raise_expected("function body")
        self._scan()
        body = self._parse_block(type)
        if type != ast_types.Void() and not body.returns:
            self.raise_error("Misisng return expression")
        return body

    def _parse_decl_arg(self) -> n.VariableDeclaration:
        """
        DECL_ARG := TYPE NAME DEFAULT
        NAME     := name | (empty)
        DEFAULT  := = EXPR | (empty)
        """
        type = self._parse_type()
        if self._is_op_in(set(",)=")):
            name = ""
        elif self.la.type != TokenType.KEYWORD or self.la.str in self.reserved_keywords:
            self.raise_expected("argument name")
        else:
            name = self.la.str
            self._scan()
        if self._is_op("="):
            self._scan()
            value = self._cast(self._parse_expr(), type)
        else:
            value = None

        return n.VariableDeclaration(name, type, value)

    def _cast(self, node: n.AstNode, target: ast_types.Type) -> n.AstNode:
        if node.datatype == target:
            return node
        # TODO check compatible types
        return n.TypeCast(node, target)

    def _cast_binop_ptr(self, operand1: n.AstNode, op: str, operand2: n.AstNode):
        t1 = operand1.datatype
        t2 = operand2.datatype

        if isinstance(t1, ast_types.Pointer) and isinstance(t2, ast_types.Pointer):
            if t1.pointee != t2.pointee:
                self.raise_error("Operation between pointers to different types")
            if op != "-":
                return None
            return operand1, operand2, ast_types.Arithmetic.DWORD

        if isinstance(t1, ast_types.Arithmetic):
            result = t2
            operand1 = self._cast(operand1, ast_types.Arithmetic.DWORD)
        elif isinstance(t2, ast_types.Arithmetic):
            result = t1
            operand2 = self._cast(operand2, ast_types.Arithmetic.DWORD)
        else:
            return None

        if op not in "+-":
            return None

        return operand1, operand2, result

    def _cast_binop(self, operand1: n.AstNode, op: str, operand2: n.AstNode):
        t1 = operand1.datatype
        t2 = operand2.datatype
        if isinstance(t1, ast_types.Pointer) or isinstance(t2, ast_types.Pointer):
            ptrcast = self._cast_binop_ptr(operand1, op, operand2)
            if ptrcast is not None:
                return ptrcast
        elif isinstance(t1, ast_types.Arithmetic) and isinstance(t2, ast_types.Arithmetic):
            # TODO float shouldn't work with << etc
            if t1.value == t2.value:
                return operand1, operand2, operand1.datatype
            elif t1.value < t2.value:
                return n.TypeCast(operand1, t2), operand2, operand2.datatype
            else:
                return operand1, n.TypeCast(operand2, t1), operand1.datatype

        self.raise_error("Invalid operation %s %s %s" % (t1, op, t2))

    def _binary_op(self, operand1, op, operand2):
        operand1, operand2, result = self._cast_binop(operand1, op, operand2)
        return n.BinaryOp(operand1, op, operand2, result)

    def _parse_type(self) -> ast_types.Type:
        """
        TYPE := PTR
        PTR := BASIC |  PTR *
        SUBPTR := void | arithmetic | COMPOUND name
        COMPOUND := struct | union | enum
        TODO register, cv, auto, pointer to member, pointer to function
        """
        type = self._parse_type_subptr()
        while self._is_op("*"):
            type = ast_types.Pointer(type)
            self._scan()
        return type

    def _parse_type_subptr(self) -> ast_types.Type:
        if self.la.type == TokenType.KEYWORD:
            if self.la.str in self.types:
                return self._parse_type_arithmetic()
            elif self.la.str == "void":
                self._scan()
                return ast_types.Void()
            elif self.la.str in self.type_keywords_comp:
                self._scan()
                if self.la.type == TokenType.KEYWORD:
                    type = self.symbol_table.get_expect(
                        self.la.str, SymbolType.type
                    ).type
                    # TODO check that it matches eg: `struct foo`, `foo` is a struct
                    self._scan()
                    return type
            else:
                type = self.symbol_table.get_expect(self.la.str, SymbolType.type).type
                self._scan()
                return type
        self.raise_expected("type")

    def _parse_type_arithmetic(self) -> ast_types.Arithmetic:
        """
        [signed|unsigned] ([long [long]|short] int|char)
        float
        double
        long double
        """
        if self.la.str == "unsigned":
            self._scan()
            type = self._parse_type_int(ast_types.Arithmetic.UDWORD)
        elif self.la.str == "signed":
            self._scan()
            type = self._parse_type_int(ast_types.Arithmetic.DWORD)
        elif self.la.str == "long":
            self._scan()
            if self.la.type == TokenType.KEYWORD and self.la.str == 'double':
                type = ast_types.Arithmetic.FLOAT
                self._scan()
            else:
                type = self._parse_type_long(ast_types.Arithmetic.DWORD)
        elif self.la.str in {"float", "double", "int", "char"}:
            type = self.types[self.la.str]
            self._scan()
        else:
            type = self._parse_type_int(ast_types.Arithmetic.DWORD)

        return ast_types.Arithmetic(type)

    def _parse_type_int(self, type: ast_types.Arithmetic) -> ast_types.Arithmetic:
        if self.la.type != TokenType.KEYWORD:
            return type # *

        if self.la.str == "long":
            self._scan()
            type = ast_types.Arithmetic.DWORD.signedness(type.is_signed)
            return self._parse_type_long(type) # * long *
        elif self.la.str == "short":
            self._scan()
            if self.la.type == TokenType.KEYWORD and self.la.str == "int":
                self._scan()
            return ast_types.Arithmetic.WORD.signedness(type.is_signed) # * short [int]
        elif self.la.str == "int":
            self._scan()
            return type # * int
        elif self.la.str == "char":
            self._scan()
            return ast_types.Arithmetic.BYTE.signedness(type.is_signed) # * char

        return type

    def _parse_type_long(self, type: ast_types.Arithmetic) -> ast_types.Arithmetic:
        if self.la.type != TokenType.KEYWORD:
            return type # * long
        if self.la.str == "long":
            type = ast_types.Arithmetic.QWORD.signedness(type.is_signed)
            self._scan()
        if self.la.type != TokenType.KEYWORD:
            return type # * long long
        if self.la.str == "int":
            self._scan()
        return type # * long [long] [int]

    def _parse_block(self, return_type: ast_types.Type) -> n.AstNode:
        """
        BLOCK := STMTS }
        STMTS := STMT | STMT STMTS | (empty)
        """
        block = n.StatementBlock()
        while True:
            if self._is_op("}"):
                break
            elif self._is_eof():
                self.raise_expected("}")
            block.add(self._parse_stmt(return_type))
        self._scan()
        if not block.statements:
            return n.Noop()
        if len(block.statements) == 1:
            return block.statements[0]
        return block

    def _parse_stmt(self, return_type: ast_types.Type) -> n.AstNode:
        di = self._debug_info()
        return di.wrap(self._parse_stmt_wrapped(return_type))

    def _parse_stmt_wrapped(self, return_type: ast_types.Type) -> n.AstNode:
        """
        STMT := if STMT_IF | (lookah: type) DECL ; | EXPR ; | return OPTEXPR ; | ; | { BLOCK
        OPTEXPR := EXPR | (empty)
        """
        if self._is_op(";"):
            self._scan()
            return n.Noop()

        if self._is_op("{"):
            self._scan()
            return self._parse_block(return_type)

        if self._is_kw("if"):
            self._scan()
            return self._parse_stmt_if(return_type)

        if self._is_kw("while"):
            self._scan()
            return self._parse_stmt_while(return_type)

        if self._is_kw("for"):
            self._scan()
            return self._parse_stmt_for(return_type)

        if self._is_kw("break"):
            if not self.breakable:
                self.raise_error("Cannot use `break` outside a loop")
            self._scan()
            return n.Break()

        if self._is_kw("continue"):
            if not self.breakable:
                self.raise_error("Cannot use `continue` outside a loop")
            self._scan()
            return n.Continue()

        if self._is_type():
            stmt = self._parse_decl()
        elif self._is_kw("return"):
            self._scan()
            if self._is_op(";"):
                if return_type != ast_types.Void():
                    self.raise_error("Returning void from function returning %s" % return_type)
                ret = n.Noop()
            else:
                ret = self._cast(self._parse_expr(), return_type)
            stmt = n.Return(ret, return_type)
        else:
            stmt = self._parse_expr()

        if not self._is_op(";"):
            self.raise_expected(";")
        self._scan()
        return stmt

    def _parse_stmt_if(self, return_type: ast_types.Type) -> n.If:
        """
        STMT_IF := ( expr ) STMT OPTELSE
        OPTELSE := else STMT | (empty)
        """
        if not self._is_op("("):
            self.raise_expected("(")
        self._scan()
        condition = self._cast(self._parse_expr(), ast_types.Arithmetic.BOOL)
        self._close_bracket()
        if_true = self._parse_stmt(return_type)
        if self._is_kw("else"):
            self._scan()
            if_false = self._parse_stmt(return_type)
        else:
            if_false = None
        return n.If(condition, if_true, if_false)

    def _parse_stmt_while(self, return_type: ast_types.Type) -> n.While:
        """
        STMT_WHILE := ( expr ) STMT
        """
        if not self._is_op("("):
            self.raise_expected("(")
        self._scan()
        condition = self._cast(self._parse_expr(), ast_types.Arithmetic.BOOL)
        self._close_bracket()
        b = self.breakable
        self.breakable = True
        body = self._parse_stmt(return_type)
        self.breakable = b
        return n.While(condition, body)

    def _parse_stmt_for(self, return_type: ast_types.Type) -> n.While:
        """
        STMT_FOR := ( STMT_FOR_INIT ; STMT_FOR_EXPR ; STMT_FOR_EXPR ) STMT
        STMT_FOR_INIT := DECL | EXPR | (empty)
        STMT_FOR_EXPR := EXPR | (empty)
        """
        self.symbol_table.push()
        if not self._is_op("("):
            self.raise_expected("(")
        self._scan()

        init = self._parse_stmt_for_init()

        if not self._is_op(";"):
            self.raise_expected(";")
        self._scan()

        condition = self._cast(
            self._parse_stmt_for_expr(n.Literal(True, ast_types.Arithmetic.BOOL)),
            ast_types.Arithmetic.BOOL
        )

        if not self._is_op(";"):
            self.raise_expected(";")
        self._scan()

        step = self._parse_stmt_for_expr()

        self._close_bracket()

        b = self.breakable
        self.breakable = True
        body = self._parse_stmt(return_type)
        self.breakable = b
        self.symbol_table.pop()

        if not isinstance(step, n.Noop):
            if isinstance(body, n.StatementBlock):
                body.add(step)
            elif isinstance(body, n.Noop):
                body = step
            else:
                body = n.StatementBlock([body, step])

        main = n.While(condition, body)

        if not isinstance(init, n.Noop):
            main = n.StatementBlock([init, main])

        return main

    def _parse_stmt_for_init(self) -> n.AstNode:
        if self._is_type():
            return self._parse_decl()
        elif self._is_op(";"):
            return n.Noop()
        return self._parse_expr()

    def _parse_stmt_for_expr(self, default: n.AstNode=n.Noop()) -> n.AstNode:
        if self._is_op_in(set(";)")):
            return default
        return self._parse_expr()

    def _parse_expr(self) -> n.AstNode:
        """
        TODO expr, expr

        EXPR := EX_ASSIGN

        EX_ASSIGN := LVAL (= += -= ...) EX_ASSIGN
                  |  EX_LOGIC ? EX_LOGIC : EX_LOGIC
                  |  EX_LOGIC

        EX_LOGIC:= EX_L_OR
        EX_L_OR := EX_L_AND | EX_L_AND "||" EX_L_AND
        EX_L_AND:= EX_BIT | EX_BIT && EX_BIT

        EX_BIT  := EX_B_OR
        EX_B_OR := EX_B_AND | EX_B_AND "|" EX_B_AND
        EX_B_XOR:= EX_B_AND | EX_B_AND ^ EX_B_AND
        EX_B_AND:= EX_CMPE | EX_CMPE & EX_CMPE

        EX_CMPE := EX_CMPO | EX_CMPO (== !=) EX_CMPO
        EX_CMPO := EX_SHIF | EX_SHIF (< <= > >=) EX_SHIF

        EX_SHIF := EX_ADD | EX_ADD (<< >>) EX_ADD
        EX_ADD  := EX_MUL | EX_MUL (+ -) EX_MUL
        EX_MUL  := EX_PTR | EX_PTR (* / %) EX_PTR
        EX_PTR  := EX_UNARY | EX_UNARY (.* ->*) EX_UNARY
        EX_UNARY:=  EX_TOP
                | ++ EX_TOP  | -- EX_TOP | + EX_TOP | - EX_TOP
                | ! EX_TOP | ~ EX_TOP
                | ( TYPE ) EX_TOP
                | * EX_TOP | & EX_TOP
                | sizeof EX_UNARY
        EX_TOP  := EX_PRIM
                | EX_TOP++ | EX_TOP--
                | TYPE ( EXPR )
                | EX_TOP ( ARGS )
                | EX_TOP [ EXPR ]
                | EX_TOP . EX_TOP
                | EX_TOP -> EX_TOP
        EX_PRIM := name | ( EXPR ) | literal | ( TYPE ) EX_TOP
        """
        return self._parse_ex_assign()

    def _require_lval(self, value: n.AstNode):
        if not value.is_lvalue:
            self.raise_error("Expected l-value")

    def _parse_ex_assign(self) -> n.AstNode:
        operand = self._parse_ex_logic()
        ops = {"+=", "-=", "*=", "/=", "%=", "<<=", ">>=", "&=", "^=", "|="}
        if self._is_op("="):
            self._require_lval(operand)
            self._scan()
            operand2 = self._cast(self._parse_ex_assign(), operand.datatype)
            return n.Assign(operand, operand2, operand.datatype)
        elif self._is_op_in(ops):
            self._require_lval(operand)
            op = self.la.str
            self._scan()
            operand2 = self._binary_op(operand, op[:-1], self._parse_ex_assign())
            return n.Assign(operand, operand2, operand.datatype)
        elif self._is_op("?"):
            self._scan()
            operand = self._cast(operand, ast_types.Arithmetic.BOOL)
            operand2 = self._parse_ex_assign()
            if not self._is_op(":"):
                self.raise_expected(":")
            self._scan()
            operand3 = self._parse_ex_assign()
            if not operand2.datatype == operand3.datatype:
                self.raise_error("Operands of ? : must be the same type")
            return n.If(operand, operand2, operand3, operand2.datatype)
        else:
            return operand

    def _parse_ex_logic(self) -> n.AstNode:
        return self._parse_ex_l_or()

    def _parse_ex_binary(self, level_up, ops) -> n.AstNode:
        operand = level_up()
        while self._is_op_in(ops):
            op = self.la.str
            self._scan()
            operand2 = level_up()
            operand = self._binary_op(operand, op, operand2)
        return operand

    def _parse_ex_l_or(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_l_and,
            {"||"}
        )

    def _parse_ex_l_and(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_b_or,
            {"&&"}
        )

    def _parse_ex_b_or(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_b_xor,
            {"|"}
        )

    def _parse_ex_b_xor(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_b_and,
            {"^"}
        )

    def _parse_ex_b_and(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_cmpe,
            {"&"}
        )

    def _parse_ex_cmpe(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_cmpo,
            {"==", "!="}
        )

    def _parse_ex_cmpo(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_shift,
            {"<", ">", "<=", ">="}
        )

    def _parse_ex_shift(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_add,
            {"<<", ">>"}
        )

    def _parse_ex_add(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_mul,
            {"+", "-"}
        )

    def _parse_ex_mul(self) -> n.AstNode:
        return self._parse_ex_binary(
            self._parse_ex_ptrmem,
            {"*", "/", "%"}
        )

    def _parse_ex_ptrmem(self) -> n.AstNode:
        operand = self._parse_ex_unary()
        ops = {".*", "->*"}
        while self._is_op_in(ops):
            op = self.la.str
            self._scan()
            if op == "->*":
                operand =  self._ex_deref(operand)
            operand2 = self._parse_ex_unary()
            if (
                not isinstance(operand2.datatype, ast_types.PointerToMember) or
                operand2.datatype.owner != operand.datatype
            ):
                self.raise_error("Expected pointer to member of %s" % operand.datatype)
            operand = n.BinaryOp(operand, ".*", operand2, operand2.datatype.pointee)
        return operand

    def _ex_deref(self, operand: n.AstNode) -> n.AstNode:
        if not isinstance(operand.datatype, ast_types.Pointer):
            self.raise_error("Dereferencing non-pointer")
        return n.UnaryOp("*", operand, operand.datatype.pointee, True)

    def _parse_ex_unary(self) -> n.AstNode:
        """
        EX_UNARY:=  EX_TOP
                | ++ EX_TOP  | -- EX_TOP | + EX_TOP | - EX_TOP
                | ! EX_TOP | ~ EX_TOP
                | * EX_TOP | & EX_TOP
                | sizeof EX_UNARY
        """
        if self._is_op("*"):
            self._scan()
            operand = self._parse_ex_unary()
            return self._ex_deref(operand)
        elif self._is_op_in({"++", "--"}):
            op = self.la.str
            self._scan()
            operand = self._parse_ex_unary()
            self._require_lval(operand)
            return n.UnaryOp(op, operand, operand.datatype, True)
        elif self._is_op("!"):
            op = self.la.str
            self._scan()
            operand = self._cast(self._parse_ex_unary(), ast_types.Arithmetic.BOOL)
            return n.UnaryOp(op, operand, operand.datatype)
        elif self._is_op("&"):
            op = self.la.str
            self._scan()
            operand = self._parse_ex_unary()
            self._require_lval(operand)
            return n.UnaryOp(op, operand, ast_types.Pointer(operand.datatype))
        elif self._is_op_in({"+", "-", "~"}):
            # TODO check for invalid operations
            op = self.la.str
            self._scan()
            operand = self._parse_ex_unary()
            return n.UnaryOp(op, operand, operand.datatype)
        elif self._is_kw("sizeof"):
            self._scan()
            if self._is_op("("):
                self._scan()
                if self._is_type():
                    type = self._parse_type()
                else:
                    type = self._parse_expr().datatype
                self._close_bracket()
            else:
                type = self._parse_ex_unary().datatype
            return n.Literal(self.typesizer.sizeof(type), self.types["long"].unsigned())

        return self._parse_ex_top()

    def _is_callable(self, obj: ast_types.Type) -> typing.Optional[ast_types.Function]:
        if isinstance(obj, ast_types.Pointer):
            obj = obj.pointee
        if isinstance(obj, ast_types.Function):
            return obj
        return None

    def _parse_ex_member(self, owner: n.AstNode) -> n.MemberAccess:
        if not isinstance(owner.datatype, ast_types.CompoundType):
            self.raise_error("%s is not a compound type" % owner.datatype)
        if self.la.type != TokenType.KEYWORD:
            self.raise_expected("member of %s" % owner)
        name = self.la.str
        sym = owner.datatype.scope.get(name)
        self._scan()
        return n.MemberAccess(owner, name, sym.type)

    def _close_bracket(self):
        if not self._is_op(")"):
            self.raise_expected(")")
        self._scan()

    def _parse_ex_top(self) -> n.AstNode:
        """
        EX_TOP  := EX_PRIM
                | TYPE ( EXPR )
                | EX_TOP++ | EX_TOP--
                | EX_TOP . EX_TOP
                | EX_TOP -> EX_TOP
                | EX_TOP ( ARGS )
                | EX_TOP [ EXPR ]
        """
        if self._is_type():
            type = self._parse_type()
            if not self._is_op("("):
                self.raise_expected("(")
            self._scan()
            operand = self._parse_expr()
            self._close_bracket()
            operand = self._cast(operand, type)
        else:
            operand = self._parse_ex_prim()

        while True:
            if self._is_op_in({"++", "--"}):
                self._require_lval(operand)
                op = self.la.str
                self._scan()
                operand = n.UnaryOp(op + "2", operand, operand.datatype)
            elif self._is_op_in({".", "->"}):
                if self.la.str == "->":
                    operand = self._ex_deref(operand)
                self._scan()
                operand = self._parse_ex_member(operand)
            elif self._is_op("["):
                self._scan()
                operand2 = self._parse_expr()
                if not self._is_op("]"):
                    self.raise_expected("]")
                operand = self._binary_op(operand, "+", operand2)
                self._scan()
                operand = n.UnaryOp("*", operand, operand.datatype.pointee)
            elif self._is_op("("):
                callable_info = self._is_callable(operand.datatype)
                if not callable_info:
                    self.raise_error("Function call on non-function")
                self._scan()
                args = self._parse_args(callable_info.args)
                operand = n.FunctionCall(operand, args, callable_info.retval)
            else:
                return operand

    def _parse_ex_prim(self) -> n.AstNode:
        if self._is_op("("):
            self._scan()
            if self._is_type():
                type = self._parse_type()
                self._close_bracket()
                return self._cast(self._parse_ex_top(), type)
            operand = self._parse_expr()
            self._close_bracket()
            return operand

        if self.la.type == TokenType.KEYWORD and self.la.str not in self.reserved_keywords:
            name = self.la.str
            sym = self.symbol_table.get(name)
            if sym.what != SymbolType.variable and sym.what != SymbolType.function:
                self.raise_error("%s must be a variable or a function" % name)
            self._scan()
            return n.VariableReference(name, sym.type)

        if self.la.type == TokenType.LITERAL:
            lit = self.la
            self._scan()
            return n.Literal(lit.val, ast_types.Arithmetic(lit.valtype))

        if self.la.type == TokenType.STRING:
            lit = self.la
            self._scan()
            return n.Literal(lit.str, ast_types.Pointer(ast_types.Arithmetic.BYTE))

        self.raise_expected("primary expression")

    def _parse_args(self, argtypes: typing.List[ast_types.Type]) -> n.FunctionArgumentsPassed:
        """
        ARGS := EX_ASSIGN | EX_ASSIGN, ARGS | (empty)
        """
        args = n.FunctionArgumentsPassed()
        argtypes = list(argtypes)
        varargs = False
        while not self._is_op(")"):
            argval = self._parse_ex_assign()

            if not varargs:
                if not argtypes:
                    self.raise_error("Too many arguments")
                argtype = argtypes.pop(0)
                if isinstance(argtype, ast_types.VarArgs):
                    varargs = True
                    args.add(argval)
                else:
                    args.add(self._cast(argval, argtype))
            else:
                args.add(argval)

            if self._is_op(","):
                self._scan()
            else:
                break
        if argtypes and not isinstance(argtypes[0], ast_types.VarArgs):
            self.raise_error("Missing %s arguments" % len(argtypes))
        self._close_bracket()
        return args
