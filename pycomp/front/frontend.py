import os
import typing
import argparse

from .base import Parser
from ..class_registry import Registry
from ..symbol_table import TypeSizer


class Frontend:
    name = None
    file_extensions = []

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser):
        pass

    def __init__(self, ns: argparse.Namespace):
        pass

    def parser(self, stream: typing.TextIO) -> Parser:
        raise NotImplementedError()

    def typesizer(self, sizer: TypeSizer) -> TypeSizer:
        return sizer


class FrontendRegistry(Registry):
    def __init__(self):
        super().__init__(
            Frontend,
            "pycomp.front.frontends",
            [os.path.join(os.path.dirname(__file__), "frontends")]
        )

    def from_file(self, filename):
        ext = os.path.splitext(filename)[-1].lstrip(".")
        for fe in self.values:
            if ext in fe.file_extensions:
                return fe


frontend_registry = FrontendRegistry()
get_frontend = frontend_registry.get_class
