import io
import os
import typing
import argparse

from .base import Generator
from ..class_registry import Registry
from ..import ast_types
from ..symbol_table import TypeSizer


class Backend:
    name = None
    mode = "w"
    file_extensions = []

    def open_file(self, path, func=open):
        return func(path, self.mode)

    def open_buffer(self, bytesf=io.BytesIO, stringf=io.StringIO):
        if "b" in self.mode:
            return bytesf()
        return stringf()

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser):
        pass

    def __init__(self, ns: argparse.Namespace):
        pass

    def generator(self, stream: typing.IO, typesizer: TypeSizer) -> Generator:
        raise NotImplementedError()

    def sizeof(self, type: ast_types.Type):
        """
        sizeof for basic types
        """
        if isinstance(type, ast_types.Arithmetic):
            return Generator.sizeof(type)
        elif isinstance(type, ast_types.Pointer):
            return Generator.sizeof_ptr(type)
        else:
            return None

    def typesizer(self, sizer: TypeSizer) -> TypeSizer:
        return sizer


class FrontendBackendRegistry(Registry):
    def __init__(self):
        super().__init__(
            Backend,
            "pycomp.back.backends",
            [os.path.join(os.path.dirname(__file__), "backends")]
        )

    def from_file(self, filename):
        ext = os.path.splitext(filename)[-1].lstrip(".")
        for fe in self.values:
            if ext in fe.file_extensions:
                return fe

    @property
    def default(self):
        return self.get_class("x86_64")


backend_registry = FrontendBackendRegistry()
get_backend = backend_registry.get_class
