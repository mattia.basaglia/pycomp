import math
import typing
from . import c_api
from ... import fixint


class Data:
    container = bytes
    value_type = int
    virtual = False

    def __init__(self, header, data=None):
        self.data = data if data is not None else self.container()
        self.headers = [header] if header is not None else []

    def adjust_size(self):
        for header in self.headers:
            header.adjust_size()

    def read(self, header, stream, **kwargs):
        if header.header_data_size:
            curpos = stream.tell()
            stream.seek(header.data_offset)
            self.read_raw(stream, header.header_data_size, **kwargs)
            stream.seek(curpos)

    def read_raw(self, stream, size, **kwargs):
        self.data = stream.read(int(size))

    def __len__(self):
        return len(self.data)

    def write(self, stream):
        stream.write(self.data)

    def append(self, data):
        self.data += bytes(data)
        self.adjust_size()

    def replace(self, data):
        self.data = bytes(data)
        self.adjust_size()

    def __iter__(self):
        return iter(self.data)


class ListData(Data):
    container = list
    value_type = c_api.Struct

    def __len__(self):
        return len(self.data) * self.value_type.sizeof

    def read_raw(self, stream, size, **kwargs):
        self.data = [
            self.value_type(**kwargs).read(stream)
            for i in range(0, size, self.value_type.sizeof)
        ]

    def write(self, stream):
        for obj in self.data:
            obj.write(stream)

    def append(self, data):
        self.data.append(data)
        self.adjust_size()

    @classmethod
    def template(cls, contained, _virtual=False):
        class Derived(cls):
            value_type = contained
            virtual = _virtual
        return Derived

    def insert(self, pos, value):
        self.data.insert(pos, value)
        self.adjust_size()

    def pop(self, value):
        v = self.data.pop(value)
        self.adjust_size()
        return v

    def index(self, value):
        return self.data.index(value)

    def __contains__(self, item):
        return item in self.data


class DataHeader:
    data_type = Data
    header_type = c_api.Struct

    def __init__(self, elffile, header, data=None):
        self.elffile = elffile
        self.header = header
        if isinstance(data, Data):
            data.headers.append(self)
            self._data = data
        else:
            self._data = self.data_type(self, data)
            if data is not None:
                self.adjust_size()

    @property
    def data(self):
        return self._data

    @classmethod
    def from_stream(cls, elffile, stream):
        header = cls.header_type.from_stream(stream)
        cls = cls.select_subclass(header)
        obj = cls(elffile, header)
        for data in elffile.data:
            if data.headers[0].data_offset == obj.data_offset:
                obj._data = data
                data.headers.append(obj)
                break
        else:
            obj.data.read(obj, stream, **obj.read_data_kwargs())
        obj._on_read()
        return obj

    def _on_read(self):
        pass

    def read_data_kwargs(self):
        return {}

    @classmethod
    def select_subclass(cls, header: header_type):
        return cls

    def write_header(self, stream):
        self.header.write(stream)

    def write_data(self, stream):
        self.data.write(stream)

    @property
    def data_offset(self):
        raise NotImplementedError()

    @property
    def header_data_size(self):
        raise NotImplementedError()

    def adjust_size(self):
        raise NotImplementedError()


class Section(DataHeader):
    header_type = c_api.Elf64_Shdr

    @property
    def name(self):
        if self.elffile.shstrtab:
            return self.elffile.shstrtab.get_string_at(self.header.sh_name)
        return b""

    @classmethod
    def select_subclass(cls, header: header_type):
        if header.sh_type == c_api.SectionHeaderType.STRTAB:
            cls = StringTable
        elif header.sh_type == c_api.SectionHeaderType.SYMTAB:
            cls = SymbolTable
        elif header.sh_type == c_api.SectionHeaderType.RELA:
            cls = RelocationTable
        elif header.sh_type == c_api.SectionHeaderType.REL:
            cls = RelocationTableOld
        elif header.sh_type == c_api.SectionHeaderType.DYNAMIC:
            cls = DynamicSection
        elif header.sh_type == c_api.SectionHeaderType.NOTE:
            cls = NoteSection
        return cls

    @property
    def linked(self):
        return self.elffile.sections[self.header.sh_link]

    def __repr__(self):
        name = self.name
        if not name:
            name = self.header.sh_type.name
        elif not isinstance(name, str):
            name = name.decode("utf-8")
        return "<Section %s>" % name

    @property
    def data_offset(self):
        return self.header.sh_offset

    @property
    def header_data_size(self):
        return self.header.sh_size

    def adjust_size(self):
        self.header.sh_size = len(self._data)


class StringTable(Section):
    def get_string_at(self, offset):
        return self.data.data[offset:self.data.data.find(b'\0', offset)]

    def _on_read(self):
        if self.elffile.header.e_shstrndx == len(self.elffile.sections):
            self.elffile.shstrtab = self

    def add_string(self, string):
        ret = len(self.data)
        self.data.append(bytes(string) + b'\0')
        return ret


class Symbol(c_api.Elf64_Sy):
    class TempRel:
        def __init__(self, section, instr_offset, imm_offset):
            self.section = section
            self.offset = instr_offset
            self.imm_offset = imm_offset

    def __init__(self, elffile):
        super().__init__()
        self.elffile = elffile
        self.temp_relocations = []

    @property
    def name(self):
        if self.elffile.symtab and self.elffile.symtab.linked:
            strtab = self.elffile.symtab.linked
            return strtab.get_string_at(self.st_name)

    @property
    def section(self):
        try:
            return self.elffile.sections[self.st_shndx]
        except IndexError:
            if self.st_shndx in [x.value for x in c_api.SpecialSections.__members__.values()]:
                return None
            raise

    def __repr__(self):
        return "<Symbol %s %s %s>" % (self.name.decode("utf8"), self.st_info.type.name, self.st_info.bind.name)


class SymbolTable(Section):
    # sh_info must be kept as the index of the first non-local
    data_type = ListData.template(Symbol)

    def _on_read(self):
        self.elffile.symtab = self

    def read_data_kwargs(self):
        return {"elffile": self.elffile}

    def add_symbol(self, symbol: Symbol):
        if symbol.st_info.bind == c_api.SymbolTableBinding.LOCAL:
            self.data.insert(self.header.sh_info, symbol)
            self.header.sh_info += 1
        else:
            self.data.append(symbol)

        self.header.sh_size = len(self.data)

    def rm_symbol(self, symbol: Symbol):
        self.data.pop(self.data.index(symbol))
        if symbol.st_info.bind == c_api.SymbolTableBinding.LOCAL:
            self.header.sh_info -= 1

    def find_symbol(self, name):
        if self.elffile.symtab and self.elffile.symtab.linked:
            for sym in self.data:
                if sym.name == name:
                    return sym
        return None


class RelocationTable(Section):
    class Mixin:
        def __init__(self, section):
            super().__init__()
            self.section = section

        @property
        def symbol(self):
            return self.section.linked.data[self.r_info.sym]

    class Rela(Mixin, c_api.Elf64_Rela):
        pass

    class Rel(Mixin, c_api.Elf64_Rel):
        pass

    data_type = ListData.template(Rela)

    @property
    def affected_section(self):
        return self.elffile.sections[self.header.sh_info]

    def read_data_kwargs(self):
        return {"section": self}


class DynamicSection(Section):
    data_type = ListData.template(c_api.Elf64_Dyn)


class RelocationTableOld(RelocationTable):
    data_type = ListData.template(RelocationTable.Rel)


class Note(c_api.Elf64_Nhdr):
    def __init__(self, elffile):
        super().__init__()
        if elffile.header.e_type == c_api.Type.CORE:
            self.mode = c_api.Elf64_Nhdr.ReadValueMode.Core
        else:
            self.mode = c_api.Elf64_Nhdr.ReadValueMode.GNU
        self.value = None

    def read(self, io, big_endian=False):
        super().read(io, big_endian)
        self.value = self.read_value(io, self.mode)
        return self

    @property
    def sizeof(self):
        return self.sizeof_with_data


class NoteData(ListData.template(Note)):
    def read_raw(self, stream, size, **kwargs):
        self.data = []
        i = 0
        while i < size:
            val = self.value_type(**kwargs).read(stream)
            i += val.sizeof


class NoteSection(Section):
    data_type = NoteData

    def read_data_kwargs(self):
        return {"elffile": self.elffile}


class Program(DataHeader):
    header_type = c_api.Elf64_Phdr

    def __repr__(self):
        return "<Program %s %08x %08x>" % (self.header.p_type.name, self.header.p_offset, self.header.p_filesz)

    @property
    def data_offset(self):
        return self.header.p_offset

    @property
    def header_data_size(self):
        return self.header.p_filesz

    def adjust_size(self):
        self.header.p_filesz = len(self._data)
        self.header.p_memsz = max(self.header.p_memsz, self.header.p_filesz)

    @classmethod
    def select_subclass(cls, header: header_type):
        if header.p_type == c_api.ProgramType.NOTE:
            cls = NoteProgram
        elif header.p_type == c_api.ProgramType.DYNAMIC:
            cls = DynamicProgram
        elif header.p_type == c_api.ProgramType.PHDR:
            cls = ProgramHeaderTable
        return cls


class NoteProgram(Program):
    data_type = NoteData

    def read_data_kwargs(self):
        return {"elffile": self.elffile}


class DynamicProgram(Program):
    data_type = ListData.template(c_api.Elf64_Dyn)


class ProgramHeaderTable(Program):
    class data_type(ListData.template(c_api.Elf64_Phdr, True)):
        def read_raw(self, stream, size, elffile):
            self.data = elffile.programs

    def read_data_kwargs(self):
        return {"elffile": self.elffile}




class ElfFile:
    def __init__(self, e_type: c_api.Type=c_api.Type.NONE):
        self.header = c_api.Elf64_Ehdr(e_type)
        self.programs = []
        self.sections = []
        self.shstrtab = None
        self.symtab = None
        self.data = []

    @classmethod
    def from_file(cls, stream):
        elffile = cls()
        elffile.header = c_api.Elf64_Ehdr.from_stream(stream)

        if elffile.header.e_phoff:
            stream.seek(elffile.header.e_phoff)
            for i in range(elffile.header.e_phnum):
                program = Program.from_stream(elffile, stream)
                elffile.programs.append(program)
                if program.data not in elffile.data:
                    elffile.data.append(program.data)

        if elffile.header.e_shoff:
            stream.seek(elffile.header.e_shoff)
            for i in range(elffile.header.e_shnum):
                section = Section.from_stream(elffile, stream)
                elffile.sections.append(section)
                if section.data not in elffile.data:
                    elffile.data.append(section.data)

        # TODO sections that overlap with programs shall share the Data object

        return elffile

    def write(self, stream):
        self.header.write(stream)
        parts = sorted([
            (self.header.e_phoff, 1, self._write_progs),
            (self.header.e_shoff, 2, self._write_secs),
            (self._data_off(), 3, self._write_data)
        ])
        for off, _, func in parts:
            if off > stream.tell():
                stream.write(b'\0' * int(off - stream.tell()))
            func(stream)

    def _data_off(self):
        if self.data and self.data[0].headers[0].data_offset:
            return self.data[0].headers[0].data_offset
        return max(self.header.e_shoff, self.header.e_phoff) + 1

    def _write_progs(self, stream):
        for prog in self.programs:
            prog.write_header(stream)

    def _write_secs(self, stream):
        for section in self.sections:
            section.write_header(stream)

    def _write_data(self, stream):
        for data in self.data:
            if data.virtual:
                continue
            if data.headers[0].data_offset and data.headers[0].data_offset > stream.tell():
                stream.write(b'\0' * int(data.headers[0].data_offset - stream.tell()))
            data.write(stream)

    def find_section(self, name) -> Section:
        if not self.shstrtab:
            return None

        if isinstance(name, str):
            name = name.encode("utf-8")

        for section in self.sections:
            if section.name == name:
                return section
        return None

    def add_program(self, type: c_api.ProgramType, flags: c_api.Elf64_Phdr.Flags, data=None, cls=Program):
        header = c_api.Elf64_Phdr()
        header.p_type = type
        header.p_flags = flags
        prog = cls(self, header)
        self._on_add_program(prog)
        return prog

    def _on_add_program(self, program: Program):
        self.programs.append(program)
        if program.data not in self.data:
            self.data.append(program.data)
        self.header.e_phnum = len(self.programs)

    def add_section(self, type, name, cls=Section, data=None):
        header = c_api.Elf64_Shdr()
        sec = cls(self, header, data)
        if self.shstrtab:
            header.sh_name = self.shstrtab.add_string(name)
        header.sh_type = type
        self._on_add_section(sec)
        return sec

    def _on_add_section(self, section):
        self.sections.append(section)
        if section.data not in self.data:
            self.data.append(section.data)
        self.header.e_shnum = len(self.sections)

    def add_symbol(
        self,
        name,
        section=None,
        value: typing.Optional[c_api.types64.ElfN_Addr]=None,
        info: typing.Optional[c_api.SymbolTableInfo]=None,
        visibility: typing.Optional[c_api.SymbolTableVisibility]=None,
    ):
        sym = None
        if isinstance(name, Symbol):
            sym = name
            name = None
            add = sym not in self.symtab.data
        elif name:
            sym = self.symtab.find_symbol(name)
            add = sym is None
            if sym and info and info.bind != sym.st_info.bind:
                self.symtab.rm_symbol(sym)
                add = True

        if not sym:
            sym = Symbol(self)
            add = True

        if isinstance(section, Section):
            for i, s in enumerate(self.sections):
                if s is section:
                    sym.st_shndx = i
        elif isinstance(section, c_api.SpecialSections):
            sym.st_shndx = section.value
        elif section is not None:
            raise TypeError()

        if name:
            sym.st_name = self.symtab.linked.add_string(name)

        if info is not None:
            sym.st_info = info

        if value is not None:
            sym.st_value = value

        if visibility is not None:
            sym.st_other = visibility

        if add:
            self.symtab.add_symbol(sym)

        return sym

    @property
    def interpreter(self):
        for p in self.programs:
            if p.header.p_type == c_api.ProgramType.INTERP:
                return p.data.lstrip(b'\0').decode("utf-8")
        return None

    def data_offset(self, data: Data, start=0):
        off = start
        for d in self.data:
            if d is data:
                break
            if not data.virtual:
                off += len(d)
        return off

    def data_offsets(self, start=0):
        off = int(start)
        mapping = {}
        for d in self.data:
            if d.virtual:
                pass
            mapping[d] = off
            off += len(d)
        return mapping


class OutElfFile(ElfFile):
    def adjust_offsets(self):
        # Structure     size
        # Header        0
        # [progheads]   e_ehsize
        # [secheads]    e_shnum * e_shentsize
        # [secdata]     sum(data_len for section)

        prog_heads = fixint.UInt64(self.header.e_ehsize)
        sec_heads = prog_heads + self.header.e_phnum * self.header.e_phentsize
        data_start = sec_heads + self.header.e_shnum * self.header.e_shentsize
        data_offs = self.data_offsets(data_start)

        if self.programs:
            self.header.e_phoff = prog_heads
            base_addr = 0x400000 # TODO move this value to the x86_64 arch definition
            vaddr = base_addr
            found_loadable = False
            for program in self.programs:
                if program.header.p_type == c_api.ProgramType.PHDR:
                    vaddr = base_addr + self.header.e_phoff
                    program.header.p_offset = prog_heads
                    program.header.p_filesz = program.header.p_memsz = sec_heads - prog_heads
                elif not found_loadable and program.header.p_type == c_api.ProgramType.LOAD:
                    found_loadable = True
                    vaddr = base_addr
                else:
                    program.header.p_offset = data_offs[program.data]

                if program.header.p_align > 1:
                    vaddr = int(math.ceil(vaddr / program.header.p_align) * program.header.p_align)
                program.header.p_paddr = vaddr
                vaddr += program.header.p_memsz
                program.header.p_vaddr = program.header.p_paddr
        else:
            self.header.e_phoff = 0

        if self.sections:
            self.header.e_shoff = sec_heads
            for section in self.sections:
                section.header.sh_offset = data_offs[section.data]
                ## Align progbits section for my own sanity
                #if (
                    #section.header.sh_size and
                    #section.header.sh_type == c_api.SectionHeaderType.PROGBITS and
                    #sech_data % 16
                #):
                    #sech_data += 16 - sech_data % 16
                #sech_data += section.data_len
        else:
            self.header.e_shoff = 0

    def write(self, stream):
        self.adjust_offsets()
        super().write(stream)


class OutElfFileRel(OutElfFile):
    """
    ElifFile but assumed to be using for writing new files rather than reading existing ones
    """
    def __init__(self):
        super().__init__(c_api.Type.REL)
        self._init_nulsection()
        self._init_shstrtab()
        self.text = self._init_text()
        self.data_section = self._init_data()
        self.rodata = self._init_rodata()
        self._init_symtab()

    def _init_nulsection(self):
        header = c_api.Elf64_Shdr()
        sec = Section(self, header)
        self._on_add_section(sec)
        return sec

    def _init_shstrtab(self):
        header = c_api.Elf64_Shdr()
        sec = StringTable(self, header, b"\0")
        header.sh_name = sec.add_string(b".shstrtab")
        header.sh_type = c_api.SectionHeaderType.STRTAB
        self.shstrtab = sec
        self.header.e_shstrndx = len(self.sections)
        self._on_add_section(sec)
        return sec

    def _init_symtab(self):
        self.symtab = self.add_section(
            c_api.SectionHeaderType.SYMTAB, b".symtab", SymbolTable
        )
        self.symtab.header.sh_entsize = Symbol.sizeof
        self.symtab.header.sh_addralign = 8
        # Link to the next strtab
        self.symtab.header.sh_link = len(self.sections)
        # One greater than the symbol table index of the last local symbol
        self.symtab.header.sh_info = 0
        strtab = self.add_section(c_api.SectionHeaderType.STRTAB, b".strtab", StringTable)
        strtab.add_string(b"")
        return self.symtab

    def _init_text(self):
        sec = self.add_section(c_api.SectionHeaderType.PROGBITS, b".text")
        sec.header.sh_flags = c_api.Elf64_Shdr.EXECINSTR | c_api.Elf64_Shdr.ALLOC
        return sec

    def _init_data(self):
        sec = self.add_section(c_api.SectionHeaderType.PROGBITS, b".data")
        sec.header.sh_flags = c_api.Elf64_Shdr.WRITE | c_api.Elf64_Shdr.ALLOC
        return sec

    def _init_rodata(self):
        sec = self.add_section(c_api.SectionHeaderType.PROGBITS, b".rodata")
        sec.header.sh_flags = c_api.Elf64_Shdr.ALLOC
        return sec

    def create_rela_section(self, base: Section, force=False):
        data = RelocationTable.data_type.container()
        for i, sym in enumerate(self.symtab.data):
            for trel in sym.temp_relocations:
                if trel.section is base:
                    if sym.st_shndx != c_api.SpecialSections.UNDEF.value and sym.section is base:
                        offstart = trel.offset + trel.imm_offset
                        offend = offstart + fixint.Int32.nbytes
                        base.data.data = (
                            base.data.data[:offstart] +
                            fixint.Int32(fixint.Int32(sym.st_value) - offend).to_bytes() +
                            base.data.data[offend:]
                        )
                    else:
                        rela = c_api.Elf64_Rela()
                        rela.r_offset = trel.offset + trel.imm_offset
                        rel_type = c_api.x86_64Relocations._32S
                        if not sym.st_shndx:
                            rel_type = c_api.x86_64Relocations.PLT32
                            rela.r_addend = -4
                        rela.r_info = c_api.Elf64RelocationInfo(i, rel_type)
                        data.append(rela)

        if data or force:
            sec = self.add_section(
                c_api.SectionHeaderType.RELA,
                b".rela" + base.name,
                RelocationTable,
                data
            )
            sec.header.sh_info = self.sections.index(base)
            sec.header.sh_entsize = c_api.Elf64_Rela.sizeof
            sec.header.sh_link = self.sections.index(self.symtab)
            sec.header.sh_flags = c_api.Elf64_Shdr.ALLOC
            sec.adjust_size()
            return sec
        return None


class OutElfFileExec(OutElfFile):
    """
    ElifFile but assumed to be using for writing new files rather than reading existing ones
    """
    def __init__(self):
        super().__init__(c_api.Type.EXEC)
        self.add_program(
            c_api.ProgramType.PHDR,
            c_api.Elf64_Phdr.Flags.PF_R,
            ProgramHeaderTable.data_type(None, self.programs),
            ProgramHeaderTable
        )

    def set_interpreter(self, name):
        for p in self.programs:
            if p.header.p_type == c_api.ProgramType.INTERP:
                break
        else:
            p = self.add_program(c_api.ProgramType.INTERP, c_api.Elf64_Phdr.Flags.PF_R)

        if isinstance(name, str):
            name = name.encode("utf-8")

        p.data.replace(name+b"\0")
