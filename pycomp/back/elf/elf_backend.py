from .. import asm
from ... import ast_nodes as n
from ... import ast_types as t
from . import elf_file, c_api, linker
from ..arch.x86_64.opcode_table import x86_64OpcodeTable
from ..arch.assembler import OpcodeTable


class ElfRel(asm.Formatter):
    symbols = {}

    def __init__(self, stream, opcode_table: OpcodeTable):
        super().__init__(stream)
        self.opcode_table = opcode_table

    def print_op(self, op: asm.Operation):
        try:
            instruction = self.opcode_table.instruction(op)
            self.handle_relocations(instruction, op.op1)
            self.handle_relocations(instruction, op.op2)
            self.current_section.data.append(instruction)
        except KeyError:
            self.current_section.data.append(
                b'[%s]' % str(self.opcode_table.descriptor(op)).encode("utf-8")
            )
            #raise Exception("Invalid instruction: %s" % self.ot.descriptor(op))

    def handle_relocations(self, inst, operand: asm.Operand):
        if isinstance(operand, asm.Address):
            operand = operand.child
        if isinstance(operand, asm.Label):
            if operand.name not in self.symbols:
                sym = elf_file.Symbol(self.elf)
                self.symbols[operand.name] = sym
            else:
                sym = self.symbols[operand.name]
            pos = len(self.current_section.data)
            sym.temp_relocations.append(elf_file.Symbol.TempRel(self.current_section, pos, inst.relocation_offset()))

    def initialize_file(self):
        self.elf = elf_file.OutElfFileRel()
        self.current_section = self.elf.text

    def finalize_file(self):
        self.elf.create_rela_section(self.elf.text)
        self.elf.write(self.stream)

    def debug_info_file(self, filename: str, fileno: int):
        stype = c_api.SymbolTableType.FILE
        if (not self.elf.symtab.data or self.elf.symtab.data[0].st_info.type == stype) and fileno == 1:
            info = c_api.SymbolTableInfo(stype, c_api.SymbolTableBinding.LOCAL)
            self.elf.add_symbol(filename.encode("utf-8"), c_api.SpecialSections.ABS, info=info)

    def debug_info_line(self, info: n.DebugInfo, fileno: int):
        # TODO
        pass

    def section(self, name):
        self.current_section = self.elf.find_section(name.encode("utf-8"))
        if not self.current_section:
            type = c_api.SectionHeaderType.PROGBITS # TODO other types
            self.current_section = self.elf.add_section(type, name)

    def ident(self, text):
        comment = self.elf.find_section(b".comment")
        if not comment:
            comment = self.elf.add_section(c_api.SectionHeaderType.PROGBITS, b".comment")
            comment.header.sh_flags = c_api.Elf64_Shdr.MERGE | c_api.Elf64_Shdr.STRINGS
            comment.data.append(b"\0")
        comment.data.append(b"%s\0" % text.encode("utf-8"))

    def define_global(self, name):
        stype = c_api.SymbolTableType.NOTYPE
        info = c_api.SymbolTableInfo(stype, c_api.SymbolTableBinding.GLOBAL)
        return self.add_symbol(name, self.current_section, info=info)

    def define_extern(self, name):
        stype = c_api.SymbolTableType.NOTYPE
        info = c_api.SymbolTableInfo(stype, c_api.SymbolTableBinding.GLOBAL)
        return self.add_symbol(name, c_api.SpecialSections.UNDEF, info=info)

    def define_type(self, name, type):
        stype = c_api.SymbolTableType.NOTYPE
        if type == "function":
            stype = c_api.SymbolTableType.FUNC
        info = c_api.SymbolTableInfo(stype, c_api.SymbolTableBinding.GLOBAL)
        return self.add_symbol(name, info=info)

    def data(self, type: t.Arithmetic, value):
        if type == t.Arithmetic.BYTE:
            if isinstance(value, str):
                value = value.encode("utf-8")
            self.current_section.data.append(value)

    def place_label(self, name):
        info = None
        if name.startswith(".L"):
            info = c_api.SymbolTableInfo(c_api.SymbolTableType.SECTION, c_api.SymbolTableBinding.LOCAL)
        value = len(self.current_section.data)
        return self.add_symbol(name, self.current_section, info=info, value=value)

    def add_symbol(self, name, *a, **kw):
        if name in self.symbols:
            return self.elf.add_symbol(self.symbols[name], *a, **kw)
        encname = name.encode("utf-8")
        if encname.startswith(b".L"):
            encname = b""
        sym = self.elf.add_symbol(encname, *a, **kw)
        self.symbols[name] = sym
        return sym

    def empty_line(self):
        pass


class ElfExec(ElfRel):
    def finalize_file(self):
        self.elf.create_rela_section(self.elf.text)
        linked_elf = linker.Linker().link([self.elf])
        linked_elf.write(self.stream)

