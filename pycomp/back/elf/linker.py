import re
import os
import subprocess
from . import elf_file, c_api


class Linker:
    def __init__(self):
        self.load_settings_from_gnu()

    def load_settings_from_gnu(self):
        ls_conf = subprocess.check_output(["ld", "--verbose"]).decode("utf-8")

        # TODO find "ld.so" programmatically if possible
        self.interpreter = "/lib64/ld-linux-x86-64.so.2"

        self.library_path = [
            y.group(1) for y in re.finditer(r'SEARCH_DIR\("=(([^"]|\\")*)"\);', ls_conf)
        ] + list(filter(bool, os.environ.get("LD_LIBRARY_PATH", "").split(os.pathsep)))

    def link(self, elfs):
        if len(elfs) == 1 and elfs[0].header.e_type == c_api.Type.EXEC:
            return elfs[0]
        output = elf_file.OutElfFileExec()
        output.set_interpreter(self.interpreter)

        return output
