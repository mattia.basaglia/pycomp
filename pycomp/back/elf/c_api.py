import enum
import collections

from ... import fixint


class CharEnum(enum.Enum):
    def __bytes__(self):
        return bytes([self.value])


class IntEnumBase(enum.Enum):
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            other = self.__class__(other)
        return super().__eq__(other)


def IntEnum(type):
    class Base(IntEnumBase):
        @property
        def value(self):
            return type(super().value)

    Base.int_type = type

    return Base


class FlagBase:
    int_type = None

    def __init__(self, value):
        if isinstance(value, FlagBase):
            self.value = value.value
        else:
            self.value = self.int_type(value)

    def __int__(self):
        return self.value

    def __repr__(self):
        return repr(self.value)

    def __eq__(self, other):
        if isinstance(other, FlagBase):
            other = other.value
        return self.value == other


class FlagEnumBase(FlagBase):
    values = {}

    def __str__(self):
        vals = []
        remain = self.value
        for n, v in self.values.items():
            if self.value & v == v:
                vals.append(n)
                remain &= ~v

        if not vals:
            return "0"

        out = "|".join(vals)
        if remain:
            out += "|%s" % remain
        return out


def FlagType(_type):
    class Base(FlagBase):
        int_type = _type

    return Base


def FlagEnum(_type, _values={}):
    class Base(FlagEnumBase):
        int_type = _type
        values = _values

    for n, v in _values.items():
        setattr(Base, n, v)
    return Base


class FixedBytes:
    def __init__(self, length, data=b''):
        self.length = length

    def __call__(self, data):
        data = bytes(data)
        if len(data) > self.length:
            raise TypeError("String too long (should be <= %s)" % self.length)
        data += b'\0' * (self.length - len(data))
        return data


class StructMeta(type):
    def __new__(cls, name, bases, dct):
        if "fields" in dct:
            fields = collections.OrderedDict(dct["fields"])
            dct["fields"] = fields
            if "sizeof" not in dct:
                sizeof = StructMeta.sizeof(fields)
                dct["sizeof"] = sizeof
            for fname, ftype in fields.items():
                dct[fname] = StructMeta.mkprop(fname, ftype)

        return type.__new__(cls, name, bases, dct)

    @staticmethod
    def sizeof_type(v):
        if isinstance(v, FixedBytes):
            return v.length
        elif issubclass(v, IntEnumBase) or issubclass(v, FlagBase):
            return v.int_type.nbytes
        elif issubclass(v, Struct):
            return v.sizeof
        elif issubclass(v, fixint.FixintBase):
            return v.nbytes
        raise TypeError("Unsupported field type")

    @staticmethod
    def sizeof(fields):
        return sum(map(StructMeta.sizeof_type, fields.values()))

    @staticmethod
    def mkprop(name, ftype):
        intname = "_%s" % name
        def setter(self, value):
            return setattr(self, intname, ftype(value))
        def getter(self):
            return getattr(self, intname)
        return property(getter, setter)

    @staticmethod
    def read_field(io, type, big_endian):
        if isinstance(type, FixedBytes):
            return io.read(type.length)
        elif issubclass(type, IntEnumBase) or issubclass(type, FlagBase):
            return type(StructMeta.read_fixint(io, type.int_type, big_endian))
        elif issubclass(type, Struct):
            return type().read(io, big_endian)
        elif issubclass(type, fixint.FixintBase):
            return StructMeta.read_fixint(io, type, big_endian)
        raise TypeError("Unsupported field type: %s" % type)

    @staticmethod
    def read_fixint(io, type, big_endian):
        endianness = fixint.Endianness.Big if big_endian else fixint.Endianness.Little
        return type.from_bytes(io.read(type.nbytes), endianness)

    @staticmethod
    def write_field(io, value, big_endian):
        if isinstance(value, bytes):
            return io.write(value)
        elif isinstance(value, (IntEnumBase, FlagBase)):
            return StructMeta.write_fixint(io, value.value, big_endian)
        elif isinstance(value, Struct):
            return value.write(io, big_endian)
        elif isinstance(value, fixint.FixintBase):
            return StructMeta.write_fixint(io, value, big_endian)
        raise TypeError("Unsupported field type")

    @staticmethod
    def write_fixint(io, value, big_endian):
        endianness = fixint.Endianness.Big if big_endian else fixint.Endianness.Little
        io.write(value.to_bytes(endianness))


class Struct(metaclass=StructMeta):
    @classmethod
    def from_stream(cls, io, big_endian=False):
        return cls().read(io, big_endian)

    def read(self, io, big_endian=False):
        for name, type in self.fields.items():
            setattr(self, name, StructMeta.read_field(io, type, big_endian))
        return self

    def write(self, io, big_endian=False):
        for name, type in self.fields.items():
            value = getattr(self, name)
            StructMeta.write_field(io, value, big_endian)

    def assign(self, other):
        if type(other) != type(self):
            raise TypeError("Assigning structs of different types")

        for name in self.fields.keys():
            setattr(self, name, getattr(other, name))

    def attrdict(self):
        return {
            name: getattr(self, "_" + name)
            for name in self.fields.keys()
        }


class Class(CharEnum):
    NONE = 0
    CLASS32 = 1
    CLASS64 = 2


class Data(CharEnum):
    NONE = 0
    _2LBS = 1
    _2MSB = 2


class OsABI(CharEnum):
    NONE = 0
    LINUX = 3


class Type(IntEnum(fixint.UInt16)):
    NONE = 0
    REL = 1
    EXEC = 2
    DYN = 3
    CORE = 4
    LOPROC = 0xff00
    HIPROC = 0xffff


class Machine(IntEnum(fixint.UInt16)):
    X86_64 = 62


class Version(IntEnum(fixint.UInt32)):
    NONE = 0
    CURRENT = 1


class SpecialSections(IntEnum(fixint.UInt16)):
    UNDEF = 0 # Undefined section
    LORESERVE = 0xff00 # Start of reserved indices
    LOPROC = 0xff00 # Start of processor-specific
    BEFORE = 0xff00 # Order section before all others (Solaris).
    AFTER = 0xff01 # Order section after all others (Solaris).
    HIPROC = 0xff1f # End of processor-specific
    LOOS = 0xff20 # Start of OS-specific
    HIOS = 0xff3f # End of OS-specific
    ABS = 0xfff1 # Associated symbol is absolute
    COMMON = 0xfff2 # Associated symbol is common
    XINDEX = 0xffff # Index is in extra table.
    HIRESERVE = 0xffff # End of reserved indices


class ProgramType(IntEnum(fixint.UInt32)):
    NULL = 0 # Program header table entry unused
    LOAD = 1 # Loadable program segment
    DYNAMIC = 2 # Dynamic linking information
    INTERP = 3 # Program interpreter
    NOTE = 4 # Auxiliary information
    SHLIB = 5 # Reserved
    PHDR = 6 # Entry for header table itself
    TLS = 7 # Thread-local storage segment
    NUM = 8 # Number of defined types
    LOOS = 0x60000000 # Start of OS-specific
    GNU_EH_FRAME = 0x6474e550 # GCC .eh_frame_hdr segment
    GNU_STACK = 0x6474e551 # Indicates stack executability
    GNU_RELRO = 0x6474e552 # Read-only after relocation
    LOSUNW = 0x6ffffffa #
    SUNWBSS = 0x6ffffffa # Sun Specific segment
    SUNWSTACK = 0x6ffffffb # Stack segment
    HISUNW = 0x6fffffff #
    HIOS = 0x6fffffff # End of OS-specific
    LOPROC = 0x70000000 # Start of processor-specific
    HIPROC = 0x7fffffff # End of processor-specific


class SectionHeaderType(IntEnum(fixint.UInt32)):
    NULL = 0 # Section header table entry unused
    PROGBITS = 1 # Program data
    SYMTAB = 2 # Symbol table
    STRTAB = 3 # String table
    RELA = 4 # Relocation entries with addends
    HASH = 5 # Symbol hash table
    DYNAMIC = 6 # Dynamic linking information
    NOTE = 7 # Notes
    NOBITS = 8 # Program space with no data (bss)
    REL = 9 # Relocation entries, no addends
    SHLIB = 10 # Reserved
    DYNSYM = 11 # Dynamic linker symbol table
    INIT_ARRAY = 14 # Array of constructors
    FINI_ARRAY = 15 # Array of destructors
    PREINIT_ARRAY = 16 # Array of pre-constructors
    GROUP = 17 # Section group
    SYMTAB_SHNDX = 18 # Extended section indeces
    NUM = 19 # Number of defined types.
    LOOS = 0x60000000 # Start OS-specific.
    GNU_ATTRIBUTES = 0x6ffffff5 # Object attributes.
    GNU_HASH = 0x6ffffff6 # GNU-style hash table.
    GNU_LIBLIST = 0x6ffffff7 # Prelink library list
    CHECKSUM = 0x6ffffff8 # Checksum for DSO content.
    LOSUNW = 0x6ffffffa # Sun-specific low bound.
    SUNW_move = 0x6ffffffa
    SUNW_COMDAT = 0x6ffffffb
    SUNW_syminfo = 0x6ffffffc
    GNU_verdef = 0x6ffffffd # Version definition section.
    GNU_verneed = 0x6ffffffe # Version needs section.
    GNU_versym = 0x6fffffff # Version symbol table.
    HISUNW = 0x6fffffff # Sun-specific high bound.
    HIOS = 0x6fffffff # End OS-specific type
    LOPROC = 0x70000000 # Start of processor-specific
    HIPROC = 0x7fffffff # End of processor-specific
    LOUSER = 0x80000000 # Start of application-specific
    HIUSER = 0x8fffffff # End of application-specific


class SymbolTableType(IntEnum(fixint.UInt8)):
    NOTYPE = 0 # Symbol type is unspecified
    OBJECT = 1 # Symbol is a data object
    FUNC = 2 # Symbol is a code object
    SECTION = 3 # Symbol associated with a section
    FILE = 4 # Symbol's name is file name
    COMMON = 5 # Symbol is a common data object
    TLS = 6 # Symbol is thread-local data object
    NUM = 7 # Number of defined types.
    LOOS = 10 # Start of OS-specific
    GNU_IFUNC = 10 # Symbol is indirect code object
    HIOS = 12 # End of OS-specific
    LOPROC = 13 # Start of processor-specific
    HIPROC = 15 # End of processor-specific


class SymbolTableBinding(IntEnum(fixint.UInt8)):
    LOCAL = 0 # Local symbol
    GLOBAL = 1 # Global symbol
    WEAK = 2 # Weak symbol
    NUM = 3 # Number of defined types.
    LOOS = 10 # Start of OS-specific
    GNU_UNIQUE = 10 # Unique symbol.
    HIOS = 12 # End of OS-specific
    LOPROC = 13 # Start of processor-specific
    HIPROC = 15 # End of processor-specific


class SymbolTableInfo(FlagType(fixint.UInt8)):
    def __init__(
        self,
        type=None,
        bind=None,
    ):
        if isinstance(type, SymbolTableType) and isinstance(bind, SymbolTableBinding):
            super().__init__((bind.value << 4) | type.value & 0xf)
        elif type is None and bind is None:
            super().__init__(0)
        elif isinstance(type, (int, fixint.UInt8)) and bind is None:
            super().__init__(type)
        elif isinstance(type, SymbolTableInfo) and bind is None:
            super().__init__(type.value)
        else:
            raise TypeError("Wrong types for SymbolTableInfo.__init__")

    @property
    def type(self):
        return SymbolTableType(int(self.value & 0xf))

    @property
    def bind(self):
        return SymbolTableBinding(int(self.value >> 4))


class x86_64Relocations(IntEnum(fixint.UInt32)):
    # A     The addend used to compute the value of the relocatable field.
    # B     The base address at which a shared object is loaded into memory during execution. Generally, a shared object file is built with a base virtual address of 0. However, the execution address of the shared object is different. See Program Header.
    # G     The offset into the global offset table at which the address of the relocation entry's symbol resides during execution. See Global Offset Table (Processor-Specific).
    # GOT   The address of the global offset table. See Global Offset Table (Processor-Specific).
    # L     The section offset or address of the procedure linkage table entry for a symbol. See Procedure Linkage Table (Processor-Specific).
    # P     The section offset or address of the storage unit being relocated, computed using r_offset.
    # S     The value of the symbol whose index resides in the relocation entry.
    # Z     The size of the symbol whose index resides in the relocation entry.
    _64 = 1 # Direct 64 bit : S + A
    PC32 = 2 # PC relative 32 bit signed : S + A - P
    GOT32 = 3 # 32 bit GOT entry : G + A
    PLT32 = 4 # 32 bit PLT address : L + A - P
    COPY = 5 # Copy symbol at runtime
    GLOB_DAT = 6 # Create GOT entry : S
    JUMP_SLOT = 7 # Create PLT entry : S
    RELATIVE = 8 # Adjust by program base :  B + A
    GOTPCREL = 9 # 32 bit signed PC relative offset to GOT *
    _32 = 10 # Direct 32 bit zero extended
    _32S = 11 # Direct 32 bit sign extended
    _16 = 12 # Direct 16 bit zero extended
    PC16 = 13 # 16 bit sign extended pc relative
    _8 = 14 # Direct 8 bit sign extended
    PC8 = 15 # 8 bit sign extended pc relative
    DTPMOD64 = 16 # ID of module containing symbol
    DTPOFF64 = 17 # Offset in module's TLS block
    TPOFF64 = 18 # Offset in initial TLS block
    TLSGD = 19 # 32 bit signed PC relative offset to two GOT entries for GD symbol
    TLSLD = 20 # 32 bit signed PC relative offset to two GOT entries for LD symbol
    DTPOFF32 = 21 # Offset in TLS block
    GOTTPOFF = 22 # 32 bit signed PC relative offset to GOT entry for IE symbol
    TPOFF32 = 23 # Offset in initial TLS block
    PC64 = 24 # PC relative 64 bit
    GOTOFF64 = 25 # 64 bit offset to GOT
    GOTPC32 = 26 # 32 bit signed pc relative offset to GOT
    GOT64 = 27 # 64-bit GOT entry offset
    GOTPCREL64 = 28 # 64-bit PC relative offset to GOT entry
    GOTPC64 = 29 # 64-bit PC relative offset to GOT
    GOTPLT64 = 30 # like GOT64, says PLT entry needed
    PLTOFF64 = 31 # 64-bit GOT relative offset to PLT entry
    SIZE32 = 32 # Size of symbol plus 32-bit addend
    SIZE64 = 33 # Size of symbol plus 64-bit addend
    GOTPC32_TLSDESC = 34 # GOT offset for TLS descriptor.
    TLSDESC_CALL = 35 # Marker for call through TLS descriptor.
    TLSDESC = 36 # TLS descriptor.
    IRELATIVE = 37 # Adjust indirectly by program base
    RELATIVE64 = 38 # 64-bit adjust by program base
    # 39 Reserved was R_X86_64_PC32_BND
    # 40 Reserved was R_X86_64_PLT32_BND
    GOTPCRELX = 41 # Load from 32 bit signed pc relative offset to GOT entry without REX prefix, relaxable.
    REX_GOTPCRELX = 42 # Load from 32 bit signed pc relative offset to GOT entry with REX prefix, relaxable.
    NUM = 43

    def __str__(self):
        return "R_X86_64_%s" % self.name


class Elf64RelocationInfo(FlagType(fixint.UInt64)):
    def __init__(
        self,
        sym=None,
        type=None,
    ):
        if isinstance(type, x86_64Relocations) and sym is not None:
            super().__init__(fixint.UInt64((int(sym) << 32)) + type.value)
        elif type is None and sym is None:
            super().__init__(0)
        elif isinstance(sym, (int, fixint.UInt64)) and type is None:
            super().__init__(sym)
        elif isinstance(sym, Elf64RelocationInfo) and type is None:
            super().__init__(sym.value)
        else:
            raise TypeError("Wrong types for SymbolTableInfo.__init__")

    @property
    def type(self):
        return x86_64Relocations(int(self.value & 0xffffffff))

    @property
    def sym(self):
        return fixint.UInt32(self.value >> 32)


class SymbolTableVisibility(IntEnum(fixint.UInt8)):
    DEFAULT = 0 # Default symbol visibility rules
    INTERNAL = 1 # Processor specific hidden class
    HIDDEN = 2 # Sym unavailable in other modules
    PROTECTED = 3 # Not preemptible, not exported


class types64:
    uintN_t = fixint.UInt64M
    intN_t = fixint.Int64M
    ElfN_Addr = uintN_t
    ElfN_Off = uintN_t
    ElfN_Section = fixint.UInt16M
    ElfN_Versym = fixint.UInt16M
    Elf_Byte = fixint.UInt8M
    ElfN_Half = fixint.UInt16M
    ElfN_Sword = fixint.Int32M
    ElfN_Word = fixint.UInt32M
    ElfN_Sxword = fixint.Int64M
    ElfN_Xword = fixint.UInt64M


class Elf64_Ehdr(Struct):
    EI_NIDENT = 16
    types = types64

    fields = [
        ("e_ident", FixedBytes(EI_NIDENT)),
        ("e_type", Type),
        ("e_machine", Machine),
        ("e_version", Version),
        ("e_entry", types.ElfN_Addr),
        ("e_phoff", types.ElfN_Off),
        ("e_shoff", types.ElfN_Off),
        ("e_flags", fixint.UInt32M),
        ("e_ehsize", fixint.UInt16M),
        ("e_phentsize", fixint.UInt16M),
        ("e_phnum", fixint.UInt16M),
        ("e_shentsize", fixint.UInt16M),
        ("e_shnum", fixint.UInt16M),
        ("e_shstrndx", fixint.UInt16M)
    ]

    def __init__(self, e_type=Type.REL):
        self.e_ident = (
            b'\x7fELF' +
            b''.join(map(bytes, [
                Class.CLASS64,
                Data._2LBS,
                bytes([Version.CURRENT.value]),
                OsABI.LINUX,
                b'\0'
            ])) +
            b'\0' * (16-9)
        )
        self.e_type = e_type
        self.e_machine = Machine.X86_64
        self.e_version = Version.CURRENT
        self.e_entry = self.types.ElfN_Addr(0) # Virtual addressto transfer control to (_start) or zero
        self.e_phoff = self.types.ElfN_Off(0)  # Program header table file offset or zero
        self.e_shoff = self.types.ElfN_Off(0) # Section header table file offset or zero
        self.e_flags = fixint.UInt32(0)
        self.e_ehsize = self.sizeof
        self.e_phentsize = Elf64_Phdr.sizeof
        self.e_phnum = fixint.UInt16(0) # Number of entries in the program header
        self.e_shentsize = Elf64_Shdr.sizeof
        self.e_shnum = fixint.UInt16(0) # Number of entries in the section header
        # Section header table index of the entry with section name string table
        # If no section name string table, UNDEF.
        # If the  index is larger than or equal to LORESERVE (0xff00),
        # this member holds XINDEX (0xffff) and the real index
        # is in the sh_link member of the initial entry in section header table
        # Otherwise, the sh_link member is zero.
        self.e_shstrndx = SpecialSections.UNDEF.value

        #self.e_ehsize = fixint.UInt16(self.sizeof)

    @property
    def mag0(self):
        return self.e_ident[0]

    @property
    def mag1(self):
        return self.e_ident[1]

    @property
    def mag2(self):
        return self.e_ident[2]

    @property
    def mag3(self):
        return self.e_ident[3]

    @property
    def ident_class(self):
        return Class(self.e_ident[4])

    @property
    def data(self):
        return Data(self.e_ident[5])

    @property
    def ident_version(self):
        return Version(self.e_ident[6])

    @property
    def os_abi(self):
        return OsABI(self.e_ident[7])

    @property
    def abi_version(self):
        return self.e_ident[8]


class Elf64_Phdr(Struct):
    types = types64
    Flags = FlagEnum(fixint.UInt32M, dict(
        PF_X = fixint.UInt32(1 << 0),
        PF_W = fixint.UInt32(1 << 1),
        PF_R = fixint.UInt32(1 << 2),
    ))

    fields = [
        ("p_type", ProgramType),
        ("p_flags", Flags),
        ("p_offset", types.ElfN_Off),
        ("p_vaddr", types.ElfN_Addr),
        ("p_paddr", types.ElfN_Addr),
        ("p_filesz", fixint.UInt64M),
        ("p_memsz", fixint.UInt64M),
        ("p_align", fixint.UInt64M),
    ]

    def __init__(self):
        self.p_type = ProgramType.NULL
        self.p_offset = 0 # Offset from beginning of file of the first byte of the segment
        self.p_vaddr = 0 # Virtual addr of the first byte of this segment in memory
        self.p_paddr = 0 # Physical address
        self.p_filesz = 0 # Size of the file image of the segment
        self.p_memsz = 0 # Size of the memoryimage of the segment
        self.p_flags = 0 # Text segment should have PF_X|PF_R, data segment should have PF_X|PF_W|PF_R
        self.p_align = 0 # if > 1, must be power of two and p_vaddr = p_offset % p_align


class Elf64_Shdr(Struct):
    types = types64

    WRITE = types.uintN_t(1 << 0) # Writable
    ALLOC = types.uintN_t(1 << 1) # Occupies memory during execution
    EXECINSTR = types.uintN_t(1 << 2) # Executable
    MERGE = types.uintN_t(1 << 4) # Might be merged
    STRINGS = types.uintN_t(1 << 5) # Contains nul-terminated strings
    MASKPROC = types.uintN_t(0xf0000000) # Processor-specific

    fields = [
        ("sh_name", fixint.UInt32M),
        ("sh_type", SectionHeaderType),
        ("sh_flags", types.uintN_t),
        ("sh_addr", types.ElfN_Addr),
        ("sh_offset", types.ElfN_Off),
        ("sh_size", types.uintN_t),
        ("sh_link", fixint.UInt32M),
        ("sh_info", fixint.UInt32M),
        ("sh_addralign", types.uintN_t),
        ("sh_entsize", types.uintN_t),
    ]

    def __init__(self):
        self.sh_name = 0 # index in the string table section giving a NUL-terminated string
        self.sh_type = SectionHeaderType.NULL
        self.sh_flags = 0 # Flag mask WRITE|ALLOC|EXECINSTR
        self.sh_addr = 0
        self.sh_offset = 0
        self.sh_size = 0
        self.sh_link = 0
        self.sh_info = 0
        self.sh_addralign = 0
        self.sh_entsize = 0


class Elf64_Sy(Struct):
    types = types64
    fields = [
        ("st_name", fixint.UInt32M),
        ("st_info", SymbolTableInfo),
        ("st_other", SymbolTableVisibility),
        ("st_shndx", fixint.UInt16M),
        ("st_value", types.ElfN_Addr),
        ("st_size", types.uintN_t),
    ]

    def __init__(self):
        self.st_name = 0 # Index in the symbol string table
        self.st_value = 0 # Value of the symbol
        self.st_size = 0 # Size of the symbol (0 if unknown)
        self.st_info = SymbolTableInfo()
        self.st_other = SymbolTableVisibility.DEFAULT
        self.st_shndx = 0 # Index of the section header this entry belongs to


class Elf64_Rel(Struct):
    types = types64
    fields = [
        ("r_offset", types.ElfN_Addr),
        ("r_info", Elf64RelocationInfo),
    ]

    def __init__(self, r_offset, r_info):
        self.r_offset = r_offset
        self.r_info = r_info


class Elf64_Rela(Struct):
    types = types64
    fields = [
        ("r_offset", types.ElfN_Addr),
        ("r_info", Elf64RelocationInfo),
        ("r_addend", types.intN_t),
    ]

    def __init__(self):
        self.r_offset = 0
        self.r_info = 0
        self.r_addend = 0


class NoteType(IntEnum(types64.ElfN_Word)):
    _INVALID = 0

    NT_PRSTATUS = 1 #  Contains copy of prstatus struct
    NT_PRFPREG = 2 #  Contains copy of fpregset struct.
    NT_FPREGSET = 2 #  Contains copy of fpregset struct
    NT_PRPSINFO = 3 #  Contains copy of prpsinfo struct
    NT_PRXREG = 4 #  Contains copy of prxregset struct
    NT_TASKSTRUCT = 4 #  Contains copy of task structure
    NT_PLATFORM = 5 #  String from sysinfo(SI_PLATFORM)
    NT_AUXV = 6 #  Contains copy of auxv array
    NT_GWINDOWS = 7 #  Contains copy of gwindows struct
    NT_ASRS = 8 #  Contains copy of asrset struct
    NT_PSTATUS = 10 #  Contains copy of pstatus struct
    NT_PSINFO = 13 #  Contains copy of psinfo struct
    NT_PRCRED = 14 #  Contains copy of prcred struct
    NT_UTSNAME = 15 #  Contains copy of utsname struct
    NT_LWPSTATUS = 16 #  Contains copy of lwpstatus struct
    NT_LWPSINFO = 17 #  Contains copy of lwpinfo struct
    NT_PRFPXREG = 20 #  Contains copy of fprxregset struct
    NT_SIGINFO = 0x53494749 #  Contains copy of siginfo_t, size might increase
    NT_FILE = 0x46494c45 #  Contains information about mapped files
    NT_PRXFPREG = 0x46e62b7f #  Contains copy of user_fxsr_struct
    NT_PPC_VMX = 0x100 #  PowerPC Altivec/VMX registers
    NT_PPC_SPE = 0x101 #  PowerPC SPE/EVR registers
    NT_PPC_VSX = 0x102 #  PowerPC VSX registers
    NT_PPC_TAR = 0x103 #  Target Address Register
    NT_PPC_PPR = 0x104 #  Program Priority Register
    NT_PPC_DSCR = 0x105 #  Data Stream Control Register
    NT_PPC_EBB = 0x106 #  Event Based Branch Registers
    NT_PPC_PMU = 0x107 #  Performance Monitor Registers
    NT_PPC_TM_CGPR = 0x108 #  TM checkpointed GPR Registers
    NT_PPC_TM_CFPR = 0x109 #  TM checkpointed FPR Registers
    NT_PPC_TM_CVMX = 0x10a #  TM checkpointed VMX Registers
    NT_PPC_TM_CVSX = 0x10b #  TM checkpointed VSX Registers
    NT_PPC_TM_SPR = 0x10c #  TM Special Purpose Registers
    NT_PPC_TM_CTAR = 0x10d #  TM checkpointed Target Address Register
    NT_PPC_TM_CPPR = 0x10e #  TM checkpointed Program Priority Register
    NT_PPC_TM_CDSCR = 0x10f #  TM checkpointed Data Stream Control Register
    NT_PPC_PKEY = 0x110 #  Memory Protection Keys registers.
    NT_386_TLS = 0x200 #  i386 TLS slots (struct user_desc)
    NT_386_IOPERM = 0x201 #  x86 io permission bitmap (1=deny)
    NT_X86_XSTATE = 0x202 #  x86 extended state using xsave
    NT_S390_HIGH_GPRS = 0x300 #  s390 upper register halves
    NT_S390_TIMER = 0x301 #  s390 timer register
    NT_S390_TODCMP = 0x302 #  s390 TOD clock comparator register
    NT_S390_TODPREG = 0x303 #  s390 TOD programmable register
    NT_S390_CTRS = 0x304 #  s390 control registers
    NT_S390_PREFIX = 0x305 #  s390 prefix register
    NT_S390_LAST_BREAK = 0x306 #  s390 breaking event address
    NT_S390_SYSTEM_CALL = 0x307 #  s390 system call restart data
    NT_S390_TDB = 0x308 #  s390 transaction diagnostic block
    NT_S390_VXRS_LOW = 0x309 #  s390 vector registers 0-15 upper half.
    NT_S390_VXRS_HIGH = 0x30a #  s390 vector registers 16-31.
    NT_S390_GS_CB = 0x30b #  s390 guarded storage registers.
    NT_S390_GS_BC = 0x30c #  s390 guarded storage broadcast control block.
    NT_S390_RI_CB = 0x30d #  s390 runtime instrumentation.
    NT_ARM_VFP = 0x400 #  ARM VFP/NEON registers
    NT_ARM_TLS = 0x401 #  ARM TLS register
    NT_ARM_HW_BREAK = 0x402 #  ARM hardware breakpoint registers
    NT_ARM_HW_WATCH = 0x403 #  ARM hardware watchpoint registers
    NT_ARM_SYSTEM_CALL = 0x404 #  ARM system call number
    NT_ARM_SVE = 0x405 #  ARM Scalable Vector Extension registers
    NT_VMCOREDD = 0x700 #  Vmcore Device Dump Note.

    NT_GNU_ABI_TAG = 1
    NT_GNU_HWCAP = 2
    NT_GNU_BUILD_ID = 3
    NT_GNU_GOLD_VERSION = 4

    NT_VERSION = 1


class NoteGnuAbiOS(IntEnum(types64.ElfN_Word)):
    ELF_NOTE_OS_LINUX = 0
    ELF_NOTE_OS_GNU = 1
    ELF_NOTE_OS_SOLARIS2 = 2
    ELF_NOTE_OS_FREEBSD = 3


class NoteGnuABI(Struct):
    types = types64
    fields = [
        ("os", NoteGnuAbiOS),
        ("major", types.ElfN_Word),
        ("minor", types.ElfN_Word),
        ("subminor", types.ElfN_Word),
    ]


class Elf64_Nhdr(Struct):
    types = types64
    fields = [
        ("n_namesz", types.ElfN_Word),
        ("n_descsz", types.ElfN_Word),
        ("n_type", NoteType),
    ]

    class ReadValueMode(enum.Enum):
        Generic = 0
        Core = 1
        GNU = 2
        Auto = 3

    def __init__(self):
        self.n_namesz = 0
        self.n_descsz = 0
        self.n_type = NoteType._INVALID

    def read_value(self, stream, read_value_mode=ReadValueMode.Generic):
        name = stream.read(self.n_namesz)
        stream.read(self.name_padding)

        value = None
        if read_value_mode == self.ReadValueMode.GNU and name == "GNU\0":
            if self.n_type == NoteType.NT_GNU_ABI_TAG:
                value = NoteGnuABI.read(stream)
        if value is None:
            value = stream.read(self.n_descsz)

        stream.read(self.desc_padding)
        return name, value

    @classmethod
    def _padding(cls, val):
        return -(val % -4)

    @property
    def name_padding(self):
        return self._padding(self.n_namesz)

    @property
    def desc_padding(self):
        return self._padding(self.n_descsz)

    @property
    def sizeof_with_data(self):
        return (
            StructMeta.sizeof(self.fields)
            + self.n_namesz + self.name_padding
            + self.n_descsz + self.desc_padding
        )


class DynTag(IntEnum(types64.ElfN_Sxword)):
    DT_NULL = 0             # (d_un ignored) Marks end of dynamic section
    DT_NEEDED = 1           # (d_val) Name of needed library
    DT_PLTRELSZ = 2         # (d_val) Size in bytes of PLT relocs
    DT_PLTGOT = 3           # (d_ptr) Processor defined value
    DT_HASH = 4             # (d_ptr) Address of symbol hash table
    DT_STRTAB = 5           # (d_ptr) Address of string table
    DT_SYMTAB = 6           # (d_ptr) Address of symbol table
    DT_RELA = 7             # (d_ptr) Address of Rela relocs
    DT_RELASZ = 8           # (d_val) Total size of Rela relocs
    DT_RELAENT = 9          # (d_val) Size of one Rela reloc
    DT_STRSZ = 10           # (d_val) Size of string table
    DT_SYMENT = 11          # (d_val) Size of one symbol table entry
    DT_INIT = 12            # (d_ptr) Address of init function
    DT_FINI = 13            # (d_ptr) Address of termination function
    DT_SONAME = 14          # (d_val) Name of shared object
    DT_RPATH = 15           # (d_val) Library search path (deprecated)
    DT_SYMBOLIC = 16        # (d_un ignored) Start symbol search here
    DT_REL = 17             # (d_ptr) Address of Rel relocs
    DT_RELSZ = 18           # (d_val) Total size of Rel relocs
    DT_RELENT = 19          # (d_val) Size of one Rel reloc
    DT_PLTREL = 20          # (d_val) Type of reloc in PLT
    DT_DEBUG = 21           # (d_ptr) For debugging; unspecified
    DT_TEXTREL = 22         # (d_un ignored) Reloc might modify .text
    DT_JMPREL = 23          # (d_ptr) Address of PLT relocs
    DT_BIND_NOW = 24        #  Process relocations of object
    DT_INIT_ARRAY = 25      #  Array with addresses of init fct
    DT_FINI_ARRAY = 26      #  Array with addresses of fini fct
    DT_INIT_ARRAYSZ = 27    #  Size in bytes of DT_INIT_ARRAY
    DT_FINI_ARRAYSZ = 28    #  Size in bytes of DT_FINI_ARRAY
    DT_RUNPATH = 29         #  Library search path
    DT_FLAGS = 30           #  Flags for the object being loaded
    DT_ENCODING = 32        #  Start of encoded range
    DT_PREINIT_ARRAY = 32   #  Array with addresses of preinit fct
    DT_PREINIT_ARRAYSZ = 33 #  size in bytes of DT_PREINIT_ARRAY
    DT_SYMTAB_SHNDX = 34    #  Address of SYMTAB_SHNDX section
    DT_NUM = 35             #  Number used
    DT_LOOS = 0x6000000d    #  Start of OS-specific
    DT_HIOS = 0x6ffff000    #  End of OS-specific
    DT_LOPROC = 0x70000000  #  Start of processor-specific
    DT_HIPROC = 0x7fffffff  #  End of processor-specific


class Elf64_Dyn(Struct):
    types = types64()
    fields = {
        "d_tag": types.ElfN_Sxword,
        "d_un": types.ElfN_Xword, # Technically a union {xword, addr}
    }
