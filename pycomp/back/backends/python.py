import typing
from inspect import cleandoc

from ..backend import Backend
from ..base import Generator
from ...autohandler import handler
from ... import ast_nodes as n
from ...symbol_table import TypeSizer


class PythonBackend(Backend):
    name = "python"
    file_extensions = ["py"]

    def generator(self, stream: typing.IO, typesizer: TypeSizer) -> Generator:
        return PythonGenerator(stream)


class PythonGenerator(Generator):
    indent_str = " " * 4
    opmap = {
        "&&": "and",
        "||": "or",
        "!": "not",
    }

    @property
    def indent(self):
        return self.indent_str * self.indent_amount

    @property
    def indent_amount(self):
        return self._indent_amount

    @indent_amount.setter
    def indent_amount(self, amount):
        self._indent_amount = max(0, amount)

    @handler
    def program(self, node: n.Program):
        self.indent_amount = 0

        self.stream.write(cleandoc(
            """
            import os
            import sys

            def printf(fmt, *args):
                sys.stdout.write(fmt % args)
            """
        ))
        self.stream.write("\n")

        for c in node.statements:
            self.handle(c)
            self.stream.write("\n")

        self.stream.write(cleandoc(
            """
            if __name__ == "__main__":
                sys.exit(main(len(sys.argv), sys.argv))
            """
        ))
        self.stream.write("\n")

    @handler
    def block(self, node: n.StatementBlock):
        for c in node.statements:
            self.handle_stmt(c)

    def handle_stmt(self, node: n.AstNode):
        if isinstance(node, (n.BinaryOp, n.UnaryOp, n.FunctionCall)):
            self.expr_stmt(node)
        else:
            self.handle(node)

    def expr_stmt(self, node: n.AstNode):
        self.stream.write(self.indent)
        self.handle(node)
        self.stream.write("\n")

    @handler
    def var(self, node: n.VariableDeclaration):
        self.stream.write(self.indent)
        self.stream.write("%s = " % node.name)
        if node.value:
            self.handle(node.value)
        else:
            self.stream.write("None")
        self.stream.write("\n")

    @handler
    def varags(self, node: n.VarArgs):
        self.stream.write("*__varargs")

    @handler
    def func(self, node: n.FunctionDeclaration):
        if node.body:
            self.stream.write("def %s(" % node.name)
            for i, v in enumerate(node.params.params):
                if i:
                    self.stream.write(", ")
                if isinstance(v, n.VariableDeclaration) and not v.value:
                    self.stream.write(v.name)
                else:
                    self.handle(v)
            self.stream.write("):\n")
            self.indent_amount += 1
            self.handle(node.body)
            self.indent_amount -= 1
            self.stream.write("\n\n")

    @handler
    def binop(self, node: n.BinaryOp):
        self.handle(node.left)
        self.stream.write(" %s " % self.opmap.get(node.op, node.op))
        self.handle(node.right)

    @handler
    def unop(self, node: n.UnaryOp):
        if node.op == "++":
            self.handle(node.child)
            self.stream.write(" += 1")
        else:
            self.stream.write("%s " % self.opmap.get(node.op, node.op))
            self.handle(node.child)

    @handler
    def funcall(self, node: n.FunctionCall):
        self.handle(node.function)
        self.stream.write("(")
        for i, arg in enumerate(node.arg_list.args):
            if i:
                self.stream.write(", ")
            self.handle(arg)
        self.stream.write(")")

    @handler
    def varref(self, node: n.VariableReference):
        self.stream.write(node.name)

    @handler
    def literal(self, node: n.Literal):
        self.stream.write(repr(node.value))

    @handler
    def stmt_return(self, node: n.Return):
        self.stream.write(self.indent)
        self.stream.write("return ")
        if node.value:
            self.handle(node.value)

    @handler
    def noop(self, node: n.Noop):
        self.stream.write(self.indent)
        self.stream.write("pass")

    @handler
    def stmt_if(self, node: n.If):
        self.stream.write(self.indent)
        self.stream.write("if ")
        self.handle(node.condition)
        self.stream.write(":\n")
        self.indent_amount += 1
        self.handle_stmt(node.if_true)
        self.indent_amount -= 1
        if node.if_false:
            self.stream.write(self.indent)
            self.stream.write("else:\n")
            self.indent_amount += 1
            self.handle_stmt(node.if_false)
            self.indent_amount -= 1

    @handler
    def stmt_while(self, node: n.While):
        self.stream.write(self.indent)
        self.stream.write("while ")
        self.handle(node.condition)
        self.stream.write(":\n")
        self.indent_amount += 1
        self.handle_stmt(node.block)
        self.indent_amount -= 1

    @handler
    def stmt_cast(self, node: n.TypeCast):
        self.handle(node.child)

    @handler
    def stmt_break(self, node: n.Break):
        self.stream.write(self.indent)
        self.stream.write("break")

    @handler
    def stmt_continue(self, node: n.Continue):
        self.stream.write(self.indent)
        self.stream.write("continue")



