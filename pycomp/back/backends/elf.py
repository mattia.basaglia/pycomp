import typing
import argparse

from ..backend import Backend
from ..base import Generator
from ... import symbol_table
from .. import asm
from ..arch.list import arch_names, get_arch, x86_64
from ..elf.elf_backend import ElfRel, ElfExec


class ElfRelBackend(Backend):
    name = "elf_rel"
    file_extensions = ["o"]
    mode = "wb"

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            "--arch",
            help="Architecture",
            choices=arch_names,
            default=x86_64,
            type=get_arch
        )

    def __init__(self, ns: argparse.Namespace):
        super().__init__(ns)
        self.architecture = ns.arch

    def generator(self, stream: typing.IO, typesizer: symbol_table.TypeSizer) -> Generator:
        return self.architecture.generator_class(stream, ElfRel(stream, self.architecture.opcode_table), typesizer)

    def typesizer(self, sizer: symbol_table.TypeSizer) -> symbol_table.TypeSizer:
        return symbol_table.FixedPointerSizer(
            symbol_table.ArithmeticSizer(
                sizer
            ),
            self.architecture.generator_class.pointer_size
        )


class ElfExecBackend(ElfRelBackend):
    name = "elf"
    file_extensions = ["", "out", "elf"]

    def generator(self, stream: typing.IO, typesizer: symbol_table.TypeSizer) -> Generator:
        return self.architecture.generator_class(stream, ElfExec(stream, self.architecture.opcode_table), typesizer)
