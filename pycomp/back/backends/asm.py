import typing
import argparse

from ..backend import Backend
from ..base import Generator
from ... import symbol_table
from .. import asm
from ..asm_flavours.gas import Gas
from ..arch.list import arch_names, get_arch, x86_64


class AsmX86_64Backend(Backend):
    name = "asm"
    file_extensions = ["s", "S", "asm"]

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser):
        parser.add_argument(
            "--oasm",
            help="Ouput assembly flavour",
            choices=asm.asm_flavour_registry.instances,
            default=Gas(),
            type=asm.asm_flavour_registry.get_instance,
            dest="out_asm_flavour"
        )
        parser.add_argument(
            "--arch",
            help="Architecture",
            choices=arch_names,
            default=x86_64,
            type=get_arch
        )

    def __init__(self, ns: argparse.Namespace):
        super().__init__(ns)
        self.flavour = ns.out_asm_flavour
        self.architecture = ns.arch

    def generator(self, stream: typing.IO, typesizer: symbol_table.TypeSizer) -> Generator:
        return self.architecture.generator_class(stream, self.flavour.formatter(stream), typesizer)

    def typesizer(self, sizer: symbol_table.TypeSizer) -> symbol_table.TypeSizer:
        return symbol_table.FixedPointerSizer(
            symbol_table.ArithmeticSizer(
                sizer
            ),
            self.architecture.generator_class.pointer_size
        )
