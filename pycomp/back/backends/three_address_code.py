import typing
from inspect import cleandoc

from ..backend import Backend
from ..base import Generator
from ...autohandler import handler
from ... import ast_nodes as n
from ...symbol_table import TypeSizer


class ThreeAddressCodeBackend(Backend):
    name = "3ac"
    file_extensions = ["3ac"]

    def generator(self, stream: typing.IO, typesizer: TypeSizer) -> Generator:
        return ThreeAddressCodeGenerator(stream)


class ThreeAddressCodeGenerator(Generator):
    def __init__(self, stream: typing.IO):
        super().__init__(stream)
        self.tempcount = -1
        self.jumpcount = -1
        self._target = []
        self._loop_labels = []

    def _push_loop_labels(self, breakto, continueto):
        self._loop_labels.append((breakto, continueto))

    def _break_label(self):
        if self._loop_labels:
            return self._loop_labels[-1][0]
        else:
            raise Exception("Extra break")

    def _continue_label(self):
        if self._loop_labels:
            return self._loop_labels[-1][1]
        else:
            raise Exception("Extra continue")

    def _pop_loop_labels(self):
        self._loop_labels.pop()

    def _temp_label(self):
        self.tempcount += 1
        return "__temp_%s" % self.tempcount

    def _jump_label(self):
        self.jumpcount += 1
        return "__label_%s" % self.jumpcount

    def _dest_label(self, label=None):
        if label:
            return label
        return self._temp_label()

    def _pop_target(self):
        if self._target:
            return self._target.pop()
        return None

    def _push_target(self, label):
        self._target.append(label)

    def _ensure_popped_target(self, target):
        if self._target and self._target[-1] == target:
            self._target.pop()

    @handler(strict=False)
    def block(self, node: n.StatementBlock):
        for c in node.statements:
            self.handle(c)

    @handler
    def var(self, node: n.VariableDeclaration):
        if node.value:
            label = self.handle(node.value)
            self.stream.write("decl(%s) %s = %s\n" % (node.datatype, node.name, label))
        else:
            self.stream.write("decl(%s) %s\n" % (node.datatype, node.name))

    @handler
    def varags(self, node: n.VarArgs):
        pass

    @handler
    def func(self, node: n.FunctionDeclaration):
        if node.body:
            self.stream.write("%s:\n" % node.name)
            for v in node.params.params:
                if isinstance(v, n.VariableDeclaration) and not v.value:
                    self.stream.write("read arg(%s) %s\n" % (v.datatype, v.name))
            self.handle(node.body)
            self.stream.write("return // end of %s\n\n" % node.name)

    @handler
    def binop(self, node: n.BinaryOp):
        lab = self._pop_target()
        lab_l = self.handle(node.left)
        lab_r = self.handle(node.right)
        lab = self._dest_label(lab)
        self.stream.write("%s = %s %s %s\n" % (lab, lab_l, node.op, lab_r))
        return lab

    @handler
    def assign(self, node: n.Assign):
        target = self.handle(node.left)
        self._push_target(target)
        self.handle(node.right)
        self._ensure_popped_target(target)
        return target

    @handler
    def unop(self, node: n.UnaryOp):
        lab = self._pop_target()
        lab_c = self.handle(node.child)
        lab = self._dest_label(lab)
        self.stream.write("%s = %s %s\n" % (lab, node.op, lab_c))
        return lab

    @handler
    def funcall(self, node: n.FunctionCall):
        lab = self._pop_target()
        lab_f = self.handle(node.function)
        labels = list(map(self.handle, node.arg_list.args))
        for arg, lab_a in zip(node.arg_list.args, labels):
            self.stream.write("push arg(%s) %s\n" % (arg.datatype, lab_a))
        lab = self._dest_label(lab)
        self.stream.write("%s = call %s\n" % (lab, lab_f))
        return lab

    @handler
    def varref(self, node: n.VariableReference):
        return node.name

    @handler
    def literal(self, node: n.Literal):
        return repr(node.value)

    @handler
    def stmt_return(self, node: n.Return):
        if node.value:
            lab_r = self.handle(node.value)
            self.stream.write("return %s\n" % lab_r)
        else:
            self.stream.write("return\n")

    @handler
    def noop(self, node: n.Noop):
        pass

    @handler
    def stmt_if(self, node: n.If):
        cond = self.handle(node.condition)
        jumpfalse = self._jump_label()
        self.stream.write("if not %s goto %s\n" % (cond, jumpfalse))
        self.handle(node.if_true)
        if node.if_false:
            jumpend = self._jump_label()
            self.stream.write("goto %s\n" % jumpend)
            self.stream.write("%s:\n" % jumpfalse)
            self.handle(node.if_false)
            self.stream.write("%s:\n" % jumpend)
        else:
            self.stream.write("%s:\n" % jumpfalse)

    @handler
    def stmt_while(self, node: n.While):
        jumpstart = self._jump_label()
        self.stream.write("%s:\n" % jumpstart)
        cond = self.handle(node.condition)
        jumpfalse = self._jump_label()
        self._push_loop_labels(jumpfalse, jumpstart)
        self.stream.write("if not %s goto %s\n" % (cond, jumpfalse))
        self.handle(node.block)
        self._pop_loop_labels()
        self.stream.write("goto %s\n" % jumpstart)
        self.stream.write("%s:\n" % jumpfalse)

    @handler
    def stmt_cast(self, node: n.TypeCast):
        lab = self._pop_target()
        lab_c = self.handle(node.child)
        lab = self._dest_label(lab)
        self.stream.write("%s = cast(%s) %s\n" % (lab, node.datatype, lab_c))
        return lab


