import os
import math
import typing

from ..autohandler import handler, AutoHandler
from .. import ast_nodes as n
from .. import ast_types as t
from ..class_registry import Registry


def sizename(sz):
    if sz == 1:
        return "byte"
    if sz == 2:
        return "word"
    if sz == 4:
        return "dword"
    if sz == 8:
        return "qword"
    return "size_%s" % sz


class DataLocation:
    def __init__(self, size):
        self.size = size


class Operand(DataLocation):
    pass


class Register(Operand):
    def __init__(self, size, name):
        super().__init__(size)
        self.name = name

    def resized(self, size):
        return self.sized(size, self.name)

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<%s %s (%s)>" % (self.__class__.__name__, self.name, self.size)

    @classmethod
    def from_name(cls, name):
        raise NotImplementedError()


class IntLiteral(Operand):
    def __init__(self, size, value):
        super().__init__(size)
        self.value = value

    def resized(self, size):
        if size > 0 and math.floor(math.log2(self.value))+1 > size * 8:
            raise Exception("Cannot cast integer %s to %s bytes" % (self.value, size))
        return IntLiteral(size, self.value)

    def __str__(self):
        return "(%s)0x%x" % (sizename(self.size), self.value)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)


class DerefRegister(Operand):
    def __init__(self, size: int, register: Register, offset: int):
        super().__init__(size)
        self.register = register
        self.offset = offset

    def __str__(self):
        return "(%s)%s[%s]" % (sizename(self.size), self.register.name, self.offset)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)


class Label(Operand):
    def __init__(self, size, name):
        super().__init__(size)
        self.name = name

    def __str__(self):
        return "label(%s)" % (self.name)

    def __repr__(self):
        return "<%s %s (%s)>" % (self.__class__.__name__, self.name, self.size)


class Address(Operand):
    def __init__(self, size, child=None):
        if child is None and isinstance(size, Operand):
            child = size
            size = size.size
        super().__init__(size)
        self.child = child

    def __str__(self):
        return str(self.child)

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)


class Operation:
    def __init__(self, mnem, op1=None, op2=None):
        self.mnem = mnem
        self.op1 = op1
        self.op2 = op2
        self.n_op = len(list(filter(bool, [op1, op2])))

    def __str__(self):
        return "%s %s %s" % (self.mnem, self.op1 or "", self.op2 or "")

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)


class StackTemp(DataLocation):
    def __init__(self, size, name, type: t.Type):
        super().__init__(size)
        self.name = name
        self.type = type


class RegisterPusher:
    class ProtectedRegister(Operand):
        def __init__(self, reg, resizeable):
            super().__init__(reg.size)
            self.reg = reg
            self.resizeable = resizeable
            self.temp = None

        @property
        def loc(self):
            if self.temp is None:
                return self.reg
            return self.temp

        def __eq__(self, register):
            if self.resizeable and self.reg.size != register.size:
                return self.reg.resized(register.size).name == register.name
            return self.reg.name == register.name

    def __init__(self, push_temp):
        self.registers = []
        self.push_temp = push_temp

    def protect(self, register, resizeable=True):
        pr = self.ProtectedRegister(register, resizeable)
        self.registers.append(pr)
        return pr

    def use(self, register):
        for i, pr in enumerate(self.registers):
            if pr == register:
                pr.temp = self.push_temp()
                self.registers.pop(i)

    def remove(self, register):
        for i, pr in enumerate(self.registers):
            if pr == register:
                self.registers.pop(i)

    @staticmethod
    def unwrap(loc):
        if isinstance(loc, RegisterPusher.ProtectedRegister):
            return loc.loc
        return loc


class Formatter(AutoHandler):
    def __init__(self, stream: typing.IO):
        super().__init__()
        self.stream = stream
        self.files = {}

    def empty_line(self):
        self.stream.write("\n")

    def op(self, *args):
        self.print_op(Operation(*args))

    def print_op(self, op: Operation):
        raise NotImplementedError

    def debug_info(self, info: n.DebugInfo):
        if info.filename not in self.files:
            fileno = len(self.files) + 1
            self.files[info.filename] = fileno
            self.debug_info_file(info.filename, fileno)
        else:
            fileno = self.files[info.filename]
        self.debug_info_line(info, fileno)

    def debug_info_file(self, filename: str):
        pass

    def debug_info_line(self, info: n.DebugInfo, fileno: int):
        pass

    def declare_function(self, name):
        pass

    def place_label(self, name):
        self.stream.write("%s:\n" % name)

    @handler
    def protected_reg(self, reg: RegisterPusher.ProtectedRegister):
        return self.handle(reg.loc)

    def section(self, name):
        pass

    def ident(self, text):
        pass

    def define_global(self, name):
        pass

    def define_extern(self, name):
        pass

    def define_type(self, name, type):
        pass

    def data(self, type: t.Arithmetic, value):
        pass

    def initialize_file(self):
        pass

    def finalize_file(self):
        pass

    def sized_op(self, size, mnem, op1=None, op2=None) -> Operation:
        return Operation(mnem, op1, op2)


class Flavour:
    @classmethod
    def formatter(cls, stream) -> Formatter:
        raise NotImplementedError()

    @classmethod
    def parser(cls, stream, typesizer):
        raise NotImplementedError()

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return type(self).name == type(other).name

    def __hash__(self):
        return hash(type(self).name)


asm_flavour_registry = Registry(
    Flavour,
    "pycomp.back.asm_flavours",
    [os.path.join(os.path.dirname(__file__), "asm_flavours")]
)
