import typing

from .. import ast_nodes
from .. import ast_types
from .. import autohandler


class Generator(autohandler.AutoHandler):
    def __init__(self, stream: typing.IO):
        super().__init__()
        self.stream = stream

    def generate(self, node: ast_nodes.Program):
        return self.handle(node)
