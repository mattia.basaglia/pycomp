import re
import math
from ..asm import *
from ...front.base import Lexer, Token, TokenType, Parser
from ... import ast_types
from ... import ast_asm as n
from ...ast_nodes import DebugInfo, Literal


class Nasm(Flavour):
    name = "nasm"

    @classmethod
    def formatter(cls, stream) -> Formatter:
        return NasmFormatter(stream)

    @classmethod
    def parser(cls, stream, typesizer, arch):
        return NasmParser(NasmLexer(stream), typesizer, arch)


class NasmFormatter(Formatter):
    name = "nasm"

    def print_op(self, op: Operation):
        self.stream.write(op.mnem)
        if op.n_op >= 1:
            self.stream.write(" ")
            self.handle(op.op1)
        if op.n_op == 2:
            self.stream.write(", ")
            self.handle(op.op2)
        self.stream.write("\n")

    @handler(strict=False)
    def register(self, reg: Register):
        self.stream.write(reg.name)

    def _sizename(self, sz):
        if sz == 1:
            return "byte"
        elif sz == 2:
            return "word"
        elif sz == 4:
            return "dword"
        elif sz == 8:
            return "qword"
        return ""

    @handler
    def literal(self, lit: IntLiteral):
        #szn = self._sizename(lit.size)
        #if szn:
            #self.stream.write(szn)
            #self.stream.write(" ")
        self.stream.write(str(lit.value))

    def string_lit(self, s):
        qopen = False
        for c in s:
            if c.isprintable():
                if not qopen:
                    qopen = True
                    self.stream.write("'")
                self.stream.write(c)
            else:
                if qopen:
                    self.stream.write("'")
                    qopen = False
                self.stream.write(", ")
                self.stream.write(str(ord(c)))
        if qopen:
            self.stream.write("'")

    @handler
    def string(self, s: str):
        self.stream.write(s)

    @handler
    def label(self, lab: Label):
        self.stream.write(lab.name)

    @handler
    def address(self, lab: Address):
        self.handle(lab.child)

    def debug_info_file(self, filename: str, fileno: int):
        # TODO
        pass

    def debug_info_line(self, info: DebugInfo, fileno: int):
        # TODO
        pass

    @handler
    def deref(self, x: DerefRegister):
        szn = self._sizename(x.size)
        if szn:
            self.stream.write(szn)
            self.stream.write(" ")
        self.stream.write("[")
        self.handle(x.register)
        self.stream.write("%+d" % x.offset)
        self.stream.write("]")

    def section(self, name):
        self.stream.write("section %s\n" % name)

    def ident(self, text):
        self.section(".comment")
        self.stream.write("db ")
        self.string_lit(text)

    def define_global(self, name):
        self.stream.write("global %s\n" % name)
        # TODO type?

    def define_extern(self, name):
        self.stream.write("extern %s\n" % name)

    def data(self, type: ast_types.Arithmetic, value):
        if type == ast_types.Arithmetic.BYTE:
            self.stream.write("db ")
            self.string_lit(value)
            self.stream.write("\n")


class NasmLexer(Lexer):
    def next(self) -> Token:
        c = self._skipws()
        if c == '':
            return Token(TokenType.EOF, "")

        if c.isalpha() or c in set("_.?"):
            return Token(
                TokenType.KEYWORD,
                str=self._keyword(c)
            )

        if c.isdigit() or c in set("+-"):
            return self._number(c)

        if c == '"' or c == "'":
            return self._string(c)

        if c in ";\n":
            while c != "\n":
                c = self._get()
            return Token(TokenType.OPERATOR, str=";")

        if c in set(",:[]"):
            return Token(TokenType.OPERATOR, str=c)

        self.raise_error("Unknown token")

    def _mkliteral(self, lit, sign, val, valtype):
        if sign == -1:
            lit = "-" + lit
            val *= sign
        return Token(TokenType.LITERAL, str=lit, val=val, valtype=valtype)

    def _number(self, c):
        sign = 1
        if c == "-":
            sign = -1
            c = "0"
        elif c == "+":
            c = self._get()

        if c == "0":
            return self._oct(sign)

        type = ast_types.Arithmetic.DWORD
        lit = ''
        while c.isdecimal():
            lit += c
            c = self._get()
        if c in set(".eE"):
            return self._float(lit, sign, c)
        self._unget(c)
        return self._mkliteral(lit, sign, int(lit), type)

    def _oct(self, sign) -> Token:
        c = self._get().lower()
        if c == "x":
            return self._hex(sign)
        elif c == "b":
            return self._bin(sign)

        lit = '0'
        while c in set("01234567"):
            lit += c
            c = self._get()

        self._unget(c)
        return self._mkliteral(lit, sign, int(lit, 8), ast_types.Arithmetic.DWORD)

    def _bin(self, sign) -> Token:
        lit = ''
        c = self._get()
        while c in set("01"):
            lit += c
            c = self._get()

        self._unget(c)
        return self._mkliteral("0b"+lit, sign, int(lit, 2), ast_types.Arithmetic.DWORD)

    def _hex(self, sign) -> Token:
        lit = ''
        c = self._get()
        while c.isdecimal() or c.lower() in set("abcdef"):
            lit += c
            c = self._get()

        if not lit:
            self.raise_error("Expected hexadecimal number")

        self._unget(c)

        # nbytes = minimum power of two greater than len(lit) / 2
        nbytes = 2**math.ceil(math.log2(math.ceil(len(lit) / 2)))
        if nbytes == 1:
            valtype = ast_types.Arithmetic.UBYTE
        elif nbytes == 2:
            valtype = ast_types.Arithmetic.UWORD
        elif nbytes == 4:
            valtype = ast_types.Arithmetic.UDWORD
        else:
            valtype = ast_types.Arithmetic.UQWORD

        return self._mkliteral("0x" + lit, sign, int(lit, 16), valtype)

    def _skipws(self) -> str:
        c = ' '
        while c.isspace() and c != "\n":
            c = self._get()
        return c

    def _keyword(self, c: str) -> str:
        result = c
        while True:
            c = self._get()
            if not c.isalnum() and c not in set("_$#@~.?"):
                break
            result += c
        self._unget(c)
        return result

    def _string(self, terminator: str) -> Token:
        lit = ''
        while True:
            c = self._get()
            if c == terminator:
                break
            if not c:
                self.raise_error("Unterminated string literal")
            if c == '\\':
                lit += self._char_esc()
            else:
                lit += c
        return Token(TokenType.STRING, str=lit)

    def _char_esc(self):
        c = self._get()
        if c in "\\\'\"?":
            return c
        elif c == 'a':
            return '\a'
        elif c == 'b':
            return '\b'
        elif c == 'f':
            return '\f'
        elif c == 'n':
            return '\n'
        elif c == 'r':
            return '\r'
        elif c == 't':
            return '\t'
        elif c == 'v':
            return '\v'
        # TODO \x \0 \u \U
        else:
            self.raise_error("Unknown escape sequence: \%s" % c)

    def _float(self, lit, mult, c) -> Token:
        if c == '.':
            lit += c
            c = self._get()
            while c.isdigit():
                lit += c
                c = self._get()

        if c.lower() == 'e':
            lit += c
            c = self._get()
            if c in "+-":
                lit += c
                c = self._get()
            if not c.isdecimal():
                self.raise_error("Invalid float literal %s" % lit)

            while c.isdecimal():
                lit += c
                c = self._get()

        return Token(TokenType.LITERAL, str=lit, val=float(lit)*mult, valtype=t.Arithmetic.FLOAT)


class NasmParser(Parser):
    size_quals = {
        "byte": 1,
        "word": 2,
        "dword": 4,
        "qword": 8,
    }
    data_types = {
        "db": ast_types.Arithmetic.BYTE,
        "dw": ast_types.Arithmetic.WORD,
        "dq": ast_types.Arithmetic.QWORD,
        "dd": ast_types.Arithmetic.FLOAT,
        "dt": ast_types.Arithmetic.FLOAT,
    }

    def __init__(self, lexer, typesizer, arch):
        super().__init__(lexer, typesizer)
        self.arch = arch

    def _parse(self) -> n.AsmProgram:
        """
        PROGRAM := STMT | STMT ; PROGRAM | (empty)
        """
        root = self._debug_info.wrap(n.AsmProgram())
        while not self._is_eof():
            while self._is_op(";"):
                self._scan()
            root.add(self._parse_stmt())
        return root

    def _is_eof(self) -> bool:
        return self.la.type == TokenType.EOF

    def _is_op(self, op) -> bool:
        return self.la.type == TokenType.OPERATOR and self.la.str == op

    def _is_eol(self) -> bool:
        return self._is_eof() or self._is_op(";")

    def _parse_stmt(self):
        """
        STMT := label: STMT | mnem OPERANDS | directive DIRECTIVE | (empty)
        """
        if self.la.type != TokenType.KEYWORD or not self.la.str:
            self.raise_error("Expected label, mnemonic or directive")

        name = self.la.str
        self._scan()

        if self._is_op(":"):
            self._scan()
            return n.AsmLabel(name)

        if name.upper() not in self.arch.mnemonics:
            stmt = self._parse_directive(name)
        else:
            stmt = self._parse_operation(name)

        if not self._is_eol():
            self.raise_error("Expected new line")
        self._scan()
        return stmt

    def _parse_operation(self, name):
        """
        OPERANDS := OPERAND | OPERAND , OPERAND | (empty)
        """
        mnem = name.lower()
        op1 = op2 = None
        if not self._is_eol():
            op1 = self._parse_operand()
            if self._is_op(","):
                self._scan()
                op2 = self._parse_operand(op1.size)
        return n.AsmOp(Operation(mnem, op1, op2))

    def _la_to_register(self):
        try:
            if self.la.type == TokenType.KEYWORD:
                return self.arch.register_class.from_name(self.la.str)
        except KeyError:
            pass
        return None

    def _parse_operand(self, size=None):
        """
        OPERAND := SIZEQUAL OPUNQUAL | OPUNQUAL
        OPUNQUAL := REGISTER | immediate | [ REGISTER offset ] | label
        """
        if self.la.type == TokenType.KEYWORD and self.la.str in self.size_quals:
            size = self.size_quals[self.la.str]
            self._scan()

        if self.la.type == TokenType.LITERAL:
            if isinstance(self.la.val, Label):
                val = self.la.val
                val.size = self.arch.generator_class.label_size
                self._scan()
                return val
            else:
                if not size:
                    size = self.typesizer.sizeof(self.la.valtype)
                val = IntLiteral(size, self.la.val)
                self._scan()
                return val
        elif self._is_op("["):
            self._scan()
            reg = self._la_to_register()
            if not reg:
                self.raise_error("Expected register")
            self._scan()
            if self.la.type != TokenType.LITERAL:
                self.raise_error("Expected offset")
            off = self.la.val
            self._scan()
            if not self._is_op("]"):
                self.raise_error("Expected ]")
            self._scan()
            return DerefRegister(size, reg, off)
        elif self.la.type == TokenType.KEYWORD:
            reg = self._la_to_register()
            if reg:
                self._scan()
                return reg

            val = Label(self.arch.generator_class.label_size, self.la.str)
            self._scan()
            return val
        self.raise_error("Invalid operand")

    def _parse_directive(self, direc):
        """
        DIRECTIVE :=
                  | DATA_TYPE DATA_LIT
                  | .globl label |  .global label | .extern label
                  | .type label , @function
                  | .section .name | .text | .data
                  | .ident string
                  | .file num string
                  | .loc LOC

        https://nasm.us/doc/nasmdoc3.html#section-3.2
        https://nasm.us/doc/nasmdoc6.html
        """
        if direc == "db":
            lit = b''
            while True:
                self._scan()
                if self.la.type == TokenType.LITERAL:
                    lit += bytes([self.la.val])
                elif self.la.type == TokenType.STRING:
                    lit += self.la.str.encode("utf-8")
                else:
                    self.raise_error("Expected character literal")
                self._scan()
                if self._is_eof() or self._is_op(";"):
                    return n.AsmString(bytes(lit))
                if not self._is_op(","):
                    self.raise_error("Expected ,")
        elif direc in {"df", "dt", "dd", "dq"}:
            self._scan()
            if self.la.type != TokenType.LITERAL:
                self.raise_error("Expected numeric literal")
            val = self.la.val
            self._scan()
            return Literal(val, self.data_types[direc])
        elif direc == "global":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected label name")
            val = n.AsmGlobal(self.la.str)
            self._scan()
            return val
        elif direc == "extern":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected label name")
            val = n.AsmExtern(self.la.str)
            self._scan()
            return val
        elif direc == "section":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected section name")
            val = n.AsmSection(self.la.str)
            self._scan()
            return val

        dir = n.AsmDirective(direc[1:], [])
        while not self._is_eol() and not self._is_eof():
            dir.args.append(self.la.str)
            self._scan()
            if self._is_op(","):
                self._scan()
        return dir

    def _parse_data(self):
        dtype = self.data_types[self.la.str]
        self._scan()
