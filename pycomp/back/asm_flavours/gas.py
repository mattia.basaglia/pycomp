import math

from ..asm import *
from ...front.base import Lexer, Token, TokenType, Parser
from ... import ast_asm as n
from ... import ast_types
from ...ast_nodes import DebugInfo, Literal
from ... import fixint


class Gas(Flavour):
    name = "gas"

    @classmethod
    def formatter(cls, stream) -> Formatter:
        return GasFormatter(stream)

    @classmethod
    def parser(cls, stream, typesizer, arch):
        return GasParser(GasLexer(stream), typesizer, arch)


class GasFormatter(Formatter):
    name = "gas"

    def print_op(self, op: Operation):
        self.stream.write(op.mnem)
        if op.n_op == 1:
            self.stream.write(" ")
            self.handle(op.op1)
        elif op.n_op == 2:
            self.stream.write(" ")
            self.handle(op.op2)
            self.stream.write(", ")
            self.handle(op.op1)
        self.stream.write("\n")

    @handler(strict=False)
    def register(self, reg: Register):
        self.stream.write("%%%s" % reg.name)

    @handler
    def literal(self, lit: IntLiteral):
        self.stream.write("$%s" % lit.value)

    def string_lit(self, s):
        if s and s[-1] == "\0":
            s = s[:-1]
        self.stream.write(repr(s).replace("'", '"'))

    @handler
    def string(self, s: str):
        self.stream.write(s)

    @handler
    def label(self, lab: Label):
        self.stream.write("$")
        self.stream.write(lab.name)

    @handler
    def address(self, lab: Address):
        if isinstance(lab.child, Label):
            self.stream.write(lab.child.name)
        elif isinstance(lab.child, IntLiteral):
            self.stream.write(str(lab.child.value))

    def debug_info_file(self, filename: str, fileno: int):
        self.stream.write(".file %s " % fileno)
        self.string_lit(filename)
        self.stream.write("\n")

    def debug_info_line(self, info: DebugInfo, fileno: int):
        self.stream.write(".loc %s %s" % (fileno, info.line))
        if info.column:
            self.stream.write(" %s" % info.column)
        if info.is_stmt:
            self.stream.write("is_stmt 1")
        self.stream.write("\n")

    @handler
    def deref(self, x: DerefRegister):
        self.stream.write(str(x.offset))
        self.stream.write("(")
        self.handle(x.register)
        self.stream.write(")")

    def section(self, name):
        self.stream.write(".section %s\n" % name)

    def ident(self, text):
        self.stream.write(".ident ")
        self.string_lit(text)
        self.stream.write("\n")

    def define_global(self, name):
        self.stream.write(".global %s\n" % name)

    def define_type(self, name, type):
        self.stream.write(".type ")
        self.stream.write(name)
        self.stream.write(", ")
        self.string_lit(type)
        self.stream.write("\n")

    def define_extern(self, name):
        self.stream.write(".extern %s\n" % name)

    def data(self, type: t.Arithmetic, value):
        if type == t.Arithmetic.BYTE:
            self.stream.write(".string ")
            self.string_lit(value)
            self.stream.write("\n")

    def sized_op(self, size, mnem, op1=None, op2=None):
        if size == 1:
            mnem += "b"
        elif size == 2:
            mnem += "w"
        elif size == 4:
            mnem += "l"
        elif size == 8:
            mnem += "q"
        else:
            raise Exception("Unknown mnemonic")
        return Operation(mnem, op1, op2)


class GasLexer(Lexer):
    def next(self) -> Token:
        c = self._skipws()
        if c == '':
            return Token(TokenType.EOF, "")

        if c.isalpha() or c == "_" or c == ".":
            return Token(
                TokenType.KEYWORD,
                str=self._keyword(c),
                register=False
            )

        if c.isdigit() or c == "-":
            token = self._number(c)
            token.is_offset = True
            return token

        if c == '"' or c == "'":
            return self._string(c)

        if c == '%':
            name = self._keyword("")
            return Token(
                TokenType.KEYWORD,
                str=name,
                register=True
            )

        if c == "$":
            return self._immediate()

        if c in "#\n":
            while c != "\n":
                c = self._get()
            return Token(TokenType.OPERATOR, str=";")

        if c in set(",:();"):
            return Token(TokenType.OPERATOR, str=c)

        if c == "/" and self._get():
            while True:
                c = self._get()
                if c == "":
                    self.raise_error("Unterminated nulti-line comment")
                else:
                    while c == "*":
                        c = self._get()
                        if c == "/":
                            return self.next()

        self.raise_error("Unknown token")

    def _immediate(self):
        c = self._get()
        if not c.isdecimal() and c != "-":
            name = self._keyword(c)
            token = Token(
                TokenType.LITERAL,
                str=name,
                val=Label(None, name),
                valtype=ast_types.Arithmetic.DWORD
            )
        else:
            token = self._number(c)
        token.is_offset = False
        return token

    def _number(self, c):
        if c == "0":
            return self._oct()

        mul = 1
        if c == "-":
            mul = -1
            c = "0"
        type = ast_types.Arithmetic.DWORD
        lit = ''
        while c.isdecimal():
            lit += c
            c = self._get()
        self._unget(c)
        return Token(TokenType.LITERAL, str=lit, val=int(lit)*mul, valtype=type)

    def _oct(self) -> Token:
        c = self._get().lower()
        if c == "x":
            return self._hex()
        elif c == "f":
            return self._float()
        elif c == "b":
            return self._bin()

        lit = '0'
        while c in set("01234567"):
            lit += c
            c = self._get()

        self._unget(c)
        return Token(TokenType.LITERAL, str=lit, val=int(lit, 8), valtype=ast_types.Arithmetic.DWORD)

    def _bin(self) -> Token:
        lit = ''
        c = self._get()
        while c in set("01"):
            lit += c
            c = self._get()

        self._unget(c)
        return Token(TokenType.LITERAL, str="0b"+lit, val=int(lit, 2), valtype=ast_types.Arithmetic.DWORD)

    def _hex(self) -> Token:
        lit = ''
        c = self._get()
        while c.isdecimal() or c.lower() in set("abcdef"):
            lit += c
            c = self._get()

        if not lit:
            self.raise_error("Expected hexadecimal number")

        self._unget(c)

        # nbytes = minimum power of two greater than len(lit) / 2
        nbytes = 2**math.ceil(math.log2(math.ceil(len(lit) / 2)))
        if nbytes == 1:
            valtype = ast_types.Arithmetic.UBYTE
        elif nbytes == 2:
            valtype = ast_types.Arithmetic.UWORD
        elif nbytes == 4:
            valtype = ast_types.Arithmetic.UDWORD
        else:
            valtype = ast_types.Arithmetic.UQWORD

        return Token(TokenType.LITERAL, str="0x" + lit, val=int(lit, 16), valtype=valtype)

    def _skipws(self) -> str:
        c = ' '
        while c.isspace() and c != "\n":
            c = self._get()
        return c

    def _keyword(self, c: str) -> str:
        result = c
        while True:
            c = self._get()
            if not c.isalnum() and c not in {'_', ".", "@", "$"}:
                break
            result += c
        self._unget(c)
        return result

    def _string(self, terminator: str) -> Token:
        lit = ''
        while True:
            c = self._get()
            if c == terminator:
                break
            if not c:
                self.raise_error("Unterminated string literal")
            if c == '\\':
                lit += self._char_esc()
            else:
                lit += c
        if terminator == "'":
            if len(lit) != 1:
                self.raise_error("Character literal is too long")
            return Token(TokenType.LITERAL, str=lit, val=ord(lit), valtype=t.Arithmetic.BYTE)
        return Token(TokenType.STRING, str=lit)

    def _char_esc(self):
        c = self._get()
        if c in "\\\'\"?":
            return c
        elif c == 'a':
            return '\a'
        elif c == 'b':
            return '\b'
        elif c == 'f':
            return '\f'
        elif c == 'n':
            return '\n'
        elif c == 'r':
            return '\r'
        elif c == 't':
            return '\t'
        elif c == 'v':
            return '\v'
        # TODO \x \0 \u \U
        else:
            self.raise_error("Unknown escape sequence: \%s" % c)

    def _float(self) -> Token:
        mult = 1
        c = self._get()
        lit = ""

        if c in set("-+"):
            lit += c
            c = self._get()

        while c.isdecimal():
            lit += c
            c = self._get()

        if c == '.':
            lit += c
            c = self._get()
            while c.isdigit():
                lit += c
                c = self._get()

        if c.lower() == 'e':
            lit += c
            c = self._get()
            if c in "+-":
                lit += c
                c = self._get()
            if not c.isdecimal():
                self.raise_error("Invalid float literal %s" % lit)

            while c.isdecimal():
                lit += c
                c = self._get()

        return Token(TokenType.LITERAL, str=lit, val=float(lit), valtype=t.Arithmetic.FLOAT)


class GasParser(Parser):
    suffixes = {
        "b": 1,
        "s": 2,
        "w": 2,
        "l": 4,
        "q": 8,
        "t": 10,
    }

    def __init__(self, lexer, typesizer, arch):
        super().__init__(lexer, typesizer)
        self.arch = arch

    def _parse(self) -> n.AsmProgram:
        """
        PROGRAM := STMT | STMT ; PROGRAM | (empty)
        """
        root = self._debug_info().wrap(n.AsmProgram())
        while not self._is_eof():
            while self._is_op(";"):
                self._scan()
            if self._is_eof():
                break
            root.add(self._parse_stmt())
        return root

    def _is_eof(self) -> bool:
        return self.la.type == TokenType.EOF

    def _is_op(self, op) -> bool:
        return self.la.type == TokenType.OPERATOR and self.la.str == op

    def _is_eol(self) -> bool:
        return self._is_eof() or self._is_op(";")

    def _parse_stmt(self):
        """
        STMT := label: STMT | mnem OPERANDS | "."directive DIRECTIVE | (empty)
        """
        if self.la.type != TokenType.KEYWORD or self.la.register or not self.la.str:
            self.raise_error("Expected label, mnemonic or directive")

        name = self.la.str
        self._scan()

        if self._is_op(":"):
            self._scan()
            return n.AsmLabel(name)

        if name[0] == ".":
            stmt = self._parse_directive(name)
        else:
            stmt = self._parse_operation(name)

        if not self._is_eol():
            self.raise_error("Expected new line")
        self._scan()
        return stmt

    def _parse_operation(self, name):
        """
        OPERANDS := OPERAND | OPERAND , OPERAND | (empty)
        """
        size = None
        if name.upper() not in self.arch.mnemonics:
            if name[-1] in self.suffixes:
                size = self.suffixes[name[-1]]
                if name[:-1].upper() not in self.arch.mnemonics:
                    self.raise_error("Unknown mnemonic %s" % name)
                name = name[:-1]
            else:
                self.raise_error("Unknown mnemonic %s" % name)

        mnem = name.lower()
        op1 = op2 = None
        if not self._is_eol():
            op1 = self._parse_operand(size)
            if self._is_op(","):
                self._scan()
                op2 = op1
                op1 = self._parse_operand(size)
        return n.AsmOp(Operation(mnem, op1, op2))

    def _parse_operand(self, size):
        """
        OPERAND := REGISTER | immediate | offset ( REGISTER ) | label
        """
        if self.la.type == TokenType.LITERAL:
            if isinstance(self.la.val, Label):
                val = self.la.val
                val.size = self.arch.generator_class.label_size
                self._scan()
                return val
            elif self.la.is_offset:
                off = self.la.val
                self._scan()
                if not self._is_op("("):
                    self.raise_error("Expected (")
                self._scan()
                if not self.la.type == TokenType.KEYWORD or not self.la.register:
                    self.raise_error("Expected register")
                reg = self.arch.register_class.from_name(self.la.str)
                self._scan()
                if not self._is_op(")"):
                    self.raise_error("Expected )")
                self._scan()
                return DerefRegister(size, reg, off)
            else:
                if not size:
                    size = self.typesizer.sizeof(self.la.valtype)
                val = IntLiteral(size, self.la.val)
                self._scan()
                return val
        elif self.la.type == TokenType.KEYWORD:
            if self.la.register:
                val = self.arch.register_class.from_name(self.la.str)
                self._scan()
                return val
            else:
                val = Address(Label(self.arch.generator_class.label_size, self.la.str))
                self._scan()
                return val
        self.raise_error("Invalid operand")

    def _parse_directive(self, direc):
        """
        DIRECTIVE :=
                  | .string STRING_LIT | .byte BYTE_LIT | .float FLOAT_LIT | .octa BIGNUM_LIT
                  | .globl label |  .global label | .extern label
                  | .type label , @function
                  | .section .name | .text | .data
                  | .ident string
                  | .file num string
                  | .loc LOC

        See https://sourceware.org/binutils/docs-2.23/as/Pseudo-Ops.html
        """
        if direc == ".string":
            if self.la.type != TokenType.STRING:
                self.raise_error("Expected string")
            val = n.AsmString(self.la.str + "\0")
            self._scan()
            return val
        elif direc == ".byte":
            lit = []
            while True:
                self._scan()
                if self.la.type != TokenType.LITERAL:
                    self.raise_error("Expected character literal")
                lit.append(self.la.val)
                self._scan()
                if self._is_eof() or self._is_op(";"):
                    return n.AsmString(bytes(lit))
                if not self._is_op(","):
                    self.raise_error("Expected ,")
        elif direc == ".float":
            self._scan()
            if self.la.type != TokenType.LITERAL:
                self.raise_error("Expected float literal")
            val = float(self.la.val)
            self._scan()
            return Literal(val, t.Arithmetic.FLOAT)
        elif direc == ".octa":
            self._scan()
            if self.la.type != TokenType.LITERAL or not self.la.valtype.is_int:
                self.raise_error("Expected integer literal")
            lit = Literal(self.la.val, self.la.valtype)
            self._scan()
            return lit
        elif direc in {".globl", ".global"}:
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected label name")
            val = n.AsmGlobal(self.la.str)
            self._scan()
            return val
        elif direc == ".extern":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected label name")
            val = n.AsmExtern(self.la.str)
            self._scan()
            return val
        elif direc == ".type":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected label name")
            varname = self.la.str
            self._scan()
            if not self._is_op(","):
                self.raise_error("Expected ,")
            self._scan()

            if self.la.type == TokenType.KEYWORD:
                typename = self.la.str.lstrip("@#%")
            elif self.la.type == TokenType.STRING:
                typename = self.la.str
            else:
                self.raise_error("Expected type name")
            self._scan()
            return n.AsmType(varname, typename)
        elif direc == ".section":
            if self.la.type != TokenType.KEYWORD:
                self.raise_error("Expected section name")
            val = n.AsmSection(self.la.str)
            self._scan()
            return val
        elif direc in {".text", ".data"}:
            val = n.AsmSection(self.la.str)
            self._scan()
            return val
        elif direc == ".fill":
            if self.la.type != TokenType.LITERAL:
                self.raise_error("Expected repeat literal")
            repeat = self.la.val
            size = 1
            value = 0
            self._scan()
            if self._is_op(","):
                self._scan()
                if self.la.type != TokenType.LITERAL:
                    self.raise_error("Expected size literal")
                size = min(8, max(1, self.la.val))
                self._scan()
                if self._is_op(","):
                    self._scan()
                    if self.la.type != TokenType.LITERAL:
                        self.raise_error("Expected value literal")
                    value = self.la.val
                    self._scan()
            data = fixint.int_factory(size, False, False)(value).to_bytes() * repeat
            return n.AsmString(data)

        dir = n.AsmDirective(direc[1:], [])
        while not self._is_eol() and not self._is_eof():
            dir.args.append(self.la.str)
            self._scan()
            if self._is_op(","):
                self._scan()
        return dir
