from .x86_64 import x86_64

archs = [
    x86_64,
]

arch_by_name = {
    a.name: a
    for a in archs
}

arch_names = list(arch_by_name.keys())


def get_arch(name):
    return arch_by_name[name]

