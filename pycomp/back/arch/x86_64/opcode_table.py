import enum
from .... import fixint
from ... import asm
from .registers import register_encoding as re
from .. import assembler


def rex(w=False, r=False, x=False, b=False, condition=True):
    """
    0100W00B -> register operand encoded in opcode byte Opcode (.....bbb)
                reg=Bbbb (1 prepended to the actual register encoding)
    0100WRXB -> ModRM (mod!=11,reg=rrr,r/m=100) SIB(scale=ss,index=xxx,base=bbb)
                reg=Rrrr, index=Xxxx, base=Bbbb
    0100WR0B -> Memory addressing without SIB ModRM(mod!=11,reg=rrr,r/m=bbb)
                reg=Rrrr, r/b=Bbbb
             -> Register-Register addressing  ModRM(mod==11,reg=rrr,r/m=bbb)
                reg=Rrrr, r/m=Bbbb
    """
    if not condition:
        return None
    rex = 0x40 # REX starts with 0100....
    if w: # 64 bit operand
        rex |= 1 << 3
    if r: # Extension of the ModR/M ref field
        rex |= 1 << 2
    if x: # Extension of the SIB index field
        rex |= 1 << 1
    if b: # Extension of ModR/M r/m, SIB base, or Opcode reg field
        rex |= 1 << 0
    return rex


prefix_override_operand_size = 0x66
prefix_override_address_size = 0x67

mod_displacement_0 = 0
mod_displacement_1 = 1
mod_displacement_4 = 2
mod_reg_reg = 3


def modrm(mod, reg, rm):
    return (
        ((mod & 0x3) << 6) | # mod: 2 bits one of the mod_ values
        ((reg & 0x7) << 3) | # reg: 3 bits
         (rm  & 0x7)         # rm:  3 bits
    )


class x86_64Instruction(assembler.Instruction):
    def __init__(
        self,
        *,
        prefix=None, # legacy prefix
        rex=None, # REX prefix (1B)
        opcode=None, # 1,2,3 B
        modrm=None, # 1 B
        sib=None, # 1 B
        displacement=None, # 1,2,4 B
        immediate=None, # 1,2,4 B
    ):
        self.prefix = prefix
        self.rex = rex
        self.opcode = opcode
        self.modrm = modrm
        self.sib = sib
        self.displacement = displacement
        self.immediate = immediate

        if isinstance(immediate, asm.Address):
            immediate = immediate.child

        if isinstance(immediate, asm.IntLiteral):
            #if immediate.value < 0:
            n = {
                    1: fixint.Int8,
                    2: fixint.Int16,
                    4: fixint.Int32,
                    8: fixint.Int64,
                }
            #else:
                #n = {
                    #1: fixint.UInt8,
                    #2: fixint.UInt16,
                    #4: fixint.UInt32,
                    #8: fixint.UInt64,
                #}
            self.immediate = n[immediate.size](immediate.value)
        elif isinstance(immediate, asm.Label):
            self.immediate = fixint.UInt32(0)

        if isinstance(self.prefix, (tuple, list, bytes)):
            self.prefix = list(self.prefix)
        elif self.prefix is not None:
            self.prefix = [self.prefix]
        else:
            self.prefix = []

    def __bytes__(self):
        by = []
        by += self.prefix

        if self.rex is not None:
            by.append(self.rex)

        if isinstance(self.opcode, tuple):
            by += list(self.opcode)
        else:
            by.append(self.opcode)

        if self.modrm is not None:
            by.append(self.modrm)

        if self.sib is not None:
            by.append(self.sib)

        by = bytes(by)
        # TODO endianness
        if self.displacement is not None:
            by += self.displacement.to_bytes()

        if self.immediate is not None:
            by += self.immediate.to_bytes()
        return by

    def relocation_offset(self):
        if self.immediate is not None:
            return len(bytes(self)) - self.immediate.nbytes
        return 0


class x84_64OperandType(assembler.OperandType):
    none    = 0x00

    register= 0x10
    r8      = 0x11
    r16     = 0x12
    r32     = 0x14
    r64     = 0x18

    immediate = 0x20
    imm8    = 0x21
    imm16   = 0x22
    imm32   = 0x24
    imm64   = 0x28

    memory  = 0x40
    m8      = 0x41
    m16     = 0x42
    m32     = 0x44
    m64     = 0x48

    @classmethod
    def from_operand(cls, op: asm.Operand) -> "OperandType":
        if op is None:
            return cls.none
        if isinstance(op, asm.Address):

            op = op.child
        if isinstance(op, asm.Register):
            return cls(cls.register.value|op.size)
        if isinstance(op, asm.IntLiteral):
            return cls(cls.immediate.value|op.size)
        if isinstance(op, asm.DerefRegister):
            return cls(cls.memory.value|op.size)

        if isinstance(op, asm.Label):
            return cls.imm32
        raise Exception("Not implemented")


class x86_64OperationDescriptor(assembler.OperationDescriptor):
    operand_type = x84_64OperandType


class x86_64Opcode:
    def __init__(self, code, widerex=True, relimm=False, invops=False,
                 modrm_reg=None):
        self.code = code
        self.widerex = widerex
        self.relimm = relimm
        self.invops = invops
        self.modrm_reg = modrm_reg

    def _isreg(self, obj):
        return isinstance(obj, (asm.DerefRegister, asm.Register))

    def _bits3(self, obj):
        if self._isreg(obj):
            return re.bit3(obj)
        return 0

    def _build_rex(self, op):
        rex_r = self._isreg(op.op2) and re.is_high(op.op2)
        rex_w = self.widerex and op.op1.size == 8
        rex_b = self._isreg(op.op1) and re.is_high(op.op1)

        if rex_r or rex_w or rex_b or (
            self._isreg(op.op1) and op.op1.size == 1 and
            re[op.op1] in range(4, 8) and
            not re.has_high_byte(op.op1)
        ):
            return rex(w=rex_w, r=rex_r, b=rex_b)
        return None

    def __call__(self, op):
        return self.mk_instr(op)

    def _modrm_size(self, op: asm.DerefRegister):
        if op.offset == 0:
            return mod_displacement_0, None

        displacement = fixint.best_fit(op.offset, True)
        if type(displacement).nbytes == 1:
            return mod_displacement_1, displacement

        return mod_displacement_4, fixint.Int32(displacement)

    def _is_immediate(self, operand):
        return isinstance(operand, (asm.IntLiteral, asm.Label, asm.Address))

    def mk_instr(self, op):
        prefix = []
        rexp = None
        modrms = None
        imm = None
        displacement = None

        if self.invops:
            op = asm.Operation(op.mnem, op.op2, op.op1)

        if op.op1:
            op1 = op.op1.child if isinstance(op.op1, asm.Address) else op.op1
            op2 = op.op2.child if isinstance(op.op2, asm.Address) else op.op2

            if op1.size == 2 or (op2 and op2.size == 2):
                prefix.append(prefix_override_operand_size)
            rexp = self._build_rex(op)

            if self._isreg(op1) or self._isreg(op2):
                mod = mod_displacement_0
                if isinstance(op1, asm.Register) and isinstance(op2, asm.Register):
                    mod = mod_reg_reg
                elif isinstance(op1, asm.Register) and (self._is_immediate(op2) or op2 is None):
                    mod = mod_reg_reg
                elif isinstance(op1, asm.DerefRegister):
                    mod, displacement = self._modrm_size(op1)
                    if op1.register.size == 4:
                        prefix.insert(0, prefix_override_address_size)

                modrm_reg = self.modrm_reg if self.modrm_reg is not None else self._bits3(op2)
                modrms = modrm(mod, modrm_reg, self._bits3(op1))

            if self._is_immediate(op1):
                imm = op1
            elif self._is_immediate(op2):
                imm = op2

            if imm and self.relimm and isinstance(imm, asm.IntLiteral):
                imm.value -= imm.size

        return x86_64Instruction(
            prefix=prefix,
            rex=rexp,
            opcode=self.code,
            modrm=modrms,
            displacement=displacement,
            immediate=imm
        )


class x86_64OpcodeOrReg(x86_64Opcode):
    def _build_rex(self, op):
        rex_b = self._isreg(op.op1) and re.is_high(op.op1)
        rex_w = self.widerex and op.op1.size == 8
        rex_r = False
        if rex_r or rex_w or rex_b:
            return rex(w=rex_w, r=rex_r, b=rex_b)
        return None

    def mk_instr(self, op):
        prefix = None
        rexp = None
        modrms = None
        imm = None

        if op.op1.size == 2:
            prefix = 0x66
        rexp = self._build_rex(op)

        if isinstance(op.op2, asm.IntLiteral):
            imm = op.op2

        return x86_64Instruction(
            prefix=prefix,
            rex=rexp,
            opcode=self.code|re.bit3(op.op1),
            modrm=modrms,
            immediate=imm
        )


class x86_64OpcodeFixed:
    def __init__(self, code):
        self.code = code

    def __call__(self, op):
        return x86_64Instruction(opcode=self.code)


class x86_64Rax:
    def __init__(self, norax, rax, name, **kw):
        if isinstance(norax, int):
            norax = x86_64Opcode(norax, **kw)
        self.norax = norax
        if isinstance(rax, int):
            rax = x86_64Opcode(rax)
        self.rax = rax
        self.name = name

    def __call__(self, op):
        if op.op1.name == self.name:
            instr = self.rax(asm.Operation(op.mnem, op.op2))
            instr.rex = self.rax._build_rex(op)
            return instr
        else:
            return self.norax(op)


class x86_64OpcodeTable(assembler.OpcodeTable):
    d = x86_64OperationDescriptor
    o = x84_64OperandType

    table = {
        # TODO add a way to group same-opcode instructions together
        d("add",  o.r8,    o.r8):    x86_64Opcode(0x00),
        d("add",  o.m8,    o.r8):    x86_64Opcode(0x00),
        d("add",  o.r8,    o.imm8):  x86_64Rax(0x80, 0x04, "al"),
        d("add",  o.r64,   o.imm32): x86_64Rax(0x81, 0x05, "rax"),
        d("add",  o.r64,   o.imm8):  x86_64Opcode(0x83),
        d("add",  o.r16,   o.r16):   x86_64Opcode(0x01),
        d("add",  o.r32,   o.r32):   x86_64Opcode(0x01),
        d("add",  o.r64,   o.r64):   x86_64Opcode(0x01),

        d("sub",  o.r8,    o.r8):    x86_64Opcode(0x28),
        d("sub",  o.m8,    o.r8):    x86_64Opcode(0x28),
        d("sub",  o.r8,    o.imm8):  x86_64Rax(0x80, 0x2C, "al", modrm_reg=5),
        d("sub",  o.r64,   o.imm32): x86_64Rax(0x81, 0x2D, "rax", modrm_reg=5),
        d("sub",  o.r64,   o.imm8):  x86_64Opcode(0x83, modrm_reg=5),
        d("sub",  o.r16,   o.r16):   x86_64Opcode(0x29),
        d("sub",  o.r32,   o.r32):   x86_64Opcode(0x29),
        d("sub",  o.r64,   o.r64):   x86_64Opcode(0x29),

        d("mov",  o.r64,   o.imm64): x86_64OpcodeOrReg(0xB8),
        d("mov",  o.r32,   o.imm32): x86_64OpcodeOrReg(0xB8),
        d("mov",  o.r16,   o.imm16): x86_64OpcodeOrReg(0xB8),
        d("mov",  o.m64,   o.imm32): x86_64Opcode(0xC7),
        d("mov",  o.m32,   o.imm32): x86_64Opcode(0xC7),
        d("mov",  o.m16,   o.imm16): x86_64Opcode(0xC7),
        d("mov",  o.r64,   o.imm32): x86_64Opcode(0xC7),
        d("mov",  o.r8,    o.r8):    x86_64Opcode(0x88),
        d("mov",  o.r16,   o.r16):   x86_64Opcode(0x89),
        d("mov",  o.r32,   o.r32):   x86_64Opcode(0x89),
        d("mov",  o.r64,   o.r64):   x86_64Opcode(0x89),
        d("mov",  o.r64,   o.m64):   x86_64Opcode(0x8B, invops=True),
        d("mov",  o.r32,   o.m32):   x86_64Opcode(0x8B, invops=True),
        d("mov",  o.r16,   o.m16):   x86_64Opcode(0x8B, invops=True),
        d("mov",  o.r8,   o.m8):     x86_64Opcode(0x8A, invops=True),

        d("push", o.r64,   o.none):  x86_64OpcodeOrReg(0x50, False),
        d("push", o.r16,   o.none):  x86_64OpcodeOrReg(0x50),
        d("push", o.imm16, o.none):  x86_64Opcode(0x68),
        d("push", o.imm32, o.none):  x86_64Opcode(0x68),

        d("pop",  o.r64,   o.none):  x86_64OpcodeOrReg(0x58, False),
        d("pop",  o.r16,   o.none):  x86_64OpcodeOrReg(0x58),

        d("ret",  o.none,  o.none):  x86_64OpcodeFixed(0xC3),
        d("call", o.imm32, o.none):  x86_64Opcode(0xE8, relimm=True),
        d("jmp",  o.imm32, o.none):  x86_64Opcode(0xE9, relimm=True),

        d("imul",  o.r16,   o.r16):   x86_64Opcode((0x0F, 0xAF), invops=True),
        d("imul",  o.r32,   o.r32):   x86_64Opcode((0x0F, 0xAF), invops=True),
        d("imul",  o.r64,   o.r64):   x86_64Opcode((0x0F, 0xAF), invops=True),
        d("imul",  o.r16,   o.m16):   x86_64Opcode((0x0F, 0xAF), invops=True),
        d("imul",  o.r32,   o.m32):   x86_64Opcode((0x0F, 0xAF), invops=True),
        d("imul",  o.r64,   o.m64):   x86_64Opcode((0x0F, 0xAF), invops=True),

        d("xor",  o.r8,    o.r8):    x86_64Opcode(0x30),
        d("xor",  o.r16,   o.r16):   x86_64Opcode(0x31),
        d("xor",  o.r32,   o.r32):   x86_64Opcode(0x31),
        d("xor",  o.r64,   o.r64):   x86_64Opcode(0x31),
        d("xor",  o.r8,    o.m8):    x86_64Opcode(0x32, invops=True),
        d("xor",  o.r16,   o.m16):   x86_64Opcode(0x33, invops=True),
        d("xor",  o.r32,   o.m32):   x86_64Opcode(0x33, invops=True),
        d("xor",  o.r64,   o.m64):   x86_64Opcode(0x33, invops=True),

        d("seta",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x97)),
        d("setae",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x93)),
        d("setb",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x92)),
        d("setbe",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x96)),
        d("setc",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x92)),
        d("sete",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x94)),
        d("setg",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x9F)),
        d("setge",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9D)),
        d("setl",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x9C)),
        d("setle",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9E)),
        d("setna",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x96)),
        d("setnae", o.r8,  o.none):   x86_64Opcode((0x0F, 0x92)),
        d("setnb",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x93)),
        d("setnbe", o.r8,  o.none):   x86_64Opcode((0x0F, 0x97)),
        d("setnc",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x93)),
        d("setne",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x95)),
        d("setng",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9E)),
        d("setnge", o.r8,  o.none):   x86_64Opcode((0x0F, 0x9C)),
        d("setnl",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9D)),
        d("setnle", o.r8,  o.none):   x86_64Opcode((0x0F, 0x9F)),
        d("setno",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x91)),
        d("setnp",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9B)),
        d("setns",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x99)),
        d("setnz",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x95)),
        d("seto",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x90)),
        d("setp",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x9A)),
        d("setpe",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9A)),
        d("setpo",  o.r8,  o.none):   x86_64Opcode((0x0F, 0x9B)),
        d("sets",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x98)),
        d("setz",   o.r8,  o.none):   x86_64Opcode((0x0F, 0x94)),

        d("test",  o.r8,    o.imm8):  x86_64Rax(0xF6, 0xA8, "al"),
        d("test",  o.r16,   o.imm16): x86_64Rax(0xF7, 0xA9, "ax"),
        d("test",  o.r32,   o.imm32): x86_64Rax(0xF7, 0xA9, "eax"),
        d("test",  o.r64,   o.imm32): x86_64Rax(0xF7, 0xA9, "rax"),
        d("test",  o.r8,    o.r8):    x86_64Opcode(0x84),
        d("test",  o.r16,   o.r16):   x86_64Opcode(0x85),
        d("test",  o.r32,   o.r32):   x86_64Opcode(0x85),
        d("test",  o.r64,   o.r64):   x86_64Opcode(0x85),

        d("cmp",   o.r8,    o.imm8):  x86_64Rax(0x80, 0x3C, "al", modrm_reg=7),
        d("cmp",   o.r16,   o.imm16): x86_64Rax(0x81, 0x3D, "ax", modrm_reg=7),
        d("cmp",   o.r32,   o.imm32): x86_64Rax(0x81, 0x3D, "eax", modrm_reg=7),
        d("cmp",   o.r64,   o.imm32): x86_64Rax(0x81, 0x3D, "rax", modrm_reg=7),
        d("cmp",   o.r8,    o.r8):    x86_64Opcode(0x38),
        d("cmp",   o.r16,   o.r16):   x86_64Opcode(0x39),
        d("cmp",   o.r32,   o.r32):   x86_64Opcode(0x39),
        d("cmp",   o.r64,   o.r64):   x86_64Opcode(0x39),

        d("ja",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x87), relimm=True),
        d("jae",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x83), relimm=True),
        d("jb",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x82), relimm=True),
        d("jbe",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x86), relimm=True),
        d("jc",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x82), relimm=True),
        d("je",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x84), relimm=True),
        d("jg",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x8F), relimm=True),
        d("jge",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8D), relimm=True),
        d("jl",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x8C), relimm=True),
        d("jle",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8E), relimm=True),
        d("jna",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x86), relimm=True),
        d("jnae",  o.imm32, o.none):  x86_64Opcode((0x0F, 0x82), relimm=True),
        d("jnb",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x83), relimm=True),
        d("jnbe",  o.imm32, o.none):  x86_64Opcode((0x0F, 0x87), relimm=True),
        d("jnc",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x83), relimm=True),
        d("jne",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x85), relimm=True),
        d("jng",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8E), relimm=True),
        d("jnge",  o.imm32, o.none):  x86_64Opcode((0x0F, 0x8C), relimm=True),
        d("jnl",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8D), relimm=True),
        d("jnle",  o.imm32, o.none):  x86_64Opcode((0x0F, 0x8F), relimm=True),
        d("jno",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x81), relimm=True),
        d("jnp",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8B), relimm=True),
        d("jns",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x89), relimm=True),
        d("jnz",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x85), relimm=True),
        d("jo",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x80), relimm=True),
        d("jp",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x8A), relimm=True),
        d("jpe",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8A), relimm=True),
        d("jpo",   o.imm32, o.none):  x86_64Opcode((0x0F, 0x8B), relimm=True),
        d("js",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x88), relimm=True),
        d("jz",    o.imm32, o.none):  x86_64Opcode((0x0F, 0x84), relimm=True),
    }

    def assembler(self, od: x86_64OperationDescriptor):
        return self.table[od]
