import copy
import typing
from collections import OrderedDict

from ...base import Generator
from .... import ast_nodes as n
from .... import ast_asm as an
from .... import ast_types as t
from .... import symbol_table
from .... import fixint
from ....autohandler import handler
from ... import asm
from .registers import x86_64Register


def label_generator(template):
    counter = 0
    while True:
        yield template % counter
        counter += 1


class AsmX86_64Generator(Generator):
    binops_int = {
        "+": "add",
        "-": "sub",
        "*": "imul",
    }
    binops_int_cmp = {
        "<": "setl",
        "<=": "setle",
        "==": "sete",
        ">=": "setge",
        ">": "setg",
        "!=": "setne",

    }
    pointer_size = 8
    label_size = 4
    stack_pointer = x86_64Register.from_name("rsp")
    stack_base_pointer = x86_64Register.from_name("rbp")
    rax = x86_64Register.from_name("rax")
    rbx = x86_64Register.from_name("rbx")
    args_int = [
        x86_64Register.from_name("rdi"),
        x86_64Register.from_name("rsi"),
        x86_64Register.from_name("rdx"),
        x86_64Register.from_name("rcx"),
        x86_64Register.from_name("r8"),
        x86_64Register.from_name("r9"),
    ]
    stack_frame_size = pointer_size

    def __init__(self, stream: typing.IO, flavour: asm.Flavour, typesizer: symbol_table.TypeSizer):
        super().__init__(stream)
        self.flavour = flavour
        self.jumpcount = -1
        self._loop_labels = []
        self.typesizer = typesizer
        self.symbol_table = symbol_table.StackSymbolTable(typesizer, self.pointer_size)
        self.labgen_constants = label_generator(".L__const_%s")
        self.labgen_jump = label_generator(".L__jump_%s")
        self.labgen_temp = label_generator(".L__temp_%s")
        self.labgen_return = label_generator(".L__return_%s")
        self.constants = OrderedDict()
        self._return_label = None
        self.register_pusher = asm.RegisterPusher(self.push_temp)

    # utils --------------------

    def _64b_operand(self, operand):
        if isinstance(operand, asm.IntLiteral) and operand.size == 8:
            if fixint.best_fit(operand.value, operand.value < 0).nbytes == 8:
                reg = x86_64Register.from_name("r15")
                self.register_pusher.use(reg)
                self.sizedop("mov", reg, operand)
                return reg
            else:
                operand.size = 4
        return operand

    def print_op(self, op):
        if op.mnem not in {"mov", "movq"}:
            op.op1 = self._64b_operand(op.op1)
            op.op2 = self._64b_operand(op.op2)
        self.flavour.print_op(op)

    def sizedop(self, mnem, op1: asm.Operand, op2=None):
        op1 = self.register_pusher.unwrap(op1)
        op2 = self.register_pusher.unwrap(op2)

        if mnem in ["push", "pop"]:
            # Must push/pop quadwords but can't push imm64 o_O
            if isinstance(op1, asm.Register) and op1.size != self.pointer_size:
                op1 = op1.resized(self.pointer_size)

            self.print_op(
                self.flavour.sized_op(self.pointer_size, mnem, op1, op2)
            )
            return
        elif (
            mnem == "mov" and isinstance(op1, asm.Register)
            and isinstance(op2, asm.IntLiteral) and op2.value == 0
        ):
            return self.sizedop("xor", op1, op1)

        if op2 and op2.size != op1.size and isinstance(op2, asm.Register):
            op2 = op2.resized(op1.size)
        self.print_op(
            self.flavour.sized_op(op1.size, mnem, op1, op2)
        )

    def _move_to_register(self, loc, dest: asm.Register):
        if loc.size != dest.size:
            dest = dest.resized(loc.size)

        loc = self.register_pusher.unwrap(loc)

        if isinstance(loc, asm.StackTemp) and self._is_on_stack(loc):
            self.symbol_table.pop_symbol()
            self.sizedop("pop", dest)
        elif isinstance(loc, asm.Register):
            if loc.name == dest.name:
                return loc
            self.sizedop("mov", dest, loc)
        else:
            self.sizedop("mov", dest, loc)

        return dest

    # loops  --------------------

    def _push_loop_labels(self, breakto, continueto):
        self._loop_labels.append((breakto, continueto))

    def _break_label(self):
        if self._loop_labels:
            return self._loop_labels[-1][0]
        else:
            raise Exception("Extra break")

    def _continue_label(self):
        if self._loop_labels:
            return self._loop_labels[-1][1]
        else:
            raise Exception("Extra continue")

    def _pop_loop_labels(self):
        self._loop_labels.pop()

    # type detection --------------------

    def _is_charstar(self, node: n.AstNode) -> bool:
        return isinstance(node, n.Literal) and node.datatype == t.Pointer(t.Arithmetic.BYTE)

    def _is_int(self, node: n.AstNode) -> bool:
        return isinstance(node.datatype, t.Arithmetic) and (
            node.datatype.is_int
            or node.datatype == t.Arithmetic.BOOL
        )

    def _is_int_ptr(self, node: n.AstNode) -> bool:
        return self._is_int(node) or isinstance(node.datatype, t.Pointer)

    # stack manipulation --------------------

    def _allocate(self, nbytes):
        if nbytes > 0:
            self.sizedop("sub", self.stack_pointer, asm.IntLiteral(4, nbytes))

    def _deallocate(self, nbytes):
        if nbytes > 0:
            self.sizedop("add", self.stack_pointer, asm.IntLiteral(4, nbytes))

    def push_stack(self, stack=None):
        self.symbol_table.push_stack(stack)
        self.symbol_table.push()

    def pop_stack(self):
        stack_size = self.symbol_table.stack_size()
        self.symbol_table.pop_stack()
        self._deallocate(stack_size)

    def push_temp(self, type: t.Type, arg: asm.Register):
        lab = next(self.labgen_temp)
        self.symbol_table.declare_temp(lab, type)
        self.sizedop("push", arg.resized(self.typesizer.sizeof(type)))
        return asm.StackTemp(self.typesizer.sizeof(type), lab, type)

    def _is_on_stack(self, operand):
        if not isinstance(operand, asm.StackTemp):
            return False
        if not self.symbol_table.check_temp(operand.name, operand.type):
            raise Exception("Invalid pop")
        return True

    def _declared_symbol_location(self, symbol: symbol_table.Symbol):
        return asm.DerefRegister(
            symbol.metadata["size"],
            self.stack_base_pointer,
            -symbol.metadata["offset"] - self.stack_frame_size
        )

    # AST > asm --------------------

    def handle(self, node: n.AstNode):
        if node.debug_info:
            self.flavour.debug_info(node.debug_info)
        return super().handle(node)

    def generate(self, node: n.Program):
        self.flavour.initialize_file()
        self.handle(node)
        self.flavour.finalize_file()

    @handler
    def program(self, node: n.Program):
        for charconst in node.findall(self._is_charstar):
            if charconst.value in self.constants:
                continue
            lab = next(self.labgen_constants)
            self.constants[charconst.value] = (charconst, asm.Label(self.pointer_size, lab))
            self.symbol_table.declare_raw(lab, symbol_table.SymbolType.temp, charconst.datatype)

        self.flavour.empty_line()
        self.flavour.section(".text")

        defined_funcs = set()
        for c in node.statements:
            if isinstance(c, n.FunctionDeclaration) and c.body:
                self.flavour.define_global(c.name)
                self.flavour.define_type(c.name, "function")
                defined_funcs.add(c.name)

        for c in node.statements:
            if isinstance(c, n.FunctionDeclaration) and not c.body and c.name not in defined_funcs:
                self.flavour.define_extern(c.name)
                self.flavour.define_type(c.name, "function")
                defined_funcs.add(c.name)

        self.flavour.empty_line()
        for c in node.statements:
            self.handle(c)

        self.flavour.section(".rodata")
        for name, (literal, label) in self.constants.items():
            self.flavour.place_label(label.name)
            if self._is_charstar(literal):
                type = t.Arithmetic.BYTE
            else:
                type = literal.datatype
            self.flavour.data(type, literal.value)

        self.flavour.empty_line()
        self.flavour.ident("Compiled with pycomp")

    @handler
    def block(self, node: n.StatementBlock):
        for c in node.statements:
            self.handle(c)

    @handler
    def varags(self, node: n.VarArgs):
        pass

    @handler
    def binop(self, node: n.BinaryOp):
        if self._is_int_ptr(node):
            return self.binop_int(node)
        raise Exception("asm.Operation not implemented")

    def binop_int(self, node: n.BinaryOp):
        lhs = self.handle(node.left)
        if isinstance(lhs, asm.Register) and self.rax.resized(lhs.size).name != self.rax.name:
            self.register_pusher.use(self.rax)
            self.sizedop("mov", self.rax, lhs)
            lhs = self.register_pusher.protect(self.rax)

        rhs = self.handle(node.right)

        areg = self._move_to_register(lhs, self.rax)
        self.register_pusher.use(self.rbx)
        breg = self._move_to_register(rhs, self.rbx)

        if node.op in self.binops_int:
            self.sizedop(self.binops_int[node.op], areg, breg)
            return self.register_pusher.protect(areg)
        elif node.op in self.binops_int_cmp:
            self.sizedop("cmp", areg, breg)
            self.sizedop("xor", areg, areg)
            areg = areg.resized(1)
            self.sizedop(self.binops_int_cmp[node.op], areg)
            return self.register_pusher.protect(
                self.rax.resized(self.typesizer.sizeof(node.datatype))
            )

        raise Exception("asm.Operation not implemented")

    @handler
    def assign(self, node: n.Assign):
        src = self.handle(node.right)
        dest = self.handle(node.left)
        self.sizedop("mov", dest, src)
        return dest

    @handler
    def unop(self, node: n.UnaryOp):
        pass # TODO

    @handler
    def func(self, node: n.FunctionDeclaration):
        if node.body:
            self.flavour.place_label(node.name)
            self.sizedop("push", self.stack_base_pointer)
            self.sizedop("mov", self.stack_base_pointer, self.stack_pointer)

            self.push_stack()

            subfuncs = node.body.findall(lambda x: isinstance(x, n.FunctionCall))
            max_called = max(len(sf.arg_list.args) for sf in subfuncs) if subfuncs else 0

            avail_intregs = list(self.args_int)
            for i, arg in enumerate(node.params.params):
                if isinstance(arg, n.VarArgs):
                    break
                if self._is_int_ptr(arg):
                    reg = avail_intregs.pop(0)
                    if i >= max_called:
                        self.symbol_table.declare_raw(
                            arg.name,
                            symbol_table.SymbolType.variable,
                            arg.datatype,
                            {"operand": reg}
                        )
                    else:
                        sym = self.symbol_table.declare_variable(
                            arg.name,
                            arg.datatype
                        )
                        self.sizedop("push", reg.resized(sym.metadata["size"]))
                        sym.metadata["operand"] = self._declared_symbol_location(sym)
                else:
                    raise Exception("Argument type not implemented")

            self._return_label = next(self.labgen_return)
            self.handle(node.body)
            self.flavour.place_label(self._return_label)
            self.pop_stack()
            self.sizedop("pop", self.stack_base_pointer)
            self.flavour.op("ret")
            self.flavour.empty_line()

    @handler
    def funcall(self, node: n.FunctionCall):
        funcloc = self.handle(node.function)
        avail_intregs = list(self.args_int)
        arglocs = {}
        nfloats = 0

        self.symbol_table.push()
        for i, param in reversed(list(enumerate(node.arg_list.args))):
            if param.findall(lambda x: isinstance(x, n.FunctionCall)):
                argloc = self.handle(param)
                if isinstance(argloc, asm.Register):
                    argloc = self.push_temp(param.datatype, argloc)
                arglocs[i] = argloc

        for i, param in enumerate(node.arg_list.args):
            if i in arglocs:
                argloc = arglocs[i]
            else:
                argloc = self.handle(param)

            if self._is_int_ptr(param):
                if avail_intregs:
                    self._move_to_register(argloc, avail_intregs.pop(0))
                else:
                    self.sizedop("push", argloc)
            elif param.datatype == t.Arithmetic.FLOAT:
                nfloats += 1
            # TODO float, etc

        scope = self.symbol_table.pop()
        self.push_stack([scope])
        if node.function.datatype.args and node.function.datatype.args[-1] == t.VarArgs():
            self.sizedop("mov", self.rax.resized(4), asm.IntLiteral(4, nfloats))

        self.flavour.op("call", self.register_pusher.unwrap(funcloc))
        # TODO non-int return types
        retval = self.rax.resized(self.typesizer.sizeof(node.datatype))
        self.pop_stack()
        return retval

    @handler
    def var(self, node: n.VariableDeclaration):
        sym = self.symbol_table.declare_variable(node.name, node.datatype)

        if not node.value:
            self._allocate(self.typesizer.sizeof(node.datatype))
        elif self._is_int(node.value):
            loc = self.handle(node.value)
            if self._is_on_stack(loc):
                self.symbol_table.pop_symbol()
            else:
                self.sizedop("push", loc)
            location = self._declared_symbol_location(sym)
            sym.metadata["operand"] = location
            return location
        raise Exception("Unsupported declaration")

    @handler
    def varref(self, node: n.VariableReference):
        if isinstance(node.datatype, t.Function):
            return asm.Address(asm.Label(self.pointer_size, node.name))
        sym = self.symbol_table.get_expect(node.name, symbol_table.SymbolType.variable)
        return sym.metadata["operand"]

    @handler
    def literal(self, node: n.Literal):
        if self._is_int(node):
            return asm.IntLiteral(self.typesizer.sizeof(node.datatype), node.value)
        elif self._is_charstar(node):
            return self.constants[node.value][1]
        raise Exception("%s literals are not implemented" % node.value.datatype)

    @handler
    def stmt_return(self, node: n.Return):
        if node.value:
            loc = self.handle(node.value)
            if self._is_int_ptr(node.value):
                self._move_to_register(loc, self.rax)
            else:
                raise Exception("Returning %s is not implemented" % node.value.datatype)

        self.flavour.op("jmp", asm.Address(asm.Label(self.pointer_size, self._return_label)))

    @handler
    def noop(self, node: n.Noop):
        pass

    @handler
    def stmt_if(self, node: n.If):
        cond = self.handle(node.condition)
        jumpfalse = next(self.labgen_jump)
        rax = self._move_to_register(cond, self.rax)
        self.sizedop("test", rax, rax)
        self.flavour.op("jne", asm.Address(asm.Label(self.pointer_size, jumpfalse)))
        self.handle(node.if_true)
        if node.if_false:
            jumpend = next(self.labgen_jump)
            self.flavour.op("jmp", asm.Address(asm.Label(self.pointer_size, jumpend)))
            self.flavour.place_label(jumpfalse)
            self.handle(node.if_false)
            self.flavour.place_label(jumpend)
        else:
            self.stream.write("%s:\n" % jumpfalse)

    @handler
    def stmt_while(self, node: n.While):
        jumpstart = next(self.labgen_jump)
        self.flavour.place_label(jumpstart)
        jumpfalse = next(self.labgen_jump)
        self._push_loop_labels(jumpfalse, jumpstart)

        cond = self.handle(node.condition)
        rax = self._move_to_register(cond, self.rax)
        self.sizedop("test", rax, rax)
        self.flavour.op("jne", asm.Address(asm.Label(self.pointer_size,jumpfalse)))

        self.handle(node.block)
        self._pop_loop_labels()
        self.flavour.op("jmp", asm.Address(asm.Label(self.pointer_size, jumpstart)))
        self.flavour.place_label(jumpfalse)

    @handler
    def stmt_cast(self, node: n.TypeCast):
        from_type = node.child.datatype
        to_type = node.datatype
        loc = self.handle(node.child)
        if self._is_int_ptr(node.child) and self._is_int_ptr(node):
            if not isinstance(loc, asm.Register):
                loc = self._move_to_register(loc, self.rax)
            return loc.resized(self.typesizer.sizeof(to_type))
        raise Exception("Cast not implemented")

    # asm AST > asm --------------------

    @handler
    def asm_program(self, node: an.AsmProgram):
        for c in node.statements:
            self.handle(c)

    @handler
    def asm_op(self, node: an.AsmOp):
        self.print_op(node.op)

    @handler
    def asm_label(self, node: an.AsmLabel):
        if not node.name.startswith(".L"):
            self.flavour.empty_line()
        self.flavour.place_label(node.name)

    @handler
    def asm_section(self, node: an.AsmSection):
        self.flavour.empty_line()
        self.flavour.section(node.name)

    @handler
    def asm_global(self, node: an.AsmGlobal):
        self.flavour.define_global(node.name)

    @handler
    def asm_extern(self, node: an.AsmExtern):
        self.flavour.define_extern(node.name)

    @handler
    def asm_type(self, node: an.AsmType):
        self.flavour.define_type(node.name, node.type)

    @handler
    def asm_directive(self, node: an.AsmDirective):
        if node.name == "ident":
            self.flavour.empty_line()
            self.flavour.ident(node.args[0])
            return
        self.flavour.directive(node.name, node.args)

    @handler
    def asm_string(self, node: an.AsmString):
        self.flavour.data(node.datatype, node.value)
