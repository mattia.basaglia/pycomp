from . import generator, opcode_table, registers, mnem
from ..assembler import Architecture

x86_64 = Architecture(
    "x86_64",
    generator.AsmX86_64Generator,
    opcode_table.x86_64OpcodeTable(),
    registers.x86_64Register,
    mnem.mnemonics
)
