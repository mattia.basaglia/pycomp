import re
from ... import asm


class RegisterEncoding:
    re_rax = re.compile("^[a-d][xhl]|[er][a-d]x$")
    re_rsp = re.compile("^[er]?(sp|bp|di|si)l?$")
    re_r8 = re.compile("^(r[0-9]+)([bwd]?)$")

    register_encoding = {
        "rax": 0,
        "rbx": 3,
        "rcx": 1,
        "rdx": 2,
        "rsi": 6,
        "rdi": 7,
        "rbp": 5,
        "rsp": 4,
        "r8": 8,
        "r9": 9,
        "r10": 10,
        "r11": 11,
        "r12": 12,
        "r13": 13,
        "r14": 14,
        "r15": 15,
    }

    def get_register(self, name):
        """
        Find register by name
        Returns a tuple of (full_register_name, index, size)
        EG: eax will be ("rax", 0, 4)
        """
        if self.re_rax.match(name):
            base = "r%sx" % name[-2]
            if name[0] == "r":
                size = 8
            elif name[0] == "e":
                size = 4
            elif name[-1] == "x":
                size = 2
            else:
                size = 1
            index = self.register_encoding[base]
            if name[-1] == "h":
                index += 4
            return base, index, size

        match = self.re_r8.match(name)
        if match:
            base = match.group(1)
            suf = match.group(2)
            if suf == "":
                size = 8
            elif suf == "d":
                size = 4
            elif suf == "w":
                size = 2
            else:
                size = 1
            index = self.register_encoding[base]
            return base, index, size

        match = self.re_rsp.match(name)
        if match:
            base = "r" + match.group(1)
            if name[0] == "r":
                size = 8
            elif name[0] == "e":
                size = 4
            elif name[-1] == "l":
                size = 1
            else:
                size = 2
            index = self.register_encoding[base]
            return base, index, size

        raise KeyError("Register %s not found" % name)

    def _extract_key(self, key):
        if isinstance(key, asm.Register):
            return key.name
        elif isinstance(key, asm.DerefRegister):
            return key.register.name
        return key

    def __getitem__(self, key):
        key = self._extract_key(key)

        if key in self.register_encoding:
            return self.register_encoding[key]

        return self.get_register(key)[1]

    def bit3(self, key):
        return self[key] & 7

    def is_high(self, key):
        return self[key] >= 8

    def has_high_byte(self, key):
        key = self._extract_key(key)
        base, index, size = self.get_register(key)
        if self.register_encoding[base] < 4:
            return True
        return False


register_encoding = RegisterEncoding()


class x86_64Register(asm.Register):
    @classmethod
    def _sized_rsp(cls, size, name):
        root = name[1:]
        if size == 1:
            return cls(size, root + "l")
        if size == 2:
            return cls(size, root)
        if size == 4:
            return cls(size, "e" + root)
        if size == 8:
            return cls(size, "r" + root)
        return None

    @classmethod
    def _sized_rax(cls, size, name):
        letter = name[1]
        if size == 1:
            return cls(size, letter + "l")
        if size == 2:
            return cls(size, letter + "x")
        if size == 4:
            return cls(size, "e" + letter + "x")
        if size == 8:
            return cls(size, "r" + letter + "x")
        return None

    @classmethod
    def _sized_r8(cls, size, name):
        if size == 1:
            return cls(size, name + "b")
        if size == 2:
            return cls(size, name + "w")
        if size == 4:
            return cls(size, name + "d")
        if size == 8:
            return cls(size, name)
        return None

    @classmethod
    def sized(cls, size, name):
        base, index, acsz = register_encoding.get_register(name)
        if acsz == size:
            return cls(size, name)
        if index < 4:
            return cls._sized_rax(size, base)
        if index < 8:
            return cls._sized_rsp(size, base)
        if index < 15:
            return cls._sized_r8(size, base)
        raise Exception("Unknown register %s of size %s" % (name, size))

    @classmethod
    def from_name(cls, name):
        size = register_encoding.get_register(name)[2]
        return cls(size, name)

