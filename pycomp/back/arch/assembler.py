import enum
from .. import asm
from ..base import Generator


class OperandType(enum.Enum):
    @classmethod
    def from_operand(cls, op: asm.Operand) -> "OperandType":
        raise NotImplementedError()


class OperationDescriptor:
    operand_type = OperandType

    def __init__(self, mnem: str, op1: OperandType, op2: OperandType):
        self.mnem = mnem
        self.op1 = op1
        self.op2 = op2

    @property
    def tuple(self):
        return (self.mnem, self.op1.value, self.op2.value)

    def __hash__(self):
        return hash(self.tuple)

    def __eq__(self, other):
        return self.tuple == other.tuple

    def __repr__(self):
        return "<%s %s>" % (self.__class__.__name__, self)

    def __str__(self):
        return "%s %s %s" % (self.mnem, self.op1.name, self.op2.name)

    @classmethod
    def from_op(cls, op: asm.Operation):
        return cls(op.mnem, cls.operand_type.from_operand(op.op1), cls.operand_type.from_operand(op.op2))


class Instruction:
    def __bytes__(self):
        raise NotImplementedError()

    def relocation_offset(self):
        return 0


class OpcodeTable:
    d = OperationDescriptor
    o = d.operand_type

    def instruction(self, op: asm.Operation) -> Instruction:
        return self.assembler(self.descriptor(op))(op)

    def descriptor(self, op: asm.Operation) -> OperationDescriptor:
        return self.d.from_op(op)

    def assembler(self, od: OperationDescriptor):
        raise NotImplementedError


class Architecture:
    def __init__(self, name: str, generator_class, opcode_table: OpcodeTable, register_class=asm.Register, mnemonics=None):
        self.name = name
        self.generator_class = generator_class
        self.opcode_table = opcode_table
        self.register_class = register_class
        self.mnemonics = mnemonics or set()
