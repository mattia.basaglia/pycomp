import inspect


class Handler:
    _id = 0

    def __init__(self, type, callable, strict=True):
        self.type = type
        self.callable = callable
        self.strict = strict
        Handler._id += 1
        self._id = Handler._id

    def matches(self, obj):
        if self.strict:
            return type(obj) is self.type
        return isinstance(obj, self.type)

    def __call__(self, otherself, obj):
        if not self.matches(obj):
            raise Exception("Invalid handler for %s" % type(obj))
        self.callable(otherself, obj)


def handler(func=None, *, type=None, strict=True):
    if func is None:
        def deco(func):
            return handler(func, type=type, strict=strict)
        return deco

    if type is None:
        argspec = inspect.getfullargspec(func)
        ndef = 0 if argspec.defaults is None else len(argspec.defaults)
        args = list(argspec.args)
        if args[0] == "self":
            args.pop(0)
        if len(args) - ndef == 1 and args[0] in argspec.annotations:
            type = argspec.annotations[args[0]]
    return Handler(type, func, strict)


class HandlerNotFound(Exception):
    pass


class AutoHandler:
    def __init__(self):
        self.handlers = [
            v
            for n, v in inspect.getmembers(self, lambda m: isinstance(m, Handler))
        ]
        self.handlers.sort(key=lambda m: m._id)

    def handle(self, node):
        for handler in self.handlers:
            if handler.matches(node):
                return handler.callable(self, node)
        raise HandlerNotFound("Cannot handle %s" % type(node))
