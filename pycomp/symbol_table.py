import math
import enum
import typing
from collections import OrderedDict

from .ast_types import Type
from . import ast_nodes, ast_types
from .autohandler import AutoHandler, handler, HandlerNotFound


class ScopeError(Exception):
    pass


class SymbolType(enum.Enum):
    variable = 1
    function = 2
    type = 3
    temp = 4


class Symbol:
    def __init__(self, what: SymbolType, type: Type, metadata=None):
        self.what = what
        self.type = type
        self.metadata = metadata


class IScope:
    def declare_raw(self, name: str, what: SymbolType, type: Type, metadata=None):
        raise NotImplementedError()

    def declare(self, object):
        if isinstance(object, ast_nodes.TypeDeclaration):
            self.declare_raw(object.name, SymbolType.type, object.datatype)
        elif isinstance(object, ast_nodes.VariableDeclaration):
            self.declare_raw(object.name, SymbolType.variable, object.datatype)
        elif isinstance(object, ast_nodes.FunctionDeclaration):
            self.declare_raw(object.name, SymbolType.function, object.datatype)
        else:
            raise Exception("Unknown symbol type")
        return object

    def get(self, name: str) -> Symbol:
        raise NotImplementedError()

    def get_expect(self, name: str, what: SymbolType) -> Symbol:
        sym = self.get(name)
        if sym.what != what:
            raise ScopeError("%s is a %s but is used as a %s" % (
                name, sym.what.name, what.name
            ))
        return sym

    def contains(self, name: str) -> bool:
        raise NotImplementedError()

    def contains_expect(self, name: str, what: SymbolType) -> bool:
        try:
            self.get_expect(name, what)
            return True
        except ScopeError:
            return False


class Scope(IScope):
    def __init__(self):
        self.symbols = OrderedDict()

    def declare_raw(self, name: str, what: SymbolType, type: Type, metadata=None):
        if name in self.symbols:
            raise ScopeError("%s already declared" % name)
        sym = Symbol(what, type, metadata)
        self.symbols[name] = sym
        return sym

    def get(self, name: str) -> Symbol:
        if name not in self.symbols:
            raise ScopeError("%s not declared" % name)
        return self.symbols[name]

    def contains(self, name: str) -> bool:
        return name in self.symbols


class StackScope(Scope):
    def __init__(self, sizer, align):
        super().__init__()
        self.size = 0
        self.sizer = sizer
        self.align = align

    def declare_stack(self, name: str, what: SymbolType, type: Type, metadata=None):
        metadata = metadata or {}
        metadata["offset"] = self.size
        size = math.ceil(self.sizer.sizeof(type) / self.align) * self.align
        metadata["size"] = size
        self.size += size
        return self.declare_raw(name, what, type, metadata)

    def pop_last(self):
        item = self.symbols.popitem()
        self.size -= item[1].metadata["size"]
        return item

    def last(self):
        return list(self.symbols.items())[-1]


class SymbolTable(IScope):
    def __init__(self):
        self.scopes = [self._default_scope()]

    @property
    def scope(self) -> Scope:
        return self.scopes[0]

    def _default_scope(self):
        return Scope()

    def push(self, scope: typing.Optional[Scope]=None) -> Scope:
        if scope is None:
            scope = self._default_scope()
        self.scopes.insert(0, scope)
        return scope

    def pop(self) -> Scope:
        if len(self.scopes) <= 1:
            raise Exception("Popping the global scope")
        return self.scopes.pop(0)

    def declare_raw(self, name: str, what: SymbolType, type: Type, metadata=None):
        return self.scope.declare_raw(name, what, type, metadata)

    def get(self, name: str) -> Symbol:
        for s in self.scopes:
            if s.contains(name):
                return s.get(name)
        raise ScopeError("%s not declared" % name)

    def contains(self, name: str) -> bool:
        for s in self.scopes:
            if s.contains(name):
                return True
        return False


class StackSymbolTable(IScope):
    """
    Symbol table that keeps separate stacks, making it more of a tree
    """
    def __init__(self, sizer, align):
        self.sizer = sizer
        self.align = align
        self.global_scope = self._default_scope()
        self.branches = []

    def _default_scope(self):
        return StackScope(self.sizer, self.align)

    def push_stack(self, scopes=None):
        """
        Add a new stack that keeps only the global scope but otherwise
        disregards the current stack
        """
        self.branches.insert(0, scopes or [self._default_scope()])

    def pop_stack(self):
        return self.branches.pop(0)

    @property
    def scope(self) -> Scope:
        if self.is_global_current:
            return self.global_scope
        else:
            return self.branches[0][0]

    @property
    def is_global_current(self):
        return not self.branches or not self.branches[0]

    @property
    def stack_scopes(self):
        if not self.branches:
            raise ScopeError("Not on the stack")
        return self.branches[0]

    def stack_size(self):
        if self.is_global_current:
            return 0
        return sum(x.size for x in self.branches[0])

    def push(self, scope: typing.Optional[Scope]=None) -> Scope:
        if scope is None:
            scope = self._default_scope()
        self.stack_scopes.insert(0, scope)
        return scope

    def pop(self) -> Scope:
        if self.is_global_current:
            raise Exception("Popping the global scope")
        return self.stack_scopes.pop(0)

    def declare_raw(self, name: str, what: SymbolType, type: Type, metadata=None):
        return self.scope.declare_raw(name, what, type, metadata)

    def get(self, name: str) -> Symbol:
        if self.branches:
            for s in self.stack_scopes:
                if s.contains(name):
                    return s.get(name)
        if self.global_scope.contains(name):
            return self.global_scope.get(name)
        raise ScopeError("%s not declared" % name)

    def contains(self, name: str) -> bool:
        if self.branches:
            for s in self.scopes:
                if s.contains(name):
                    return True
        if self.global_scope.contains(name):
            return True

    def declare_temp(self, name: str, type: Type, metadata=None):
        if self.is_global_current:
            raise ScopeError("Cannot have global temp")
        return self.scope.declare_stack(name, SymbolType.temp, type, metadata)

    def check_temp(self, name: str, type: Type):
        if self.is_global_current:
            return False
        last_name, last = self.scope.last()
        return last_name == name and last.what == SymbolType.temp and last.type == type

    def declare_variable(self, name: str, type: Type, metadata=None):
        return self.scope.declare_stack(name, SymbolType.variable, type, metadata)

    def pop_symbol(self):
        if self.is_global_current:
            raise ScopeError("Cannot pop global")
        return self.scope.pop_last()


class TypeSizer(AutoHandler):
    def sizeof(self, type: Type) -> int:
        return self.handle(type)


class TypeSizerDecorator(TypeSizer):
    def __init__(self, wrapped):
        super().__init__()
        self.wrapped = wrapped

    def handle(self, type: Type) -> int:
        try:
            return super().handle(type)
        except HandlerNotFound:
            return self.wrapped.handle(type)


class ArithmeticSizer(TypeSizerDecorator):
    typesizes = {
        ast_types.Arithmetic.BOOL: 1,
        ast_types.Arithmetic.BYTE: 1,
        ast_types.Arithmetic.WORD: 2,
        ast_types.Arithmetic.DWORD: 4,
        ast_types.Arithmetic.QWORD: 8,
        ast_types.Arithmetic.FLOAT: 4,
    }

    def __init__(self, wrapped, typesizes=None):
        super().__init__(wrapped)
        self.typesizes = typesizes or ArithmeticSizer.typesizes

    @handler
    def sizeof_arith(self, type: ast_types.Arithmetic):
        return self.typesizes[type.signed() if type.is_int else type]


class FixedPointerSizer(TypeSizerDecorator):
    def __init__(self, wrapped, pointer_size):
        super().__init__(wrapped)
        self.pointer_size = pointer_size

    @handler
    def sizeof_ptr(self, type: ast_types.Pointer):
        return self.pointer_size


