from . import ast_types
from .ast_nodes import AstNode, Literal, Program
from .back import asm


class AsmNode(AstNode):
    def __init__(self, datatype: ast_types.Type=ast_types.Void()):
        super().__init__(datatype)


class AsmProgram(Program, AstNode):
    pass


class AsmOp(AsmNode):
    def __init__(self, op: asm.Operation):
        super().__init__()
        self.op = op


class AsmLabel(AsmNode):
    def __init__(self, name):
        super().__init__()
        self.name = name


class AsmSection(AsmNode):
    def __init__(self, name):
        super().__init__()
        self.name = name


class AsmGlobal(AsmNode):
    def __init__(self, name):
        super().__init__()
        self.name = name


class AsmExtern(AsmNode):
    def __init__(self, name):
        super().__init__()
        self.name = name


class AsmType(AsmNode):
    def __init__(self, name, type):
        super().__init__()
        self.name = name
        self.type = type


class AsmDirective(AsmNode):
    def __init__(self, name, args):
        super().__init__()
        self.name = name
        self.args = args


class AsmString(Literal):
    def __init__(self, value):
        super().__init__(value, ast_types.Arithmetic.BYTE)
