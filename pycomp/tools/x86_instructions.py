#!/usr/bin/env python
import urllib.request
import xml.etree.ElementTree as ET
import argparse
import json
import sys


parser = argparse.ArgumentParser(
    description="Shows how instructions are assembled"
)

parser.add_argument(
    "--uri", "-u",
    help="URI with an XML file defining the mnemonics",
    default="http://ref.x86asm.net/x86reference.xml",
)
parser.add_argument(
    "--xpath", "-x",
    help="XPath expression to fetch elements containing the mnemonics",
    default=".//mnem",
)
parser.add_argument(
    "--format", "-f",
    help="Output format",
    choices=["json", "csv"]
)
parser.add_argument(
    "--output", "-o",
    help="Output file",
    default="-"
)

ns = parser.parse_args()

with urllib.request.urlopen(ns.uri) as f:
    xml = ET.parse(f)
    data = sorted(list({n.text for n in xml.iterfind(ns.xpath)}))

if ns.output == "-" or ns.output == "/dev/stdout":
    outf = sys.stdout
else:
    outf = open(ns.output, "w")

if ns.format == "json":
    json.dump(data, outf, indent=4)
    outf.write("\n")
else:
    for m in data:
        outf.write(m)
        outf.write("\n")

if outf is not sys.stdout:
    outf.close()
