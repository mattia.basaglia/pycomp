#!/usr/bin/env python
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import re
from pycomp.front.c import CLexer
from pycomp.front.base import TokenType


define = 0
out = ""


parser = CLexer(sys.stdin, True)
for tok in parser:
    if tok.type == TokenType.KEYWORD:
        if tok.str == "define" and getattr(tok, "preprocessor"):
            define = 1
            out += "\n"
        elif define == 1:
            out += tok.str + " = "
            define = 2
    elif define == 2:
        out += tok.str
        define = 3
    elif define == 3 and tok.type == TokenType.COMMENT:
        out += " # " + re.sub("\s+", " ", tok.str)


print(out)
