#!/usr/bin/env python
"""
Basically the same as the usual python shell, but with some extra imports and stuff
"""
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import io
import types
import subprocess
import argparse
import importlib

from pycomp.util import pyshell, debugtools
from pycomp import fixint
from pycomp.back.elf import elf_file, c_api


def elf_open(filename):
    """
    Opens an ELF file by name
    """
    with open(filename, "rb") as f:
        return elf_file.ElfFile.from_file(f)


def gcc(infile, outfile, *args):
    """
    Compiles with gcc
    """
    cmd = ["gcc", "-no-pie", infile, "-o", outfile] + list(args)
    if outfile.endswith(".o"):
        cmd.append("-c")
    elif outfile.endswith(".s"):
        cmd.append("-S")
    subprocess.call(cmd)


def elf_print_section(elffile: elf_file.ElfFile, section: str, output=sys.stdout):
    """
    Prints the given section for an ELF file
    """
    sec = elffile.find_section(section)
    if not sec:
        output.write("Not found\n")
        return

    output.write(sec.name.decode("utf8"))
    output.write("\n")
    for k, v in sec.header.attrdict().items():
        output.write("%s %s\n" % (k, v))

    output.write("\n")

    if isinstance(sec.data, list):
        for rec in sec.data:
            debugtools.print_r(rec, set([elffile] + elffile.sections), output)
            output.write("\n")
    else:
        debugtools.print_hex(sec.data, output=output)
        output.write("\n")


def elf_compare_sections(elf1, elf2, section):
    stream = io.StringIO()
    elf_print_section(elf1, section, stream)
    stream.seek(0)
    lines1 = list(stream)

    stream = io.StringIO()
    elf_print_section(elf2, section, stream)
    stream.seek(0)
    lines2 = list(stream)

    maxl = max(map(debugtools.strlen_stripansi, lines1))

    for i in range(max(len(lines1), len(lines2))):
        l1 = lines1[i].rstrip("\n") if i < len(lines1) else ""
        l2 = lines2[i].rstrip("\n") if i < len(lines2) else ""
        sys.stdout.write("%s%s | %s\n" % (debugtools.ansi_align(l1, maxl), l2))


def reload_modules():
    importlib.reload(fixint)
    importlib.reload(elf_file)
    importlib.reload(c_api)
    importlib.reload(debugtools)


custom_builtins = {
    "fixint": fixint,
    "elf_file": elf_file,
    "c_api": c_api,
    "ElfFile": elf_file.ElfFile,
    "elf_open": elf_open,
    "gcc": gcc,
    "elf_print_section": elf_print_section,
    "print_r": debugtools.print_r,
    "print_hex": debugtools.print_hex,
    "elf_print_r": debugtools.elf_print_r,
    "elf_compare_sections": elf_compare_sections,
    "reload_module": importlib.reload,
    "reload_modules": reload_modules,
}
globals = {
    "os": os,
    "sys": sys,
    "subprocess": subprocess,
    "custom_builtins": custom_builtins,
}
globals.update(custom_builtins)
welcome = "ELF inspection shell, see \"print_r(custom_builtins)\" for custom functionality"
shell = pyshell.PyShell(globals, pyshell.PyShell.default_welcome + "\n" + welcome)
shell.init_readline("/tmp/pycomp_%s_history" % os.path.splitext(os.path.basename(__file__))[0])
shell.main()
