#!/usr/bin/env python
import os
import io
import sys
import tempfile
import argparse
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))

from pycomp.back import asm
from pycomp.back.asm_flavours.gas import Gas
from pycomp.back.asm_flavours.nasm import Nasm
from pycomp.back.arch.list import x86_64, arch_names, get_arch
from pycomp.ast_types import Arithmetic
from pycomp.front.frontends.x86_64 import AsmX86_64Frontend
from pycomp.symbol_table import TypeSizer


inf = tempfile.NamedTemporaryFile("w", suffix=".s", delete=False)
outf = tempfile.NamedTemporaryFile("rb", suffix=".o", delete=False)
cmd_nasm = ["nasm", "-f", "elf64", inf.name, "-o", outf.name]
cmd_gas = ["gcc", "-no-pie", "-c", inf.name, "-o", outf.name]

start = b"start["
sep = b"]["
end = b"][end"


def parse_args():
    parser = argparse.ArgumentParser(
        description="Shows how instructions are assembled"
    )

    parser.add_argument(
        "--syntax", "-s",
        help="Input syntax",
        choices=asm.asm_flavour_registry.instances,
        default=Nasm(),
        type=asm.asm_flavour_registry.get_instance,
        dest="asm_flavour"
    )

    parser.add_argument(
        "--asm",
        choices=["nasm", "gas"], #asm.asm_flavour_registry.values,
        default="nasm",
        dest="oasm_flavour"
    )

    parser.add_argument(
        "--instruction", "-i",
        default=[],
        nargs="+",
        action="append"
    )
    parser.add_argument(
        "--arch",
        help="Architecture",
        choices=arch_names,
        default=x86_64,
        type=get_arch
    )
    parser.add_argument(
        "--keep",
        help="Keep temp files",
        action="store_true"
    )
    return parser.parse_args()


def print_chunk(chunk):
    for b in chunk:
        sys.stdout.write(("%02x" % b).upper().center(8)+" ")
    sys.stdout.write("\n")

    for b in chunk:
        b2 = bin(b)[2:]
        sys.stdout.write(b2.rjust(8, '0')+" ")
    sys.stdout.write("\n")


def main():
    global start, end, sep
    ns = parse_args()
    flavour = asm.asm_flavour_registry.get_class(ns.oasm_flavour).formatter(inf)
    frontend = AsmX86_64Frontend(ns)
    typesizer = frontend.typesizer(TypeSizer())
    flavour.section(".text")
    flavour.data(Arithmetic.BYTE, start.decode("utf8"))
    instructions = []
    labels = set()
    for ops in ns.instruction:
        insttxt = ops[0]
        if len(ops) > 1:
            insttxt += " " + ops[1]
        if len(ops) > 2:
            insttxt += ", " + ops[2]

        instr = frontend.parser(io.StringIO(insttxt), typesizer).parse().statements[0].op
        instructions.append(instr)
        flavour.print_op(instr)
        flavour.data(Arithmetic.BYTE, sep.decode("utf8"))
        if isinstance(instr.op1, asm.Label):
            labels.add(instr.op1.name)
        elif isinstance(instr.op1, asm.Address) and isinstance(instr.op1.child, asm.Label):
            labels.add(instr.op1.child.name)
        if isinstance(instr.op2, asm.Label):
            labels.add(instr.op2.name)
        elif isinstance(instr.op2, asm.Address) and isinstance(instr.op2.child, asm.Label):
            labels.add(instr.op2.child.name)
    flavour.data(Arithmetic.BYTE, end.decode("utf8"))
    for label in labels:
        flavour.define_extern(label)
    inf.close()

    if ns.oasm_flavour == "nasm":
        cmd = cmd_nasm
    else:
        cmd = cmd_gas
        start += b'\0'
        sep += b'\0'
        end += b'\0'

    if subprocess.call(cmd) == 0:
        data = outf.read()
        starti = data.find(start)+len(start)
        endi = data.find(end)

        formatter = ns.asm_flavour.formatter(sys.stdout)

        for chunk in data[starti:endi].split(sep)[:-1]:
            operation = instructions.pop(0)
            sys.stdout.write(str(ns.arch.opcode_table.descriptor(operation)))
            sys.stdout.write(" - ")
            formatter.print_op(operation)
            print_chunk(chunk)
            try:
                instr = ns.arch.opcode_table.instruction(operation)
                print_chunk(bytes(instr))
            except KeyError:
                sys.stdout.write("not implemented\n")

            sys.stdout.write("\n")

        if not ns.keep:
            os.unlink(outf.name)

    outf.close()

    if not ns.keep:
        os.unlink(inf.name)
    else:
        print(inf.name)
        print(outf.name)


if __name__ == "__main__":
    main()
