#!/usr/bin/env python
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import argparse
from pycomp import fixint
from pycomp.util.pyshell import RecursivePrinter
from pycomp.back.elf import elf_file, c_api

parser = argparse.ArgumentParser(description="Show info on an ELF file")
parser.add_argument("elffile", help="file to view")

print_r = RecursivePrinter(
    [fixint.FixintBase],
    {c_api.Struct: lambda obj: { "_" + k for k in obj.fields.keys()}}
)

ns = parser.parse_args()
with open(ns.elffile, "rb") as f:
    elffile = elf_file.ElfFile.from_file(f)
    print_r(elffile.header)
    avoid = set([elffile] + elffile.sections)
    for section in elffile.sections:
        print_r(section, avoid - set([section]))
