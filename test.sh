#!/bin/bash
here="$(readlink -f "$(dirname "$0")")"

if [ "$#" = 0 ]
then
    coverage erase
fi

coverage run -m unittest "${@}"

if [ "$#" = 0 ]
then
    coverage html --include="$here/pycomp/*,$here/test/*"
fi
